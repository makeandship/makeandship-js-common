/* @flow */

import { isDelete } from '../src/components/ui/form/fields/FileInputBinary/utils';

const fs = require('fs-extra');
const path = require('path');
const express = require('express');
const cors = require('cors');
const _get = require('lodash.get');
const _set = require('lodash.set');
const Busboy = require('busboy');
const os = require('os');

const USE_JSEND = true;

fs.ensureDirSync(path.join(__dirname, '__attachments__'));
fs.ensureDirSync(path.join(__dirname, '__incidents__'));

const host = process.env.HOST || os.hostname() || 'localhost';
const port = process.env.PORT || 3001;

const app = express();

// Allow CORS
app.use(cors());
app.options('*', cors());

const getMockIncident = (id) => {
  const src = path.join(__dirname, '__incidents__', `${id}.json`);
  if (!fs.existsSync(src)) {
    return {};
  }
  const data = fs.readFileSync(src);
  const mockIncident = JSON.parse(data);
  return mockIncident;
};

app.use(
  '/attachments',
  express.static(path.join(__dirname, '__attachments__'))
);

app.get('/swagger.json', (req, res) => {
  const swagger = require('./swagger');
  swagger.host = `${host}:${port}`;
  res.json(swagger);
});

app.get('/openapi.json', (req, res) => {
  const openapi = require('./openapi');
  openapi.servers[0].url = `${host}:${port}`;
  res.json(openapi);
});

app.get('/health-check', (req, res) => {
  res.send('ok');
});

app.get('/incidents/:id', (req, res) => {
  const id = _get(req.params, 'id');
  const mockIncident = getMockIncident(id);
  console.log('mockIncident', JSON.stringify(mockIncident, null, 2));
  res.json(
    USE_JSEND
      ? {
          status: 'success',
          data: mockIncident,
        }
      : mockIncident
  );
});

app.put('/incidents/:id', (req, res) => {
  const id = _get(req.params, 'id');
  console.log(JSON.stringify(req.headers, null, 2));
  const busboy = new Busboy({ headers: req.headers });
  const formData = {};
  busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
    const val = {
      filename,
      mimetype,
      url: `http://${host}:${port}/attachments/${id}/${filename}`,
    };
    const dest = path.join(__dirname, '__attachments__', id, filename);
    let size = 0;
    fs.ensureDirSync(path.join(__dirname, '__attachments__', id));
    fs.removeSync(dest);
    file.pipe(fs.createWriteStream(dest));
    console.log(
      'File [' +
        fieldname +
        ']: filename: ' +
        filename +
        ', encoding: ' +
        encoding +
        ', mimetype: ' +
        mimetype
    );
    file.on('data', (data) => {
      console.log('File [' + fieldname + '] got ' + data.length + ' bytes');
      size += data.length;
    });
    file.on('end', () => {
      console.log('File [' + fieldname + '] Finished');
      val.size = size;
    });
    _set(formData, fieldname, val);
  });
  busboy.on(
    'field',
    (fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) => {
      _set(formData, fieldname, val);
      console.log(
        JSON.stringify(
          {
            fieldname,
            val,
            fieldnameTruncated,
            valTruncated,
            encoding,
            mimetype,
          },
          null,
          2
        )
      );
    }
  );
  busboy.on('finish', () => {
    const mockIncident = getMockIncident(id);
    console.log('Done parsing form!');

    // unpack the fields nested in 'incident'
    Object.assign(formData, formData.incident);
    delete formData.incident;
    console.log('formData', JSON.stringify(formData, null, 2));

    // process attachments set to 'DELETE'
    const attachment = _get(formData, 'attachment');
    if (isDelete(attachment)) {
      const dest = path.join(
        __dirname,
        '__attachments__',
        id,
        mockIncident.attachment.filename
      );
      console.log(`Deleting ${dest}`);
      fs.removeSync(dest);
    }

    const attachments = _get(formData, 'attachments');

    if (attachments && Array.isArray(attachments)) {
      // delete the files
      attachments.forEach((attachment, index) => {
        if (isDelete(attachment)) {
          const dest = path.join(
            __dirname,
            '__attachments__',
            id,
            mockIncident.attachments[index].filename
          );
          console.log(`Deleting ${dest}`);
          fs.removeSync(dest);
        }
      });
    }

    // update the values in the stored incident
    Object.assign(mockIncident, formData);

    // clean up attachment fields
    if (mockIncident.attachment) {
      if (isDelete(mockIncident.attachment)) {
        delete mockIncident.attachment;
      }
    }

    if (mockIncident.attachments) {
      // if user removes all the attachments, the formData will look like;
      // "attachments": {
      //   "": ""
      // }
      if (Array.isArray(attachments)) {
        // compact the array
        mockIncident.attachments = mockIncident.attachments.filter(
          (attachment) => !isDelete(attachment) && attachment !== ''
        );
        if (mockIncident.attachments.length === 0) {
          delete mockIncident.attachments;
        }
      } else {
        // can be removed
        delete mockIncident.attachments;
      }
    }

    // TODO: handle case where the user removed files from the array
    // they will need to be removed from the system

    fs.writeFileSync(
      path.join(__dirname, '__incidents__', `${id}.json`),
      JSON.stringify(mockIncident, null, 2)
    );
    res.json(
      USE_JSEND
        ? {
            status: 'success',
            data: mockIncident,
          }
        : mockIncident
    );
  });
  req.pipe(busboy);
});

// Start dev API server
const server = app.listen(port, (err) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log(`API dev server listening at http://${host}:${port}`);
});

// Stop dev API server
process.on('SIGTERM', () => {
  console.log('Stopping dev server');
  server.close(() => {
    process.exit(0);
  });
});
