/* @flow */

import * as React from 'react';
import filesize from 'filesize';
import {
  Button,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
} from 'reactstrap';

import mimetypes from '../../../../../utils/mimetypes.json';
import { isString, isPlainObject, isFile } from '../../../../../utils/is';
import withDownload, {
  withDownloadPropTypes,
} from '../../../../../hoc/withDownload';
import AuthenticatedImage from '../../../AuthenticatedImage';

import styles from '../FileInput.module.scss';

import { isDelete, DELETE, urlForImageValue } from './utils';

type State = {
  filePreview: any,
  userSelectedFile: boolean,
};

class FileInputBinary extends React.Component<*, State> {
  _fileUpload: HTMLInputElement;
  _originalValue: any;

  state = { filePreview: undefined, userSelectedFile: false };

  componentDidMount = () => {
    if (!isFile(this.props.value) && !isDelete(this.props.value)) {
      this._originalValue = this.props.value;
    }
  };

  componentDidUpdate = (prevProps) => {
    if (
      prevProps.value !== this.props.value &&
      !isFile(this.props.value) &&
      !isDelete(this.props.value)
    ) {
      this._originalValue = this.props.value;
      this.setState({
        filePreview: undefined,
        userSelectedFile: false,
      });
    }
  };

  onChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
    e.preventDefault();
    const file: File = e.target.files[0];
    if (!file) {
      return;
    }
    const { onChange } = this.props;
    onChange && onChange(file);
    this.setState({
      userSelectedFile: true,
    });
    // Only process image files
    if (!file.type.match('image.*')) {
      this.setState({
        filePreview: undefined,
      });
      return;
    }
    const reader = new FileReader();
    reader.onload = (e) => {
      this.setState({
        filePreview: e.target.result,
      });
    };
    // Read in the image file as a data URL
    reader.readAsDataURL(file);
  };

  onBrowseFileClick = (e: Event) => {
    e.preventDefault();
    this._fileUpload.click();
  };

  onRemoveFileClick = (e: Event) => {
    e.preventDefault();
    const { onChange } = this.props;
    if (onChange) {
      if (this._originalValue) {
        // mark for deletion
        onChange(DELETE);
      } else {
        // clear
        onChange();
      }
    }
  };

  onCancelRemoveFileClick = (e: Event) => {
    e.preventDefault();
    const { onChange } = this.props;
    onChange && onChange(this._originalValue);
  };

  onResetFileClick = (e: Event) => {
    e.preventDefault();
    const { onChange } = this.props;
    onChange && onChange(this._originalValue);
  };

  fileUploadRef = (ref: any) => {
    this._fileUpload = ref;
  };

  onDownloadFileClick = (e: Event) => {
    e.preventDefault();
    const { value, download } = this.props;
    download(value);
  };

  render() {
    /* eslint-disable no-unused-vars */
    const { multiple, id, autofocus, value, readonly } = this.props;
    /* eslint-enable no-unused-vars */
    const { filePreview, userSelectedFile } = this.state;
    if (multiple) {
      throw new Error('FileInput does not support multiple');
    }
    const disabled = this.props.disabled || readonly;
    const isDeleted = isDelete(value);
    let isImage = false;
    let rendered = null;
    let description = null;
    let menu = null;
    if (value) {
      if (isDeleted) {
        description = (
          <div className={styles.description}>
            <i className="fa fa-trash" /> File will be deleted on save
          </div>
        );
      } else if (isString(value)) {
        rendered = (
          <a
            href={value}
            target="_blank"
            rel="noopener noreferrer"
            className={'mr-3'}
          >
            {value}
          </a>
        );
      } else if (isPlainObject(value)) {
        const { filename, mimetype, size } = value;
        if (mimetype && size && filename) {
          const filetype = mimetypes[mimetype] || 'Unknown type';
          const prettySize = filesize(size, { fullform: true });
          isImage = filetype.match('image.*');
          description = (
            <div className={styles.description}>
              <i className="fa fa-file-o" /> {filename}{' '}
              <span className={styles.fileTypeAndSize}>
                {' '}
                &middot; {filetype} &middot; {prettySize}
              </span>
            </div>
          );
        } else {
          rendered = 'Cannot render current value';
        }
      } else if (isFile(value)) {
        const { name, type, size } = value;
        const filetype = mimetypes[type] || 'Unknown type';
        const prettySize = filesize(size, { fullform: true });
        description = (
          <div className={styles.description}>
            <i className="fa fa-file-o" /> {name}{' '}
            <span className={styles.fileTypeAndSize}>
              {' '}
              &middot; {filetype} &middot; {prettySize}
            </span>
          </div>
        );
      } else {
        rendered = 'Cannot render current value';
      }
    }
    if (filePreview) {
      rendered = (
        <div
          className={styles.imagePreviewSizerWidth}
          onClick={this.onBrowseFileClick}
        >
          <div className={styles.imagePreviewSizerHeight}>
            <div className={styles.imagePreviewWrap}>
              <AuthenticatedImage
                className={styles.imagePreviewBinary}
                placeholder={filePreview}
              />
            </div>
          </div>
        </div>
      );
    } else if (isImage) {
      const url = urlForImageValue(value);
      rendered = (
        <div
          className={styles.imagePreviewSizerWidth}
          onClick={this.onBrowseFileClick}
        >
          <div className={styles.imagePreviewSizerHeight}>
            <div className={styles.imagePreviewWrap}>
              <AuthenticatedImage
                className={styles.imagePreviewBinary}
                url={url}
              />
            </div>
          </div>
        </div>
      );
    }
    menu = (
      <DropdownMenu>
        {userSelectedFile && (
          <DropdownItem disabled>
            <i className="fa fa-check-circle" /> Save to upload
          </DropdownItem>
        )}
        {isDeleted && (
          <DropdownItem disabled>
            <i className="fa fa-check-circle" /> Save to remove
          </DropdownItem>
        )}
        <DropdownItem onClick={this.onBrowseFileClick}>
          <i className="fa fa-folder-open" /> Upload file
        </DropdownItem>
        {userSelectedFile ? (
          <DropdownItem onClick={this.onResetFileClick}>
            <i className="fa fa-undo" /> Reset file
          </DropdownItem>
        ) : isDeleted ? (
          <DropdownItem onClick={this.onCancelRemoveFileClick}>
            <i className="fa fa-undo" /> Cancel delete
          </DropdownItem>
        ) : (
          <DropdownItem onClick={this.onRemoveFileClick}>
            <i className="fa fa-trash" /> Remove file
          </DropdownItem>
        )}
        <DropdownItem divider />
        <DropdownItem>Cancel</DropdownItem>
      </DropdownMenu>
    );
    const isEmpty = !value;
    return (
      <div className={styles.componentWrap}>
        <input
          id={id}
          ref={this.fileUploadRef}
          onChange={this.onChange}
          type="file"
          className={styles.fileUpload}
          disabled={disabled}
        />
        {isEmpty ? (
          <Button
            outline
            color="mid"
            onClick={this.onBrowseFileClick}
            disabled={disabled}
          >
            <i className="fa fa-folder-open" /> Upload file
          </Button>
        ) : (
          <div className={styles.contentWrap}>
            {rendered}
            <div className={styles.descriptionAndActions}>
              {description}
              <div className={styles.actions}>
                {(isString(value) || isPlainObject(value)) && (
                  <Button
                    className={'mr-2'}
                    outline
                    color="mid"
                    onClick={this.onDownloadFileClick}
                    disabled={disabled && !readonly}
                  >
                    <i className="fa fa-download" /> Download file
                  </Button>
                )}
                {!readonly && (
                  <UncontrolledDropdown>
                    <DropdownToggle
                      caret
                      outline
                      color="mid"
                      disabled={disabled}
                    >
                      Change file
                    </DropdownToggle>
                    {menu}
                  </UncontrolledDropdown>
                )}
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

FileInputBinary.propTypes = {
  ...withDownloadPropTypes,
};

// FIX for https://github.com/rjsf-team/react-jsonschema-form/issues/1309

const ConnectedFileInputBinary = withDownload(FileInputBinary);

const ConnectedFileInputBinaryAsComponent = (
  props: React.ElementProps<*>
): React.Element<*> => <ConnectedFileInputBinary {...props} />;

export default ConnectedFileInputBinaryAsComponent;
