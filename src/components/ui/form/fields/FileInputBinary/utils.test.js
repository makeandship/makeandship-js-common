/* @flow */

import { isDelete, DELETE, urlForImageValue } from './utils';

const thumbnailsValue = {
  url: 'https://picsum.photos/id/237/500/500',
  filename: '500.jpeg',
  mimetype: 'image/jpeg',
  size: 6232691,
  thumbnails: [
    {
      url: 'https://picsum.photos/id/237/128/128',
      height: 128,
      width: 128,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/256/256',
      height: 256,
      width: 256,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/512/512',
      height: 512,
      width: 512,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/1024/1024',
      height: 1024,
      width: 1024,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
  ],
};

describe('FileInputBinary util isDelete', function spec() {
  it('should handle bad input', () => {
    expect(isDelete(undefined)).toBeFalsy();
    expect(isDelete(null)).toBeFalsy();
    expect(isDelete({})).toBeFalsy();
    expect(isDelete()).toBeFalsy();
  });
  it('should validate', () => {
    expect(isDelete(DELETE)).toBeTruthy();
  });
});

describe('FileInputBinary util urlForImageValue', function spec() {
  it('should handle bad input', () => {
    expect(urlForImageValue(undefined)).toBeFalsy();
    expect(urlForImageValue(null)).toBeFalsy();
    expect(urlForImageValue({})).toBeFalsy();
    expect(urlForImageValue()).toBeFalsy();
  });
  it('should validate', () => {
    expect(urlForImageValue('https://picsum.photos/id/237/500/500')).toEqual(
      'https://picsum.photos/id/237/500/500'
    );
    expect(urlForImageValue(thumbnailsValue)).toEqual(
      'https://picsum.photos/id/237/512/512'
    );
  });
});
