/* @flow */

import * as React from 'react';
import { storiesOf } from '@storybook/react';

import ConnectedStorybook from '../../../../../utils/ConnectedStorybook';

import FileInputBinary from './FileInputBinary';

const value = {
  filename: '500.jpeg',
  mimetype: 'image/jpeg',
  url: 'https://picsum.photos/id/237/500/500',
  size: 50958,
};

const thumbnailsValue = {
  url: 'https://picsum.photos/id/237/500/500',
  filename: '500.jpeg',
  mimetype: 'image/jpeg',
  size: 6232691,
  thumbnails: [
    {
      url: 'https://picsum.photos/id/237/128/128',
      height: 128,
      width: 128,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/256/256',
      height: 256,
      width: 256,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/512/512',
      height: 512,
      width: 512,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/1024/1024',
      height: 1024,
      width: 1024,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
  ],
};

const variations = {
  default: {},
  populated: {
    value,
  },
  populatedDisabled: {
    value,
    disabled: true,
  },
  populatedReadonly: {
    value,
    readonly: true,
  },
  populatedUrl: {
    value: 'https://picsum.photos/id/237/500/500',
  },
  populatedThumbnails: {
    value: thumbnailsValue,
  },
};

type State = {
  value?: string,
};

class FileInputPreservingValue extends React.Component<*, State> {
  state = {};

  constructor(props) {
    super(props);
    const { value } = props;
    this.state = { value };
  }

  onChange = (value) => {
    console.log('onChange', value);
    this.setState({
      value,
    });
  };

  render() {
    const { value, ...rest } = this.props; // eslint-disable-line
    return (
      <FileInputBinary
        id={'test'}
        value={this.state.value}
        onChange={this.onChange}
        {...rest}
      />
    );
  }
}

storiesOf('FileInputBinary', module)
  .addDecorator((story) => <ConnectedStorybook story={story()} />)
  .add('Default', () => <FileInputPreservingValue {...variations.default} />)
  .add('Populated', () => (
    <FileInputPreservingValue {...variations.populated} />
  ))
  .add('Populated, disabled', () => (
    <FileInputPreservingValue {...variations.populatedDisabled} />
  ))
  .add('Populated, readonly', () => (
    <FileInputPreservingValue {...variations.populatedReadonly} />
  ))
  .add('Populated, url', () => (
    <FileInputPreservingValue {...variations.populatedUrl} />
  ))
  .add('Populated, thumbnails', () => (
    <FileInputPreservingValue {...variations.populatedThumbnails} />
  ));
