/* @flow */

import { isString, isPlainObject } from '../../../../../utils/is';

export const DELETE = {
  action: 'delete',
};

export const isDelete = (value: any) => {
  return !!(isPlainObject(value) && value.action && value.action === 'delete');
};

export const LARGEST_THUMBNAIL_EDGE = 512;

export const urlForImageValue = (value: any) => {
  if (isString(value)) {
    return value;
  }
  if (!isPlainObject(value)) {
    return;
  }
  let url = value.url;
  if (value.thumbnails) {
    value.thumbnails.forEach((entry) => {
      if (
        entry.width <= LARGEST_THUMBNAIL_EDGE &&
        entry.height <= LARGEST_THUMBNAIL_EDGE
      ) {
        url = entry.url;
      }
    });
  }
  return url;
};
