/* @flow */

import * as React from 'react';
import { storiesOf } from '@storybook/react';

import PasswordInput from './PasswordInput';

const onChange = (name, value) => {
  console.log('onChange', name, value);
};

const variations = {
  default: {
    onChange,
  },
};

storiesOf('PasswordInput', module).add('Default', () => (
  <PasswordInput {...variations.default} />
));
