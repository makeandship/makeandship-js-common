/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';

import styles from './TitleField.module.scss';

const TitleField = (props: any) => {
  const { id, title, required } = props;
  return (
    <div className={styles.componentWrap}>
      <legend id={id} className={styles.title}>
        {title}
        {required ? '*' : null}
      </legend>
    </div>
  );
};

TitleField.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  required: PropTypes.bool,
};

export default TitleField;
