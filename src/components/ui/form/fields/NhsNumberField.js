/* @flow */

import * as React from 'react';

class NhsNumberField extends React.Component<*> {
  onChange = (event: SyntheticInputEvent<*>) => {
    const { onChange } = this.props;
    let value = event.target.value;
    value = value.replace(/[^\d]/, '');
    onChange(value);
  };

  render() {
    const { id, value, required, readonly, disabled, autofocus } = this.props;
    return (
      <input
        id={id}
        required={required}
        type="text"
        className="form-control"
        readOnly={readonly}
        disabled={disabled}
        autoFocus={autofocus}
        value={value || ''}
        onChange={this.onChange}
        maxLength="10"
      />
    );
  }
}

export default NhsNumberField;
