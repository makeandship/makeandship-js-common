/* @flow */

import * as React from 'react';
import ReactDatePicker from 'react-datepicker';
import * as dateFns from 'date-fns';

import { getCurrentLocale, formats } from '../../../../utils/date';
import DatePickerCustomInput from './DatePickerCustomInput';

class DatePicker extends React.PureComponent<*> {
  onChange = (value: Date) => {
    const { onChange } = this.props;
    onChange(value ? dateFns.formatISO(value) : undefined);
  };

  render() {
    /* eslint-disable no-unused-vars */
    const {
      value,
      onChange,
      readonly,
      dateFormat,
      maxDate,
      ...rest
    } = this.props;
    /* eslint-enable no-unused-vars */
    const selectedValue = value ? dateFns.parseISO(value) : undefined;
    if (readonly) {
      return (
        <ReactDatePicker
          selected={selectedValue}
          dateFormat={dateFormat}
          disabled
        />
      );
    }
    return (
      <ReactDatePicker
        selected={selectedValue}
        onChange={this.onChange}
        maxDate={maxDate}
        dateFormat={dateFormat}
        {...rest}
      />
    );
  }

  static defaultProps = {
    maxDate: new Date(),
    dateFormat: formats.date,
    locale: getCurrentLocale(),
    autoComplete: 'off',
    customInput: <DatePickerCustomInput />,
  };
}

export default DatePicker;

export const DatePickerBeforeNow = (props: any) => (
  <DatePicker maxDate={new Date()} {...props} />
);

export const DatePickerAny = (props: any) => (
  <DatePicker maxDate={dateFns.addYears(new Date(), 1000)} {...props} />
);

export const DatePickerFromNow = (props: any) => (
  <DatePicker
    minDate={new Date()}
    maxDate={dateFns.addYears(new Date(), 1000)}
    {...props}
  />
);
