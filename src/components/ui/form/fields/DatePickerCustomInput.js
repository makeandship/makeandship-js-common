/* @flow */

import * as React from 'react';
import { InputGroup, InputGroupAddon, InputGroupText, Input } from 'reactstrap';

class DatePickerCustomInput extends React.PureComponent<*> {
  _inputRef: null | Input;

  onClick = () => {
    this._inputRef && this._inputRef.focus();
  };

  inputRef = (ref: null | Input) => {
    this._inputRef = ref;
  };

  render() {
    return (
      <InputGroup>
        <Input type="text" ref={this.inputRef} {...this.props} />
        <InputGroupAddon addonType="append">
          <InputGroupText onClick={this.props.onClick}>
            <i className="fa fa-calendar" />
          </InputGroupText>
        </InputGroupAddon>
      </InputGroup>
    );
  }
}

export default DatePickerCustomInput;
