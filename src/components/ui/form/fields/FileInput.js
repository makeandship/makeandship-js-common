/* @flow */

import * as React from 'react';
import filesize from 'filesize';
import { Button } from 'reactstrap';

import mimetypes from '../../../../utils/mimetypes.json';
import { isDataURI } from '../../../../utils/is';

import styles from './FileInput.module.scss';
import { parseDataURI } from '../utils';

const addNameToDataURL = (dataURL: string, name: string) =>
  dataURL.replace(';base64', `;name=${name};base64`);

class FileInput extends React.Component<*> {
  _fileUpload: HTMLInputElement;

  onChange = (e: SyntheticInputEvent<HTMLInputElement>) => {
    e.preventDefault();
    const file: File = e.target.files[0];
    if (!file) {
      return;
    }
    const { onChange } = this.props;
    const reader = new FileReader();
    reader.onload = (e) => {
      // add the original file name to the encoded string
      const dataURL = addNameToDataURL(e.target.result, file.name);
      onChange && onChange(dataURL);
    };
    // Read in the image file as a data URL
    reader.readAsDataURL(file);
  };

  onBrowseFileClick = (e: Event) => {
    e.preventDefault();
    this._fileUpload.click();
  };

  onRemoveFileClick = (e: Event) => {
    e.preventDefault();
    const { onChange } = this.props;
    onChange && onChange();
  };

  fileUploadRef = (ref: any) => {
    this._fileUpload = ref;
  };

  onDownloadFileClick = (e: Event) => {
    e.preventDefault();
    const { value } = this.props;
    if (isDataURI(value)) {
      const parsed = parseDataURI(value);
      const { filename } = parsed;
      const element = document.createElement('a');
      element.setAttribute('style', 'display: none');
      element.setAttribute('href', value);
      element.setAttribute('download', filename);
      document.body && document.body.appendChild(element);
      element.click();
      setTimeout(() => {
        document.body && document.body.removeChild(element);
      }, 0);
    }
  };

  render() {
    /* eslint-disable no-unused-vars */
    const { multiple, id, autofocus, value, readonly } = this.props;
    /* eslint-enable no-unused-vars */
    if (multiple) {
      throw new Error('FileInput does not support multiple');
    }
    const disabled = this.props.disabled || readonly;
    let rendered = null;
    let description = null;
    if (value) {
      if (isDataURI(value)) {
        const parsed = parseDataURI(value);
        const { type, name, sizeInBytes } = parsed;
        const filetype = mimetypes[type] || 'Unknown type';
        const prettySize = filesize(sizeInBytes, { fullform: true });
        description = (
          <div className={styles.description}>
            <i className="fa fa-file-o" /> {name}{' '}
            <span className={styles.fileTypeAndSize}>
              {' '}
              &middot; {filetype} &middot; {prettySize}
            </span>
          </div>
        );
        if (type.split('/')[0] === 'image') {
          rendered = (
            <div
              className={styles.imagePreviewSizerWidth}
              onClick={this.onBrowseFileClick}
            >
              <div className={styles.imagePreviewSizerHeight}>
                <div className={styles.imagePreviewWrap}>
                  <img className={styles.imagePreview} src={value} />
                </div>
              </div>
            </div>
          );
        }
      } else {
        rendered = (
          <a
            href={value}
            target="_blank"
            rel="noopener noreferrer"
            className={'mr-3'}
          >
            {value}
          </a>
        );
      }
    }
    const isEmpty = !value;
    return (
      <div className={styles.componentWrap}>
        <input
          id={id}
          ref={this.fileUploadRef}
          onChange={this.onChange}
          type="file"
          className={styles.fileUpload}
          disabled={disabled}
        />
        {isEmpty ? (
          <Button
            outline
            color="mid"
            onClick={this.onBrowseFileClick}
            disabled={disabled}
          >
            <i className="fa fa-folder-open" /> Upload file
          </Button>
        ) : (
          <div className={styles.contentWrap}>
            {rendered}
            <div className={styles.descriptionAndActions}>
              {description}
              <div className={styles.actions}>
                {isDataURI(value) && (
                  <Button
                    outline
                    color="mid"
                    onClick={this.onDownloadFileClick}
                    disabled={disabled && !readonly}
                  >
                    <i className="fa fa-download" /> Download file
                  </Button>
                )}
                {!readonly && (
                  <Button
                    className={'ml-2'}
                    outline
                    color="mid"
                    onClick={this.onBrowseFileClick}
                    disabled={disabled}
                  >
                    <i className="fa fa-folder-open" /> Change file
                  </Button>
                )}
                {!readonly && (
                  <Button
                    className={'ml-2'}
                    outline
                    color="mid"
                    onClick={this.onRemoveFileClick}
                    disabled={disabled}
                  >
                    <i className="fa fa-times" /> Remove file
                  </Button>
                )}
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default FileInput;
