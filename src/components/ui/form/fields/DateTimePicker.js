/* @flow */

import * as React from 'react';
import DatePicker from 'react-datepicker';
import * as dateFns from 'date-fns';

import { getCurrentLocale, formats } from '../../../../utils/date';
import DatePickerCustomInput from './DatePickerCustomInput';

class DateTimePicker extends React.PureComponent<*> {
  _datePickerRef: any;

  onChange = (value: Date) => {
    const { onChange, inline } = this.props;
    onChange(value ? dateFns.formatISO(value) : undefined);
    if (!inline) {
      // TODO: find a cleaner way to close the popup on select
      this._datePickerRef && this._datePickerRef.setFocus(false);
    }
  };

  onDatePickerRef = (ref: any | null) => {
    this._datePickerRef = ref;
  };

  render() {
    /* eslint-disable no-unused-vars */
    const {
      value,
      readonly,
      selected,
      onChange,
      dateFormat,
      ...rest
    } = this.props;
    /* eslint-enable no-unused-vars */
    const selectedValue = value ? dateFns.parseISO(value) : undefined;
    if (readonly) {
      return (
        <DatePicker selected={selectedValue} dateFormat={dateFormat} disabled />
      );
    }
    return (
      <DatePicker
        ref={this.onDatePickerRef}
        selected={selectedValue}
        onChange={this.onChange}
        dateFormat={dateFormat}
        {...rest}
      />
    );
  }
  static defaultProps = {
    showTimeSelect: true,
    timeFormat: formats.time,
    dateFormat: formats.dateTime,
    locale: getCurrentLocale(),
    maxDate: new Date(),
    timeIntervals: 15,
    inline: true,
    isOpen: false,
    autoComplete: 'off',
    customInput: <DatePickerCustomInput />,
  };
}

export default DateTimePicker;

export const DateTimePickerBeforeNow = (props: any) => (
  <DateTimePicker maxDate={new Date()} {...props} />
);

export const DateTimePickerAny = (props: any) => (
  <DateTimePicker maxDate={dateFns.addYears(new Date(), 1000)} {...props} />
);

export const DateTimePickerFromNow = (props: any) => (
  <DateTimePicker
    minDate={new Date()}
    maxDate={dateFns.addYears(new Date(), 1000)}
    {...props}
  />
);
