/* @flow */

import * as React from 'react';
import { storiesOf } from '@storybook/react';

import DatePicker, {
  DatePickerBeforeNow,
  DatePickerAny,
  DatePickerFromNow,
} from './DatePicker';

const onChange = (value) => {
  console.log('onChange', value);
};

const variations = {
  default: {
    name: 'test',
    onChange,
  },
};

storiesOf('DatePicker', module)
  .add('Default', () => <DatePicker {...variations.default} />)
  .add('DatePickerBeforeNow', () => (
    <DatePickerBeforeNow {...variations.default} />
  ))
  .add('DatePickerAny', () => <DatePickerAny {...variations.default} />)
  .add('DatePickerFromNow', () => (
    <DatePickerFromNow {...variations.default} />
  ));
