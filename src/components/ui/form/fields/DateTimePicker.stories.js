/* @flow */

import * as React from 'react';
import { storiesOf } from '@storybook/react';

import DateTimePicker, {
  DateTimePickerBeforeNow,
  DateTimePickerAny,
  DateTimePickerFromNow,
} from './DateTimePicker';

const onChange = (value) => {
  console.log('onChange', value);
};

const variations = {
  default: {
    name: 'test',
    onChange,
  },
};

storiesOf('DateTimePicker', module)
  .add('Default', () => <DateTimePicker {...variations.default} />)
  .add('Default not inline', () => (
    <DateTimePicker {...variations.default} inline={false} />
  ))
  .add('DateTimePickerBeforeNow', () => (
    <DateTimePickerBeforeNow {...variations.default} />
  ))
  .add('DateTimePickerAny', () => <DateTimePickerAny {...variations.default} />)
  .add('DateTimePickerFromNow', () => (
    <DateTimePickerFromNow {...variations.default} />
  ));
