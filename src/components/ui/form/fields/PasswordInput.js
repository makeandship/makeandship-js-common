/* @flow */

import * as React from 'react';
import zxcvbn from 'zxcvbn';

import styles from './PasswordInput.module.scss';

const renderPasswordStrength = (value) => {
  if (!value) {
    return null;
  }
  const report = zxcvbn(value);
  const { score } = report;
  const possibleScores = [4, 3, 2, 1, 0];
  return (
    <div className={styles.passwordStrengthIndicatorContainer}>
      {possibleScores.map((possibleScore) => {
        let className = styles.passwordStrengthIndicatorOff;
        if (value.length > 0) {
          if (score > 2) {
            className = styles.passwordStrengthIndicatorG;
          } else if (score > 0) {
            className = styles.passwordStrengthIndicatorA;
          } else {
            className = styles.passwordStrengthIndicatorR;
          }
        }
        return (
          <div
            key={possibleScore}
            className={
              score >= possibleScore
                ? className
                : styles.passwordStrengthIndicatorOff
            }
          />
        );
      })}
    </div>
  );
};

const PasswordInput = ({
  id,
  value,
  required,
  onChange,
  label,
}: React.ElementProps<*>): React.Element<*> => (
  <div>
    <input
      id={id}
      type="password"
      className="form-control"
      value={value || ''}
      required={required}
      onChange={(e) => onChange(e.target.value)}
      label={label}
    />
    {renderPasswordStrength(value)}
  </div>
);

export default PasswordInput;
