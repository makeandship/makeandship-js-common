/* @flow */

import * as React from 'react';
import { storiesOf } from '@storybook/react';

import FileInput from './FileInput';

const value =
  'data:image/svg+xml;charset=UTF-8,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20baseProfile%3D%22full%22%20width%3D%22undefined%22%20height%3D%22undefined%22%3E%20%3Crect%20width%3D%22100%25%22%20height%3D%22100%25%22%20fill%3D%22grey%22%2F%3E%20%20%3Ctext%20x%3D%220%22%20y%3D%2220%22%20font-size%3D%2220%22%20text-anchor%3D%22start%22%20fill%3D%22white%22%3Eundefinedxundefined%3C%2Ftext%3E%20%3C%2Fsvg%3E';

const variations = {
  default: {},
  populated: {
    value,
  },
  populatedDisabled: {
    value,
    disabled: true,
  },
  populatedReadonly: {
    value,
    readonly: true,
  },
  populatedUrl: {
    value: '/path/to/document.pdf',
  },
};

type State = {
  value?: string,
};

class FileInputPreservingValue extends React.Component<*, State> {
  state = {};

  constructor(props) {
    super(props);
    const { value } = props;
    this.state = { value };
  }

  onChange = (value) => {
    console.log('onChange', value);
    this.setState({
      value,
    });
  };

  render() {
    const { value, ...rest } = this.props; // eslint-disable-line
    return (
      <FileInput
        id={'test'}
        value={this.state.value}
        onChange={this.onChange}
        {...rest}
      />
    );
  }
}

storiesOf('FileInput', module)
  .add('Default', () => <FileInputPreservingValue {...variations.default} />)
  .add('Populated', () => (
    <FileInputPreservingValue {...variations.populated} />
  ))
  .add('Populated, disabled', () => (
    <FileInputPreservingValue {...variations.populatedDisabled} />
  ))
  .add('Populated, readonly', () => (
    <FileInputPreservingValue {...variations.populatedReadonly} />
  ))
  .add('Populated, url', () => (
    <FileInputPreservingValue {...variations.populatedUrl} />
  ));
