/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';

import styles from './DescriptionField.module.scss';

const DescriptionField = (props: any) => {
  const { id, description } = props;
  if (!description) {
    return null;
  }
  if (typeof description === 'string') {
    return (
      <div id={id} className={styles.componentWrap}>
        <p className={styles.description}>{description}</p>
      </div>
    );
  } else {
    return (
      <div id={id} className={styles.componentWrap}>
        <div className={styles.description}>{description}</div>
      </div>
    );
  }
};

DescriptionField.propTypes = {
  id: PropTypes.string,
  description: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

export default DescriptionField;
