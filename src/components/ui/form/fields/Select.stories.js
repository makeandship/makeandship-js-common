/* @flow */

import * as React from 'react';
import { storiesOf } from '@storybook/react';

import Select from './Select';

const onChange = (name, value) => {
  console.log('onChange', name, value);
};

const options = [
  {
    label: 'Option 1 Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    value: 'option-1',
  },
  {
    label: 'Option 2 Phasellus at magna vel odio placerat pharetra eget eget',
    value: 'option-2',
  },
  {
    label: 'Option 3 Aenean blandit pulvinar enim ut tristique',
    value: 'option-3',
  },
];

const variations = {
  default: {
    name: 'test',
    options,
    onChange,
  },
  initiallyOpen: {
    name: 'test',
    value: 'option-2',
    options,
    onChange,
    initiallyOpen: true,
  },
  initiallyOpenFalse: {
    name: 'test',
    value: 'option-2',
    options,
    onChange,
    initiallyOpen: false,
  },
  wideMenu: {
    name: 'test',
    options,
    onChange,
    wideMenu: true,
  },
  disabled: {
    name: 'test',
    options,
    onChange,
    disabled: true,
  },
  readonly: {
    name: 'test',
    options,
    onChange,
    readonly: true,
  },
  enumOptions: {
    name: 'test',
    options: { enumOptions: options },
    onChange,
  },
  multiple: {
    name: 'test',
    options,
    onChange,
    multiple: true,
  },
};

type State = {
  value: any,
};

class SelectWithState extends React.Component<*, State> {
  constructor(props: any) {
    super(props);
    this.state = { value: props.value || undefined };
  }
  onChange = (name, value) => {
    this.setState({ value });
    this.props.onChange && this.props.onChange(name, value);
  };
  render() {
    /* eslint-disable no-unused-vars */
    const { value: passedValue, onChange, ...rest } = this.props;
    /* eslint-enable no-unused-vars */
    const { value } = this.state;
    return <Select value={value} onChange={this.onChange} {...rest} />;
  }
}

storiesOf('Select', module)
  .add('Default', () => <SelectWithState {...variations.default} />)
  .add('Initially open', () => (
    <SelectWithState {...variations.initiallyOpen} />
  ))
  .add('Initially open false', () => (
    <SelectWithState {...variations.initiallyOpenFalse} />
  ))
  .add('Wide menu', () => (
    <div style={{ width: '25%' }}>
      <SelectWithState {...variations.wideMenu} />
    </div>
  ))
  .add('Disabled', () => <SelectWithState {...variations.disabled} />)
  .add('Read only', () => <SelectWithState {...variations.readonly} />)
  .add('Enum options', () => <SelectWithState {...variations.enumOptions} />)
  .add('Multiple', () => <SelectWithState {...variations.multiple} />);
