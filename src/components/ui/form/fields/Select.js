/* @flow */

import * as React from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import hoistNonReactStatic from 'hoist-non-react-statics';

import styles from './Select.module.scss';
import { isArray, isPlainObject } from '../../../../utils/is';
// enhance Select to pass back the 'name' prop as the first argument of the onChange callback
// and unwrap react-jsonschema-form options

const customStylesWideMenu = {
  option: (provided) => ({
    ...provided,
    whiteSpace: 'noWrap',
  }),
  menu: (provided) => ({
    ...provided,
    width: 'auto',
  }),
};

function enhanceSelect(WrappedComponent) {
  class EnhanceSelect extends React.PureComponent<*> {
    _didInitiallyOpen = false;

    onChange = (value: any) => {
      const { name, onChange } = this.props;
      let changeValue = value;
      if (isArray(value)) {
        // unpack array of {label, value} in multi select
        changeValue = value.map((entry) => entry.value);
      } else if (isPlainObject(value) && value.value) {
        // unpack single {label, value} in select
        changeValue = value.value;
      }
      if (name) {
        onChange(name, changeValue);
      } else {
        onChange(changeValue);
      }
    };

    render() {
      /* eslint-disable no-unused-vars */
      let {
        name,
        onChange,
        options,
        wideMenu,
        initiallyOpen,
        readonly,
        multiple,
        clearable,
        value,
        ...rest
      } = this.props;
      /* eslint-enable no-unused-vars */
      if (options.enumOptions) {
        options = options.enumOptions;
        // options from react-jsonschema-form enum
      }
      const disabled = this.props.disabled || readonly;
      let selectedValue;
      if (value) {
        if (isArray(value)) {
          selectedValue = options.filter((option) =>
            value.includes(option.value)
          );
        } else {
          selectedValue = options.filter((option) => value === option.value);
        }
      }
      if (initiallyOpen) {
        if (!this._didInitiallyOpen) {
          this._didInitiallyOpen = true;
        } else {
          initiallyOpen = undefined;
        }
      } else {
        // make sure this is not set to false, which prevents menu from opening
        initiallyOpen = undefined;
      }
      return (
        <WrappedComponent
          {...rest}
          styles={wideMenu ? customStylesWideMenu : undefined}
          value={selectedValue}
          options={options}
          onChange={this.onChange}
          isDisabled={disabled}
          isClearable={clearable}
          isMulti={multiple}
          menuIsOpen={initiallyOpen}
          autoFocus={initiallyOpen}
        />
      );
    }
    static defaultProps = {
      wideMenu: false,
      initiallyOpen: undefined,
      clearable: false,
      className: styles.container,
      classNamePrefix: 'react-select',
    };
  }

  EnhanceSelect.displayName = `EnhanceSelect(${getDisplayName(
    WrappedComponent
  )})`;

  EnhanceSelect.propTypes = {
    name: PropTypes.string,
    onChange: PropTypes.func,
    options: PropTypes.any.isRequired,
    wideMenu: PropTypes.bool,
    initiallyOpen: PropTypes.bool,
    readonly: PropTypes.bool,
    disabled: PropTypes.bool,
    multiple: PropTypes.bool,
  };

  hoistNonReactStatic(EnhanceSelect, WrappedComponent);

  return EnhanceSelect;
}

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

export default enhanceSelect(Select);
