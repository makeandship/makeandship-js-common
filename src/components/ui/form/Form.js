/* @flow */

import * as React from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Prompt } from 'react-router-dom';
import scrollIntoView from 'scroll-into-view';

import JsonSchemaForm from 'react-jsonschema-form';

import FieldTemplate from './templates/FieldTemplate';
import ArrayFieldTemplate from './templates/ArrayFieldTemplate';
import ObjectFieldTemplate from './templates/ObjectFieldTemplate';
import ErrorList from './templates/ErrorList';
import TitleField from './fields/TitleField';
import DescriptionField from './fields/DescriptionField';

import { isFunction, isReactComponent } from '../../../utils/is';

import {
  addErrorPairs,
  validateWithSchema,
  hasErrors,
  isFormDataDirty,
  isFormDataEqual,
} from './utils';
import {
  getFormData,
  setFormData,
  setFormDataForKeyPath,
  clearFormData,
  getFormTransientData,
  setFormTransientData,
  setFormTransientDataForKeyPath,
  clearFormTransientData,
} from '../../../modules/form';

import styles from './Form.module.scss';

class Form extends React.Component<*> {
  _isInitialValidation = false;
  _formRef: React.ElementRef<*> | null;
  _submitTimeout: TimeoutID;

  formRef = (ref) => {
    this._formRef = ref;
  };

  submit = () => {
    this._formRef && this._formRef.submit();
  };

  updateForErrorProps = () => {
    this._submitTimeout && clearTimeout(this._submitTimeout);
    if (this.props.error && !this._isInitialValidation) {
      // FIXME: this is a workaround for if formData and errors change simultaneously
      // allows the formData to propagate before forcing a submit
      // to inject the errors via validate
      this._submitTimeout = setTimeout(() => {
        this._isInitialValidation = true;
        this.submit();
      }, 0);
    }
  };

  scrollToErrorsDeferred = () => {
    setTimeout(this.scrollToErrors, 0);
  };

  scrollToErrors = () => {
    const element = document.getElementById('errors');
    if (element) {
      scrollIntoView(element, { time: 500, align: { top: 0 } });
    }
  };

  componentDidMount = () => {
    const { setFormData } = this.props;
    // were we passed form data?
    if (this.props.passedFormData) {
      setFormData(this.props.passedFormData);
    }
    this.updateForErrorProps();
  };

  componentDidUpdate = (prevProps) => {
    const { setFormData } = this.props;
    if (!isFormDataEqual(prevProps.passedFormData, this.props.passedFormData)) {
      // passed in data changed
      setFormData(this.props.passedFormData);
    }
    const errorChanged = !isFormDataEqual(prevProps.error, this.props.error);
    const schemaChanged = !isFormDataEqual(prevProps.schema, this.props.schema);
    const hasErrors = !!this.props.error;
    if (errorChanged || (hasErrors && schemaChanged)) {
      this.updateForErrorProps();
      if (this.props.error && this.props.autoScrollToErrors) {
        this.scrollToErrorsDeferred();
      }
    }
    if (prevProps.isDirty !== this.props.isDirty) {
      this.props.onIsDirtyChange &&
        this.props.onIsDirtyChange(this.props.isDirty);
    }
  };

  onSubmit = ({ formData }) => {
    if (this._isInitialValidation) {
      // console.log(
      //   'onSubmit called as a result of validation with no errors, aborting'
      // );
      this._isInitialValidation = false;
      return;
    }
    const { setFormData, onSubmit } = this.props;
    setFormData(formData);
    onSubmit(formData);
  };

  onChange = ({ formData }) => {
    const { setFormData, onChange } = this.props;
    setFormData(formData);
    onChange && onChange(formData);
  };

  validate = (formData, errors) => {
    if (this._isInitialValidation) {
      const { error } = this.props;
      if (!error || !error.errors) {
        return errors;
      }
      // error.errors is pairs of errors from API
      errors = addErrorPairs(errors, error.errors);
      if (hasErrors(errors)) {
        // if we have errors here, then initial validation is finished
        this._isInitialValidation = false;
      } else {
        // else, onSubmit() will be called and we can finish there
        // console.log('No errors - form submission will be triggered');
      }
      return errors;
    }

    const { schema, validationEnabled } = this.props;
    if (!validationEnabled) {
      return [];
    }
    const { valid, errors: schemaErrorPairs } = validateWithSchema(
      schema,
      formData
    );

    if (!valid) {
      errors = addErrorPairs(errors, schemaErrorPairs);
    }

    // run any passed in validation - usually for 'confirm password' fields
    if (this.props.validate) {
      errors = this.props.validate(formData, errors);
    }

    errors && this.scrollToErrorsDeferred();

    return errors;
  };

  // eslint-disable-next-line
  transformErrors = (errors) => {
    // clear any errors created by react-jsonschema-form
    // since it is buggy (errors not cleared in ajv)
    // plus we run our own validation and message formatting
    return [];
  };

  componentWillUnmount() {
    const {
      clearFormData,
      clearFormDataOnUnmount,
      clearFormTransientData,
    } = this.props;
    if (clearFormDataOnUnmount) {
      clearFormData();
      clearFormTransientData();
    }
  }

  promptUnsavedChangesMessage = () =>
    `You have unsaved changes, are you sure you want to leave this page?`;

  render() {
    // extract props that we don't want passed into the underlying react-json-schema-form
    /* eslint-disable no-unused-vars */
    const {
      isFetching,
      promptUnsavedChanges,
      error,
      passedFormData,
      setFormData,
      setFormDataForKeyPath,
      clearFormData,
      formTransientData,
      setFormTransientData,
      setFormTransientDataForKeyPath,
      clearFormTransientData,
      formKey,
      clearFormDataOnUnmount,
      validate,
      onSubmit,
      onChange,
      transformErrors,
      isDirty,
      formContext,
      fields,
      children,
      __debug__,
      ...rest
    } = this.props;
    /* eslint-enable no-unused-vars */
    const enhancedFormContext = {
      ...this.props.formContext,
      formKey,
      setFormData,
      setFormDataForKeyPath,
      clearFormData,
      error,
      passedFormData,
      isDirty,
      isFetching,
      formTransientData,
      setFormTransientData,
      setFormTransientDataForKeyPath,
      clearFormTransientData,
      formData: this.props.formData,
      onSubmit: this.onSubmit,
      onChange: this.onChange,
    };
    const enhancedFields = {
      TitleField,
      DescriptionField,
      ...this.props.fields,
    };
    let enhancedChildren = children;
    if (children) {
      if (isFunction(children) && !isReactComponent(children)) {
        enhancedChildren = children(enhancedFormContext);
      }
    }
    return (
      <React.Fragment>
        {process.env.NODE_ENV === 'development' && __debug__ && (
          <React.Fragment>
            <pre>formData: {JSON.stringify(this.props.formData, null, 2)}</pre>
            <pre>
              passedFormData:{' '}
              {JSON.stringify(this.props.passedFormData, null, 2)}
            </pre>
            <pre>isDirty: {isDirty ? 'yes' : 'no'}</pre>
          </React.Fragment>
        )}
        {promptUnsavedChanges && (
          <Prompt
            when={isDirty && !isFetching}
            message={this.promptUnsavedChangesMessage}
          />
        )}
        <div className={isFetching ? styles.isFetching : null}>
          <JsonSchemaForm
            validate={this.validate}
            transformErrors={this.transformErrors}
            onSubmit={this.onSubmit}
            onChange={this.onChange}
            ref={this.formRef}
            formContext={enhancedFormContext}
            fields={enhancedFields}
            noHtml5Validate
            {...rest}
          >
            {enhancedChildren}
          </JsonSchemaForm>
        </div>
      </React.Fragment>
    );
  }

  static defaultProps = {
    clearFormDataOnUnmount: true,
    promptUnsavedChanges: true,
    autoScrollToErrors: true,
    validationEnabled: true,
    ObjectFieldTemplate,
    ArrayFieldTemplate,
    FieldTemplate,
    ErrorList,
  };
}

Form.propTypes = {
  schema: JsonSchemaForm.propTypes.schema,
  onSubmit: PropTypes.func,
  onChange: PropTypes.func,
  error: PropTypes.any,
  formData: PropTypes.object,
  formTransientData: PropTypes.object,
  passedFormData: PropTypes.object,
  isFetching: PropTypes.bool,
  setFormData: PropTypes.func.isRequired,
  setFormDataForKeyPath: PropTypes.func.isRequired,
  clearFormData: PropTypes.func.isRequired,
  setFormTransientData: PropTypes.func.isRequired,
  setFormTransientDataForKeyPath: PropTypes.func.isRequired,
  clearFormTransientData: PropTypes.func.isRequired,
  formKey: PropTypes.string.isRequired,
  clearFormDataOnUnmount: PropTypes.bool.isRequired,
  promptUnsavedChanges: PropTypes.bool.isRequired,
  validate: PropTypes.func,
  transformErrors: PropTypes.func,
  fields: PropTypes.object,
  onIsDirtyChange: PropTypes.func,
  autoScrollToErrors: PropTypes.bool,
  isDirty: PropTypes.bool,
  validationEnabled: PropTypes.bool,
  __debug__: PropTypes.bool,
};

// TODO: memoize with makeGetFormData()

const withRedux = connect(
  (state, ownProps) => {
    const passedFormData = ownProps.formData;
    const formData = getFormData(state, ownProps.formKey);
    const formTransientData = getFormTransientData(state, ownProps.formKey);
    const isDirty = isFormDataDirty(passedFormData, formData);
    return {
      passedFormData,
      formData,
      formTransientData,
      isDirty,
    };
  },
  (dispatch, ownProps) => ({
    setFormData(formData) {
      dispatch(setFormData(ownProps.formKey, formData));
    },
    setFormDataForKeyPath(keyPath, value) {
      dispatch(setFormDataForKeyPath(ownProps.formKey, keyPath, value));
    },
    clearFormData() {
      dispatch(clearFormData(ownProps.formKey));
    },
    setFormTransientData(formTransientData) {
      dispatch(setFormTransientData(ownProps.formKey, formTransientData));
    },
    setFormTransientDataForKeyPath(keyPath, value) {
      dispatch(
        setFormTransientDataForKeyPath(ownProps.formKey, keyPath, value)
      );
    },
    clearFormTransientData() {
      dispatch(clearFormTransientData(ownProps.formKey));
    },
  }),
  null,
  { forwardRef: true }
);

export default withRedux(Form);
