/* @flow */

import * as React from 'react';
import { storiesOf } from '@storybook/react';

import ConnectedStorybook from '../../../../utils/ConnectedStorybook';
import JsonSchemaDisplay from './JsonSchemaDisplay';
import incidentSchema from '../__fixtures__/incident.json';
import renderRowBrowser from './renderRowBrowser';

const dataUrlValue =
  'data:image/svg+xml;charset=UTF-8,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20baseProfile%3D%22full%22%20width%3D%22undefined%22%20height%3D%22undefined%22%3E%20%3Crect%20width%3D%22100%25%22%20height%3D%22100%25%22%20fill%3D%22grey%22%2F%3E%20%20%3Ctext%20x%3D%220%22%20y%3D%2220%22%20font-size%3D%2220%22%20text-anchor%3D%22start%22%20fill%3D%22white%22%3Eundefinedxundefined%3C%2Ftext%3E%20%3C%2Fsvg%3E';

const thumbnailsValue = {
  url: 'https://picsum.photos/id/237/500/500',
  filename: '500.jpeg',
  mimetype: 'image/jpeg',
  size: 6232691,
  thumbnails: [
    {
      url: 'https://picsum.photos/id/237/128/128',
      height: 128,
      width: 128,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/256/256',
      height: 256,
      width: 256,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/512/512',
      height: 512,
      width: 512,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/1024/1024',
      height: 1024,
      width: 1024,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
  ],
};

const data = {
  status: 'Status in Storybook',
  type: {
    type: 'STAFF',
  },
  involved: [
    {
      personType: 'Patient',
    },
    {
      personType: 'Employee/Member of staff',
    },
  ],
  when: {
    when: '2020-07-16T10:59:55+0000',
  },
  details: {
    documents: [thumbnailsValue, dataUrlValue],
  },
};

storiesOf('JsonSchemaDisplay', module)
  .addDecorator((story) => <ConnectedStorybook story={story()} />)
  .add('Default', () => (
    <JsonSchemaDisplay
      schema={incidentSchema}
      data={data}
      renderRow={renderRowBrowser}
      hideKeyPaths={['status', 'type.category']}
    />
  ))
  .add('Hide empty', () => (
    <JsonSchemaDisplay
      schema={incidentSchema}
      data={data}
      renderRow={renderRowBrowser}
      hideEmpty
    />
  ));
