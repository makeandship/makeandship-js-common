/* @flow */

import * as React from 'react';
import _ from 'lodash';
import {
  retrieveSchema,
  getDefaultFormState,
} from 'react-jsonschema-form/lib/utils';

import {
  isArray,
  isString,
  isDataURI,
  isPlainObject,
} from '../../../../utils/is';
import { formatDate, formats } from '../../../../utils/date';

export const hasString = (s: any) => isString(s) && s.length > 0;

export const rowsForSchema = ({
  schema,
  definitions,
  keyPath,
  data,
  hideEmpty,
  depth = 0,
  arrayDepth = 0,
}: any): Array<*> => {
  let rows = [];
  let dataValue: any = _.get(data, keyPath);
  const hasDataValue =
    dataValue !== undefined && dataValue !== null && dataValue !== '';

  const keyPathElements = _.toPath(keyPath);
  const hasSchemaKeyPath = keyPathElements.length > 0;
  const retreivedSchema = retrieveSchema(
    schema,
    definitions,
    hasSchemaKeyPath ? dataValue : data
  );

  if (
    hideEmpty &&
    !hasDataValue &&
    retreivedSchema.type !== 'object' &&
    retreivedSchema.type !== 'array'
  ) {
    return rows;
  }

  const title = retreivedSchema.title || keyPath;
  const description = retreivedSchema.description;

  if (retreivedSchema.type === 'object') {
    // iterate object properties
    Object.entries(retreivedSchema.properties).forEach((entry) => {
      const [key, value] = entry;
      const nextKeyPath = keyPath ? `${keyPath}.${key}` : `${key}`;
      rows = rows.concat(
        rowsForSchema({
          schema: value,
          definitions,
          keyPath: nextKeyPath,
          data,
          hideEmpty,
          arrayDepth,
          depth: depth + 1,
        })
      );
    });

    if (rows.length) {
      if (hasString(title) || hasString(description)) {
        rows.unshift({
          type: 'title',
          keyPath,
          arrayDepth,
          depth,
          title,
          description,
        });
      }
    }
  } else if (retreivedSchema.type === 'array') {
    const itemsSchema = retrieveSchema(retreivedSchema.items, definitions);
    // iterate form data array values
    if (isArray(dataValue) && keyPath) {
      let arrayRows = [];
      dataValue.forEach((dataArrayValue, index) => {
        const dataArrayValueKeyPath = `${keyPath}[${index}]`;
        arrayRows = arrayRows.concat(
          rowsForSchema({
            schema: itemsSchema,
            definitions,
            keyPath: dataArrayValueKeyPath,
            data,
            hideEmpty,
            arrayDepth: arrayDepth + 1,
            depth: depth + 1,
          })
        );
      });

      rows = rows.concat(arrayRows);
    }
  } else if (retreivedSchema.enum) {
    // radio / select
    if (retreivedSchema.enumNames) {
      const index = retreivedSchema.enum.indexOf(dataValue);
      if (index !== -1 && index < retreivedSchema.enumNames.length) {
        dataValue = retreivedSchema.enumNames[index];
      }
    }
    rows.push({ keyPath, arrayDepth, depth, title, value: dataValue });
  } else if (retreivedSchema.type === 'string') {
    if (
      retreivedSchema.format === 'data-url' ||
      isDataURI(dataValue) ||
      isPlainObject(dataValue)
    ) {
      rows.push({
        type: 'data',
        keyPath,
        arrayDepth,
        depth,
        title,
        value: dataValue,
      });
    } else if (
      retreivedSchema.format === 'date' ||
      retreivedSchema.format === 'date-time' ||
      retreivedSchema.format === 'time'
    ) {
      const format =
        retreivedSchema.format === 'date'
          ? formats.date
          : retreivedSchema.format === 'date-time'
          ? formats.dateTime
          : formats.time;
      const timeValue = dataValue ? formatDate(dataValue, format) : undefined;
      rows.push({ keyPath, arrayDepth, depth, title, value: timeValue });
    } else {
      // regular string
      rows.push({ keyPath, arrayDepth, depth, title, value: dataValue });
    }
  } else if (
    retreivedSchema.type === 'number' ||
    retreivedSchema.type === 'integer'
  ) {
    // number input, set value as number on change
    rows.push({ keyPath, arrayDepth, depth, title, value: dataValue });
  } else if (retreivedSchema.type === 'boolean') {
    // boolean input, set value as boolean on change
    const booleanValue =
      dataValue === true ? 'Yes' : dataValue === false ? 'No' : '–';
    rows.push({ keyPath, arrayDepth, depth, title, value: booleanValue });
  } else if (retreivedSchema.type === 'null') {
    // no additional cell
  } else {
    console.log(
      'Not yet implemented:',
      JSON.stringify(retreivedSchema, null, 2)
    );
  }

  return rows;
};

const JsonSchemaDisplay = ({
  schema = {},
  data: passedData = {},
  hideKeyPaths,
  hideEmpty = false,
  renderRow,
  renderEmpty,
}: React.ElementProps<*>): React.Element<*> | null => {
  const { definitions } = schema;
  const data = React.useMemo(
    () => getDefaultFormState(schema, passedData, definitions),
    [schema, passedData, definitions]
  );
  const rows = React.useMemo(() => {
    let rows = rowsForSchema({
      schema,
      definitions,
      data,
      hideEmpty,
      hideKeyPaths,
    });
    if (hideKeyPaths) {
      rows = rows.filter((row) => {
        const { keyPath } = row;
        if (hideKeyPaths.includes(keyPath)) {
          return false;
        }
        return true;
      });
    }
    return rows;
  }, [schema, definitions, data, hideEmpty, hideKeyPaths]);
  if (!rows.length) {
    return renderEmpty({ schema, data });
  }
  return (
    <React.Fragment>
      {rows.map((row, index) => renderRow({ index, data, ...row }))}
    </React.Fragment>
  );
};

export default JsonSchemaDisplay;
