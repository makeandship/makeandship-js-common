/* @flow */

import * as React from 'react';

import TitleField from '../fields/TitleField';
import AuthenticatedImage from '../../AuthenticatedImage';
import { urlForImageValue } from '../fields/FileInputBinary/utils';
import mimetypes from '../../../../utils/mimetypes.json';
import { isString, isPlainObject, isDataURI } from '../../../../utils/is';
import { parseDataURI } from '../utils';
import withDownload from '../../../../hoc/withDownload';

const Attachment = ({ value, download }) => {
  const onDownloadFileClick = React.useCallback(
    (e: Event) => {
      e.preventDefault();
      download(value);
    },
    [download, value]
  );

  if (isDataURI(value)) {
    const parsed = parseDataURI(value);
    const { type } = parsed;
    const filetype = mimetypes[type] || 'Unknown type';
    const isImage = filetype.match('image.*');
    if (isImage) {
      return (
        <a href="#" onClick={onDownloadFileClick}>
          <img src={value} />
        </a>
      );
    }
  }
  if (isString(value)) {
    return (
      <a href={value} target="_blank" rel="noopener noreferrer">
        {value}
      </a>
    );
  }
  if (isPlainObject(value)) {
    const { filename, mimetype, size } = value;
    if (mimetype && size && filename) {
      const filetype = mimetypes[mimetype] || 'Unknown type';
      const isImage = filetype.match('image.*');
      const url = urlForImageValue(value);
      if (isImage) {
        return (
          <a href="#" onClick={onDownloadFileClick}>
            <AuthenticatedImage background={false} url={url} />
          </a>
        );
      } else {
        return (
          <a href="#" onClick={onDownloadFileClick}>
            {filename}
          </a>
        );
      }
    }
  }

  return 'Cannot render current value';
};

const ConnectedAttachment = withDownload(Attachment);

const renderRowBrowser = ({
  index,
  type,
  title,
  value,
  keyPath,
  arrayDepth,
  depth,
}: React.ElementProps<*>): React.Element<*> | null => {
  if (type === 'title') {
    const Title = () => <TitleField title={title} />;

    if (arrayDepth <= 1) {
      return <Title key={`title-${index}`} />;
    }
    return (
      <div className="row" key={`${keyPath}`}>
        <div className="col-md-3"></div>
        <div className="col-md-9">
          <Title />
        </div>
      </div>
    );
  }

  const Row = () => (
    <div
      className="row form-group"
      data-array-depth={arrayDepth}
      data-depth={depth}
    >
      <label className="col-md-3 col-form-label">{title}</label>
      {type === 'data' ? (
        <div className="col-md-3">
          <div className="form-control-plaintext" data-keypath={keyPath}>
            <ConnectedAttachment value={value} />
          </div>
        </div>
      ) : (
        <div className="col-md-6">
          <div className="form-control-plaintext" data-keypath={keyPath}>
            {value}
          </div>
        </div>
      )}
    </div>
  );

  if (arrayDepth === 0) {
    return <Row key={`${keyPath}`} />;
  }
  return (
    <div key={`${keyPath}`} className="row">
      <div className="col-md-3"></div>
      <div className="col-md-9">
        <Row />
      </div>
    </div>
  );
};

export default renderRowBrowser;
