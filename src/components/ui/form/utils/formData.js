/* @flow */

import memoizeOne from 'memoize-one';
import _isEqual from 'lodash.isequal';
import _toPath from 'lodash.topath';

import { filterNewUninitialised } from '../../../../utils/clean';

// Keys

export const formFieldIdToKeyPathArray = (id: string): Array<string> => {
  // id = root_medications_0_amppId
  // convert to dotted keypath medications.0.amppId
  const dottedId = id.replace(/_/g, '.');
  const keyPathArray = _toPath(dottedId);
  // drop the intial 'root'
  keyPathArray.shift();
  return keyPathArray;
};

export const formFieldIdToKeyPath = (id: string): string => {
  const keyPathArray = formFieldIdToKeyPathArray(id);
  return keyPathArray.join('.');
};

export const formFieldIdToKeyPathWithSibling = (
  id: string,
  sibling: string
): string => {
  const keyPathArray = formFieldIdToKeyPathArray(id);
  keyPathArray.pop();
  return keyPathArray.concat(sibling).join('.');
};

export const cloneObject = (o: any) =>
  o === undefined ? o : JSON.parse(JSON.stringify(o));

// Convenience to check passed form data

export const isFormDataEqual = memoizeOne(
  (referenceFormData?: any, newFormData?: any) => {
    const isEqual = _isEqual(referenceFormData, newFormData);
    return isEqual;
  }
);

// Dirty passed-in data

export const isFormDataDirty = memoizeOne(
  (referenceFormData?: any, newFormData?: any) => {
    // strip any new 'uninitialised' values which may be added by form library
    const filteredNewFormData = filterNewUninitialised(
      referenceFormData,
      newFormData
    );
    const hasReferenceFormData = referenceFormData
      ? Object.keys(referenceFormData).length > 0
      : false;
    const hasFilteredNewFormData = !!filteredNewFormData;
    let isDirty;
    if (hasReferenceFormData) {
      // if there was original data, compare with new data
      if (!hasFilteredNewFormData) {
        // there is no current data, so not dirty
        return false;
      }
      // dirty if they are not equal
      const isEqual = _isEqual(referenceFormData, filteredNewFormData);
      isDirty = !isEqual;
    } else {
      // no original data, so dirty if there is any new data
      isDirty = !!hasFilteredNewFormData;
    }
    return isDirty;
  }
);
