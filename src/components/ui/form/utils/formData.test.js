/* @flow */

import produce from 'immer';

import {
  formFieldIdToKeyPath,
  formFieldIdToKeyPathWithSibling,
  isFormDataDirty,
  cloneObject,
  isFormDataEqual,
} from './formData';

describe('Form util formFieldIdToKeyPath', function spec() {
  it('should convert id to keypath', () => {
    expect(formFieldIdToKeyPath('root_medications_0_amppId')).toEqual(
      'medications.0.amppId'
    );
    expect(
      formFieldIdToKeyPathWithSibling('root_medications_0_amppId', 'dose')
    ).toEqual('medications.0.dose');
  });
});

describe('Form util isFormDataDirty', function spec() {
  it('should handle bad input', () => {
    expect(isFormDataDirty(undefined, undefined)).toBeFalsy();
    expect(isFormDataDirty(undefined, null)).toBeFalsy();
    expect(isFormDataDirty(null, null)).toBeFalsy();
    expect(isFormDataDirty({}, undefined)).toBeFalsy();
    expect(isFormDataDirty({}, null)).toBeFalsy();
    expect(isFormDataDirty(undefined, {})).toBeFalsy();
    expect(isFormDataDirty(null, {})).toBeFalsy();
    expect(isFormDataDirty({}, {})).toBeFalsy();
  });

  it('should determine if form data is not dirty', () => {
    expect(isFormDataDirty({ test: 'test' }, undefined)).toBeFalsy();
    expect(isFormDataDirty({ test: 'test' }, { test: 'test' })).toBeFalsy();
    expect(isFormDataDirty({}, { test: undefined })).toBeFalsy();
    expect(
      isFormDataDirty({ test: 'test' }, { test: 'test', other: undefined })
    ).toBeFalsy();
    expect(isFormDataDirty({ test: 'test' }, {})).toBeFalsy();
    expect(isFormDataDirty({ test: ['test'] }, { test: ['test'] })).toBeFalsy();
    expect(
      isFormDataDirty({ test: ['test'] }, { test: ['test'], other: undefined })
    ).toBeFalsy();
  });

  it('should determine if form data is dirty', () => {
    expect(isFormDataDirty(undefined, { test: 'test' })).toBeTruthy();
    expect(isFormDataDirty({}, { test: 'test' })).toBeTruthy();

    expect(isFormDataDirty({ test: 'test' }, { test: null })).toBeTruthy();
    expect(isFormDataDirty({ test: 'test' }, { test: undefined })).toBeTruthy();
    expect(isFormDataDirty({ test: 'test' }, { test: '' })).toBeTruthy();
    expect(isFormDataDirty({ test: 'test' }, { test: 'test123' })).toBeTruthy();
    expect(isFormDataDirty({ test: ['test'] }, { test: [] })).toBeTruthy();
    expect(
      isFormDataDirty({ test: undefined }, { test: ['test123'] })
    ).toBeTruthy();
    expect(isFormDataDirty({}, { test: ['test123'] })).toBeTruthy();
  });

  it('should determine if nested form data is dirty', () => {
    expect(isFormDataDirty(undefined, { test: 'test' })).toBeTruthy();
    expect(isFormDataDirty(undefined, { test: undefined })).toBeFalsy();
    expect(isFormDataDirty(undefined, { test: {} })).toBeFalsy();
    expect(isFormDataDirty(undefined, { test: { test: {} } })).toBeFalsy();
  });
});

describe('Form util isFormDataEqual', function spec() {
  it('should handle bad input', () => {
    expect(isFormDataEqual(undefined, undefined)).toBeTruthy();
    expect(isFormDataEqual(undefined, null)).toBeFalsy();
    expect(isFormDataEqual(null, null)).toBeTruthy();
    expect(isFormDataEqual({}, undefined)).toBeFalsy();
    expect(isFormDataEqual({}, null)).toBeFalsy();
    expect(isFormDataEqual(undefined, {})).toBeFalsy();
    expect(isFormDataEqual(null, {})).toBeFalsy();
    expect(isFormDataEqual({}, {})).toBeTruthy();
  });

  it('should determine if form data is equal', () => {
    expect(isFormDataEqual({ test: 'test' }, undefined)).toBeFalsy();
    expect(isFormDataEqual({ test: 'test' }, { test: 'test' })).toBeTruthy();
    expect(
      isFormDataEqual({ test: ['test'] }, { test: ['test'] })
    ).toBeTruthy();
    expect(
      isFormDataEqual({ test: ['test'] }, cloneObject({ test: ['test'] }))
    ).toBeTruthy();
    const immutableCopy = produce({ test: ['test'] }, () => {});
    expect(isFormDataEqual({ test: ['test'] }, immutableCopy)).toBeTruthy();
  });
});
