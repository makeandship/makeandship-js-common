/* @flow */

import { parseDataURI } from './dataURI';

describe('Form util parseDataURI', function spec() {
  it('handle bad input', () => {
    expect(parseDataURI()).toBeUndefined();
    expect(parseDataURI(null)).toBeUndefined();
    expect(parseDataURI('http://www.example.com')).toBeUndefined();
    expect(parseDataURI('/path/to/document.pdf')).toBeUndefined();
  });

  it('parse test data', () => {
    const parsed = parseDataURI(
      'data:image/svg+xml;charset=UTF-8,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20baseProfile%3D%22full%22%20width%3D%22undefined%22%20height%3D%22undefined%22%3E%20%3Crect%20width%3D%22100%25%22%20height%3D%22100%25%22%20fill%3D%22grey%22%2F%3E%20%20%3Ctext%20x%3D%220%22%20y%3D%2220%22%20font-size%3D%2220%22%20text-anchor%3D%22start%22%20fill%3D%22white%22%3Eundefinedxundefined%3C%2Ftext%3E%20%3C%2Fsvg%3E'
    );
    expect(parsed.type).toEqual('image/svg+xml');
    expect(parsed.encoding).toEqual('utf-8');
    expect(parsed.sizeInBytes).toEqual(411);
    expect(parsed.body).toMatchSnapshot();
  });

  it('parse data with media type', () => {
    const parsed = parseDataURI(
      'data:text/html,%3Ch1%3EHello%2C%20World!%3C%2Fh1%3E'
    );
    expect(parsed.type).toEqual('text/html');
    expect(parsed.encoding).toEqual('base64');
    expect(parsed.body).toMatchSnapshot();
  });

  it('parse base64 encoded data with simple media type', () => {
    const parsed = parseDataURI(
      'data:text/plain;base64,SGVsbG8sIFdvcmxkIQ%3D%3D'
    );
    expect(parsed.type).toEqual('text/plain');
    expect(parsed.encoding).toEqual('base64');
    expect(parsed.body).toMatchSnapshot();
  });

  it('parse base64 encoded data with complex media type', () => {
    const parsed = parseDataURI(
      'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMDAiIGhlaWdodD0iMTAwIj48cmVjdCBmaWxsPSIjMDBCMUZGIiB3aWR0aD0iMTAwIiBoZWlnaHQ9IjEwMCIvPjwvc3ZnPg=='
    );
    expect(parsed.type).toEqual('image/svg+xml');
    expect(parsed.encoding).toEqual('base64');
    expect(parsed.body).toMatchSnapshot();
  });

  it('parse data with media types that contain .', () => {
    const parsed = parseDataURI(
      'data:application/vnd.ms-excel;base64,PGh0bWw%2BPC9odG1sPg%3D%3D'
    );
    expect(parsed.type).toEqual('application/vnd.ms-excel');
    expect(parsed.encoding).toEqual('base64');
    expect(parsed.body).toMatchSnapshot();
  });

  it('parse data with complex media type and single attribute', () => {
    const parsed = parseDataURI(
      'data:image/svg+xml;charset=UTF-8,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%22100%22%20height%3D%22100%22%3E%3Crect%20fill%3D%22%2300B1FF%22%20width%3D%22100%22%20height%3D%22100%22%2F%3E%3C%2Fsvg%3E'
    );
    expect(parsed.type).toEqual('image/svg+xml');
    expect(parsed.encoding).toEqual('utf-8');
    expect(parsed.body).toMatchSnapshot();
  });

  it('parse data with media type and multiple attributes', () => {
    const parsed = parseDataURI(
      'data:image/png;name=foo.bar;baz=quux;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAIBAMAAAA2IaO4AAAAFVBMVEXk5OTn5+ft7e319fX29vb5+fn///++GUmVAAAALUlEQVQIHWNICnYLZnALTgpmMGYIFWYIZTA2ZFAzTTFlSDFVMwVyQhmAwsYMAKDaBy0axX/iAAAAAElFTkSuQmCC'
    );
    expect(parsed.type).toEqual('image/png');
    expect(parsed.name).toEqual('foo.bar');
    expect(parsed.baz).toEqual('quux');
    expect(parsed.encoding).toEqual('base64');
    expect(parsed.body).toMatchSnapshot();
  });
});
