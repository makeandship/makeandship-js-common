/* @flow */

import { isDataURI } from '../../../../utils/is';

export const parseDataURI = (dataURI: any): any => {
  if (!dataURI || !isDataURI(dataURI)) {
    return;
  }
  try {
    const parts = dataURI.split(',');
    const params = parts[0].split(';');
    let type, body, sizeInBytes, encoding, keyValues;
    encoding = 'base64';
    keyValues = {
      name: 'unknown',
    };
    params.forEach((param) => {
      if (param.startsWith('data:')) {
        type = param.replace('data:', '');
      }
      const keyValue = param.split('=');
      if (keyValue.length === 2) {
        const key = keyValue[0];
        const value = keyValue[1];
        if (key === 'charset') {
          encoding = value.toLowerCase();
        } else {
          keyValues[key] = value;
        }
      }
    });
    body = Buffer.from(parts[1], encoding);
    sizeInBytes = body.length;
    return { type, body, sizeInBytes, encoding, ...keyValues };
  } catch (error) {
    // ignore error - return undefined
  }
};
