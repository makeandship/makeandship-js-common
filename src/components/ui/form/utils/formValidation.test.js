/* @flow */

import incidentStepsSchema from '../__fixtures__/incidentStepsSchema.json';
import incidentSchema from '../__fixtures__/incident.json';
import {
  dateValidator,
  findSchemaPropertyForKeyPath,
  formIdForErrorKey,
  validateWithSchema,
  hasErrors,
} from './formValidation';

describe('Form util dateValidator', function spec() {
  it('should validate with date', () => {
    expect(dateValidator('2000-12-25')).toBeTruthy();
    expect(dateValidator('2000-12')).toBeTruthy();
    expect(dateValidator('2000')).toBeTruthy();
  });
  it('should validate with full date time', () => {
    expect(dateValidator('2018-08-29T17:03:07+01:00')).toBeTruthy();
  });
  it('should not validate non date/time strings', () => {
    expect(dateValidator('')).toBeFalsy();
    expect(dateValidator('20-18')).toBeFalsy();
    expect(dateValidator('abc')).toBeFalsy();
    expect(dateValidator(null)).toBeFalsy();
    expect(dateValidator()).toBeFalsy();
  });
});

describe('Form util validateWithSchema', function spec() {
  it('should validate with schema', () => {
    expect(validateWithSchema(incidentSchema, {})).toMatchSnapshot();
  });
  it('should validate with schema', () => {
    expect(
      validateWithSchema(incidentSchema, {
        status: 'Submitted',
      })
    ).toMatchSnapshot();
  });
});

describe('Form util findSchemaPropertyForKeyPath', function spec() {
  it('should find schema property for keypath', () => {
    const prop = findSchemaPropertyForKeyPath(
      incidentStepsSchema[7],
      'action.beingOpen'
    );
    expect(prop).toBeDefined();
    if (prop) {
      expect(prop['title']).toEqual('Being Open');
      expect(prop['type']).toEqual('string');
    }
  });

  it('should find schema property for keypath with index', () => {
    const prop = findSchemaPropertyForKeyPath(
      incidentStepsSchema[5],
      'involved[0].patient.hospitalNumber'
    );
    expect(prop).toBeDefined();
    if (prop) {
      expect(prop['title']).toEqual('Hospital Number');
      expect(prop['type']).toEqual('string');
    }
  });

  it('should find schema property for keypath with index in dependencies', () => {
    const prop = findSchemaPropertyForKeyPath(
      incidentStepsSchema[5],
      'involved[0].injury.bodyPart'
    );
    expect(prop).toBeDefined();
    if (prop) {
      expect(prop['title']).toEqual('Body part');
      expect(prop['type']).toEqual('string');
    }
  });
});

describe('Form util formIdForErrorKey', function spec() {
  it('should create form property for keypath', () => {
    expect(formIdForErrorKey('action.beingOpen')).toEqual(
      'root_action_beingOpen'
    );
  });

  it('should create form property for keypath with index', () => {
    expect(formIdForErrorKey('involved[0].patient.hospitalNumber')).toEqual(
      'root_involved_0_patient_hospitalNumber'
    );
  });
});

describe('Form util hasErrors', function spec() {
  it('should detect errors', () => {
    expect(hasErrors({})).toBeFalsy();
    expect(hasErrors({ __errors: [] })).toBeFalsy();
    expect(hasErrors({ __errors: ['true'] })).toBeTruthy();
    expect(
      hasErrors({ __errors: [], email: { __errors: ['true'] } })
    ).toBeTruthy();
    expect(
      hasErrors({ email: { deep: { nested: { __errors: ['true'] } } } })
    ).toBeTruthy();
    expect(
      hasErrors({
        __errors: [],
        email: { deep: { nested: { addError: () => {} } } },
      })
    ).toBeFalsy();
  });
});
