/* @flow */

import type { FormSchema } from '../types';
import _get from 'lodash.get';
import _toPath from 'lodash.topath';
import _trim from 'lodash.trim';
import * as dateFns from 'date-fns';

import Ajv from 'ajv';
import nhsNumberValidator from 'nhs-number-validator';

// TODO: here we create new ajv instance per validation
// this appears to be the cleanest method to avoid issues with;
// * errors validating against same schema more than once
// * errors persisting in ajv global state (global ajv.errors)
// * errors caching schemas with no id
// * errors caching schemas with duplicate id

const createAjv = () => {
  const ajv = new Ajv({ allErrors: true });
  ajv.addFormat('nhsNumber', (data) => nhsNumberValidator.validate(data));
  ajv.addFormat('date', dateValidator);
  ajv.addFormat('date-time', dateValidator);
  return ajv;
};

// allow both date and full datetime as valid 'date' formats
export const dateValidator = (data: any) => {
  return dateFns.isValid(dateFns.parseISO(data));
};

export const validateWithSchema = (
  schema: FormSchema,
  data: any
): { valid: boolean, errors: ?{ [string]: string } } => {
  let ajv = createAjv();
  const valid = ajv.validate(schema, data);
  let errors;
  if (!valid) {
    errors = format(ajv.errors);
  }
  ajv = null;
  return { valid, errors };
};

/*

// ajv error payload examples

[
  {
    "keyword": "format",
    "dataPath": ".email",
    "schemaPath": "#/properties/email/format",
    "params": {
      "format": "email"
    },
    "message": "should match format \"email\""
  },
  {
    "keyword": "required",
    "dataPath": "",
    "schemaPath": "#/required",
    "params": {
      "missingProperty": "password"
    },
    "message": "should have required property 'password'"
  }
]

{
  "keyword": "switch",
  "dataPath": "action.actionTaken",
  "message": "should have required property 'actionTaken'",
  "data": {
    "status": "Submitted"
  },
  "schemaPath": "#/when"
}

{
  "keyword": "switch",
  "dataPath": "involved[0].type",
  "message": "should have required property 'type'",
  "data": {
    "status": "Submitted"
  },
  "schemaPath": "#/when"
}

*/

const format = (errors) => {
  const result = {};
  errors.forEach((error) => {
    if (error.dataPath && error.dataPath.length > 0) {
      // use dataPath as the key, trim the dots
      const key = _trim(error.dataPath, '.');
      result[key] = error.message;
    } else if (error.params) {
      // use the first item in 'params' as the key
      const paramValues = Object.values(error.params);
      if (paramValues.length > 0) {
        const key = paramValues[0];
        result[key] = error.message;
      }
    } else {
      console.warn('Ignored error: ', error);
    }
  });
  return result;
};

export const addErrorPairs = (errors: any, errorPairs: any) => {
  Object.keys(errorPairs).forEach((keyPath) => {
    const fieldError = _get(errors, keyPath);
    if (fieldError) {
      fieldError.addError(errorPairs[keyPath]);
    } else {
      console.error('no error for keypath', errors, keyPath);
    }
  });
  return errors;
};

export const hasErrors = (o: any) => {
  const keys = Object.keys(o);
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    const value = o[key];
    if (key === '__errors') {
      if (value.length && value.length > 0) {
        return true;
      }
    }
    if (key !== 'addError') {
      if (hasErrors(value)) {
        return true;
      }
    }
  }
};

// recursively deep search through properies and into ref's
// to find a schema object with the given key e.g. 'type'

export const findSchemaPropertyForKey = (
  schema: FormSchema,
  schemaKey: string,
  originalSchema: FormSchema
) => {
  if (schemaKey === 'bodyPart') {
    console.log({ schema, schemaKey });
  }
  if (!originalSchema) {
    originalSchema = schema;
  }
  if (schema['$ref']) {
    const definition = findSchemaDefinitionForRef(
      originalSchema,
      schema['$ref']
    );
    return findSchemaPropertyForKey(definition, schemaKey, originalSchema);
  }
  if (schema.items) {
    return findSchemaPropertyForKey(schema.items, schemaKey, originalSchema);
  }
  if (schema.oneOf) {
    for (let i = 0; i < schema.oneOf.length; i++) {
      const result = findSchemaPropertyForKey(
        schema.oneOf[i],
        schemaKey,
        originalSchema
      );
      if (result) {
        return result;
      }
    }
  }
  if (schema.properties) {
    const keys = Object.keys(schema.properties);
    if (keys.includes(schemaKey)) {
      return schema.properties[schemaKey];
    }
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      const result = findSchemaPropertyForKey(
        schema.properties[key],
        schemaKey,
        originalSchema
      );
      if (result) {
        return result;
      }
    }
  }
  if (schema.dependencies) {
    const keys = Object.keys(schema.dependencies);
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      const result = findSchemaPropertyForKey(
        schema.dependencies[key],
        schemaKey,
        originalSchema
      );
      if (result) {
        return result;
      }
    }
  }
};

// recursively deep search through properies and into ref's
// to find a schema object with the given key path e.g. 'type.subType'

export const findSchemaPropertyForKeyPath = (
  schema: FormSchema,
  schemaKeyPath: string
) => {
  // discard array indices e.g. involved[0].patient.hospitalNumber
  const cleanedSchemaKeyPath = schemaKeyPath.replace(/\[\d+\]/g, '');
  const keys = cleanedSchemaKeyPath.split('.');
  let currentSchema = schema;
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    const foundSchema = findSchemaPropertyForKey(currentSchema, key, schema);
    if (foundSchema) {
      currentSchema = foundSchema;
    } else {
      // couldn't find it
      currentSchema = undefined;
      break;
    }
  }
  return currentSchema;
};

export const findSchemaDefinitionForRef = (schema: FormSchema, ref: string) => {
  const parts = ref.split('/').slice(1);
  const definition = _get(schema, parts);
  return definition;
};

// map e.g. involved[0].patient.hospitalNumber to involved_0_patient_hospitalNumber

export const formIdForErrorKey = (errorKey: string) => {
  const snakePath = _toPath(errorKey).join('_');
  return `root_${snakePath}`;
};
