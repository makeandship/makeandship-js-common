/* @flow */

import * as React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { storiesOf } from '@storybook/react';

import ConnectedStorybook from '../../../utils/ConnectedStorybook';

import Form from './Form';
import { SubmitButton } from '../../ui/buttons';
import DatePicker from './fields/DatePicker';
import Select from './fields/Select';
import { JsonApiError } from '../../../modules/api';

const __debug__ = true;

const login401Error = new JsonApiError(401, '401 - Unauthorized', {});

const options = [
  {
    label: 'Option 1 Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    value: 'option-1',
  },
  {
    label: 'Option 2 Phasellus at magna vel odio placerat pharetra eget eget',
    value: 'option-2',
  },
  {
    label: 'Option 3 Aenean blandit pulvinar enim ut tristique',
    value: 'option-3',
  },
];

const schema = {
  title: 'Test',
  type: 'object',
  properties: {
    age: {
      type: 'number',
    },
    date: {
      type: 'string',
      format: 'date',
    },
    helpText: {
      title: 'A null field',
      description:
        'Null fields like this are great for adding extra information',
      type: 'null',
    },
    boolean: {
      type: 'object',
      title: 'Boolean field',
      properties: {
        default: {
          type: 'boolean',
          title: 'checkbox (default)',
          description: 'This is the checkbox-description',
        },
        disabled: {
          type: 'boolean',
          title: 'checkbox (disabled)',
        },
        checked: {
          type: 'boolean',
          title: 'checkbox (checked, disabled)',
        },
        radio: {
          type: 'boolean',
          title: 'Radio buttons',
          description: 'This is the radio-description',
        },
        radioAligned: {
          type: 'boolean',
          title: 'Radio buttons aligned?',
        },
        select: {
          type: 'boolean',
          title: 'Select box',
          description: 'This is the select-description',
        },
        multipleSelect: {
          type: 'array',
          title: 'A multiple-choice list',
          items: {
            type: 'string',
            enum: ['foo', 'bar', 'fuzz', 'qux'],
          },
          uniqueItems: true,
        },
        multipleSelectToHide: {
          type: 'array',
          title: 'A multiple-choice list hidden by ui:widget',
          items: {
            type: 'string',
            enum: ['foo', 'bar', 'fuzz', 'qux'],
          },
          uniqueItems: true,
        },
        proms: {
          title: 'PROMS',
          description: 'Patient PROMS',
          type: 'array',
          items: {
            title: 'URL',
            type: 'string',
            format: 'url',
            enum: [
              'https://cms.mend-dev.makeandship.com/wp-json/v1/surveys/qor15pre',
              'https://cms.mend-dev.makeandship.com/wp-json/v1/surveys/qor15post',
              'https://cms.mend-dev.makeandship.com/wp-json/v1/surveys/bauer',
              'https://cms.mend-dev.makeandship.com/wp-json/v1/surveys/overallpost',
              'https://cms.mend-dev.makeandship.com/wp-json/v1/surveys/prems',
              'https://cms.mend-dev.makeandship.com/wp-json/v1/surveys/haq',
              'https://cms.mend-dev.makeandship.com/wp-json/v1/surveys/eq5d',
            ],
            enumNames: [
              'QoR 15 Pre Operative',
              'QoR 15 Post Operative',
              'Bauer',
              'Overall Post Operative',
              'PREMS',
              'Health Assessment Questionnaire',
              'EQ5D',
            ],
          },
        },
        objectToHide: {
          type: 'object',
          title: 'An object hidden by ui:widget',
          properties: {
            age: {
              type: 'number',
            },
          },
        },
      },
    },
  },
  required: ['age'],
};

const uiSchema = {
  date: {
    'ui:widget': DatePicker,
  },
  boolean: {
    disabled: {
      'ui:disabled': true,
    },
    checked: {
      'ui:disabled': true,
    },
    radio: {
      'ui:widget': 'radio',
      'ui:options': {
        inline: true,
      },
    },
    radioAligned: {
      'ui:widget': 'radio',
    },
    select: {
      'ui:widget': Select,
    },
    multipleSelect: {
      'ui:widget': Select,
    },
    multipleSelectToHide: {
      'ui:widget': 'hidden',
    },
    proms: {
      'ui:layout': {
        md: 9,
      },
      items: {
        'ui:widget': Select,
        'ui:layout': {
          md: 9,
        },
      },
    },
    objectToHide: {
      'ui:widget': 'hidden',
    },
  },
};

const loginSchema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  title: 'Log in',
  type: 'object',
  properties: {
    email: {
      type: 'string',
      format: 'email',
      title: 'Email',
    },
    password: {
      type: 'string',
      title: 'Password',
    },
  },
  required: ['email', 'password'],
};

const loginUiSchema = {
  email: {
    'ui:placeholder': 'sam.smith@example.com',
    'ui:autofocus': true,
  },
  password: {
    'ui:widget': 'password',
  },
};

const arraySchema = {
  type: 'object',
  properties: {
    proms: {
      title: 'PROMS',
      description: 'Patient PROMS',
      type: 'array',
      items: {
        title: 'URL',
        type: 'string',
      },
    },
  },
};

const schemaForDynamicUi = {
  title: 'Test for dynamic UI',
  type: 'object',
  properties: {
    multipleSelect: {
      type: 'array',
      title: 'A multiple-choice list',
      items: {
        type: 'string',
        enum: ['foo', 'bar', 'fuzz', 'qux'],
      },
      uniqueItems: true,
    },
    dynamic: {
      type: 'string',
      title: 'Dynamic',
    },
    dynamicArray: {
      title: 'Dynamic array',
      type: 'array',
      items: {
        type: 'string',
        title: 'Dynamic string by select',
      },
    },
  },
};

const schemaForDynamicUiSchema = {
  multipleSelect: {
    'ui:widget': Select,
  },
  dynamic: {
    'ui:widget': ({ formContext }: any) => {
      const { formData, setFormDataForKeyPath, clearFormData } = formContext;
      let statusText = 'Please make a multiple-choice selection';
      if (
        formData &&
        formData.multipleSelect &&
        formData.multipleSelect.length
      ) {
        statusText = `In multiple-choice, you selected: ${formContext.formData[
          'multipleSelect'
        ].join(', ')}`;
      }
      return (
        <React.Fragment>
          <div className="form-control-plaintext">{statusText}</div>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              setFormDataForKeyPath('multipleSelect', ['fuzz', 'qux']);
            }}
          >
            Set to fuzz, qux
          </a>{' '}
          or{' '}
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              clearFormData();
            }}
          >
            Clear form data
          </a>
        </React.Fragment>
      );
    },
  },
  dynamicArray: {
    items: {
      'ui:widget': ({ id, value, onChange, formContext }: any) => {
        const {
          formTransientData,
          setFormTransientDataForKeyPath,
        } = formContext;
        const transientValue = formTransientData && formTransientData[id];
        const onInputChange = (inputString, { action }) => {
          if (action === 'input-change') {
            setFormTransientDataForKeyPath(id, inputString);
          }
        };
        const onSelectChange = (value) => {
          onChange(value);
          // allow state to propagate before updating the transientId value
          // TODO - find a simpler workaround for this
          setTimeout(() => {
            setFormTransientDataForKeyPath(id, undefined);
          }, 0);
        };
        return (
          <React.Fragment>
            <div className="form-control-plaintext">
              form data field value: {value}
            </div>
            <pre>transient value in redux state: {transientValue}</pre>
            <Select
              options={options}
              inputValue={transientValue}
              onInputChange={onInputChange}
              onChange={onSelectChange}
            />
          </React.Fragment>
        );
      },
    },
  },
};

const onSubmit = (formData) => {
  console.log('onSubmit - passed validation:', formData);
};

const onErrors = (errors) => {
  console.log('onErrors', errors);
  return errors;
};

const onChange = (formData) => {
  console.log('onChange', formData);
  return formData;
};

const variations = {
  default: {
    __debug__,
    formKey: 'test/test',
    schema,
    uiSchema,
    formData: {
      age: 567,
      boolean: {
        checked: true,
      },
    },
    onSubmit,
  },
  defaultNoUiSchema: {
    __debug__,
    formKey: 'test/test',
    schema,
    formData: {
      age: 567,
      boolean: {
        checked: true,
      },
    },
    onSubmit,
  },
  defaultReadonly: {
    __debug__,
    formKey: 'test/test',
    schema,
    uiSchema: {
      ...uiSchema,
      'ui:readonly': true,
    },
    formData: {
      age: 567,
      boolean: {
        checked: true,
      },
    },
    onSubmit,
  },
  login: {
    __debug__,
    formKey: 'test/test',
    schema: loginSchema,
    uiSchema: loginUiSchema,
    submitTitle: 'Log in',
    keyboardShouldPersistTaps: 'always',
    onSubmit,
  },
  loginWithInitialErrors: {
    __debug__,
    formKey: 'test/test',
    schema: loginSchema,
    uiSchema: loginUiSchema,
    formData: {
      email: 'exists@example.com',
    },
    error: {
      errors: {
        email: 'is already in use',
      },
    },
    onSubmit,
    onErrors,
    onChange,
  },
  loginWithInitialErrors2: {
    __debug__,
    formKey: 'test/test',
    schema: loginSchema,
    uiSchema: loginUiSchema,
    formData: {
      email: 'exists@example.com',
    },
    error: {
      errors: {
        email: 'is already in use',
      },
    },
    asyncFormData: {
      email: 'async.exists@example.com',
    },
    asyncError: {
      errors: {
        email: 'async - is already in use',
      },
    },
    onSubmit,
    onErrors,
    onChange,
  },
  loginWithInitialErrors3: {
    __debug__,
    formKey: 'test/test',
    schema: loginSchema,
    uiSchema: loginUiSchema,
    formData: {
      email: 'exists@example.com',
    },
    error: {
      errors: {
        email: 'is already in use',
      },
    },
    asyncFormData: {
      email: 'async.exists@example.com',
    },
    asyncError: login401Error,
    onSubmit,
    onErrors,
    onChange,
  },
  arrayWithErrors: {
    __debug__,
    formKey: 'test/test',
    schema: arraySchema,
    onSubmit,
    onErrors,
    onChange,
  },
  dynamicUi: {
    __debug__,
    formKey: 'test/test',
    schema: schemaForDynamicUi,
    uiSchema: schemaForDynamicUiSchema,
    onSubmit,
    onErrors,
    onChange,
    error: {
      errors: {
        multipleSelect: 'Simulated error for testing',
      },
    },
  },
};

type State = {
  formData: any,
  isFetching: boolean,
  error: any,
};

class FormWithAsyncData extends React.Component<*, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      formData: props.formData || {},
      error: props.error || {},
      isFetching: false,
    };
  }
  onSubmit = (formData: any) => {
    this.setState({ formData, isFetching: true, error: {} });
    console.log('formData:', JSON.stringify(formData, null, 2));
    setTimeout(() => {
      console.log('Setting async data');
      this.setState({
        formData: this.props.asyncFormData,
        isFetching: false,
      });
    }, 1000);
    setTimeout(() => {
      console.log('Setting async error');
      this.setState({
        error: this.props.asyncError,
      });
    }, 2000);
  };
  render() {
    /* eslint-disable no-unused-vars */
    const {
      formData,
      error,
      onSubmit,
      asyncFormData,
      asyncError,
      ...rest
    } = this.props;
    /* eslint-enable no-unused-vars */
    return (
      <React.Fragment>
        <pre>{JSON.stringify(this.state, null, 2)}</pre>
        <Form
          onSubmit={this.onSubmit}
          formData={this.state.formData}
          error={this.state.error}
          isFetching={this.state.isFetching}
          {...rest}
        />
      </React.Fragment>
    );
  }
}

const dynamicSubmitButton = ({ isFetching }: React.ElementProps<*>) => (
  <SubmitButton busy={isFetching} marginRight>
    Save metadata
  </SubmitButton>
);

const dynamicLoginButton = ({ isFetching }: React.ElementProps<*>) => (
  <SubmitButton busy={isFetching} marginRight>
    Login
  </SubmitButton>
);

storiesOf('Form 1', module)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .addDecorator((story) => <ConnectedStorybook story={story()} />)
  .add('Default', () => (
    <Form {...variations.default}>{dynamicSubmitButton}</Form>
  ))
  .add('Default busy', () => (
    <Form {...variations.default} isFetching>
      {dynamicSubmitButton}
    </Form>
  ))
  .add('Default no ui schema', () => (
    <Form {...variations.defaultNoUiSchema}>{dynamicSubmitButton}</Form>
  ))
  .add('Default readonly', () => (
    <Form {...variations.defaultReadonly}>{dynamicSubmitButton}</Form>
  ))
  .add('Default loading', () => (
    <Form {...variations.default} isFetching>
      {dynamicSubmitButton}
    </Form>
  ))
  .add('Login with validation', () => (
    <Form {...variations.login}>{dynamicLoginButton}</Form>
  ))
  .add('Login with initial state', () => (
    <Form {...variations.loginWithInitialErrors}>{dynamicLoginButton}</Form>
  ))
  .add('Login with async error', () => (
    <FormWithAsyncData {...variations.loginWithInitialErrors2}>
      {dynamicLoginButton}
    </FormWithAsyncData>
  ))
  .add('Login with async error 401', () => (
    <FormWithAsyncData {...variations.loginWithInitialErrors3}>
      {dynamicLoginButton}
    </FormWithAsyncData>
  ))
  .add('Array', () => (
    <FormWithAsyncData {...variations.arrayWithErrors}>
      {dynamicLoginButton}
    </FormWithAsyncData>
  ))
  .add('Dynamic UI', () => (
    <Form {...variations.dynamicUi}>{dynamicSubmitButton}</Form>
  ));
