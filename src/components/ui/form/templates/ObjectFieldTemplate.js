/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';

// https://github.com/mozilla-services/react-jsonschema-form#field-template

const ObjectFieldTemplate = (props: any) => {
  const {
    uiSchema,
    title,
    idSchema,
    required,
    formContext,
    description,
    properties,
    TitleField,
    DescriptionField,
  } = props;
  if (uiSchema['ui:widget']) {
    if (uiSchema['ui:widget'] === 'hidden') {
      return null;
    }
  }
  return (
    <div>
      {(uiSchema['ui:title'] || title) && (
        <TitleField
          id={`${idSchema.$id}__title`}
          title={title || uiSchema['ui:title']}
          required={required}
          formContext={formContext}
        />
      )}
      {description && (
        <DescriptionField
          id={`${idSchema.$id}__description`}
          description={description}
          formContext={formContext}
        />
      )}
      {properties.map((prop) => prop.content)}
    </div>
  );
};

ObjectFieldTemplate.propTypes = {
  uiSchema: PropTypes.object,
  title: PropTypes.string,
  description: PropTypes.string,
  idSchema: PropTypes.object,
  required: PropTypes.bool,
  formContext: PropTypes.any,
  properties: PropTypes.array,
  TitleField: PropTypes.any,
  DescriptionField: PropTypes.any,
};

export default ObjectFieldTemplate;
