/* @flow */

import React from 'react';
import { Row, Col } from 'reactstrap';

const ErrorList = (props: any) => {
  const { errors } = props;
  return (
    <div className="pb-3 mb-3 border-bottom" id="errors">
      <Row>
        <Col sm={3}>
          <p className="text-danger">
            <i className="fa fa-warning" /> Errors
          </p>
        </Col>
        <Col>
          <ul>
            {errors.map((error, i) => {
              return (
                <li key={i} className="text-danger">
                  {error.stack}
                </li>
              );
            })}
          </ul>
        </Col>
      </Row>
    </div>
  );
};

export default ErrorList;
