/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { Col, Label, FormGroup } from 'reactstrap';

// https://github.com/mozilla-services/react-jsonschema-form#field-template

const DEFAULT_LAYOUT_ATTRIBUTES = {
  md: 6,
};

const FieldTemplate = (props: any) => {
  const {
    id,
    hidden,
    label,
    help,
    required,
    description,
    rawErrors,
    children,
    schema,
    uiSchema,
    displayLabel,
  } = props;
  if (hidden) {
    return children;
  }
  if (schema.type === 'object') {
    return <div>{children}</div>;
  }
  if (schema.type === 'array') {
    if (schema.items.enum && schema.uniqueItems) {
      // render as below
    } else {
      return <div>{children}</div>;
    }
  }
  const layoutAttributes = uiSchema['ui:layout'] || DEFAULT_LAYOUT_ATTRIBUTES;
  return (
    <FormGroup
      row
      className={rawErrors && rawErrors.length !== 0 ? 'has-error' : null}
    >
      {displayLabel ? (
        <Label for={id} md={3}>
          {label}
          {required ? '*' : null}
        </Label>
      ) : (
        <Col md={3} />
      )}
      <Col {...layoutAttributes}>
        {displayLabel && description ? description : null}
        {children}
        {rawErrors && (
          <div className="invalid-feedback">{rawErrors.join(', ')}</div>
        )}
        {help}
      </Col>
    </FormGroup>
  );
};

FieldTemplate.propTypes = {
  id: PropTypes.string,
  classNames: PropTypes.string,
  hidden: PropTypes.bool,
  required: PropTypes.bool,
  label: PropTypes.string,
  help: PropTypes.node,
  description: PropTypes.node,
  rawErrors: PropTypes.arrayOf(PropTypes.string),
  children: PropTypes.node,
  schema: PropTypes.any,
  uiSchema: PropTypes.any,
  displayLabel: PropTypes.bool,
};

export default FieldTemplate;
