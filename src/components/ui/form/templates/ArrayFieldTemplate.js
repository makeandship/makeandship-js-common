/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { Col, Label, FormGroup, Button } from 'reactstrap';
import { SecondaryButton } from '../../buttons';

// https://github.com/mozilla-services/react-jsonschema-form#object-field-template

const ArrayFieldTemplate = (props: any) => {
  const {
    title,
    items,
    schema,
    uiSchema,
    onAddClick,
    canAdd,
    readonly,
  } = props;
  if (uiSchema['ui:widget']) {
    if (uiSchema['ui:widget'] === 'hidden') {
      return null;
    }
  }
  const hasDescription = !!schema.description;
  return (
    <div className="pb-3">
      <FormGroup row className="mb-0">
        <Label md={3}>{title}</Label>
        <Col className="border-left" md={6}>
          {hasDescription && (
            <div className="form-control-plaintext">{schema.description}</div>
          )}
        </Col>
      </FormGroup>
      {items.map((element, index) => {
        return (
          <FormGroup className="mb-0" row key={index}>
            <Col
              className="border-left border-bottom pt-3"
              md={{ size: 9, offset: 3 }}
            >
              {element.children}
              {element.hasRemove && !readonly && (
                <FormGroup row>
                  <Col>
                    <SecondaryButton
                      icon="minus-circle"
                      onClick={element.onDropIndexClick(index)}
                    >
                      {schema.removeTitle || 'Remove'}
                    </SecondaryButton>
                  </Col>
                </FormGroup>
              )}
            </Col>
          </FormGroup>
        );
      })}
      {canAdd && !readonly && (
        <FormGroup row>
          <Col className="border-left py-3" md={{ size: 9, offset: 3 }}>
            <Button outline onClick={onAddClick}>
              <i className="fa fa-plus-circle" /> {schema.addTitle || 'Add'}
            </Button>
          </Col>
        </FormGroup>
      )}
    </div>
  );
};

ArrayFieldTemplate.propTypes = {
  title: PropTypes.string,
  items: PropTypes.array,
  schema: PropTypes.any,
  onAddClick: PropTypes.func,
  canAdd: PropTypes.any,
  readonly: PropTypes.bool,
};

export default ArrayFieldTemplate;
