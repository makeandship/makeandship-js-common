/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import hoistNonReactStatic from 'hoist-non-react-statics';

import { NEW_ENTITY_KEY } from '../../../../modules/generic';
import type { GenericEntityModule } from '../../../../modules/generic/types';

import DecoratedForm, { FormFooter } from '../DecoratedForm';
import { SubmitButton, CancelButton, DestructiveButton } from '../../buttons';

function decoratedFormWithModule(
  WrappedComponent: React.ElementProps<*> = DecoratedForm,
  module: GenericEntityModule,
  schema: any,
  uiSchema?: any
) {
  class DecoratedFormWithModule extends React.Component<*> {
    componentDidMount = () => {
      const {
        requestEntityOnMount,
        requestEntity,
        isNew,
        entityId,
      } = this.props;
      if (requestEntityOnMount && !isNew) {
        requestEntity(entityId);
      }
    };

    onSubmit = (formData: any) => {
      const { createEntity, updateEntity, isNew, entityId } = this.props;

      if (isNew) {
        createEntity(formData);
      } else {
        // if id is not speicified in schema, it will not be in the formData
        formData.id = entityId;
        updateEntity(formData);
      }
    };

    onCancelClick = (e) => {
      e && e.preventDefault();
      const { history } = this.props;
      history.goBack();
    };

    onDeleteClick = (e) => {
      e && e.preventDefault();
      const { entity, deleteEntity } = this.props;
      if (confirm(`Delete ${module.entityName}?`)) {
        deleteEntity(entity);
      }
    };

    render() {
      const {
        isNew,
        entity,
        isFetching,
        isCreating,
        isDeleting,
        isUpdating,
        error,
        ...rest
      } = this.props;
      const title = isNew
        ? `New ${module.entityName}`
        : `Edit ${module.entityName}`;
      return (
        <WrappedComponent
          formKey={`decoratedFormWithModule/${module.entityName}`}
          title={title}
          schema={schema}
          uiSchema={uiSchema}
          onSubmit={this.onSubmit}
          isFetching={isFetching}
          error={error}
          formData={entity}
          {...rest}
        >
          {({ isFetching }) => (
            <FormFooter>
              <div>
                <SubmitButton
                  busy={isCreating || isUpdating}
                  disabled={isFetching}
                  marginRight
                >
                  {isNew
                    ? `Create ${module.entityName}`
                    : `Save ${module.entityName}`}
                </SubmitButton>
                <CancelButton
                  disabled={isFetching}
                  onClick={this.onCancelClick}
                />
              </div>
              {!isNew && (
                <DestructiveButton
                  busy={isDeleting}
                  disabled={isFetching}
                  onClick={this.onDeleteClick}
                >
                  Delete {module.entityName}
                </DestructiveButton>
              )}
            </FormFooter>
          )}
        </WrappedComponent>
      );
    }

    static defaultProps = {
      requestEntityOnMount: false,
    };
  }

  DecoratedFormWithModule.displayName = `DecoratedFormWithModule(${getDisplayName(
    WrappedComponent
  )})`;

  DecoratedFormWithModule.propTypes = {
    match: PropTypes.object.isRequired,
    entity: PropTypes.object,
    isFetching: PropTypes.bool,
    getIsCreating: PropTypes.bool,
    getIsDeleting: PropTypes.bool,
    getIsUpdating: PropTypes.bool,
    error: PropTypes.object,
    requestEntity: PropTypes.func.isRequired,
    createEntity: PropTypes.func.isRequired,
    updateEntity: PropTypes.func.isRequired,
    deleteEntity: PropTypes.func.isRequired,
    history: PropTypes.any.isRequired,
    isNew: PropTypes.bool.isRequired,
    entityId: PropTypes.string,
    requestEntityOnMount: PropTypes.bool,
  };

  const getEntityId = (props) => props.match.params[`${module.entityName}Id`];
  const isNewEntityId = (entityId) => !entityId || entityId === NEW_ENTITY_KEY;
  const isKeyedModule = !!module.keyExtractor;
  const {
    getEntity,
    getIsFetching,
    getIsCreating,
    getIsDeleting,
    getIsUpdating,
    getError,
  } = module.selectors;
  const {
    requestEntity,
    createEntity,
    updateEntity,
    deleteEntity,
  } = module.actions;

  const makeMapStateToProps = () => {
    // TODO: memoize selectors via 'make' pattern
    // e.g. makeGetVisibleTodos - https://redux.js.org/recipes/computing-derived-data#selectors-todoselectorsjs-1
    const mapStateToProps = (state, ownProps) => {
      const entityId = getEntityId(ownProps);
      const isNew = isNewEntityId(entityId);
      const entity = isKeyedModule
        ? getEntity(state, entityId)
        : getEntity(state);
      const isFetching = isKeyedModule
        ? getIsFetching(state, entityId)
        : getIsFetching(state);
      const isCreating = isKeyedModule
        ? getIsCreating(state, entityId)
        : getIsCreating(state);
      const isDeleting = isKeyedModule
        ? getIsDeleting(state, entityId)
        : getIsDeleting(state);
      const isUpdating = isKeyedModule
        ? getIsUpdating(state, entityId)
        : getIsUpdating(state);
      const error = isKeyedModule ? getError(state, entityId) : getError(state);
      return {
        entity,
        isFetching,
        isCreating,
        isDeleting,
        isUpdating,
        error,
        entityId,
        isNew,
      };
    };
    return mapStateToProps;
  };

  const mapDispatchToProps = {
    requestEntity,
    createEntity,
    updateEntity,
    deleteEntity,
  };

  const withRedux = connect(makeMapStateToProps, mapDispatchToProps);

  const DecoratedFormWithModuleWithRedux = withRedux(
    withRouter(DecoratedFormWithModule)
  );

  hoistNonReactStatic(DecoratedFormWithModuleWithRedux, WrappedComponent);

  return DecoratedFormWithModuleWithRedux;
}

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

export default decoratedFormWithModule;
