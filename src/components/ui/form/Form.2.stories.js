/* @flow */

import * as React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { storiesOf } from '@storybook/react';

import ConnectedStorybook from '../../../utils/ConnectedStorybook';

import Form from './Form';

const status = {
  type: 'string',
  enum: ['Draft', 'Submitted', 'Under Review', 'Reviewed'],
};

const type = {
  type: 'object',
  properties: {
    category: {
      title: 'Category',
      type: 'string',
      enum: [
        'Admission',
        'Medication',
        'Medical Device / Equipment',
        'Point of Care Testing Device',
        'Pressure Ulcer',
      ],
    },
  },
};

const involved = {
  type: 'array',
  title: 'Who was involved',
  description: 'Patients and staff directly involved',
  items: {
    type: 'object',
    title: 'Injury',
    properties: {
      injured: {
        title: 'Injured',
        description: 'Was the person injured in the incident?',
        type: 'string',
        enum: ['Y', 'N'],
        enumNames: ['Yes', 'No'],
      },
    },
    dependencies: {
      injured: {
        oneOf: [
          {
            properties: {
              injured: {
                enum: ['N'],
              },
            },
          },
          {
            properties: {
              injured: {
                enum: ['Y'],
              },
              injury: {
                title: 'Injury',
                type: 'string',
                enum: [
                  'Abrasion',
                  'Allergic Reaction',
                  'Amputation',
                  'Asphyxiation',
                ],
              },
              bodyPart: {
                title: 'Body part',
                type: 'string',
                enum: ['Abdomen', 'Ankle (Left)', 'Ankle (Right)'],
              },
            },
          },
        ],
      },
    },
  },
};

const medicationDetails = {
  type: 'object',
  title: 'Medication Details',
  properties: {
    stage: {
      title: 'Stage of Medication Error',
      type: 'string',
      enum: [
        'Administration/supply of a medicine from a clinical area',
        'ADVICE',
        'Monitoring/follow-up of medicine use',
        'OTHER',
        'Preparation of medicines/dispensing in a pharmacy',
        'Prescribing',
        'Supply or use of over-the-counter (OTC) medicine',
      ],
    },
  },
};

const pressureUlcer = {
  type: 'object',
  title: 'Pressure Ulcer',
  properties: {
    admission: {
      title: 'Admission Date',
      type: 'string',
      format: 'date',
    },
  },
};

const equipment = {
  type: 'object',
  title: 'Equipment',
  properties: {
    type: {
      title: 'Product Type',
      type: 'string',
      enum: [
        'Administration and giving sets',
        'Anaesthetic and breathing masks',
        'Anaesthetic machines and monitors',
      ],
    },
  },
};

const schema = {
  type: 'object',
  properties: {
    status,
    type,
    involved,
  },
  dependencies: {
    type: {
      oneOf: [
        {
          properties: {
            type: {
              properties: {
                category: {
                  enum: [''],
                },
              },
            },
          },
        },
        {
          properties: {
            type: {
              properties: {
                category: {
                  enum: ['Medication'],
                },
              },
            },
            medicationDetails,
          },
        },
        {
          properties: {
            type: {
              properties: {
                category: {
                  enum: ['Pressure Ulcer'],
                },
              },
            },
            pressureUlcer,
          },
        },
        {
          properties: {
            type: {
              properties: {
                category: {
                  enum: [
                    'Medical Device / Equipment',
                    'Point of Care Testing Device',
                  ],
                },
              },
            },
            equipment,
          },
        },
      ],
    },
  },
};

const variations = {
  default: {
    formKey: 'test/test',
    schema,
  },
};

storiesOf('Form 2', module)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .addDecorator((story) => <ConnectedStorybook story={story()} />)
  .add('Default', () => <Form __debug__ {...variations.default} />);
