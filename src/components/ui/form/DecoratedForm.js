/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';

import Form from './Form';
import PageHeader from '../PageHeader';

import styles from './DecoratedForm.module.scss';

const FormFooter = ({
  children,
  ...rest
}: React.ElementProps<*>): React.Element<*> => (
  <div {...rest} className={styles.formFooter}>
    {children}
  </div>
);

const FormFooterButtonGroup = ({
  children,
  ...rest
}: React.ElementProps<*>): React.Element<*> => (
  <div {...rest} className={styles.formFooterButtonGroup}>
    {children}
  </div>
);

export { FormFooter, FormFooterButtonGroup };
export { PageHeader as FormHeader };

class DecoratedForm extends React.Component<*> {
  render() {
    const {
      title,
      children,
      schema: { title: schemaTitle, ...schemaRest },
      ...rest
    } = this.props;
    return (
      <React.Fragment>
        <PageHeader title={schemaTitle || title} />
        <div className={styles.formWrap}>
          <Form schema={schemaRest} {...rest}>
            {children}
          </Form>
        </div>
      </React.Fragment>
    );
  }

  static defaultProps = {
    title: 'Decorated Form',
  };
}

DecoratedForm.propTypes = {
  ...Form.propTypes,
  title: PropTypes.string.isRequired,
  isFetching: PropTypes.bool,
};

export default DecoratedForm;
