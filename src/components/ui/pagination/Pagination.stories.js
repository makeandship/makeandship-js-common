import React from 'react';
import { storiesOf } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';

import Pagination from './Pagination';

storiesOf('Pagination', module)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .add('Page 1 of 100', () => <Pagination current={1} />)
  .add('Page 3 of 100', () => <Pagination current={3} />)
  .add('Page 5 of 100', () => <Pagination current={5} />)
  .add('Page 7 of 100', () => <Pagination current={7} />)
  .add('Page 9 of 100', () => <Pagination current={9} />)
  .add('Page 92 of 100', () => <Pagination current={92} />)
  .add('Page 94 of 100', () => <Pagination current={94} />)
  .add('Page 96 of 100', () => <Pagination current={96} />)
  .add('Page 98 of 100', () => <Pagination current={98} />)
  .add('Page 100 of 100', () => <Pagination current={100} />)
  .add('Page 1 of 1', () => <Pagination current={1} last={1} />)
  .add('Page 1 of 3', () => <Pagination current={1} last={3} />)
  .add('Page 2 of 3', () => <Pagination current={2} last={3} />);
