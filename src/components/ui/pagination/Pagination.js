/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';

import styles from './Pagination.module.scss';

type PageItem = {
  label: React.Element<*> | string,
  page?: number,
};

export const itemsForProps = ({
  current,
  first,
  last,
}: {
  current: number,
  first: number,
  last: number,
}): Array<PageItem> => {
  current = parseInt(current);

  const isFirst = current === first;
  const isLast = current === last;
  const items: Array<PageItem> = [];

  // group of 5 numbers
  let firstNumber = current - 2;
  let lastNumber = current + 2;

  // don't overlap the start
  if (firstNumber < first) {
    const difference = first - firstNumber;
    firstNumber += difference;
    lastNumber += difference;
  }

  // don't overlap the end
  if (lastNumber > last) {
    const difference = last - lastNumber;
    firstNumber += difference;
    lastNumber += difference;
  }

  // truncate
  firstNumber = Math.max(first, firstNumber);
  lastNumber = Math.min(last, lastNumber);

  if (!isFirst) {
    items.push({
      label: (
        <div>
          <i className="fa fa-angle-left" /> Previous
        </div>
      ),
      page: current - 1,
    });
  }

  if (firstNumber > first) {
    items.push({
      label: `${first}`,
      page: first,
    });
  }
  if (firstNumber > first + 1) {
    items.push({
      label: '…',
    });
  }

  for (let number = firstNumber; number <= lastNumber; number++) {
    items.push({
      label: `${number}`,
      page: number,
    });
  }

  if (lastNumber < last - 1) {
    items.push({
      label: '…',
    });
  }
  if (lastNumber < last) {
    items.push({
      label: `${last}`,
      page: last,
    });
  }

  if (!isLast) {
    items.push({
      label: (
        <div>
          Next <i className="fa fa-angle-right" />
        </div>
      ),
      page: current + 1,
    });
  }

  return items;
};

class Pagination extends React.PureComponent<*> {
  render() {
    if (this.props.last === 0) {
      return null;
    }
    const { onPageClick } = this.props;
    const items = itemsForProps(this.props);
    const current = parseInt(this.props.current);
    return (
      <div className={styles.componentWrap}>
        {items.map((item, index) => {
          const { label, page } = item;
          if (!page) {
            return (
              <div className={styles.item} key={index}>
                {label}
              </div>
            );
          }
          const isCurrent = current === page;
          if (isCurrent) {
            return (
              <div className={styles.currentItem} key={index}>
                {label}
              </div>
            );
          }
          return (
            <a
              className={styles.itemLink}
              onClick={(e) => {
                e.preventDefault();
                onPageClick(page);
              }}
              href=""
              key={index}
            >
              <div className={styles.item}>{item.label}</div>
            </a>
          );
        })}
      </div>
    );
  }
  static defaultProps = {
    current: 1,
    first: 1,
    last: 100,
  };
}

Pagination.propTypes = {
  current: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  first: PropTypes.number,
  last: PropTypes.number,
  onPageClick: PropTypes.func,
};

export default Pagination;
