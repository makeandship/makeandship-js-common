/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';

import './ActivityIndicator.scss';

import styles from './ActivityIndicator.module.scss';

class ActivityIndicator extends React.PureComponent<*> {
  render() {
    const { thickness, color } = this.props;
    let style = null;
    if (thickness) {
      style = {
        borderWidth: thickness,
      };
    }
    return (
      <div
        className={`${styles.activityIndicator} activity-indicator__border--${color}`}
        style={style}
        data-tid="component-activity-indicator"
      />
    );
  }
  static defaultProps = {
    color: 'primary',
  };
}

ActivityIndicator.propTypes = {
  thickness: PropTypes.string,
  color: PropTypes.string,
};

export default ActivityIndicator;
