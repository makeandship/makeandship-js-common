/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';

import ActivityIndicator from './ActivityIndicator';

import styles from './Loading.module.scss';

class Loading extends React.PureComponent<*> {
  render() {
    const { overlay, delayed, ...rest } = this.props;
    return (
      <div
        className={
          overlay
            ? delayed
              ? styles.componentWrapOverlayDelayed
              : styles.componentWrapOverlay
            : delayed
            ? styles.componentWrapDelayed
            : styles.componentWrap
        }
        data-tid="component-loading"
      >
        <div className={styles.activityIndicatorContainer}>
          <ActivityIndicator {...rest} />
        </div>
      </div>
    );
  }
  static defaultProps = {
    overlay: false,
    delayed: false,
    thickness: '0.4rem',
  };
}

Loading.propTypes = {
  ...ActivityIndicator.propTypes,
  overlay: PropTypes.bool,
  delayed: PropTypes.bool,
};

export default Loading;
