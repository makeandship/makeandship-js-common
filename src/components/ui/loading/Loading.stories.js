import React from 'react';
import { storiesOf } from '@storybook/react';

import Loading from './Loading';

storiesOf('Loading', module)
  .add('default', () => <Loading />)
  .add('delayed', () => <Loading delayed />)
  .add('overlay', () => <Loading overlay />)
  .add('overlay delayed', () => <Loading overlay delayed />)
  .add('styles', () => (
    <React.Fragment>
      <Loading color="primary" />
      <Loading color="secondary" />
      <Loading color="mid" />
      <Loading color="muted" />
    </React.Fragment>
  ));
