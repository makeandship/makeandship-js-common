import React from 'react';
import { storiesOf } from '@storybook/react';

import ActivityIndicator from './ActivityIndicator';

storiesOf('ActivityIndicator', module).add('default', () => (
  <React.Fragment>
    <div style={{ width: '60px', height: '60px' }}>
      <ActivityIndicator />
    </div>
    <div style={{ width: '60px', height: '60px' }}>
      <ActivityIndicator color={'success'} />
    </div>
    <div style={{ width: '60px', height: '60px' }}>
      <ActivityIndicator color={'info'} />
    </div>
    <div style={{ width: '60px', height: '60px' }}>
      <ActivityIndicator color={'warning'} />
    </div>
    <div style={{ width: '60px', height: '60px' }}>
      <ActivityIndicator color={'danger'} />
    </div>
    <div style={{ width: '60px', height: '60px' }}>
      <ActivityIndicator color={'mid'} />
    </div>
    <div style={{ width: '60px', height: '60px' }}>
      <ActivityIndicator color={'muted'} />
    </div>
    <div style={{ width: '60px', height: '60px' }}>
      <ActivityIndicator color={'white'} />
    </div>
    <div style={{ width: '60px', height: '60px' }}>
      <ActivityIndicator color={'primary'} thickness={'10px'} />
    </div>
    <div style={{ width: '60px', height: '60px' }}>
      <ActivityIndicator color={'secondary'} thickness={'10px'} />
    </div>
  </React.Fragment>
));
