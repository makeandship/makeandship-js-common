/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { Table as ReactBootstrapTable } from 'reactstrap';
import _isEqual from 'lodash.isequal';
import clsx from 'clsx';

import styles from './Table.module.scss';

export { styles as tableStyles };

const Th = ({
  heading,
  sort,
  order,
  onChangeOrder,
}: React.ElementProps<*>): React.Element<*> => {
  const { title } = heading;
  let cellStyle = styles.tableHeaderCell;
  let innerStyle = null;
  let onClick = null;
  if (heading.sort) {
    innerStyle = styles.tableHeaderCellInner;
    const isSelected = _isEqual(sort, heading.sort);
    onClick = () => {
      if (isSelected) {
        onChangeOrder({
          sort,
          order:
            order === Table.Order.Descending
              ? Table.Order.Ascending
              : Table.Order.Descending,
        });
      } else {
        onChangeOrder({
          sort: heading.sort,
          order: Table.Order.Descending,
        });
      }
    };
    cellStyle = onClick
      ? styles.tableHeaderCellClickable
      : styles.tableHeaderCell;
    if (isSelected) {
      cellStyle = styles.tableHeaderCellSelected;
      innerStyle =
        order === Table.Order.Descending
          ? styles.tableHeaderCellInnerSelectedDescending
          : styles.tableHeaderCellInnerSelectedAscending;
    }
  }
  return (
    <th onClick={onClick} className={cellStyle}>
      <div className={innerStyle}>{title}</div>
    </th>
  );
};

const renderBodyDefault = ({
  data = [],
  renderRow,
}: React.ElementProps<*>): React.Element<*> => {
  return <tbody className={styles.tableBody}>{data.map(renderRow)}</tbody>;
};

const Table = ({
  headings,
  data,
  renderRow,
  renderBody = renderBodyDefault,
  sort,
  order,
  onChangeOrder,
  isFetching,
  sortIcons = false,
  responsive = true,
  stickyHeadings = false,
}: React.ElementProps<*>): React.Element<*> => {
  const headClassName = clsx(
    sortIcons ? styles.tableHeaderIconAlways : styles.tableHeader,
    stickyHeadings && styles.tableHeaderSticky
  );
  return (
    <ReactBootstrapTable
      className={isFetching ? styles.tableIsFetching : styles.table}
      responsive={responsive && !stickyHeadings}
      hover
    >
      <thead className={headClassName}>
        <tr>
          {headings.map((heading, index) => (
            <Th
              key={`${index}`}
              heading={heading}
              sort={sort}
              order={order}
              onChangeOrder={onChangeOrder}
            />
          ))}
        </tr>
      </thead>
      {renderBody({
        headings,
        data,
        renderRow,
        sort,
        order,
        onChangeOrder,
        isFetching,
      })}
    </ReactBootstrapTable>
  );
};

Table.Order = {
  Ascending: 'asc',
  Descending: 'desc',
};

Table.propTypes = {
  data: PropTypes.array.isRequired,
  headings: PropTypes.array.isRequired,
  renderRow: PropTypes.func,
  renderBody: PropTypes.func,
  sort: PropTypes.string,
  sortIcons: PropTypes.bool,
  order: PropTypes.string,
  onChangeOrder: PropTypes.func,
  isFetching: PropTypes.bool,
};

export default Table;
