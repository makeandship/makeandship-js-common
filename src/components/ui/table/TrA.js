/* @flow */

import * as React from 'react';
import clsx from 'clsx';

// ignore clicks on child interactive elements
export const TAG_BLACKLIST = ['a', 'button', 'input'];

import styles from './TrA.module.scss';

const TrA = ({
  href,
  tag: Tag = 'tr',
  className,
  ...rest
}: React.ElementProps<*>): React.Element<*> => {
  const onClick = React.useCallback(
    (e) => {
      e.stopPropagation();
      const tag = e.target.nodeName.toLowerCase();
      if (TAG_BLACKLIST.includes(tag)) {
        return;
      }
      if (e.button === 0 && !e.ctrlKey) {
        // left click, no control key
        if (e.metaKey || e.shiftKey) {
          // with modifider
          window.open(href);
        } else {
          window.location = href;
        }
      }
    },
    [href]
  );
  return (
    <Tag
      className={clsx(styles.container, className)}
      onClick={onClick}
      {...rest}
    />
  );
};

export default TrA;
