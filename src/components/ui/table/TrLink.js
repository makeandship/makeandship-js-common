/* @flow */

import * as React from 'react';
import { withRouter } from 'react-router-dom';
import clsx from 'clsx';

import { TAG_BLACKLIST } from './TrA';

import styles from './TrA.module.scss';

const TrLink = ({
  to,
  tag: Tag = 'tr',
  className,
  history,
  staticContext, // eslint-disable-line no-unused-vars
  ...rest
}: React.ElementProps<*>): React.Element<*> => {
  const onClick = React.useCallback(
    (e) => {
      e.stopPropagation();
      const tag = e.target.nodeName.toLowerCase();
      if (TAG_BLACKLIST.includes(tag)) {
        return;
      }
      if (e.button === 0 && !e.ctrlKey) {
        // left click, no control key
        if (e.metaKey || e.shiftKey) {
          // with modifider
          const url = `${window.location.origin}${to}`;
          window.open(url);
        } else {
          history.push(to);
        }
      }
    },
    [history, to]
  );
  return (
    <Tag
      className={clsx(styles.container, className)}
      onClick={onClick}
      {...rest}
    />
  );
};

export default withRouter(TrLink);
