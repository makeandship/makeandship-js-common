/* @flow */

import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';

import Table from './Table';
import TrLink from './TrLink';
import TrA from './TrA';

const data = [
  {
    id: '1',
    email: 'example1@example.com',
    firstname: 'Lorem',
    lastname: 'Ipsum',
  },
  {
    id: '2',
    email: 'example2@example.com',
    firstname: 'Lorem',
    lastname: 'Ipsum',
  },
  {
    id: '3',
    email: 'example3@example.com',
    firstname: 'Lorem',
    lastname: 'Ipsum',
  },
  {
    id: '4',
    email: 'example4@example.com',
    firstname: 'Lorem',
    lastname: 'Ipsum dolor sit amet, consectetur adipiscing elit',
  },
  {
    id: '5',
    email: 'example5@example.com',
    firstname: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    lastname: 'Ipsum',
  },
];

const headings = [
  {
    title: 'Email',
    sort: 'email',
  },
  {
    title: 'Last name ipsum dolor sit amet, consectetur adipiscing elit',
    sort: 'lastname',
  },
  {
    title: 'First name',
    sort: 'firstname',
  },
  {
    title: 'No sort',
  },
];

const renderRow = ({
  id,
  firstname,
  lastname,
  email,
}: React.ElementProps<*>): React.Element<*> => {
  return (
    <TrLink key={id} to={`/users/${id}`}>
      <td>{email}</td>
      <td>{lastname}</td>
      <td>{firstname}</td>
      <td>–</td>
    </TrLink>
  );
};

const renderBody = ({ data }) => {
  return data.map(({ id, firstname, lastname, email }) => (
    <TrA tag={'tbody'} key={id} href={`/users/${id}`}>
      <tr>
        <td rowSpan={2}>Repeating `tbody` with `td` spanning 2 rows {email}</td>
        <td>1 {lastname}</td>
        <td>1 {firstname}</td>
        <td>–</td>
      </tr>
      <tr>
        <td>2 {lastname}</td>
        <td>2 {firstname}</td>
        <td>–</td>
      </tr>
    </TrA>
  ));
};

const onChangeOrder = (order) => console.log('onChangeOrder', order);

const variations = {
  empty: {
    headings,
    renderRow,
    data: [],
  },
  default: {
    headings,
    renderRow,
    data,
  },
  renderBody: {
    headings,
    renderBody,
    data,
  },
  busy: {
    headings,
    renderRow,
    data,
    isFetching: true,
  },
  sortable: {
    headings,
    renderRow,
    data,
    sort: 'lastname',
    order: Table.Order.Ascending,
    onChangeOrder,
  },
};

storiesOf('Table', module)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .add('Empty', () => <Table {...variations.empty} />)
  .add('Default', () => <Table {...variations.default} />)
  .add('Render body', () => <Table {...variations.renderBody} />)
  .add('Busy', () => <Table {...variations.busy} />)
  .add('Sortable', () => <Table {...variations.sortable} />)
  .add('Sortable icons', () => <Table {...variations.sortable} sortIcons />)
  .add('Sortable sticky', () => (
    <Table {...variations.sortable} stickyHeadings />
  ))
  .add('Sortable icons sticky', () => (
    <Table {...variations.sortable} sortIcons stickyHeadings />
  ));
