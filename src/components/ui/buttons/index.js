/* @flow */

import * as React from 'react';

import IconButton from './IconButton';

const SubmitButton = (props: React.ElementProps<*>): React.Element<*> => (
  <IconButton type="submit" {...props} />
);

const PrimaryButton = (props: React.ElementProps<*>): React.Element<*> => (
  <IconButton tag="a" href="" color="secondary" {...props} />
);

const SecondaryButton = (props: React.ElementProps<*>): React.Element<*> => (
  <IconButton tag="a" href="" outline color="mid" {...props} />
);

const CancelButton = (props: React.ElementProps<*>): React.Element<*> => (
  <IconButton
    icon="times-circle"
    tag="a"
    href=""
    outline
    color="mid"
    {...props}
  >
    Cancel
  </IconButton>
);

const DestructiveButton = (props: React.ElementProps<*>): React.Element<*> => (
  <IconButton icon="trash" tag="a" href="" outline color="danger" {...props} />
);

export {
  IconButton,
  SubmitButton,
  PrimaryButton,
  SecondaryButton,
  CancelButton,
  DestructiveButton,
};
