import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';

import {
  IconButton,
  SubmitButton,
  PrimaryButton,
  SecondaryButton,
  CancelButton,
  DestructiveButton,
} from './';

import LinkButton from './LinkButton';

function Examples() {
  const [busy, setBusy] = useState(true);
  const toggleBusy = () => {
    setBusy(!busy);
  };
  return (
    <React.Fragment>
      <IconButton onClick={toggleBusy} busy={busy} disabled={false}>
        Toggle busy
      </IconButton>
      <br />
      <IconButton busy={busy}>IconButton</IconButton>
      <br />
      <IconButton busy={busy} disabled={false}>
        IconButton force enabled
      </IconButton>
      <br />
      <IconButton busy={busy} size={'sm'}>
        IconButton sm
      </IconButton>
      <IconButton busy={busy} size={'md'}>
        IconButton md
      </IconButton>
      <IconButton busy={busy} size={'lg'}>
        IconButton lg
      </IconButton>
      <br />
      <SubmitButton busy={busy}>SubmitButton</SubmitButton>
      <br />
      <PrimaryButton busy={busy}>PrimaryButton</PrimaryButton>
      <br />
      <SecondaryButton busy={busy}>SecondaryButton</SecondaryButton>
      <br />
      <CancelButton busy={busy}>CancelButton</CancelButton>
      <br />
      <DestructiveButton busy={busy}>DestructiveButton</DestructiveButton>
      <br />
      <LinkButton to="#">LinkButton</LinkButton>
    </React.Fragment>
  );
}

storiesOf('Buttons', module)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .add('Examples', () => <Examples />);
