/* @flow */

import * as React from 'react';
import { Button } from 'reactstrap';
import { Transition } from 'react-transition-group';

import ActivityIndicator from '../loading/ActivityIndicator';

import styles from './Buttons.module.scss';

const iconButtonClassName = ({ marginLeft, marginRight }) => {
  if (!marginLeft && !marginRight) {
    return styles.button;
  }
  if (marginLeft && marginRight) {
    return `${styles.button} mx-sm-2`;
  }
  return marginLeft ? `${styles.button} ml-sm-2` : `${styles.button} mr-sm-2`;
};

const IconButton = ({
  icon,
  busy,
  children,
  marginLeft,
  marginRight,
  color,
  outline,
  ...rest
}: React.ElementProps<*>): React.Element<*> => {
  return (
    <Button
      className={iconButtonClassName({ marginLeft, marginRight })}
      disabled={busy}
      color={color}
      outline={outline}
      {...rest}
    >
      <div className={styles.contentContainer}>
        {icon ? (
          <span>
            <i className={`fa fa-${icon}`} /> {children}
          </span>
        ) : (
          children
        )}
        <Transition in={busy} timeout={150}>
          {(state) => {
            const className =
              styles[`activityIndicatorAnimatedContainer--${state}`];
            return (
              <div className={className}>
                <div className={styles.activityIndicatorContainer}>
                  <ActivityIndicator color={outline ? color : 'white'} />
                </div>
              </div>
            );
          }}
        </Transition>
      </div>
    </Button>
  );
};

IconButton.defaultProps = {
  icon: 'chevron-circle-right',
  busy: false,
  marginLeft: false,
  marginRight: false,
  children: 'Submit',
};

export default IconButton;
