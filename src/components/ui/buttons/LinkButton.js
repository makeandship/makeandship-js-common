/* @flow */

import * as React from 'react';
import { Link } from 'react-router-dom';

import IconButton from './IconButton';

const LinkButton = (props: React.ElementProps<*>): React.Element<*> => (
  <IconButton tag={Link} color="link" {...props} />
);

export default LinkButton;
