/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'reactstrap';

import styles from './PageHeader.module.scss';

export { styles };

class PageHeader extends React.Component<*> {
  render() {
    const { title, border, children, ...rest } = this.props;
    return (
      <Row {...rest}>
        <Col>
          <div
            className={
              border ? styles.componentWrapBorder : styles.componentWrap
            }
          >
            {title && <div className={styles.title}>{title}</div>}
            {children}
          </div>
        </Col>
      </Row>
    );
  }
  static defaultProps = {
    border: true,
  };
}

PageHeader.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node,
  border: PropTypes.bool,
};

export default PageHeader;
