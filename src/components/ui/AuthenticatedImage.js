/* @flow */

import * as React from 'react';

import { getConfig } from '../../modules/api/config';

const AuthenticatedImage = ({
  url,
  background = true,
  style,
  ...rest
}: React.ElementProps<*>): React.Element<*> => {
  const [objectUrl, setObjectUrl] = React.useState('');

  React.useEffect(() => {
    let fetcherPromise, blobObjectUrl;
    const fetchUrl = async () => {
      try {
        const fetcher = getConfig().fetcher;
        fetcherPromise && fetcherPromise.cancel();
        fetcherPromise = fetcher(url, { responseType: 'blob' });
        const blob = await fetcherPromise;
        blobObjectUrl && URL.revokeObjectURL(blobObjectUrl);
        blobObjectUrl = URL.createObjectURL(blob);
        setObjectUrl(blobObjectUrl);
      } catch (error) {
        // ignore
      }
      return () => {
        blobObjectUrl && URL.revokeObjectURL(blobObjectUrl);
        fetcherPromise && fetcherPromise.cancel();
      };
    };
    fetchUrl();
  }, [url]);
  return background ? (
    <div
      style={{
        backgroundImage: `url(${objectUrl})`,
        ...style,
      }}
      {...rest}
    />
  ) : (
    <img
      src={objectUrl}
      style={{
        maxWidth: '100%',
        ...style,
      }}
      {...rest}
    />
  );
};

export default AuthenticatedImage;
