/* @flow */

import memoizeOne from 'memoize-one';

import {
  isBoolean,
  isString,
  isTrue,
  isUndefined,
  isArray,
} from '../../../utils/is';

import type { Choice } from '../types';

export const titleForChoice = (choice: Choice, delimeter: string = ' › ') => {
  return choice.titles
    ? choice.titles.join(delimeter)
    : choice.title || undefined;
};

// build an inverse tree of titles

export const choiceTitleTree = memoizeOne((choices: any) => {
  let titleTree = {};
  const choicesKeys = Object.keys(choices);
  choicesKeys.forEach((key) => {
    const choice = choices[key];
    let node = titleTree;
    if (choice.titles) {
      const reverseTitles = [...choice.titles].reverse();
      reverseTitles.forEach((title) => {
        if (!node[title]) {
          node[title] = {};
        }
        node = node[title];
      });
    }
  });
  return titleTree;
});

export const shortestTitleForChoiceWithKeyInChoices = (
  choiceKey: string,
  choices: { [string]: Choice },
  delimeter: string = ' › '
) => {
  const titleTree = choiceTitleTree(choices);
  // traverse and stop when we are the only entry
  const choice = choices[choiceKey];
  if (!choice || !choice.titles) {
    return choiceKey;
  }
  const reverseTitles = [...choice.titles].reverse();
  let titles = [];
  let node = titleTree;
  let i = 0;
  while (i < reverseTitles.length) {
    const title = reverseTitles[i];
    titles.push(title);
    node = node[title];
    if (!node) {
      break;
    }
    if (Object.keys(node).length === 1) {
      break;
    }
    i++;
  }
  titles.reverse();
  return titles.join(delimeter);
};

// ui helpers

export const choiceForValueInChoices = ({ choices, value }: any): any => {
  if (!isArray(choices)) {
    return;
  }
  return choices.find((choice) => {
    if (choice.key === value) {
      return true;
    }
    if (isBoolean(choice.key)) {
      if (isTrue(choice.key)) {
        if (value.toLowerCase() === 'true') {
          return true;
        }
      } else {
        if (value.toLowerCase() === 'false') {
          return true;
        }
      }
    }
  });
};

// convert boolean values to string
export const valueForChoice = (choice: any): string =>
  isBoolean(choice.key) ? `${choice.key}` : choice.key;

export const descriptionForChoice = (choice: any): string =>
  isString(choice.description) ? choice.description : choice.key;

export const descriptionWithCountForChoice = (choice: any): string => {
  const description = descriptionForChoice(choice);
  return isUndefined(choice.count)
    ? description
    : `${description} (${choice.count})`;
};
