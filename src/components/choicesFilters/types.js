/* @flow */

export type Choice = {
  title: string,
  titles: Array<string>,
  min?: string,
  max?: string,
  choices?: Array<*>,
};
