/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import produce from 'immer';
import _orderBy from 'lodash.orderby';
import _get from 'lodash.get';

import Select from '../ui/form/fields/Select';

import { isNumeric } from '../../utils/is';
import ChoiceFilterSelect from './ChoiceFilterSelect';
import ChoiceFilterDateRange from './ChoiceFilterDateRange';
import ChoiceFilterNumericRange from './ChoiceFilterNumericRange';
import { titleForChoice } from './utils';

import styles from './ChoicesFilters.module.scss';

import type { Choice } from './types';

type State = {
  placeholderChoiceKeys: Array<string>,
};

const MIN_CHOICES_TO_SHOW = 1;

/*

TODO: don't allow user to add more than one placeholder? since the choices might change once one value is selected

*/

class ChoicesFilters extends React.Component<*, State> {
  state = {
    placeholderChoiceKeys: [],
  };

  onChangeFilterValue = (key: string, value: any) => {
    const { setFilters, filters, setFiltersBlacklist } = this.props;
    this.removePlaceholderChoiceKey(key, () => {
      const updatedFilters = produce(filters, (draft) => {
        draft[key] = value;
        setFiltersBlacklist.forEach((blacklistedKey) => {
          if (draft[blacklistedKey]) {
            delete draft[blacklistedKey];
          }
        });
      });
      setFilters(updatedFilters);
    });
  };

  onAddPlaceholderChoiceKey = (key: string) => {
    this.setState({
      placeholderChoiceKeys: this.state.placeholderChoiceKeys.concat(key),
    });
  };

  onClearFilters = (e: any) => {
    const { clearFilters } = this.props;
    e && e.preventDefault();
    clearFilters();
    this.setState({
      placeholderChoiceKeys: [],
    });
  };

  removePlaceholderChoiceKey = (key: string, callback?: Function) => {
    this.setState(
      {
        placeholderChoiceKeys: this.state.placeholderChoiceKeys.filter(
          (placeholderChoiceKey) => placeholderChoiceKey !== key
        ),
      },
      callback
    );
  };

  removeChoiceKey = (key: string) => {
    this.onChangeFilterValue(key, undefined);
  };

  // choices for 'add filter' menu
  // remove ones that
  // - are placeholders in current state
  // - in the current filters
  // - have no values

  addFilterChoicesKeys = (): Array<string> => {
    const { choices, choicesFilters, minChoicesToShow } = this.props;
    const { placeholderChoiceKeys } = this.state;
    const choicesFiltersKeys = Object.keys(choicesFilters);
    const choicesKeys: Array<string> = Object.keys(choices);
    return choicesKeys.filter((choiceKey) => {
      const choice: Choice = choices[choiceKey];
      if (
        placeholderChoiceKeys.includes(choiceKey) ||
        choicesFiltersKeys.includes(choiceKey)
      ) {
        return false;
      }
      if (choice.choices) {
        if (choice.choices.length < minChoicesToShow) {
          return false;
        }
        return true;
      }
      if (!choice.min || !choice.max) {
        return false;
      }
      return true;
    });
  };

  renderChoice = (choiceKey: string, placeholder: boolean) => {
    const {
      choices,
      choicesFilters,
      choicesAdditionalProps,
      count,
    } = this.props;
    const choice: Choice = choices[choiceKey];
    if (!choice) {
      return null;
    }
    let component;
    const props = {
      choices,
      choicesFilters,
      choiceKey,
      placeholder,
      count,
      onChange: this.onChangeFilterValue,
      ..._get(choicesAdditionalProps, choiceKey),
    };
    console.log('props', props);
    if (choice.choices) {
      component = <ChoiceFilterSelect {...props} />;
    }
    if (choice.min && choice.max) {
      if (isNumeric(choice.min) && isNumeric(choice.max)) {
        component = <ChoiceFilterNumericRange {...props} />;
      } else {
        component = <ChoiceFilterDateRange {...props} />;
      }
    }
    if (!component) {
      return null;
    }
    return (
      <div key={choiceKey} className={styles.element}>
        {component}

        <a
          href="#"
          className={styles.remove}
          onClick={(e) => {
            e.preventDefault();
            if (placeholder) {
              this.removePlaceholderChoiceKey(choiceKey);
            } else {
              this.removeChoiceKey(choiceKey);
            }
          }}
        >
          <i className="fa fa-times-circle" />
        </a>
      </div>
    );
  };

  render() {
    const { choices, choicesFilters, hasFilters, sort } = this.props;
    const hasChoices = choices && Object.keys(choices).length > 0;

    const disabledAddFiltersButton = (
      <div className={styles.element}>
        <div className={styles.select}>
          <div className={styles.widthSizer}>{'Add filters'}</div>
          <Select
            disabled
            value={null}
            placeholder={'Add filters'}
            options={[]}
            className={styles.addFiltersControl}
            classNamePrefix={'react-select'}
          />
        </div>
      </div>
    );
    if (!hasChoices) {
      return (
        <div className={styles.componentWrap}>{disabledAddFiltersButton}</div>
      );
    }

    const { placeholderChoiceKeys } = this.state;
    const addFilterChoicesKeys = this.addFilterChoicesKeys();
    const choicesFiltersKeys = Object.keys(choicesFilters);
    const hasAddFilterChoices = addFilterChoicesKeys.length > 0;
    // FIXME: 'key' added as a way to force select to clear its value - https://github.com/JedWatson/react-select/issues/3066
    const key = placeholderChoiceKeys.concat(choicesFiltersKeys).join('_');
    const unsortedOptions = addFilterChoicesKeys.map((choiceKey) => {
      const choice: Choice = choices[choiceKey];
      const displayTitle = titleForChoice(choice) || choiceKey;
      // const displayTitle = shortestTitleForChoiceWithKeyInChoices(
      //   choiceKey,
      //   choices
      // );
      return {
        value: choiceKey,
        label: displayTitle,
      };
    });
    const options = sort
      ? _orderBy(unsortedOptions, ['label'])
      : unsortedOptions;
    return (
      <div className={styles.componentWrap}>
        {choicesFiltersKeys.map((choicesFilterKey) =>
          this.renderChoice(choicesFilterKey, false)
        )}
        {placeholderChoiceKeys.map((placeholderChoiceKey) =>
          this.renderChoice(placeholderChoiceKey, true)
        )}
        {hasAddFilterChoices ? (
          <div className={styles.element}>
            <div className={styles.select}>
              <div className={styles.widthSizer}>{'Add filters'}</div>
              <Select
                key={key}
                value={null}
                placeholder={'Add filters'}
                options={options}
                onChange={this.onAddPlaceholderChoiceKey}
                blurInputOnSelect={true}
                wideMenu
                className={styles.addFiltersControl}
                classNamePrefix={'react-select'}
              />
            </div>
          </div>
        ) : (
          disabledAddFiltersButton
        )}
        {hasFilters && (
          <div className={styles.element}>
            <a href="#" onClick={this.onClearFilters} className={styles.clear}>
              Clear all <i className="fa fa-times-circle" />
            </a>
          </div>
        )}
      </div>
    );
  }

  static defaultProps = {
    setFiltersBlacklist: ['page'],
    sort: true,
    choicesAdditionalProps: {},
    count: true,
    minChoicesToShow: MIN_CHOICES_TO_SHOW,
  };
}

ChoicesFilters.propTypes = {
  isFetching: PropTypes.bool,
  setFilters: PropTypes.func.isRequired,
  clearFilters: PropTypes.func.isRequired,
  choicesFilters: PropTypes.object.isRequired,
  filters: PropTypes.object.isRequired,
  choices: PropTypes.object,
  hasFilters: PropTypes.bool,
  setFiltersBlacklist: PropTypes.array,
  sort: PropTypes.bool,
  choicesAdditionalProps: PropTypes.any,
  count: PropTypes.bool,
};

export default ChoicesFilters;
