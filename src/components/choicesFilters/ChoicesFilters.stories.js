/* @flow */

import * as React from 'react';
import { storiesOf } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';
import _ from 'lodash';

import ChoicesFilters from './ChoicesFilters';
import ChoiceFilterDateRange from './ChoiceFilterDateRange';

const setFilters = (filters) => {
  console.log('setFilters called:', JSON.stringify(filters, null, 2));
};
const clearFilters = () => {
  console.log('resetFilters called');
};

const choices = require('./__fixtures__/choices');
const choices2 = require('./__fixtures__/choices2');

const variations = {
  empty: {
    setFilters,
    clearFilters,
    filters: {},
    choicesFilters: {},
    choices: {},
  },
  default: {
    setFilters,
    clearFilters,
    filters: {},
    choicesFilters: {},
    choices,
  },
  choices2: {
    setFilters,
    clearFilters,
    filters: {},
    choicesFilters: {},
    choices: choices2,
  },
  noCount: {
    setFilters,
    clearFilters,
    filters: {},
    choicesFilters: {},
    choices,
    count: false,
  },
  filters1: {
    setFilters,
    clearFilters,
    filters: {
      'metadata.phenotyping.gatifloxacin.method': 'Microtitre plate',
      'metadata.patient.age': {
        min: 21,
        max: 72,
      },
      'metadata.treatment.continuation.start': {
        min: '2017-10-17T17:59:11+01:00',
        max: '2018-07-29T14:34:01+01:00',
      },
    },
    choicesFilters: {
      'metadata.phenotyping.gatifloxacin.method': 'Microtitre plate',
      'metadata.patient.age': {
        min: 21,
        max: 72,
      },
      'metadata.treatment.continuation.start': {
        min: '2017-10-17T17:59:11+01:00',
        max: '2018-07-29T14:34:01+01:00',
      },
    },
    hasFilters: true,
    choices,
  },
  preserveFilters: {
    filters: { page: 2, leaveUntouched: true },
    setFiltersBlacklist: ['page'],
    choices,
    choicesAdditionalProps: {
      impactTier: {
        multiple: true,
        sort: false,
      },
      'results.distance-nearest-neighbour.analysed': {
        type: ChoiceFilterDateRange.Type.SingleMonth,
      },
    },
  },
};

type State = {
  filters: any,
};

class ChoicesFiltersPreservingFilters extends React.Component<*, State> {
  state = {
    filters: {},
  };

  constructor(props: any) {
    super(props);
    if (this.props.filters) {
      this.state = { filters: this.props.filters };
    }
  }

  clearFilters = () => {
    this.setState({
      filters: {},
    });
  };

  setFilters = (filters) => {
    const cleanFilters = _.omitBy(filters, _.isNil);
    this.setState({
      filters: { ...cleanFilters },
    });
  };

  render() {
    const { filters: discardedFilters, ...rest } = this.props; // eslint-disable-line
    const { filters } = this.state;
    const hasFilters = Object.keys(filters).length > 0;
    return (
      <React.Fragment>
        <ChoicesFilters
          filters={filters}
          choicesFilters={filters}
          clearFilters={this.clearFilters}
          setFilters={this.setFilters}
          hasFilters={hasFilters}
          {...rest}
        />
        <pre>filters: {JSON.stringify(filters, null, 2)}</pre>
      </React.Fragment>
    );
  }
}

storiesOf('ChoicesFilters', module)
  .addDecorator((story) => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .add('Default', () => <ChoicesFilters {...variations.default} />)
  .add('Choices 2', () => <ChoicesFilters {...variations.choices2} />)
  .add('No count', () => <ChoicesFilters {...variations.noCount} />)
  .add('Filters 1', () => <ChoicesFilters {...variations.filters1} />)
  .add('Preserve Filters', () => (
    <ChoicesFiltersPreservingFilters {...variations.preserveFilters} />
  ))
  .add('Empty', () => <ChoicesFilters {...variations.empty} />);
