/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import is from 'is_js';

import Select from '../ui/form/fields/Select';

import { shortestTitleForChoiceWithKeyInChoices } from './utils';

import styles from './ChoiceFilterNumericRange.module.scss';

import type { Choice } from './types';

type State = {
  min?: string,
  max?: string,
};

class ChoiceFilterNumericRange extends React.Component<*, State> {
  state = {};

  constructor(props: any) {
    super(props);
    this.state = this.minMaxFromProps(props);
  }

  minMaxFromProps = (props: any): State => {
    const { choices, choicesFilters, choiceKey } = props;
    const choice: Choice = choices[choiceKey];
    const choiceFilter: Choice = choicesFilters[choiceKey];
    // defaults from choice data
    let { max, min } = choice;
    // override if they are set in the filters
    if (choiceFilter) {
      if (choiceFilter.min) {
        min = choiceFilter.min;
      }
      if (choiceFilter.max) {
        max = choiceFilter.max;
      }
    }
    return {
      min,
      max,
    };
  };

  componentDidUpdate = (prevProps: any) => {
    const minMax = this.minMaxFromProps(this.props);
    const prevMinMax = this.minMaxFromProps(prevProps);
    if (minMax.min !== prevMinMax.min || minMax.max !== prevMinMax.max) {
      this.setState(minMax);
    }
  };

  onMinChange = (min: string) => {
    const { choiceKey, onChange } = this.props;
    // allow user to select any min, make sure that the current max doesn't overlap
    const max = Math.max(parseInt(min), parseInt(this.state.max));
    this.setState({
      min,
      max: `${max}`,
    });
    onChange(choiceKey, {
      min,
      max: `${max}`,
    });
  };

  onMaxChange = (max: string) => {
    const { choiceKey, onChange } = this.props;
    this.setState({
      max,
    });
    onChange(choiceKey, {
      min: this.state.min,
      max,
    });
  };

  customInput = (value?: string) => (
    <div>
      <div className={styles.customInput}>
        <a href="#" onClick={(e) => e.preventDefault()}>
          {value} <i className={'fa fa-caret-down'} />
        </a>
      </div>
    </div>
  );

  render() {
    const { choices, choiceKey, placeholder } = this.props;
    const { max, min } = this.state;
    const choice: Choice = choices[choiceKey];
    const displayTitle = shortestTitleForChoiceWithKeyInChoices(
      choiceKey,
      choices
    );
    let minOptions = [];
    for (let i: number = parseInt(choice.min); i <= parseInt(choice.max); i++) {
      minOptions.push({
        value: `${i}`,
        label: `${i}`,
      });
    }
    let maxOptions = [];
    for (let i: number = parseInt(min); i <= parseInt(choice.max); i++) {
      maxOptions.push({
        value: `${i}`,
        label: `${i}`,
      });
    }
    const initiallyOpen = placeholder && !is.ie(); // initiallyOpen throws on IE
    return (
      <div className={styles.componentWrap}>
        {displayTitle}
        {'\u00A0·\u00A0'}
        <div className={styles.select}>
          <div className={styles.widthSizer}>{min}</div>

          <Select
            value={min}
            placeholder={'0'}
            components={{
              SingleValue: () => (
                <span className="react-select__single-value">{min}</span>
              ),
            }}
            options={minOptions}
            onChange={this.onMinChange}
            autosize
            initiallyOpen={initiallyOpen}
            searchable
            wideMenu
            className={styles.control}
            classNamePrefix={'react-select'}
          />
        </div>
        {'\u00A0–\u00A0'}
        <div className={styles.select}>
          <div className={styles.widthSizer}>{max}</div>

          <Select
            value={max}
            placeholder={'0'}
            components={{
              SingleValue: () => (
                <span className="react-select__single-value">{max}</span>
              ),
            }}
            options={maxOptions}
            onChange={this.onMaxChange}
            autosize
            searchable
            wideMenu
            className={styles.control}
            classNamePrefix={'react-select'}
          />
        </div>
      </div>
    );
  }
}

ChoiceFilterNumericRange.propTypes = {
  choicesFilters: PropTypes.object.isRequired,
  choices: PropTypes.object.isRequired,
  choiceKey: PropTypes.string.isRequired,
  placeholder: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default ChoiceFilterNumericRange;
