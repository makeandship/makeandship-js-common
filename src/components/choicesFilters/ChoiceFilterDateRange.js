/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import * as dateFns from 'date-fns';
import _get from 'lodash.get';

import DatePicker from '../ui/form/fields/DatePicker';

import { shortestTitleForChoiceWithKeyInChoices } from './utils';
import { formatDate } from '../../utils/date';

import styles from './ChoiceFilterDateRange.module.scss';

import type { Choice } from './types';

class ChoiceFilterDateRange extends React.Component<*> {
  static Type = {
    Default: 'Default',
    SingleMonth: 'SingleMonth',
  };

  onMinChange = (min: string) => {
    const { choiceKey, onChange, choicesFilters } = this.props;
    const choiceFilter: Choice = choicesFilters[choiceKey];

    let selectedMax = _get(choiceFilter, 'max');

    if (selectedMax) {
      // adjust max if the selected min is greater
      selectedMax = dateFns.formatISO(
        dateFns.max([dateFns.parseISO(min), dateFns.parseISO(selectedMax)])
      );
      onChange(choiceKey, {
        min,
        max: selectedMax,
      });
    } else {
      // allow max to remain undefined
      onChange(choiceKey, {
        min,
      });
    }
  };

  onMaxChange = (max: string) => {
    const { choiceKey, onChange, choicesFilters } = this.props;
    const choiceFilter: Choice = choicesFilters[choiceKey];

    let selectedMin = _get(choiceFilter, 'min');

    if (selectedMin) {
      onChange(choiceKey, {
        min: selectedMin,
        max,
      });
    } else {
      // allow min to remain undefined
      onChange(choiceKey, {
        max,
      });
    }
  };

  onSingleMonthChange = (value: string) => {
    const { choiceKey, onChange } = this.props;
    const valueAsDate = dateFns.parseISO(value);
    const startOfMonth = dateFns.startOfMonth(valueAsDate);
    const endOfMonth = dateFns.endOfMonth(valueAsDate);
    const min = dateFns.formatISO(startOfMonth);
    const max = dateFns.formatISO(endOfMonth);
    onChange(choiceKey, {
      min,
      max,
    });
  };

  customInput = (value?: Date) => {
    const { type } = this.props;
    let displayValue = 'Select';
    if (value) {
      if (type === ChoiceFilterDateRange.Type.SingleMonth) {
        displayValue = formatDate(value, 'MMMM yyyy');
      } else {
        displayValue = formatDate(value, DatePicker.defaultProps.dateFormat);
      }
    }
    return (
      <div>
        <div className={styles.customInput}>
          <a href="#" onClick={(e) => e.preventDefault()}>
            {displayValue} <i className={'fa fa-caret-down'} />
          </a>
        </div>
      </div>
    );
  };

  render() {
    const {
      choices,
      choicesFilters,
      choiceKey,
      type,
      placeholder,
    } = this.props;
    const choice: Choice = choices[choiceKey];
    const choiceFilter: Choice = choicesFilters[choiceKey];
    const displayTitle = shortestTitleForChoiceWithKeyInChoices(
      choiceKey,
      choices
    );

    let choiceMinDate = dateFns.parseISO(choice.min);
    let choiceMaxDate = dateFns.parseISO(choice.max);

    if (type === ChoiceFilterDateRange.Type.SingleMonth) {
      choiceMinDate = dateFns.startOfMonth(choiceMinDate);
      choiceMaxDate = dateFns.endOfMonth(choiceMaxDate);
    }

    let selectedMin, selectedMax;

    if (choiceFilter) {
      if (choiceFilter.min) {
        selectedMin = dateFns.parseISO(choiceFilter.min);
      }
      if (choiceFilter.max) {
        selectedMax = dateFns.parseISO(choiceFilter.max);
      }
    }

    return (
      <div className={styles.componentWrap}>
        {displayTitle} &middot;
        {type === ChoiceFilterDateRange.Type.SingleMonth && (
          <DatePicker
            customInput={this.customInput(selectedMin)}
            selected={selectedMin}
            minDate={choiceMinDate}
            maxDate={choiceMaxDate}
            showMonthYearPicker
            onChange={this.onSingleMonthChange}
            open={placeholder ? true : undefined}
          />
        )}
        {type === ChoiceFilterDateRange.Type.Default && (
          <React.Fragment>
            <DatePicker
              customInput={this.customInput(selectedMin)}
              selected={selectedMin}
              selectsStart
              startDate={selectedMin}
              endDate={selectedMax}
              minDate={choiceMinDate}
              maxDate={choiceMaxDate}
              onChange={this.onMinChange}
              showMonthDropdown
              showYearDropdown
              dropdownMode="select"
              open={placeholder ? true : undefined}
            />
            {' – '}
            <DatePicker
              customInput={this.customInput(selectedMax)}
              selected={selectedMax}
              selectsEnd
              startDate={selectedMin}
              endDate={selectedMax}
              minDate={selectedMin}
              maxDate={choiceMaxDate}
              onChange={this.onMaxChange}
              showMonthDropdown
              showYearDropdown
              dropdownMode="select"
              open={selectedMin && !selectedMax ? true : undefined}
            />
          </React.Fragment>
        )}
      </div>
    );
  }

  static defaultProps = {
    type: ChoiceFilterDateRange.Type.Default,
  };
}

ChoiceFilterDateRange.propTypes = {
  choicesFilters: PropTypes.object.isRequired,
  choices: PropTypes.object.isRequired,
  choiceKey: PropTypes.string.isRequired,
  placeholder: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default ChoiceFilterDateRange;
