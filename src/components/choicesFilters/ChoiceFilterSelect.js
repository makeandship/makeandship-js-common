/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import is from 'is_js';
import _orderBy from 'lodash.orderby';

import { isArray } from '../../utils/is';
import Select from '../ui/form/fields/Select';

import {
  shortestTitleForChoiceWithKeyInChoices,
  valueForChoice,
  descriptionWithCountForChoice,
  choiceForValueInChoices,
  descriptionForChoice,
} from './utils';

import styles from './ChoiceFilterSelect.module.scss';

import type { Choice } from './types';

const ChoiceFilterSelect = ({
  choices,
  choicesFilters,
  choiceKey,
  placeholder,
  onChange,
  sort = true,
  multiple = false,
  count = true,
}: React.ElementProps<*>): React.Element<*> => {
  const representedChoice: Choice = choices[choiceKey];
  const unsortedOptions = React.useMemo(() => {
    const unsortedOptions =
      representedChoice.choices &&
      representedChoice.choices.map((choice) => {
        const value = valueForChoice(choice);
        const label = count
          ? descriptionWithCountForChoice(choice)
          : descriptionForChoice(choice);
        return {
          value,
          label,
        };
      });
    return unsortedOptions;
  }, [count, representedChoice.choices]);
  const options = React.useMemo(() => {
    const options = sort
      ? _orderBy(unsortedOptions, ['label'])
      : unsortedOptions;
    return options;
  }, [sort, unsortedOptions]);
  const displayTitle = shortestTitleForChoiceWithKeyInChoices(
    choiceKey,
    choices
  );
  const { value, displayValue } = React.useMemo(() => {
    let value, displayValue;
    if (placeholder) {
      value = '';
      displayValue = displayTitle;
    } else {
      value = choicesFilters[choiceKey];
      if (isArray(value)) {
        const descriptions = value.map((v) => {
          const selectedChoice = choiceForValueInChoices({
            choices: representedChoice.choices,
            value: v,
          });
          const description = selectedChoice
            ? descriptionForChoice(selectedChoice)
            : '?';
          return description;
        });
        displayValue = `${displayTitle} · ${descriptions.join(' · ')}`;
      } else {
        const selectedChoice = choiceForValueInChoices({
          choices: representedChoice.choices,
          value,
        });
        const description = selectedChoice
          ? descriptionForChoice(selectedChoice)
          : 'Select';
        displayValue = `${displayTitle} · ${description}`;
      }
    }
    return { value, displayValue };
  }, [
    choiceKey,
    choicesFilters,
    displayTitle,
    placeholder,
    representedChoice.choices,
  ]);
  const initiallyOpen = placeholder && !is.ie(); // initiallyOpen throws on IE
  return (
    <div className={styles.select}>
      <div className={styles.widthSizer}>{displayValue}</div>
      <Select
        name={choiceKey}
        value={value}
        components={{
          SingleValue: () => (
            <span className="react-select__single-value">{displayValue}</span>
          ),
          MultiValue: ({ index }: React.ElementProps<*>) =>
            index === 0 ? (
              <span className="react-select__single-value">{displayValue}</span>
            ) : null,
        }}
        onChange={onChange}
        placeholder={displayTitle}
        options={options}
        clearable={false}
        initiallyOpen={initiallyOpen}
        wideMenu
        className={styles.control}
        classNamePrefix={'react-select'}
        multiple={multiple}
      />
    </div>
  );
};

ChoiceFilterSelect.propTypes = {
  choicesFilters: PropTypes.object.isRequired,
  choices: PropTypes.object.isRequired,
  choiceKey: PropTypes.string.isRequired,
  placeholder: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  sort: PropTypes.bool,
  multiple: PropTypes.bool,
};

export default ChoiceFilterSelect;
