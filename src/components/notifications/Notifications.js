/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import Notification from './Notification';
import styles from './Notifications.module.scss';

import {
  getNotifications,
  hideNotification,
} from '../../modules/notifications';

class Notifications extends React.Component<*> {
  onClick = (id: string) => {
    const { hideNotification } = this.props;
    hideNotification(id);
  };

  render() {
    const { notifications } = this.props;
    return (
      <div className={styles.container} data-tid="component-notifications">
        <TransitionGroup>
          {notifications.map((notification) => {
            const { id, content, category, autoHide } = notification;
            return (
              <CSSTransition key={id} classNames={styles} timeout={300}>
                <div className={styles.notification} key={id}>
                  <Notification
                    content={content}
                    category={category}
                    id={id}
                    onClick={() => this.onClick(id)}
                    showClose={!autoHide}
                  />
                </div>
              </CSSTransition>
            );
          })}
        </TransitionGroup>
      </div>
    );
  }
}

Notifications.propTypes = {
  notifications: PropTypes.array,
  hideNotification: PropTypes.func.isRequired,
};

const withRedux = connect(
  (state) => ({
    notifications: getNotifications(state),
  }),
  (dispatch) => ({
    hideNotification(id) {
      dispatch(hideNotification(id));
    },
  })
);

export default withRedux(Notifications);
