/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';

import styles from './Notification.module.scss';
import { NotificationCategories } from '../../modules/notifications';

class Notification extends React.Component<*> {
  onClick = (e: any) => {
    const { onClick, id } = this.props;
    e.preventDefault();
    onClick(id);
  };

  render() {
    const { category, content, showClose } = this.props;
    let icon;
    switch (category) {
      case NotificationCategories.SUCCESS:
        icon = 'fa fa-check-circle';
        break;
      case NotificationCategories.MESSAGE:
        icon = 'fa fa-info-circle';
        break;
      case NotificationCategories.ERROR:
        icon = 'fa fa-exclamation-circle';
        break;
    }
    return (
      <div
        className={styles[category.toLowerCase()]}
        onClick={this.onClick}
        data-tid="notification"
      >
        <p className={styles.content}>
          {icon && (
            <span className={styles.icon}>
              <i className={icon} />
            </span>
          )}
          {content}
        </p>
        {showClose && (
          <span className={styles.close}>
            <i className="fa fa-times-circle" />
          </span>
        )}
      </div>
    );
  }
  static defaultProps = {
    category: NotificationCategories.MESSAGE,
  };
}

Notification.propTypes = {
  category: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  showClose: PropTypes.bool.isRequired,
};

export default Notification;
