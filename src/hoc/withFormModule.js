/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import hoistNonReactStatic from 'hoist-non-react-statics';

import { isFormDataEqual, isFormDataDirty } from '../components/ui/form/utils';
import {
  getFormData,
  setFormData,
  setFormDataForKeyPath,
  clearFormData,
  getFormTransientData,
  setFormTransientData,
  setFormTransientDataForKeyPath,
  clearFormTransientData,
} from '../modules/form';

function withFormModule(WrappedComponent: React.ElementProps<*>) {
  class WithFormModule extends React.Component<*> {
    componentDidMount = () => {
      const { setFormData } = this.props;
      // were we passed form data?
      if (this.props.passedFormData) {
        setFormData(this.props.passedFormData);
      }
    };

    componentDidUpdate = (prevProps) => {
      const { setFormData } = this.props;
      if (
        !isFormDataEqual(prevProps.passedFormData, this.props.passedFormData)
      ) {
        // passed in data changed
        setFormData(this.props.passedFormData);
      }
    };

    componentWillUnmount() {
      const { clearFormData, clearFormDataOnUnmount } = this.props;
      clearFormDataOnUnmount && clearFormData();
    }

    onChange = (formData) => {
      const { setFormData, onChange } = this.props;
      setFormData(formData);
      onChange && onChange(formData);
    };

    render() {
      /* eslint-disable no-unused-vars */
      const { onChange, clearFormDataOnUnmount, ...rest } = this.props;
      /* eslint-enable */
      return <WrappedComponent onChange={this.onChange} {...rest} />;
    }
    static defaultProps = {
      clearFormDataOnUnmount: true,
    };
  }

  WithFormModule.displayName = `WithFormModule(${getDisplayName(
    WrappedComponent
  )})`;

  WithFormModule.propTypes = {
    ...withFormModulePropTypes,
  };

  // TODO: memoize with makeGetFormData()

  const withRedux = connect(
    (state, ownProps) => {
      const passedFormData = ownProps.formData;
      const formData = getFormData(state, ownProps.formKey);
      const formTransientData = getFormTransientData(state, ownProps.formKey);
      const isDirty = isFormDataDirty(passedFormData, formData);
      return {
        passedFormData,
        formData,
        formTransientData,
        isDirty,
      };
    },
    (dispatch, ownProps) => ({
      setFormData(formData) {
        dispatch(setFormData(ownProps.formKey, formData));
      },
      setFormDataForKeyPath(keyPath, value) {
        dispatch(setFormDataForKeyPath(ownProps.formKey, keyPath, value));
      },
      clearFormData() {
        dispatch(clearFormData(ownProps.formKey));
      },
      setFormTransientData(formTransientData) {
        dispatch(setFormTransientData(ownProps.formKey, formTransientData));
      },
      setFormTransientDataForKeyPath(keyPath, value) {
        dispatch(
          setFormTransientDataForKeyPath(ownProps.formKey, keyPath, value)
        );
      },
      clearFormTransientData() {
        dispatch(clearFormTransientData(ownProps.formKey));
      },
    }),
    null,
    { forwardRef: true }
  );

  const WithFormModuleWithRedux = withRedux(WithFormModule);

  hoistNonReactStatic(WithFormModuleWithRedux, WrappedComponent);

  return WithFormModuleWithRedux;
}

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

export const withFormModulePropTypes = {
  formData: PropTypes.object,
  passedFormData: PropTypes.object,
  formKey: PropTypes.string.isRequired,
  clearFormDataOnUnmount: PropTypes.bool.isRequired,
  onChange: PropTypes.func,
  setFormData: PropTypes.func.isRequired,
  setFormDataForKeyPath: PropTypes.func.isRequired,
  clearFormData: PropTypes.func.isRequired,
  setFormTransientData: PropTypes.func.isRequired,
  setFormTransientDataForKeyPath: PropTypes.func.isRequired,
  clearFormTransientData: PropTypes.func.isRequired,
};

export default withFormModule;
