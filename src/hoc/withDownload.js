/* @flow */

import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { download } from '../modules/api/download';

const withDownload = connect(null, {
  download,
});

export const withDownloadPropTypes = {
  download: PropTypes.func.isRequired,
};

export default withDownload;
