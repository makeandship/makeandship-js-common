/* @flow */

import * as React from 'react';
import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect';
import locationHelperBuilder from 'redux-auth-wrapper/history4/locationHelper';

import {
  getIsAuthenticated,
  getIsInitialised,
} from '../modules/auth/selectors';

/**
 * {@link https://reactjs.org/docs/higher-order-components.html Higher-Order Component}
 * using `redux-auth-wrapper` to redirect the user to `redirectPath` if they are not logged in
 * On successful login the user is redirected back to the referring page
 *
 * @param  {React.ComponentType<*>} WrappedComponent
 * @param  {string='/auth/login'} redirectPath
 *
 * @example
 * const AuthenticatedLogin = withUserIsAuthenticatedRedirect(Login);
 */

export const withUserIsNotAuthenticatedRedirect = (
  WrappedComponent: React.ComponentType<*>,
  redirectPath: string = '/auth/login'
) =>
  connectedRouterRedirect({
    // The url to redirect user to if they fail
    redirectPath,
    // If selector is true, wrapper will not redirect
    authenticatedSelector: (state) => getIsAuthenticated(state),
    // Returns true if the user auth state is loading
    authenticatingSelector: (state) => !getIsInitialised(state),
    // A nice display name for this check
    wrapperDisplayName: 'UserIsNotAuthenticatedRedirect',
  })(WrappedComponent);

const locationHelper = locationHelperBuilder({});

/**
 * {@link https://reactjs.org/docs/higher-order-components.html Higher-Order Component}
 * using `redux-auth-wrapper` to redirect the user to `redirectPath` if they are logged in
 * e.g. if the user visits the front page `/` when signed in you may want to redirect them to e.g. `/dashboard`
 *
 * @param  {React.ComponentType<*>} WrappedComponent
 * @param  {string='/'} redirectPath
 *
 * @example
 * const AuthenticatedHome = withUserIsAuthenticatedRedirect(Home, '/dashboard');
 */

export const withUserIsAuthenticatedRedirect = (
  WrappedComponent: React.ComponentType<*>,
  redirectPath: string = '/'
) =>
  connectedRouterRedirect({
    // This sends the user either to the query param route if we have one,
    // or to the landing page if none is specified and the user is already logged in
    redirectPath: (state, ownProps) =>
      locationHelper.getRedirectQueryParam(ownProps) || redirectPath,
    // This prevents us from adding the query parameter when we send the user away from the login page
    allowRedirectBack: false,
    // If selector is true, wrapper will not redirect
    authenticatedSelector: (state) => !getIsAuthenticated(state),
    // Returns true if the user auth state is loading
    authenticatingSelector: (state) => !getIsInitialised(state),
    // A nice display name for this check
    wrapperDisplayName: 'UserIsAuthenticatedRedirect',
  })(WrappedComponent);
