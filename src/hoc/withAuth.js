/* @flow */

import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {
  getIsAuthenticated,
  getIsFetching,
  getIsInitialised,
  getProfile,
} from '../modules/auth/selectors';

import {
  login,
  logout,
  register,
  profile,
  loadProfile,
} from '../modules/auth/actions';

const withAuth = connect(
  (state) => ({
    isAuthenticated: getIsAuthenticated(state),
    isFetching: getIsFetching(state),
    isInitialised: getIsInitialised(state),
    userProfile: getProfile(state),
  }),
  {
    login,
    logout,
    register,
    profile,
    loadProfile,
  }
);

export const withAuthPropTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  isInitialised: PropTypes.bool.isRequired,
  userProfile: PropTypes.object,
  login: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  register: PropTypes.func.isRequired,
  profile: PropTypes.func.isRequired,
  loadProfile: PropTypes.func.isRequired,
};

export default withAuth;
