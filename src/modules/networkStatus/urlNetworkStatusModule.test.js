/* @flow */

import { call, delay, put, select } from 'redux-saga/effects';
import { configureStore, createAction } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';

import { delay as delayPromise } from '../utils';

import { createUrlNetworkStatusModule } from './urlNetworkStatusModule';

const mockNetworkStatusActions = {
  networkOnline: createAction('NETWORK_ONLINE'),
  networkOffline: createAction('NETWORK_OFFLINE'),
};

/* eslint-disable no-unused-vars */
const initialFetcherFunction = jest.fn(async (url, options) => {
  await delayPromise(10);
  return;
});
/* eslint-enable no-unused-vars */

const moduleOptions = {
  typePrefix: 'urlNetworkStatus/',
  getState: (state: any) => state?.urlNetworkStatus,
  url: 'http://localhost:3001/health-check/',
  fetcher: initialFetcherFunction,
  getNetworkOnline: () => true,
  timeout: 30000,
  pollInterval: 1000,
  pollBackoffInterval: 1000,
  networkStatusActions: mockNetworkStatusActions,
};

const urlNetworkStatusModule = createUrlNetworkStatusModule(moduleOptions);

const {
  options: urlNetworkStatusOptions,
  reducer: urlNetworkStatus,
  selectors: { getOnline, getInitialised, getIsChecking, getCountDownSeconds },
  sagas: { urlNetworkStatusSaga },
} = urlNetworkStatusModule;

const sagaMiddleware = createSagaMiddleware();
/* eslint-disable no-unused-vars */
const store = configureStore({
  reducer: {
    urlNetworkStatus,
  },
  middleware: [sagaMiddleware],
});
/* eslint-enable no-unused-vars */
sagaMiddleware.run(urlNetworkStatusSaga);

describe('urlNetworkStatusModule saga', () => {
  afterEach(() => {
    // jest.clearAllMocks();
  });

  describe('when started', () => {
    it('should initialise using the provided fetcher', async (done) => {
      const saga = function* () {
        // yield takeEvery('*', function* (action) {
        //   console.log(JSON.stringify(action, null, 2));
        // });

        expect(initialFetcherFunction).toHaveBeenCalledTimes(1);
        expect(initialFetcherFunction).toHaveBeenCalledWith(moduleOptions.url, {
          method: 'HEAD',
          timeout: moduleOptions.timeout,
        });

        const isChecking = yield select(getIsChecking);
        expect(isChecking).toBeTruthy();

        // wait for initialFetcherFunction to resolve
        yield delay(10);

        const initialised = yield select(getInitialised);
        expect(initialised).toBeTruthy();

        yield call(done);
      };
      sagaMiddleware.run(saga);
    });

    it('should periodically call the provided fetcher', async (done) => {
      const saga = function* () {
        // yield takeEvery('*', function* (action) {
        //   console.log(JSON.stringify(action, null, 2));
        // });
        yield delay(3 * 1000);
        expect(initialFetcherFunction).toHaveBeenCalledTimes(3);

        // const fetcherFunction2 = jest.fn(async (url, options) => {
        //   await delayPromise(10);
        //   return;
        // });
        // urlNetworkStatusOptions.fetcher = fetcherFunction2;
        // yield put(urlNetworkStatusActions.checkBeaconNow());
        // yield delay(50);
        // expect(fetcherFunction2).toHaveBeenCalled();

        yield call(done);
      };
      sagaMiddleware.run(saga);
    });

    describe('when the device network goes offline', () => {
      it('should not make any further calls to the provided fetcher', async (done) => {
        const saga = function* () {
          yield put(mockNetworkStatusActions.networkOffline());
          yield delay(3 * 1000);
          expect(initialFetcherFunction).toHaveBeenCalledTimes(3);
          yield call(done);
        };
        sagaMiddleware.run(saga);
      });
    });

    describe('when the device network goes online', () => {
      it('should continue to periodically call the provided fetcher', async (done) => {
        const saga = function* () {
          yield put(mockNetworkStatusActions.networkOnline());
          yield delay(3 * 1000);
          expect(initialFetcherFunction).toHaveBeenCalledTimes(6);
          yield call(done);
        };
        sagaMiddleware.run(saga);
      });
    });

    describe('when the fetcher fails', () => {
      it('online state selector should return false, countdown state should be set and increase', async (done) => {
        const saga = function* () {
          // yield takeEvery('*', function* (action) {
          //   console.log(JSON.stringify(action, null, 2));
          // });
          // take network offline while swapping the fetcher to reset
          yield put(mockNetworkStatusActions.networkOffline());
          /* eslint-disable no-unused-vars */
          const fetcherFailFunction = jest.fn(async (url, options) => {
            await delayPromise(10);
            throw 'mock failure';
          });
          /* eslint-enable no-unused-vars */

          urlNetworkStatusOptions.fetcher = fetcherFailFunction;
          yield put(mockNetworkStatusActions.networkOnline());

          // wait for fetcherFailFunction to resolve
          yield delay(50);

          expect(fetcherFailFunction).toHaveBeenCalledTimes(1);

          const online = yield select(getOnline);
          expect(online).toBeFalsy();

          const countDownSeconds = yield select(getCountDownSeconds);
          expect(countDownSeconds).toEqual(1);

          yield delay(1 * 1000);
          expect(fetcherFailFunction).toHaveBeenCalledTimes(2);

          const countDownSecondsNow = yield select(getCountDownSeconds);
          expect(countDownSecondsNow).toEqual(2);

          yield call(done);
        };
        sagaMiddleware.run(saga);
      });
    });
  });
});
