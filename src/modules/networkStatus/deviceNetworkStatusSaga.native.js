/* @flow */

/**
 * Module for responding to device network reachability changes
 */

// $FlowFixMe intentionally ignore missing react-native package
import NetInfo from '@react-native-community/netinfo';
import { channel, buffers } from 'redux-saga';
import {
  apply,
  put,
  all,
  fork,
  takeLatest,
  call,
  select,
  take,
} from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import type { PayloadAction } from '@reduxjs/toolkit';

export const createDeviceNetworkStatusSaga = ({
  selectors,
  actions,
}: {
  selectors: {
    getOnline: (any) => boolean,
    getInitialised: (any) => boolean,
  },
  actions: {
    deviceNetworkOnline: PayloadAction,
    deviceNetworkOffline: PayloadAction,
    deviceNetworkStatusInitialised: PayloadAction,
  },
}) => {
  const deviceNetworkChan = channel(buffers.expanding(10));

  function* deviceNetworkChanWatcher(): Saga {
    yield takeLatest(deviceNetworkChan, deviceNetworkChanWorker);
  }

  function* deviceNetworkChanWorker(): Saga {
    // on iOS, type is always 'unknown' initially, so we ignore this state
    const connectionInfo = yield take(deviceNetworkChan);
    const { type } = connectionInfo;
    if (type === 'unknown') {
      return;
    }
    const current = yield select(selectors.getOnline);
    const initialised = yield select(selectors.getInitialised);
    const online = type !== 'none';
    if (current.online !== online) {
      if (online) {
        yield put(actions.deviceNetworkOnline());
      } else {
        yield put(actions.deviceNetworkOffline());
      }
    }
    if (!initialised) {
      yield put(actions.deviceNetworkStatusInitialised());
    }
  }

  function* init(): Saga {
    // initialise NetInfo
    const connectionInfo = yield call(NetInfo.fetch);
    deviceNetworkChan.put(connectionInfo);
    // listen for changes
    yield apply(NetInfo, NetInfo.addEventListener, [
      (connectionInfo) => deviceNetworkChan.put(connectionInfo),
    ]);
  }

  function* deviceNetworkStatusSaga(): Saga {
    const sagas = [deviceNetworkChanWatcher];
    yield all(sagas.map((saga) => fork(saga)));
    yield call(init);
  }

  return {
    deviceNetworkChan,
    deviceNetworkChanWatcher,
    deviceNetworkChanWorker,
    init,
    deviceNetworkStatusSaga,
  };
};
