/* @flow */

/**
 * Module for providing network status based on polling a url
 */

import type { Saga } from 'redux-saga';
import {
  put,
  all,
  fork,
  takeLatest,
  call,
  select,
  race,
  take,
  cancelled,
  delay,
} from 'redux-saga/effects';
import { createSelector } from 'reselect';
import {
  PayloadAction,
  createAction,
  createReducer as reduxToolkitCreateReducer,
} from '@reduxjs/toolkit';
import type { Reducer } from '@reduxjs/toolkit';

import {
  selectors as deviceNetworkStatusSelectors,
  actions as deviceNetworkStatusActions,
} from './deviceNetworkStatus';

import timeoutFetcher from '../fetchers/timeoutFetcher';
import type { Fetcher } from '../fetchers/types';

export type State = {
  online: boolean,
  initialised: boolean,
  countDownSeconds: number,
  isChecking: boolean,
};

export type ModuleOptions = {
  typePrefix: string,
  getState: (state: any) => State,
  pollInterval: number,
  pollBackoffInterval: number,
  maxPollBackoffInterval: number,
  timeout: number,
  pollBackoffAdjust: (backoff: number) => number,
  url: string,
  fetcher: Fetcher,
  getNetworkOnline: (state: any) => boolean,
  networkStatusActions: {
    networkOnline: PayloadAction,
    networkOffline: PayloadAction,
  },
};

export const defaultModuleOptions: ModuleOptions = {
  typePrefix: 'networkStatus/urlNetworkStatus/',
  getState: (state: any) => state?.networkStatus?.urlNetworkStatus,
  pollInterval: 10000,
  pollBackoffInterval: 1000,
  maxPollBackoffInterval: 64000,
  timeout: 30000,
  // add some randomness to smooth aggregate traffic
  pollBackoffAdjust: (backoff: number) => backoff * (1 + Math.random() * 0.25),
  // default timeout after 50 seconds for testing only
  url: 'http://httpstat.us/200?sleep=50000',
  fetcher: timeoutFetcher,
  getNetworkOnline: deviceNetworkStatusSelectors.getOnline,
  networkStatusActions: {
    networkOnline: deviceNetworkStatusActions.deviceNetworkOnline,
    networkOffline: deviceNetworkStatusActions.deviceNetworkOffline,
  },
};

export const createUrlNetworkStatusModule = (
  moduleOptions?: Object
): {
  options: ModuleOptions,
  selectors: Selectors,
  reducer: Reducer,
  actions: Actions,
  sagas: Sagas,
} => {
  const options: ModuleOptions = Object.assign(
    {},
    defaultModuleOptions,
    moduleOptions
  );
  const selectors = createSelectors(options);
  const actions = createActions(options);
  const sagas = createSagas(actions, selectors, options);
  const reducer = createReducer(actions);
  return {
    options,
    selectors,
    reducer,
    actions,
    sagas,
  };
};

// Selectors

type Selectors = {
  getState: (state: any) => State,
  getOnline: (state: State) => boolean,
  getInitialised: (state: State) => boolean,
  getIsChecking: (state: State) => boolean,
  getCountDownSeconds: (state: State) => number,
};

export const createSelectors = (moduleOptions: ModuleOptions): Selectors => {
  const getState = moduleOptions.getState;
  const getOnline = createSelector(getState, (state) => state?.online);
  const getInitialised = createSelector(
    getState,
    (state) => state?.initialised
  );
  const getIsChecking = createSelector(getState, (state) => state?.isChecking);
  const getCountDownSeconds = createSelector(
    getState,
    (state) => state?.countDownSeconds
  );
  return {
    getState,
    getOnline,
    getInitialised,
    getIsChecking,
    getCountDownSeconds,
  };
};

// Reducer

export const createReducer = (actions: Actions): Reducer => {
  const initialState: State = {
    online: false,
    initialised: false,
    isChecking: false,
    countDownSeconds: 0,
  };

  const reducer = reduxToolkitCreateReducer(initialState, {
    [actions.networkStatusInitialised]: (state) => {
      state.initialised = true;
    },
    [actions.networkOnline]: (state) => {
      state.online = true;
    },
    [actions.networkOffline]: (state) => {
      state.online = false;
    },
    [actions.setCountDownSeconds]: (state, action) => {
      state.countDownSeconds = action.payload;
    },
    [actions.check]: (state) => {
      state.isChecking = true;
    },
    [actions.checkSuccess]: (state) => {
      state.isChecking = false;
    },
    [actions.checkFailure]: (state) => {
      state.isChecking = false;
    },
    [actions.checkCancel]: (state) => {
      state.isChecking = false;
      state.countDownSeconds = 0;
    },
  });

  return reducer;
};

type Actions = {
  networkStatusInitialised: PayloadAction,
  networkOffline: PayloadAction,
  networkOnline: PayloadAction,
  check: PayloadAction,
  checkNow: PayloadAction,
  checkCancel: PayloadAction,
  checkFailure: PayloadAction,
  checkSuccess: PayloadAction,
  setCountDownSeconds: PayloadAction,
};

export const createActions = (moduleOptions: ModuleOptions): Actions => {
  const { typePrefix } = moduleOptions;
  return {
    networkStatusInitialised: createAction(
      `${typePrefix}NETWORK_STATUS_INITIALISED`
    ),
    networkOffline: createAction(`${typePrefix}NETWORK_OFFLINE`),
    networkOnline: createAction(`${typePrefix}NETWORK_ONLINE`),
    check: createAction(`${typePrefix}CHECK`),
    checkNow: createAction(`${typePrefix}CHECK_NOW`),
    checkCancel: createAction(`${typePrefix}CHECK_CANCEL`),
    checkFailure: createAction(`${typePrefix}CHECK_FAILURE`),
    checkSuccess: createAction(`${typePrefix}CHECK_SUCCESS`),
    setCountDownSeconds: createAction(`${typePrefix}SET_COUNT_DOWN_SECONDS`),
  };
};

// Side effects

type Sagas = {
  checkWatcher: Saga,
  checkWorker: Saga,
  pollWorker: Saga,
  pollCheckUrlWorker: Saga,
  pollIntervalWorker: Saga,
  networkStatusWatcher: Saga,
  networkStatusWorker: Saga,
  urlNetworkStatusSaga: Saga,
};

export const createSagas = (
  actions: Actions,
  selectors: Selectors,
  moduleOptions: ModuleOptions
): Sagas => {
  let currentPollInterval = moduleOptions.pollInterval;

  function* checkWatcher(): Saga {
    yield takeLatest([actions.check, actions.checkCancel], checkWorker);
  }

  function* checkWorker(action: any): Saga {
    if (action.type === actions.checkCancel.type) {
      return;
    }
    try {
      yield call(moduleOptions.fetcher, moduleOptions.url, {
        method: 'HEAD',
        timeout: moduleOptions.timeout,
      });
      yield put(actions.checkSuccess());
    } catch (error) {
      yield put(actions.checkFailure());
    }
  }

  function* pollWorker(): Saga {
    try {
      while (true) {
        const { cancel } = yield race({
          worker: call(pollCheckUrlWorker),
          cancel: take(actions.checkNow),
        });
        if (cancel) {
          currentPollInterval = moduleOptions.pollInterval;
        }
      }
    } finally {
      if (yield cancelled()) {
        yield put(actions.checkCancel());
        yield put(actions.networkOffline());
      }
    }
  }

  function* pollCheckUrlWorker(): Saga {
    const online = yield select(selectors.getOnline);
    yield put(actions.check());
    const { success } = yield race({
      success: take(actions.checkSuccess),
      failure: take(actions.checkFailure),
    });
    if (success) {
      if (!online) {
        yield put(actions.networkOnline());
      }
      currentPollInterval = moduleOptions.pollInterval;
    } else {
      if (online) {
        // just went offline, reset the backoff value
        yield put(actions.networkOffline());
        currentPollInterval = moduleOptions.pollBackoffInterval;
      } else {
        // exponentially increase interval if we were already offline
        currentPollInterval = Math.min(
          currentPollInterval * 2,
          moduleOptions.maxPollBackoffInterval
        );
      }
    }
    const initialised = yield select(selectors.getInitialised);
    if (!initialised) {
      yield put(actions.networkStatusInitialised());
    }
    yield call(pollIntervalWorker, currentPollInterval);
  }

  function* pollIntervalWorker(currentPollInterval: number): Saga {
    let countDownSeconds = Math.round(
      moduleOptions.pollBackoffAdjust(currentPollInterval) / 1000
    );
    while (countDownSeconds > 0) {
      yield put(actions.setCountDownSeconds(countDownSeconds));
      yield delay(1000);
      countDownSeconds--;
    }
  }

  function* networkStatusWatcher(): Saga {
    // start if the device network is already online
    const deviceNetworkOnline = yield select(moduleOptions.getNetworkOnline);
    if (deviceNetworkOnline) {
      yield call(networkStatusWorker);
    }
    // watch for future changes
    yield takeLatest(
      moduleOptions.networkStatusActions.networkOnline,
      networkStatusWorker
    );
  }

  function* networkStatusWorker(): Saga {
    // cancel worker if the network goes offline, or checkNow() is called
    yield race({
      worker: call(pollWorker),
      cancel: take(moduleOptions.networkStatusActions.networkOffline),
    });
  }

  function* urlNetworkStatusSaga(): Saga {
    const sagas = [networkStatusWatcher, checkWatcher];
    yield all(sagas.map((saga) => fork(saga)));
  }

  return {
    checkWatcher,
    checkWorker,
    pollWorker,
    pollCheckUrlWorker,
    pollIntervalWorker,
    networkStatusWatcher,
    networkStatusWorker,
    urlNetworkStatusSaga,
  };
};
