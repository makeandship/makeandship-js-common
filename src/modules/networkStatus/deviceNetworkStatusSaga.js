/* @flow */

/**
 * Module for responding to device network reachability changes
 */

import { channel, buffers } from 'redux-saga';
import type { Saga } from 'redux-saga';
import type { PayloadAction } from '@reduxjs/toolkit';

import {
  apply,
  put,
  all,
  fork,
  takeLatest,
  call,
  select,
} from 'redux-saga/effects';

export const createDeviceNetworkStatusSaga = ({
  selectors,
  actions,
}: {
  selectors: {
    getOnline: (any) => boolean,
  },
  actions: {
    deviceNetworkOnline: PayloadAction,
    deviceNetworkOffline: PayloadAction,
    deviceNetworkStatusInitialised: PayloadAction,
  },
}) => {
  const deviceNetworkChan = channel(buffers.expanding(10));

  function* deviceNetworkChanWatcher(): Saga {
    yield takeLatest(deviceNetworkChan, deviceNetworkChanWorker);
  }

  function* deviceNetworkChanWorker(): Saga {
    const current = yield select(selectors.getOnline);
    const online = navigator.onLine;
    if (current.online !== online) {
      if (online) {
        yield put(actions.deviceNetworkOnline());
      } else {
        yield put(actions.deviceNetworkOffline());
      }
    }
  }

  function* init(): Saga {
    // listen for changes
    yield apply(window, window.addEventListener, [
      'online',
      (e) => deviceNetworkChan.put(e),
    ]);
    yield apply(window, window.addEventListener, [
      'offline',
      (e) => deviceNetworkChan.put(e),
    ]);
    const online = navigator.onLine;
    yield put(
      online ? actions.deviceNetworkOnline() : actions.deviceNetworkOffline()
    );
    yield put(actions.deviceNetworkStatusInitialised());
  }

  function* deviceNetworkStatusSaga(): Saga {
    const sagas = [deviceNetworkChanWatcher];
    yield all(sagas.map((saga) => fork(saga)));
    yield call(init);
  }

  return {
    deviceNetworkChan,
    deviceNetworkChanWatcher,
    deviceNetworkChanWorker,
    init,
    deviceNetworkStatusSaga,
  };
};
