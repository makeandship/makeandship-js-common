/* @flow */

/**
 * Module for responding to device network reachability changes
 */

import { createSelector } from 'reselect';
import { createAction, createReducer } from '@reduxjs/toolkit';

import { createDeviceNetworkStatusSaga } from './deviceNetworkStatusSaga';

export const typePrefix = 'networkStatus/deviceNetworkStatus/';

export const actions = {
  deviceNetworkStatusInitialised: createAction(
    `${typePrefix}DEVICE_NETWORK_STATUS_INITIALISED`
  ),
  deviceNetworkOnline: createAction(`${typePrefix}DEVICE_NETWORK_ONLINE`),
  deviceNetworkOffline: createAction(`${typePrefix}DEVICE_NETWORK_OFFLINE`),
};

const getState = (state: any) => state?.networkStatus?.deviceNetworkStatus;

export const selectors = {
  getState,
  getOnline: createSelector(getState, (state) => state?.online),
  getInitialised: createSelector(getState, (state) => state?.initialised),
};

// Reducer

export type State = {
  online: boolean,
  initialised: boolean,
};

const initialState: State = {
  online: false,
  initialised: false,
};

export const reducer = createReducer(initialState, {
  [actions.deviceNetworkStatusInitialised]: (state) => {
    state.initialised = true;
  },
  [actions.deviceNetworkOnline]: (state) => {
    state.online = true;
  },
  [actions.deviceNetworkOffline]: (state) => {
    state.online = false;
  },
});

const { deviceNetworkStatusSaga: saga } = createDeviceNetworkStatusSaga({
  selectors,
  actions,
});

export { saga };
