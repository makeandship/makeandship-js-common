/* @flow */

import { isValidJSON, isValidJSend } from '../utils/isValid';
import { isString, isPlainObject } from '../../utils/is';
import parseJSend from '../utils/parseJSend';

export class JsendResponseTransformerError extends Error {
  jsend: any;
  error: string;
  errors: ?{ [string]: string };

  constructor(
    jsend: number,
    error?: string,
    errors?: { [string]: string } = {}
  ) {
    super();
    this.name = 'JsendResponseTransformerError';
    this.jsend = jsend;
    if (error) {
      this.error = error;
      this.message = error;
    }
    if (errors) {
      this.errors = errors;
    }
  }
}

const jsendResponseTransformer = (data: any) => {
  // coerce from Object back to JSON string if possible
  if (isPlainObject(data)) {
    try {
      data = JSON.stringify(data);
    } catch (e) {
      // unable to coerce - ignore
    }
  }
  // do not try to parse unless string - return plain data
  if (!isString(data)) {
    return data;
  }
  if (isValidJSend(data)) {
    const { jsend, error, errors } = parseJSend(data);
    if (error || errors) {
      throw new JsendResponseTransformerError(jsend, error, errors);
    } else {
      return jsend.data;
    }
  } else if (isValidJSON(data)) {
    const json = JSON.parse(data);
    return json;
  }
  return data;
};

export default jsendResponseTransformer;
