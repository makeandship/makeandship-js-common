/* @flow */

import jsendResponseTransformer from './jsendResponseTransformer';

import okValidJSendResponse from '../utils/__fixtures__/jsend-success-1.json';
import errorJSendResponse1 from '../utils/__fixtures__/jsend-error-1.json';
import errorJSendResponse3 from '../utils/__fixtures__/jsend-error-3.json';

const validJson = { foo: 'bar' };

describe('jsendParser', () => {
  describe('when valid', () => {
    describe('response is json', () => {
      describe('object', () => {
        it('should return expected data', () => {
          expect(jsendResponseTransformer(validJson)).toEqual(validJson);
        });
      });
      describe('string', () => {
        it('should return expected data', () => {
          expect(jsendResponseTransformer(JSON.stringify(validJson))).toEqual(
            validJson
          );
        });
      });
    });

    describe('response is jsend', () => {
      describe('when status is success', () => {
        describe('object', () => {
          it('should return expected data', () => {
            expect(jsendResponseTransformer(okValidJSendResponse)).toEqual(
              okValidJSendResponse.data
            );
          });
        });
        describe('string', () => {
          it('should return expected data', () => {
            expect(
              jsendResponseTransformer(JSON.stringify(okValidJSendResponse))
            ).toEqual(okValidJSendResponse.data);
          });
        });
      });

      describe('when status is error', () => {
        describe('error is a string', () => {
          it('should return expected error', () => {
            try {
              jsendResponseTransformer(JSON.stringify(errorJSendResponse1));
            } catch (error) {
              expect(error.error).toEqual(errorJSendResponse1.message);
            }
          });
        });
        describe('error is a map', () => {
          it('should return expected errore', () => {
            try {
              jsendResponseTransformer(JSON.stringify(errorJSendResponse3));
            } catch (error) {
              expect(error.error).toEqual(errorJSendResponse3.message);
              expect(error.errors).toEqual(errorJSendResponse3.data.errors);
            }
          });
        });
      });
    });

    describe('response is text but not json or jsend', () => {
      it('should return text', () => {
        const text = 'foo';
        expect(jsendResponseTransformer(text)).toEqual(text);
      });
    });

    describe('response is empty', () => {
      it('should return', () => {
        expect(jsendResponseTransformer()).toEqual();
      });
    });
  });
});
