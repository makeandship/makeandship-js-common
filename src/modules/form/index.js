/* @flow */

import _get from 'lodash.get';
import _set from 'lodash.set';
import { createSelector } from 'reselect';
import produce from 'immer';

export const typePrefix = 'form/form/';

export const RESET = `${typePrefix}RESET`;

export const SET_DATA = `${typePrefix}SET_DATA`;
export const DELETE_DATA = `${typePrefix}DELETE_DATA`;
export const SET_DATA_FOR_KEY_PATH = `${typePrefix}SET_DATA_FOR_KEY_PATH`;

export const SET_TRANSIENT_DATA = `${typePrefix}SET_TRANSIENT_DATA`;
export const DELETE_TRANSIENT_DATA = `${typePrefix}DELETE_TRANSIENT_DATA`;
export const SET_TRANSIENT_DATA_FOR_KEY_PATH = `${typePrefix}SET_TRANSIENT_DATA_FOR_KEY_PATH`;

// Selectors

export const getState = (state: any): State => state?.form;

export const getFormData = (state: any, key: string) =>
  _get(getState(state), ['data', key]);

export const getFormDataForKeyPath = (
  state: any,
  key: string,
  keyPath: string
) => _get(getFormData(state, key), keyPath);

export const getFormTransientData = (state: any, key: string) =>
  _get(getState(state), ['transientData', key]);

export const getFormTransientDataForKeyPath = (
  state: any,
  key: string,
  keyPath: string
) => _get(getFormTransientData(state, key), keyPath);

// Selector factory for form key

export const makeGetFormData = (key: string) => {
  return createSelector(getState, (state) => _get(state, ['data', key]));
};

export const makeGetFormDataForKeyPath = (key: string, keyPath: string) => {
  return createSelector(makeGetFormData(key), (formData) =>
    _get(formData, keyPath)
  );
};

export const makeGetFormTransientData = (key: string) => {
  return createSelector(getState, (state) =>
    _get(state, ['transientData', key])
  );
};

export const makeGetFormTransientDataForKeyPath = (
  key: string,
  keyPath: string
) => {
  return createSelector(makeGetFormTransientData(key), (formData) =>
    _get(formData, keyPath)
  );
};

// Actions

export const resetFormData = () => ({
  type: RESET,
});

export const setFormData = (key: string, formData: any) => ({
  type: SET_DATA,
  payload: {
    key,
    formData,
  },
});

export const clearFormData = (key: string) => ({
  type: SET_DATA,
  payload: {
    key,
  },
});

export const setFormDataForKeyPath = (
  key: string,
  keyPath: string,
  value: any
) => ({
  type: SET_DATA_FOR_KEY_PATH,
  payload: {
    key,
    keyPath,
    value,
  },
});

export const setFormTransientData = (key: string, formTransientData: any) => ({
  type: SET_TRANSIENT_DATA,
  payload: {
    key,
    formTransientData,
  },
});

export const clearFormTransientData = (key: string) => ({
  type: SET_TRANSIENT_DATA,
  payload: {
    key,
  },
});

export const setFormTransientDataForKeyPath = (
  key: string,
  keyPath: string,
  value: any
) => ({
  type: SET_TRANSIENT_DATA_FOR_KEY_PATH,
  payload: {
    key,
    keyPath,
    value,
  },
});

// Reducer

export type State = {
  data: {
    [string]: any,
  },
  transientData: {
    [string]: any,
  },
};

const initialState: State = {
  data: {},
  transientData: {},
};

const reducer = (state?: State = initialState, action?: Object = {}): State =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_DATA:
        draft.data[action.payload.key] = action.payload.formData;
        return;
      case SET_DATA_FOR_KEY_PATH:
        if (!draft.data[action.payload.key]) {
          draft.data[action.payload.key] = {};
        }
        _set(
          draft.data[action.payload.key],
          action.payload.keyPath,
          action.payload.value
        );
        return;
      case DELETE_DATA:
        delete draft.data[action.payload.key];
        return;
      case SET_TRANSIENT_DATA:
        draft.transientData[action.payload.key] =
          action.payload.formTransientData;
        return;
      case SET_TRANSIENT_DATA_FOR_KEY_PATH: {
        if (!draft.transientData[action.payload.key]) {
          draft.transientData[action.payload.key] = {};
        }
        _set(
          draft.transientData[action.payload.key],
          action.payload.keyPath,
          action.payload.value
        );
        return;
      }
      case DELETE_TRANSIENT_DATA:
        delete draft.transientData[action.payload.key];
        return;
      case RESET:
        return initialState;
      default:
        return;
    }
  });

export default reducer;
