/* @flow */

import reducer, {
  setFormDataForKeyPath,
  setFormTransientDataForKeyPath,
  getState,
  getFormData,
  getFormDataForKeyPath,
  makeGetFormData,
  makeGetFormDataForKeyPath,
  getFormTransientData,
  getFormTransientDataForKeyPath,
  makeGetFormTransientData,
  makeGetFormTransientDataForKeyPath,
} from './index';

import type { State } from './index';

describe('form module', function spec() {
  it('should get state', () => {
    let state: {
      form: State,
    } = {
      form: {
        data: {
          a: {
            name: 'a',
            test: {
              description: 'test a',
            },
          },
          b: {
            name: 'b',
            test: {
              description: 'test a',
            },
          },
        },
        transientData: {},
      },
    };

    const getFormDataA = makeGetFormData('a');
    const getFormDataB = makeGetFormData('b');

    expect(getState(state)).toEqual(state.form);

    expect(getFormData(state, 'a')).toEqual(state.form.data.a);
    expect(getFormDataA(state)).toEqual(state.form.data.a);
    expect(getFormData(state, 'b')).toEqual(state.form.data.b);
    expect(getFormDataB(state)).toEqual(state.form.data.b);

    const getFormDataForKeyPathAName = makeGetFormDataForKeyPath('a', 'name');
    const getFormDataForKeyPathATest = makeGetFormDataForKeyPath('a', 'test');
    const getFormDataForKeyPathATestDescription = makeGetFormDataForKeyPath(
      'a',
      'test.description'
    );

    expect(getFormDataForKeyPath(state, 'a', 'name')).toEqual(
      state.form.data.a.name
    );
    expect(getFormDataForKeyPathAName(state)).toEqual(state.form.data.a.name);

    expect(getFormDataForKeyPath(state, 'a', 'test')).toEqual(
      state.form.data.a.test
    );
    expect(getFormDataForKeyPathATest(state)).toEqual(state.form.data.a.test);

    expect(getFormDataForKeyPath(state, 'a', 'test.description')).toEqual(
      state.form.data.a.test.description
    );
    expect(getFormDataForKeyPathATestDescription(state)).toEqual(
      state.form.data.a.test.description
    );

    state = {
      form: {
        ...reducer(
          state.form,
          setFormDataForKeyPath('a', 'test.description', 'test a modified')
        ),
      },
    };

    expect(getFormDataForKeyPath(state, 'a', 'test.description')).toEqual(
      'test a modified'
    );
    expect(getFormDataForKeyPathATestDescription(state)).toEqual(
      'test a modified'
    );
  });

  it('should get transient state', () => {
    let state: {
      form: State,
    } = {
      form: {
        data: {},
        transientData: {
          a: {
            name: 'a',
            test: {
              description: 'test a',
            },
          },
          b: {
            name: 'b',
            test: {
              description: 'test a',
            },
          },
        },
      },
    };

    const getFormTransientDataA = makeGetFormTransientData('a');
    const getFormTransientDataB = makeGetFormTransientData('b');

    expect(getState(state)).toEqual(state.form);

    expect(getFormTransientData(state, 'a')).toEqual(
      state.form.transientData.a
    );
    expect(getFormTransientDataA(state)).toEqual(state.form.transientData.a);
    expect(getFormTransientData(state, 'b')).toEqual(
      state.form.transientData.b
    );
    expect(getFormTransientDataB(state)).toEqual(state.form.transientData.b);

    const getFormTransientDataForKeyPathAName = makeGetFormTransientDataForKeyPath(
      'a',
      'name'
    );
    const getFormTransientDataForKeyPathATest = makeGetFormTransientDataForKeyPath(
      'a',
      'test'
    );
    const getFormTransientDataForKeyPathATestDescription = makeGetFormTransientDataForKeyPath(
      'a',
      'test.description'
    );

    expect(getFormTransientDataForKeyPath(state, 'a', 'name')).toEqual(
      state.form.transientData.a.name
    );
    expect(getFormTransientDataForKeyPathAName(state)).toEqual(
      state.form.transientData.a.name
    );

    expect(getFormTransientDataForKeyPath(state, 'a', 'test')).toEqual(
      state.form.transientData.a.test
    );
    expect(getFormTransientDataForKeyPathATest(state)).toEqual(
      state.form.transientData.a.test
    );

    expect(
      getFormTransientDataForKeyPath(state, 'a', 'test.description')
    ).toEqual(state.form.transientData.a.test.description);
    expect(getFormTransientDataForKeyPathATestDescription(state)).toEqual(
      state.form.transientData.a.test.description
    );

    state = {
      form: {
        ...reducer(
          state.form,
          setFormTransientDataForKeyPath(
            'a',
            'test.description',
            'test a modified'
          )
        ),
      },
    };

    expect(
      getFormTransientDataForKeyPath(state, 'a', 'test.description')
    ).toEqual('test a modified');
    expect(getFormTransientDataForKeyPathATestDescription(state)).toEqual(
      'test a modified'
    );
  });
});
