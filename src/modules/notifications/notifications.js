/* @flow */

import { all, fork, put, takeEvery, delay } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import { createSelector } from 'reselect';
import uuid from 'uuid';
import produce from 'immer';

export const NotificationCategories = {
  ERROR: 'ERROR',
  MESSAGE: 'MESSAGE',
  SUCCESS: 'SUCCESS',
};

export const typePrefix = 'notifications/notifications/';
export const SHOW = `${typePrefix}SHOW`;
export const HIDE = `${typePrefix}HIDE`;
export const HIDE_ALL = `${typePrefix}HIDE_ALL`;
export const UPDATE = `${typePrefix}UPDATE`;

// Selectors

type State = Array<Notification>;

export type Notification = {
  id?: string,
  category?: string,
  content: string,
  autoHide?: boolean,
};

export const getState = (state: any): State =>
  state?.notifications?.notifications;

export const getNotifications = createSelector(
  getState,
  (notifications) => notifications
);

// Action creators

export const showNotification = (arg: Notification | string) => {
  let notification: Notification;
  if (typeof arg === 'string') {
    notification = {
      content: arg,
    };
  } else {
    notification = arg;
  }
  const {
    id = uuid.v4(),
    category = NotificationCategories.SUCCESS,
    content,
    autoHide = true,
  } = notification;
  return {
    type: SHOW,
    payload: {
      id,
      category,
      content,
      autoHide,
    },
  };
};

export const hideNotification = (id: string) => ({
  type: HIDE,
  payload: id,
});

export const hideAllNotifications = () => ({
  type: HIDE_ALL,
});

export const updateNotification = (id: string, attributes: any) => ({
  type: UPDATE,
  payload: { id, ...attributes },
});

// Reducer

const initialState: State = [];

const reducer = (state?: State = initialState, action?: Object = {}): State =>
  produce(state, (draft) => {
    switch (action.type) {
      case SHOW:
        draft.push(action.payload);
        return;
      case UPDATE: {
        const notification = draft.find(
          (notification) => notification.id === action.payload.id
        );
        if (notification) {
          // update
          Object.assign(notification, action.payload);
        } else {
          // create
          draft.push(action.payload);
        }
        return;
      }
      case HIDE:
        draft.splice(
          draft.findIndex((notification) => notification.id === action.payload),
          1
        );
        return;
      case HIDE_ALL:
        return initialState;
      default:
        return;
    }
  });

export default reducer;

// Side effects

function* showNotificationWatcher() {
  yield takeEvery(SHOW, showNotificationWorker);
}

export function* showNotificationWorker(action: any): Saga {
  if (action.payload.autoHide) {
    yield delay(2000);
    yield put(hideNotification(action.payload.id));
  }
}

export function* notificationsSaga(): Saga {
  yield all([fork(showNotificationWatcher)]);
}
