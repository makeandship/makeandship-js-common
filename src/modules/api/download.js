/* @flow */

import { call, fork, put, takeEvery } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import fileDownload from 'js-file-download';
import { createAction } from '@reduxjs/toolkit';

import { getConfig } from './config';
import { isString } from '../../utils/is';

export const typePrefix = 'api/download/';

export const download = createAction(`${typePrefix}DOWNLOAD`);
export const downloadSuccess = createAction(`${typePrefix}DOWNLOAD_SUCCESS`);
export const downloadFailure = createAction(`${typePrefix}DOWNLOAD_FAILURE`);

function* downloadWatcher() {
  yield takeEvery(download, downloadWorker);
}

export function* downloadWorker(action: any): Saga {
  try {
    let url, filename;
    if (isString(action.payload)) {
      url = action.payload;
    } else {
      ({ url, filename } = action.payload);
    }
    const config = yield call(getConfig);
    const blob = yield call(config.fetcher, url, { responseType: 'blob' });
    yield call(fileDownload, blob, filename);
    yield put(downloadSuccess({ url, filename }));
  } catch (error) {
    yield put(downloadFailure(error));
  }
}

export function* saga(): Saga {
  yield fork(downloadWatcher);
}
