/* @flow */

import {
  all,
  fork,
  call,
  take,
  put,
  race,
  select,
  takeEvery,
  takeLeading,
} from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import { createSelector } from 'reselect';
import { createAction, createReducer } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

import { executeJsonApiAction, actions as jsonApiActions } from './jsonApi';
import { normalizeTypes } from './utils';
import { isSwagger2, isOAS3, buildRequestSwagger } from './utils/swagger';
import { getConfig } from './config';

export const typePrefix = 'api/swaggerApi/';

export const actions = {
  callSwaggerApi: createAction(`${typePrefix}CALL_SWAGGER_API`),
  initialise: createAction(`${typePrefix}INITIALISE`),
  initialiseRequest: createAction(`${typePrefix}INITIALISE_REQUEST`),
  initialiseSuccess: createAction(`${typePrefix}INITIALISE_SUCCESS`),
  initialiseError: createAction(`${typePrefix}INITIALISE_ERROR`),
};

// Selectors

export const getState = (state: any) => state.api.swaggerApi;

export const getSpec = createSelector(
  getState,
  (swaggerApi) => swaggerApi.spec
);

export const getSpecIsSwagger2 = createSelector(
  getSpec,
  (spec) => spec && isSwagger2(spec)
);

export const getSpecIsOAS3 = createSelector(
  getSpec,
  (spec) => spec && isOAS3(spec)
);

export const getError = createSelector(
  getState,
  (swaggerApi) => swaggerApi.error
);

export const getIsInitialised = createSelector(
  getState,
  (swaggerApi) => swaggerApi.isInitialised
);

export const getIsInitialising = createSelector(
  getState,
  (swaggerApi) => swaggerApi.isInitialising
);

// Reducer

type State = {
  isInitialising: boolean,
  isInitialised: boolean,
  spec?: any,
  error?: any,
};

const initialState: State = {
  isInitialising: false,
  isInitialised: false,
};

export const reducer = createReducer(initialState, {
  [actions.initialiseRequest]: (state) => {
    Object.assign(state, {
      isInitialising: true,
      error: undefined,
    });
  },
  [actions.initialiseSuccess]: (state, action) => {
    Object.assign(state, {
      isInitialising: false,
      isInitialised: true,
      spec: action.payload,
    });
  },
  [actions.initialiseError]: (state, action) => {
    Object.assign(state, {
      isInitialising: false,
      error: action.payload,
    });
  },
});

// execute and yield { success, failure } for an api call
// TODO: refactor - this is almost exactly the same as callJsonApiWithResponse() in jsonAPi.js

export function* callSwaggerApiWithResponse(parameters: any): Saga {
  const [REQUEST, SUCCESS, FAILURE, ABORT] = normalizeTypes(parameters.types); // eslint-disable-line
  const action = yield call(
    executeSwaggerApiAction,
    actions.callSwaggerApi(parameters)
  );
  let success, failure, abort;
  if (action.type === SUCCESS.type) {
    success = action;
  } else if (action.type === FAILURE.type) {
    failure = action;
  } else if (ABORT && action.type === ABORT.type) {
    abort = action;
  }
  return yield { success, failure, abort };
}

function* initialiseWatcher() {
  yield takeLeading(actions.initialise, initialiseWorker);
}

export function* initialiseWorker(): Saga {
  const isInitialised = yield select(getIsInitialised);
  if (isInitialised) {
    return;
  }
  yield put(actions.initialiseRequest());
  try {
    const config = yield call(getConfig);
    const apiSpecUrl = config.apiSpecUrl;
    try {
      const fetcherPromise = config.fetcher(apiSpecUrl);
      const data = yield fetcherPromise;
      yield put(actions.initialiseSuccess(data));
    } catch (error) {
      throw new Error(`Could not fetch API spec from ${apiSpecUrl}`);
    }
  } catch (error) {
    yield put(actions.initialiseError(error));
  }
}

function* swaggerApiActionWatcher() {
  yield takeEvery(actions.callSwaggerApi, executeSwaggerApiAction);
}

/**
 * Swagger call saga. Builds the request from the supplied operationId and parameters, then puts {@link callJsonApi}
 * to make the request
 *
 * @param  {Object} action
 * @param  {String} action.operationId
 * @param  {Object} action.parameters
 * @param  {Array} action.types
 * @param  {Channel} [action.chan] Saga channel passed to {@link callJsonApi} on which to put the request actions
 * @param  {Boolean} [action.immediate]
 */

export function* executeSwaggerApiAction(action: PayloadAction): Saga {
  try {
    const {
      operationId,
      parameters,
      types,
      shouldExecuteJsonApiAction,
      meta,
    } = action.payload;

    const isInitialised = yield select(getIsInitialised);

    if (!isInitialised) {
      yield put(actions.initialise());
      const { failure } = yield race({
        success: take(actions.initialiseSuccess),
        failure: take(actions.initialiseError),
      });
      if (failure) {
        // can't continue without initialised swagger - fail the original request and abort
        const [REQUEST, SUCCESS, FAILURE] = normalizeTypes(types); // eslint-disable-line
        return yield put({ ...FAILURE, payload: failure.payload, meta });
      }
    }

    const spec = yield select(getSpec);

    const request = buildRequestSwagger({
      spec,
      operationId,
      parameters,
    });

    const { url, method, headers, body } = request;

    const apiParameters = {
      url,
      options: {
        method,
        body,
        headers,
      },
      types,
      shouldExecuteJsonApiAction,
      meta,
    };

    return yield call(
      executeJsonApiAction,
      jsonApiActions.callJsonApi(apiParameters)
    );
  } catch (error) {
    // error e.g. initialising swagger
  }
}

export function* saga(): Saga {
  yield all([fork(initialiseWatcher), fork(swaggerApiActionWatcher)]);
}
