/* @flow */

import HttpStatus from 'http-status-codes';

import { isString } from '../../utils/is';

class JsonApiError extends Error {
  status: number;
  statusText: ?string;
  response: any;
  errors: ?{ [string]: string };

  constructor(
    status: number,
    statusText?: string,
    response: any,
    errors?: { [string]: string } = {}
  ) {
    super();
    this.name = 'JsonApiError';
    this.status = status;
    this.response = response;
    if (!statusText || !isString(statusText)) {
      // use standard status text
      try {
        statusText = HttpStatus.getStatusText(status);
      } catch (error) {
        // ignore unknown status
      }
    }
    if (statusText) {
      this.statusText = statusText;
      if (status >= 200 && status <= 299) {
        this.message = statusText;
      } else {
        this.message = `${status} - ${statusText}`;
      }
    }
    if (errors) {
      this.errors = errors;
    }
  }
}

export default JsonApiError;
