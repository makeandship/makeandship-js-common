/* @flow */

import { buildRequest } from 'swagger-client/src/execute';
import produce from 'immer';

import objectToFormData from '../../../../utils/objectToFormData';

import { isSwagger2, isOAS3 } from './is';
import { getOperation } from './getOperation';
import { parametersWithBodyNameForOperation } from './parametersWithBodyName';
import { getIsMultipartFormDataForOperation } from './getIsMultipartFormData';

const buildRequestSwagger = ({
  spec,
  operationId,
  parameters,
}: {
  spec: any,
  operationId: string,
  parameters?: any,
}) => {
  if (!spec) {
    throw new Error(`No spec - cannot build request`);
  }
  if (isSwagger2(spec)) {
    return buildRequestSwagger2({ spec, operationId, parameters });
  } else if (isOAS3(spec)) {
    return buildRequestOAS3({ spec, operationId, parameters });
  }
  console.log({ spec });
  throw new Error(`Swagger spec is not supported`);
};

export default buildRequestSwagger;

// Swagger 2

export const buildRequestSwagger2 = ({
  spec,
  operationId,
  parameters,
}: {
  spec: any,
  operationId: string,
  parameters?: any,
}) => {
  const operation = getOperation(spec, operationId);

  const parametersWithBodyName = parametersWithBodyNameForOperation(
    parameters,
    operation
  );

  const request = buildRequest({
    spec,
    operationId,
    parameters: parametersWithBodyName,
  });

  const operationIsMultipartFormData = getIsMultipartFormDataForOperation(
    operation
  );
  if (parametersWithBodyName && operationIsMultipartFormData) {
    request.headers['Content-Type'] = 'multipart/form-data';
    // swagger-client does not thoroughly create FormData,
    // e.g. will not handle arrays or nested objects
    // so we replace it here
    const formData = objectToFormData(parametersWithBodyName, {
      indices: false,
    });
    request.body = formData;
  }
  return request;
};

// OAS3

export const buildRequestOAS3 = ({
  spec,
  operationId,
  parameters,
}: {
  spec: any,
  operationId: string,
  parameters?: any,
}) => {
  const operation = getOperation(spec, operationId);
  if (!operation) {
    throw `Operation id ${operationId} not found`;
  }
  let requestParameters = {};
  let requestBody = {};
  if (operation.requestBody) {
    // split out parameters into parameters in spec, the rest going into requestBody
    let parameterNames = [];
    if (parameters && operation.parameters) {
      parameterNames = operation.parameters.map((parameter) => parameter.name);
      requestParameters = produce(parameters, (draft) => {
        Object.keys(parameters).forEach((parameterName) => {
          if (!parameterNames.includes(parameterName)) {
            delete draft[parameterName];
          }
        });
      });
    }
    if (parameters) {
      requestBody = produce(parameters, (draft) => {
        parameterNames.forEach((parameterName) => {
          delete draft[parameterName];
        });
      });
    }
  } else {
    // no request body, pass all as parameters
    if (parameters && operation.parameters) {
      requestParameters = parameters;
    }
  }
  if (!Object.keys(requestParameters).length) {
    requestParameters = undefined;
  }
  if (!Object.keys(requestBody).length) {
    requestBody = undefined;
  }
  const request = buildRequest({
    spec,
    operationId,
    parameters: requestParameters,
    requestBody,
  });
  const operationIsMultipartFormData = getIsMultipartFormDataForOperation(
    operation
  );
  if (request.body && operationIsMultipartFormData) {
    request.headers['Content-Type'] = 'multipart/form-data';
    // swagger-client does not thoroughly create FormData,
    // e.g. will not handle arrays or nested objects
    // so we replace it here
    const formData = objectToFormData(requestBody, {
      indices: false,
    });
    request.body = formData;
  }
  return request;
};
