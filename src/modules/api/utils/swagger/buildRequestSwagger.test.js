/* @flow */

import buildRequestSwagger from './buildRequestSwagger';
import mockSwaggerJson from '../../__fixtures__/swagger';
import mockOpenAPIJson from '../../__fixtures__/openapi';

export const buildAuthLoginRequest = (spec: any) =>
  buildRequestSwagger({
    spec,
    operationId: 'authLogin',
    parameters: { username: 'test@example.com', password: 'password123' },
  });

export const buildIncidentsCreateRequest = (spec: any) =>
  buildRequestSwagger({
    spec,
    operationId: 'incidentsCreate',
    parameters: { type: { type: 'test' } },
  });

describe('api util swagger buildRequestSwagger', () => {
  it('swagger 2 spec', async () => {
    expect(buildAuthLoginRequest(mockSwaggerJson)).toMatchSnapshot();
    expect(buildIncidentsCreateRequest(mockSwaggerJson)).toMatchSnapshot();
  });
  it('swagger 3 spec', async () => {
    expect(buildAuthLoginRequest(mockOpenAPIJson)).toMatchSnapshot();
    expect(buildIncidentsCreateRequest(mockOpenAPIJson)).toMatchSnapshot();
  });
});
