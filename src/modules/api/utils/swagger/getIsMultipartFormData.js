/* @flow */

import _get from 'lodash.get';

import { isString, isArray, isPlainObject } from '../../../../utils/is';

import { getOperation } from './getOperation';

// check for operation using FormData

export const getIsMultipartFormDataForOperationId = (
  spec: any,
  operationId: string
) => {
  const operation = getOperation(spec, operationId);
  return getIsMultipartFormDataForOperation(operation);
};

export const getIsMultipartFormDataForOperation = (operation: any) => {
  if (operation) {
    // Swagger 2
    const consumes = _get(operation, 'consumes');
    if (consumes) {
      if (
        (isString(consumes) && consumes === 'multipart/form-data') ||
        (isArray(consumes) && consumes.includes('multipart/form-data'))
      ) {
        return true;
      }
    }
    // OAS3
    const requestBodyContent = _get(operation, 'requestBody.content');
    if (isPlainObject(requestBodyContent)) {
      const requestBodyMediaTypes = Object.keys(requestBodyContent);
      if (requestBodyMediaTypes.includes('multipart/form-data')) {
        return true;
      }
    }
  }
  return false;
};
