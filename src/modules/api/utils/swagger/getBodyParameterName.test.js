/* @flow */

import {
  getBodyParameterNameForOperation,
  getBodyParameterNameForOperationId,
} from './getBodyParameterName';
import mockSwaggerJson from '../../__fixtures__/swagger';
import mockOpenAPIJson from '../../__fixtures__/openapi';

const operationOne = {
  parameters: [
    {
      in: 'path',
      name: 'id',
    },
    {
      in: 'body',
      name: 'body',
      schema: {
        type: 'object',
        properties: {
          data: { type: 'string' },
        },
      },
    },
  ],
};

const operationTwo = {
  parameters: [
    {
      in: 'path',
      name: 'id',
    },
    {
      in: 'body',
      name: 'foo',
      schema: {
        type: 'object',
        properties: {
          data: { type: 'string' },
        },
      },
    },
  ],
};

const operationThree = {
  parameters: [
    {
      in: 'formData',
      name: 'avatar',
      type: 'string',
      format: 'binary',
    },
  ],
};

describe('api util swagger getBodyParameterName', () => {
  it('swagger 2 spec', async () => {
    expect(
      getBodyParameterNameForOperationId(mockSwaggerJson, 'authLogin')
    ).toEqual('credentials');
    expect(
      getBodyParameterNameForOperationId(
        mockSwaggerJson,
        'currentUserAvatarUpdate'
      )
    ).toEqual('avatar');
    expect(
      getBodyParameterNameForOperationId(mockSwaggerJson, 'incidentsCreate')
    ).toBeUndefined();
    expect(
      getBodyParameterNameForOperationId(mockSwaggerJson, 'incidentsSearch')
    ).toBeUndefined();
  });

  it('openapi 3 spec', async () => {
    expect(
      getBodyParameterNameForOperationId(mockOpenAPIJson, 'authLogin')
    ).toBeUndefined();
  });

  it('should name body appropriately', async () => {
    expect(getBodyParameterNameForOperation(operationOne)).toEqual('body');
    expect(getBodyParameterNameForOperation(operationTwo)).toEqual('foo');
    expect(getBodyParameterNameForOperation(operationThree)).toEqual('avatar');
  });
});
