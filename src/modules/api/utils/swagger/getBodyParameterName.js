/* @flow */

import _get from 'lodash.get';

import { isSwagger2 } from './is';
import { getOperation } from './getOperation';

// find the first parameter 'in' name with type either 'body' or 'formData'
// we have to wrap our payload with this name in order for buildRequest()
// to match it up correctly where we are using "name": "incident" etc.

// swagger spec examples always use "name": "body" to avoid this issue

/*
"parameters": [
  {
    "in": "formData",
    "name": "avatar",
    "description": "The users avatar",
    "type": "string",
    "format": "binary",
    "required": true
  }
],

"parameters": [
  {
    "in": "body",
    "name": "credentials",
    "description": "The user credentials",
    "schema": {
      "type": "object",
      "required": ["email", "password"],
      "properties": {
        "email": { "type": "string" },
        "password": { "type": "string" }
      },
      "example": {
        "email": "mark@makeandship.com",
        "password": "password"
      }
    }
  }
],
*/

export const getBodyParameterNameForOperationId = (
  spec: any,
  operationId: string
) => {
  if (!isSwagger2(spec)) {
    return;
  }
  const operation = getOperation(spec, operationId);
  return getBodyParameterNameForOperation(operation);
};

export const getBodyParameterNameForOperation = (operation: any) => {
  if (operation && operation.parameters) {
    // only check formData or body
    const bodyOrFormDataParameters = operation.parameters.filter(
      (parameter) => parameter.in === 'formData' || parameter.in === 'body'
    );
    // only if there is a single formData or body parameter
    if (bodyOrFormDataParameters.length === 1) {
      // only if either in body, has a schema and is an object,
      // or is formData
      const parameter = bodyOrFormDataParameters[0];
      const parameterIn = parameter.in;
      const parameterSchemaType = _get(parameter, 'schema.type');
      if (parameterIn) {
        if (
          parameterIn === 'formData' ||
          (parameterIn === 'body' && parameterSchemaType === 'object')
        ) {
          return parameter.name;
        }
      }
    }
  }
};
