/* @flow */

import { parametersWithBodyNameForOperation } from './parametersWithBodyName';

const operationOne = {
  parameters: [
    {
      in: 'path',
      name: 'id',
    },
    {
      in: 'body',
      name: 'body',
      schema: {
        type: 'object',
        properties: {
          data: { type: 'string' },
        },
      },
    },
  ],
};

const operationTwo = {
  parameters: [
    {
      in: 'path',
      name: 'id',
    },
    {
      in: 'body',
      name: 'foo',
      schema: {
        type: 'object',
        properties: {
          data: { type: 'string' },
        },
      },
    },
  ],
};

describe('api util swagger parametersWithBodyName', () => {
  it('should name body appropriately', async () => {
    expect(
      parametersWithBodyNameForOperation(
        { id: '123', data: 'test' },
        operationOne
      )
    ).toMatchSnapshot();
    expect(
      parametersWithBodyNameForOperation(
        { id: '123', body: { data: 'test' } },
        operationOne
      )
    ).toMatchSnapshot();
    expect(
      parametersWithBodyNameForOperation(
        { foo: { id: '123', data: 'test' } },
        operationOne
      )
    ).toMatchSnapshot();

    expect(
      parametersWithBodyNameForOperation(
        { id: '123', data: 'test' },
        operationTwo
      )
    ).toMatchSnapshot();
    expect(
      parametersWithBodyNameForOperation(
        { id: '123', body: { data: 'test' } },
        operationTwo
      )
    ).toMatchSnapshot();
    expect(
      parametersWithBodyNameForOperation(
        { foo: { id: '123', data: 'test' } },
        operationTwo
      )
    ).toMatchSnapshot();
  });
});
