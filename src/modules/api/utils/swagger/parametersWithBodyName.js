/* @flow */

import { isPlainObject } from '../../../../utils/is';

import { getOperation } from './getOperation';
import { getBodyParameterNameForOperation } from './getBodyParameterName';

// check for an operation with a single 'in body' value which we will want to rename

export const parametersWithBodyNameForOperationId = (
  parameters: any,
  spec: any,
  operationId: string
) => {
  const operation = getOperation(spec, operationId);
  return parametersWithBodyNameForOperation(parameters, operation);
};

export const parametersWithBodyNameForOperation = (
  parameters: any,
  operation: any
) => {
  if (!isPlainObject(parameters)) {
    return parameters;
  }
  // if provided, wrap into single body parameter name from the Swagger spec
  const name = getBodyParameterNameForOperation(operation);
  if (name) {
    const parameterKeys = Object.keys(parameters);
    // did we supply a parameter with the same name?
    if (parameterKeys.includes(name)) {
      // assume it is already correctly formatted
      return parameters;
    }
    const result = {};
    result[name] = parameters;
    if (parameters.id) {
      // unpack id so it can be 'in path'
      result.id = parameters.id;
    }
    return result;
  }
  return parameters;
};
