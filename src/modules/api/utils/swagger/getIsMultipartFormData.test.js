/* @flow */

import { getIsMultipartFormDataForOperationId } from './getIsMultipartFormData';
import mockSwaggerJson from '../../__fixtures__/swagger';
import mockOpenAPIJson from '../../__fixtures__/openapi';

describe('api util swagger getIsMultipartFormData', () => {
  it('swagger 2 spec', async () => {
    expect(
      getIsMultipartFormDataForOperationId(mockSwaggerJson, 'authLogin')
    ).toBeFalsy();
    expect(
      getIsMultipartFormDataForOperationId(mockSwaggerJson, 'incidentsCreate')
    ).toBeTruthy();
  });
  it('swagger 3 spec', async () => {
    expect(
      getIsMultipartFormDataForOperationId(mockOpenAPIJson, 'authLogin')
    ).toBeFalsy();
    expect(
      getIsMultipartFormDataForOperationId(mockOpenAPIJson, 'incidentsCreate')
    ).toBeTruthy();
  });
});
