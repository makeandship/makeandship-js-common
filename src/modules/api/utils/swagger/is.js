/* @flow */

import _get from 'lodash.get';

export const isOAS3 = (spec: any) => {
  const oasVersion = _get(spec, 'openapi');
  return oasVersion && oasVersion.startsWith('3');
};

export const isSwagger2 = (spec: any) => {
  const swaggerVersion = _get(spec, 'swagger');
  return swaggerVersion && swaggerVersion.startsWith('2');
};
