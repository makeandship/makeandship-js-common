/* @flow */

export const getOperation = (spec: any, operationId: string) => {
  const pathsKeys = Object.keys(spec.paths);
  for (let i = 0; i < pathsKeys.length; i++) {
    const operations = spec.paths[pathsKeys[i]];
    const operationsKeys = Object.keys(operations);
    for (let j = 0; j < operationsKeys.length; j++) {
      const operation = operations[operationsKeys[j]];
      if (operation.operationId === operationId) {
        return operation;
      }
    }
  }
};
