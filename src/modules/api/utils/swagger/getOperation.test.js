/* @flow */

import { getOperation } from './getOperation';
import mockSwaggerJson from '../../__fixtures__/swagger';
import mockOpenAPIJson from '../../__fixtures__/openapi';

describe('api util swagger getOperation', () => {
  it('swagger 2 spec', () => {
    expect(getOperation(mockSwaggerJson, 'authLogin')).toBeDefined();
  });
  it('openapi 3 spec', () => {
    expect(getOperation(mockOpenAPIJson, 'authLogin')).toBeDefined();
  });
});
