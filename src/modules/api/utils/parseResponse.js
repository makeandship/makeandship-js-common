/* @flow */

import JsonApiError from '../JsonApiError';

import { isValidJSON, isValidJSend } from '../../utils/isValid';
import parseJSend from '../../utils/parseJSend';

const parseResponse = async (response: any) => {
  if (!response || response === null) {
    return {
      error: new JsonApiError(0, 'Invalid response', response),
    };
  }

  let jsonApiError = new JsonApiError(
    response.status,
    response.statusText,
    response
  );

  try {
    const text = await response.text();
    if (isValidJSend(text)) {
      const { jsend, error, errors } = parseJSend(text);
      if (error) {
        jsonApiError = new JsonApiError(
          response.status,
          error,
          response,
          errors
        );
      } else {
        return { jsend };
      }
    } else if (isValidJSON(text)) {
      const json = JSON.parse(text);
      return { json };
    } else {
      jsonApiError = new JsonApiError(
        response.status,
        `Not a valid JSend or JSON response - ${text}`,
        response
      );
    }
  } catch (error) {
    // use default error
  }

  return { error: jsonApiError };
};

export default parseResponse;
