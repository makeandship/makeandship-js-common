import fetchToCurl from './fetchToCurl';

describe('api util.js fetchToCurl', () => {
  it('should handle bad input', () => {
    expect(fetchToCurl()).toMatchSnapshot();
    expect(fetchToCurl({})).toMatchSnapshot();
    expect(fetchToCurl(null)).toMatchSnapshot();
  });
  it('should convert fetch parameters to curl', () => {
    expect(fetchToCurl('http://localhost:3000/')).toMatchSnapshot();
    expect(
      fetchToCurl('http://localhost:3000/', {
        headers: { Authorization: 'Bearer abc123' },
      })
    ).toMatchSnapshot();
  });
});
