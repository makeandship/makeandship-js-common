/* @flow */

import { isPlainObject, isString } from '../../../utils/is';

import buildOptionsWithToken from './buildOptionsWithToken';

describe('api util.js buildOptionsWithToken', () => {
  it('builds basic options', () => {
    const options = buildOptionsWithToken({}, 'test-auth-token');
    expect(options.headers).toBeDefined();
    expect(options.headers['Accept']).toEqual('application/json');
    expect(options.headers['Authorization']).toEqual('Bearer test-auth-token');
    expect(options.headers['Content-Type']).toEqual('application/json');
    expect(options.body).toBeUndefined();
  });

  it('merges with provided headers', () => {
    const options = buildOptionsWithToken(
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
      'test-auth-token'
    );
    expect(options.headers).toBeDefined();
    expect(options.headers['Accept']).toEqual('application/json');
    expect(options.headers['Authorization']).toEqual('Bearer test-auth-token');
    // we don't want to set multipart/form-data, it will be set with correct 'boundary' by fetch
    expect(options.headers['Content-Type']).toBeUndefined();
    expect(options.body).toBeUndefined();
  });

  it('stringifies body if necessary', () => {
    const options = buildOptionsWithToken(
      { body: { test: true } },
      'test-auth-token'
    );
    expect(options.body).toBeDefined();
    expect(isString(options.body)).toBeTruthy();
  });

  it('does not stringify body if not necessary', () => {
    const options = buildOptionsWithToken(
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: { test: true },
      },
      'test-auth-token'
    );
    expect(options.body).toBeDefined();
    expect(isPlainObject(options.body)).toBeTruthy();
  });
});
