/* @flow */

import parseResponse from './parseResponse';
const { Response } = jest.requireActual('node-fetch');

describe('api util.js parseResponse', () => {
  it('should handle bad input', async () => {
    expect(await parseResponse()).toMatchSnapshot();
    expect(await parseResponse({})).toMatchSnapshot();
    expect(await parseResponse(null)).toMatchSnapshot();
  });

  it('should create expected errors', async () => {
    const okBaseResponse = new Response();

    const payload = { test: true };

    const okValidJSendResponse = new Response(
      JSON.stringify({ status: 'success', data: payload })
    );

    const okInvalidJSendResponse = new Response(
      JSON.stringify({ test: 'test' })
    );
    const errorResponse = new Response('', {
      status: 401,
    });
    const errorInvalidJSendResponse2 = new Response(
      JSON.stringify({ test: 'test' }),
      {
        status: 500,
      }
    );
    const errorJSendResponse1 = new Response(
      JSON.stringify({
        status: 'error',
        code: 'Error',
        message: 'Invalid country calling code',
      })
    );
    const errorJSendResponse2 = new Response(
      JSON.stringify({
        status: 'error',
        code: 'MongoError',
        message:
          'E11000 duplicate key error collection: mend.patients index: nhsNumber_1 dup key: { : "0123456789" }',
      }),
      {
        status: 500,
      }
    );
    const errorJSendResponse3 = new Response(
      JSON.stringify({
        status: 'error',
        message: 'Missing fields',
        data: {
          errors: {
            email: 'invalid email address',
          },
        },
      })
    );
    const errorJSendResponse4 = new Response(
      `{"status":"error","code":"Request failed with status code 401","message":401}`,
      {
        status: 401,
      }
    );
    expect(await parseResponse(okBaseResponse)).toMatchSnapshot();
    expect(await parseResponse(okValidJSendResponse)).toMatchSnapshot();
    expect(await parseResponse(okInvalidJSendResponse)).toMatchSnapshot();
    expect(await parseResponse(errorResponse)).toMatchSnapshot();
    expect(await parseResponse(errorInvalidJSendResponse2)).toMatchSnapshot();
    expect(await parseResponse(errorJSendResponse1)).toMatchSnapshot();
    expect(await parseResponse(errorJSendResponse2)).toMatchSnapshot();
    expect(await parseResponse(errorJSendResponse3)).toMatchSnapshot();

    // $FlowFixMe
    const { error } = await parseResponse(errorJSendResponse4);
    expect(error).toMatchSnapshot();
    expect(error.message).toEqual('401 - Unauthorized');
  });
});
