/* @flow */

import _merge from 'lodash.merge';
import { isArray, isPlainObject } from '../../../utils/is';

export const buildOptions = (options: any = {}) =>
  buildOptionsWithToken(options);

const buildOptionsWithToken = (
  options: any = {},
  token?: string | false
): any => {
  const defaults = {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };
  options = _merge({}, defaults, options);
  if (token) {
    options.headers['Authorization'] = `Bearer ${token}`;
  }
  if (
    options.body &&
    options.headers['Content-Type'].indexOf('application/json') !== -1
  ) {
    // stringify body if necessary
    if (isPlainObject(options.body) || isArray(options.body)) {
      options.body = JSON.stringify(options.body);
    }
  }

  // we don't want to set multipart/form-data, it will be set with correct 'boundary' by fetcher
  if (options.headers['Content-Type'].indexOf('multipart/form-data') !== -1) {
    delete options.headers['Content-Type'];
  }

  return options;
};

export default buildOptionsWithToken;
