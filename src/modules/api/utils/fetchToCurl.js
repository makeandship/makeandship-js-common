/* @flow */

import { isString, isPlainObject } from '../../../utils/is';

const fetchToCurl = (url: string, options: any = {}): string => {
  if (!isString(url)) {
    return '';
  }
  const { method, headers, body } = options;
  const components = ['curl', `'${url}'`];
  if (method) {
    components.push('-X');
    components.push(method);
  }
  if (isPlainObject(headers)) {
    Object.entries(headers).forEach(([key, value]) => {
      components.push('-H');
      components.push(`'${key}: ${value}'`);
    });
  }
  if (body) {
    components.push('--data-binary');
    components.push(`'${body}'`);
  }
  const curl = components.join(' ');
  return curl;
};

export default fetchToCurl;
