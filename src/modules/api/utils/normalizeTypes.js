/* @flow */

// convert any plain strings into actions of { type: string }

const normalizeTypes = (types: Array<*>): Array<*> =>
  types.map((type) => {
    if (typeof type === 'string') {
      return {
        type,
      };
    }
    return type;
  });

export default normalizeTypes;
