/* @flow */

import { put, take, call, select } from 'redux-saga/effects';
import { createAction } from '@reduxjs/toolkit';

import {
  actions as swaggerApiActions,
  getIsInitialised,
  getError,
} from './swaggerApi';
import mockSwaggerJson from './__fixtures__/swagger';
import { setConfig } from './config';
import { sagaMiddleware } from './__mocks__/store';
import { delay as delayPromise } from '../utils';

const apiUrl = 'http://localhost:3001';
const apiSpecUrl = 'http://localhost:3001/swagger.json';

setConfig({
  apiUrl,
  apiSpecUrl,
});

const mockActions = {
  request: createAction('REQUEST'),
  success: createAction('SUCCESS'),
  failure: createAction('FAILURE'),
};

const expectedHeaders = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

describe('api module Swagger', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('when the API spec response fails', () => {
    it('should set error state', async (done) => {
      const saga = function* () {
        const errorFetcher = jest.fn(() => {
          throw 'Mock error';
        });
        setConfig({
          fetcher: errorFetcher,
        });
        const action = swaggerApiActions.callSwaggerApi({
          operationId: 'authLogin',
          types: [
            mockActions.request,
            mockActions.success,
            mockActions.failure,
          ],
        });
        yield put(action);
        yield take(swaggerApiActions.initialiseRequest);
        yield take(swaggerApiActions.initialiseError);
        expect(errorFetcher).toHaveBeenCalledWith(apiSpecUrl);
        const error = yield select(getError);
        expect(error.toString()).toEqual(
          `Error: Could not fetch API spec from ${apiSpecUrl}`
        );
        yield call(done);
      };
      sagaMiddleware.run(saga);
    });
  });

  describe('when the API spec response succeeds', () => {
    describe('when the response succeeds', () => {
      it('success action is dispatched with data payload', async (done) => {
        const saga = function* () {
          // yield takeEvery('*', function* (action) {
          //   console.log({ action });
          // });

          const loginParameters = {
            email: 'test@example.com',
            password: 'password123',
          };
          const loginPayload = {
            email: 'test@example.com',
            id: 'test-id',
            token: 'test-token',
          };
          const fetcher = jest.fn(async (url) => {
            await delayPromise(500);
            if (url.endsWith('/swagger.json')) {
              return mockSwaggerJson;
            }
            if (url.endsWith('/auth/login')) {
              return loginPayload;
            }
            throw `Unmatched test route ${url}`;
          });

          setConfig({ fetcher });

          const action = swaggerApiActions.callSwaggerApi({
            operationId: 'authLogin',
            parameters: loginParameters,
            types: [
              mockActions.request,
              mockActions.success,
              mockActions.failure,
            ],
          });

          yield put(action);

          yield take(swaggerApiActions.initialiseSuccess);
          expect(fetcher).toHaveBeenCalledWith(apiSpecUrl);
          const isInitialised = yield select(getIsInitialised);
          expect(isInitialised).toBeTruthy();

          const result = yield take(mockActions.success);
          expect(fetcher).toHaveBeenCalledTimes(2);
          expect(fetcher).toHaveBeenCalledWith(`${apiUrl}/auth/login`, {
            method: 'POST',
            headers: expectedHeaders,
            body: JSON.stringify(loginParameters),
          });
          expect(result.payload).toEqual(loginPayload);

          yield call(done);
        };
        sagaMiddleware.run(saga);
      });
    });
  });
});
