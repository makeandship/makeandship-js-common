/* @flow */

export type JSendType = {
  status: string,
  code?: number,
  data?: any,
  message?: string,
};
