/* @flow */

import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';

import api, { rootApiSaga } from '../index';

export const sagaMiddleware = createSagaMiddleware();
const store = configureStore({
  reducer: { api },
  middleware: [sagaMiddleware],
});
sagaMiddleware.run(rootApiSaga);

export default store;
