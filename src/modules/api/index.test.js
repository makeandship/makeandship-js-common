/* @flow */

import { put, take, call } from 'redux-saga/effects';
import axios from 'axios';
import AxiosMockAdapter from 'axios-mock-adapter';

import { sagaMiddleware } from './__mocks__/store';
import { createAxiosFetcher } from '../fetchers/axiosFetcher';
import jsendResponseTransformer from '../transformers/jsendResponseTransformer';

import okValidJSendResponse from '../utils/__fixtures__/jsend-success-1.json';
import errorJSendResponse1 from '../utils/__fixtures__/jsend-error-1.json';
import errorJSendResponse3 from '../utils/__fixtures__/jsend-error-3.json';

import { setConfig } from './config';

const apiUrl = 'http://localhost:3001';
const apiSpecUrl = 'http://localhost:3001/swagger.json';

const mockAxios = new AxiosMockAdapter(axios);
const axiosInstance = axios.create({
  baseURL: apiUrl,
  transformResponse: [jsendResponseTransformer],
});
const fetcher = createAxiosFetcher(axiosInstance);

setConfig({
  apiUrl,
  apiSpecUrl,
  fetcher,
});

const REQUEST = 'REQUEST';
const SUCCESS = 'SUCCESS';
const FAILURE = 'FAILURE';

import { actions } from './jsonApi';

describe('api module with axiosFetcher and jsendResponseTransformer', () => {
  afterEach(() => {
    jest.clearAllMocks();
    mockAxios.reset();
  });

  describe('when there is an error', () => {
    it('should return an error', async (done) => {
      const saga = function* () {
        mockAxios.onGet('/error').reply(500);
        const action = actions.callJsonApi({
          url: `${apiUrl}/error`,
          types: [REQUEST, SUCCESS, FAILURE],
        });
        yield put(action);
        const { payload } = yield take(FAILURE);
        expect(payload).toMatchInlineSnapshot(
          `[Error: Request failed with status code 500]`
        );
        yield call(done);
      };
      sagaMiddleware.run(saga);
    });
  });

  describe('when the response is json', () => {
    describe('when the status is success', () => {
      it('should return the json', async (done) => {
        const json = { foo: 'bar' };
        mockAxios.onGet('/test').reply(200, json);
        const saga = function* () {
          const action = actions.callJsonApi({
            url: `${apiUrl}/test`,
            types: [REQUEST, SUCCESS, FAILURE],
          });
          yield put(action);
          const { payload } = yield take(SUCCESS);
          expect(payload).toEqual(json);
          yield call(done);
        };
        sagaMiddleware.run(saga);
      });
    });
  });

  describe('when the response is jsend', () => {
    describe('when the status is success', () => {
      it('should return the jsend data', async (done) => {
        const saga = function* () {
          mockAxios.onGet('/test').reply(200, okValidJSendResponse);
          const action = actions.callJsonApi({
            url: `${apiUrl}/test`,
            types: [REQUEST, SUCCESS, FAILURE],
          });
          yield put(action);
          const { payload } = yield take(SUCCESS);
          expect(payload).toEqual(okValidJSendResponse.data);
          yield call(done);
        };
        sagaMiddleware.run(saga);
      });
    });

    describe('when the status is error', () => {
      describe('when error is a string', () => {
        it('should return the jsend error', async (done) => {
          const saga = function* () {
            mockAxios.onGet('/test').reply(200, errorJSendResponse1);
            const action = actions.callJsonApi({
              url: `${apiUrl}/test`,
              types: [REQUEST, SUCCESS, FAILURE],
            });
            yield put(action);
            const { payload } = yield take(FAILURE);
            expect(payload).toMatchInlineSnapshot(
              `[JsendResponseTransformerError: Invalid country calling code]`
            );
            yield call(done);
          };
          sagaMiddleware.run(saga);
        });
      });
      describe('when error is an object', () => {
        it('should return the jsend error', async (done) => {
          const saga = function* () {
            mockAxios.onGet('/test').reply(200, errorJSendResponse3);
            const action = actions.callJsonApi({
              url: `${apiUrl}/test`,
              types: [REQUEST, SUCCESS, FAILURE],
            });
            yield put(action);
            const failure = yield take(FAILURE);
            console.log({ failure });
            const { payload } = failure;
            expect(payload).toMatchInlineSnapshot(
              `[JsendResponseTransformerError: Missing fields]`
            );
            expect(payload.errors).toMatchInlineSnapshot(`
              Object {
                "email": "invalid email address",
              }
            `);
            yield call(done);
          };
          sagaMiddleware.run(saga);
        });
      });
    });
  });
});
