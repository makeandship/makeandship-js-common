/* @flow */

import makeConfig from '../utils/makeConfig';
import fetcher from '../fetchers/axiosFetcher';

const defaultConfig = {
  fetcher,
  apiUrl: null,
  apiSpecUrl: null,
  debug: false,
};

const { getConfig, setConfig, resetConfig } = makeConfig(defaultConfig);

export { getConfig, setConfig, resetConfig };
