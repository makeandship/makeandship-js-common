/* @flow */

import { buffers, channel } from 'redux-saga';
import {
  fork,
  call,
  take,
  put,
  select,
  race,
  cancel,
  cancelled,
  actionChannel,
  apply,
} from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import { createSelector } from 'reselect';
import { createAction, createReducer } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

import { fetchToCurl, normalizeTypes, buildOptions } from './utils';

import { getConfig } from './config';

export const typePrefix = 'api/jsonApi/';

export const actions = {
  callJsonApi: createAction(`${typePrefix}CALL_JSON_API`),
  pause: createAction(`${typePrefix}PAUSE`),
  resume: createAction(`${typePrefix}RESUME`),
  flush: createAction(`${typePrefix}FLUSH`),
  error: createAction(`${typePrefix}ERROR`),
};

// Selectors

export const getState = (state: any) => state?.api?.jsonApi;

export const getIsPaused = createSelector(
  getState,
  (jsonApi) => jsonApi.isPaused
);

// Reducer

type State = {
  isPaused: boolean,
};

const initialState: State = {
  isPaused: false,
};

export const reducer = createReducer(initialState, {
  [actions.pause]: (state) => {
    state.isPaused = true;
  },
  [actions.resume]: (state) => {
    state.isPaused = false;
  },
});

// Side effects

// Instances

export const jsonApiActionChannel = channel();

export function* jsonApiActionChannelWatcher(): Saga {
  while (true) {
    const action = yield take(jsonApiActionChannel);
    yield put(action);
  }
}

// execute and yield { success, failure } for an api call

export function* callJsonApiWithResponse(parameters: any): Saga {
  const [REQUEST, SUCCESS, FAILURE, ABORT] = normalizeTypes(parameters.types); // eslint-disable-line
  const action = yield call(
    executeJsonApiAction,
    actions.callJsonApi(parameters)
  );
  let success, failure, abort;
  if (action.type === SUCCESS.type) {
    success = action;
  } else if (action.type === FAILURE.type) {
    failure = action;
  } else if (ABORT && action.type === ABORT.type) {
    abort = action;
  }
  return yield { success, failure, abort };
}

export function* jsonApiWatcher(): Saga {
  const chan = yield actionChannel(actions.callJsonApi, buffers.expanding(10));
  while (true) {
    try {
      const action = yield take(chan);
      if (action.shouldExecuteJsonApiAction) {
        // block queue for conditional calls
        // this allows state changes e.g. isFetching
        // to propagate and be read correctly by subsequent
        // calls to shouldExecuteJsonApiAction()
        yield call(jsonApiWorker, action);
      } else {
        // non-blocking call
        yield fork(jsonApiWorker, action);
      }
    } catch (error) {
      if (getConfig().debug) {
        console.error(error);
      }
    }
  }
}

// pause or cancel requests as appropriate

export function* jsonApiWorker(action: PayloadAction): Saga {
  const isPaused = yield select(getIsPaused);
  if (isPaused) {
    const { cancelTask } = yield race({
      resume: take(actions.resume),
      cancelTask: take(actions.flush),
    });
    if (cancelTask) {
      yield cancel();
    }
  }
  const { pause, cancelTask } = yield race({
    complete: call(executeJsonApiAction, action),
    pause: take(actions.pause),
    cancelTask: take(actions.flush),
  });
  if (cancelTask) {
    yield cancel();
  } else if (pause) {
    // invoke jsonApiWorker again
    yield put(action);
  }
}

/**
 * Json API call saga. Builds and make the request from the supplied parameters, adding authentication token.
 *
 * @param  {Object} action
 * @param  {String} action.url
 * @param  {Array} action.types Array of action types to dispatch at each stage of request [REQUEST, SUCCESS, FAILURE]
 */

export function* executeJsonApiAction(action: PayloadAction): Saga {
  const { url, types, shouldExecuteJsonApiAction, meta } = action.payload;

  const [
    REQUEST,
    SUCCESS,
    FAILURE,
    ABORT,
    UPLOAD_PROGRESS,
    DOWNLOAD_PROGRESS,
  ] = normalizeTypes(types);

  let fetcherPromise;

  try {
    const config = yield call(getConfig);
    if (shouldExecuteJsonApiAction) {
      const shouldExecute = yield call(shouldExecuteJsonApiAction, action);
      if (!shouldExecute) {
        if (getConfig().debug) {
          console.log('Not executing');
        }
        return yield put({ ...ABORT, meta });
      }
    }

    yield put({ ...REQUEST, meta });

    const options = buildOptions(action.payload.options);

    if (config.debug) {
      console.log(`fetch ${url} options`, options);
      const curl = fetchToCurl(url, options);
      console.log(curl);
    }

    // add Axios download events

    const updateProgress = (action, { loaded, total }) => {
      const fraction = loaded / total;
      const percent = Math.floor(100 * fraction);
      jsonApiActionChannel.put({
        type: action.type,
        payload: { loaded, total, fraction, percent },
        meta,
      });
    };

    const onDownloadProgress = (event) => {
      updateProgress(DOWNLOAD_PROGRESS, event);
    };

    const onUploadProgress = (event) => {
      updateProgress(UPLOAD_PROGRESS, event);
    };

    options.onDownloadProgress = onDownloadProgress;
    options.onUploadProgress = onUploadProgress;

    // make the request

    fetcherPromise = config.fetcher(url, options);
    const data = yield fetcherPromise;

    if (config.debug) {
      console.log('data', JSON.stringify(data, null, 2));
    }

    return yield put({ ...SUCCESS, payload: data, meta });
  } catch (error) {
    yield put(actions.error(error));
    return yield put({ ...FAILURE, payload: error, meta });
  } finally {
    if (yield cancelled()) {
      if (fetcherPromise?.cancel) {
        yield apply(fetcherPromise, 'cancel');
      }
      yield put({ ...ABORT, meta });
    }
  }
}

export function* saga(): Saga {
  yield fork(jsonApiActionChannelWatcher);
  yield fork(jsonApiWatcher);
}
