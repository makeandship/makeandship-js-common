/* @flow */

import { put, take, call, delay } from 'redux-saga/effects';
import { createAction } from '@reduxjs/toolkit';

import { setConfig } from './config';
import { sagaMiddleware } from './__mocks__/store';
import {
  actions,
  callJsonApiWithResponse,
  executeJsonApiAction,
} from './jsonApi';

const apiUrl = 'http://localhost:3001';
const apiSpecUrl = 'http://localhost:3001/swagger.json';

setConfig({
  apiUrl,
  apiSpecUrl,
});

const mockActions = {
  request: createAction('REQUEST'),
  success: createAction('SUCCESS'),
  failure: createAction('FAILURE'),
  abort: createAction('ABORT'),
};

const expectedHeaders = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

describe('api module', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('GET request', () => {
    const parameters = {
      url: `${apiUrl}/test`,
      types: [
        mockActions.request,
        mockActions.success,
        mockActions.failure,
        mockActions.abort,
      ],
    };
    const action = actions.callJsonApi(parameters);

    describe('when using actions', () => {
      describe('when the response succeeds', () => {
        it('success action is dispatched with data payload', async (done) => {
          const saga = function* () {
            // yield takeEvery('*', function* (action) {
            //   console.log({ action });
            // });

            const fetcher = jest.fn(() => 'data');
            setConfig({
              fetcher,
            });

            yield put(action);
            yield take(mockActions.request);
            const result = yield take(mockActions.success);
            expect(fetcher).toHaveBeenCalledWith(`${apiUrl}/test`, {
              headers: expectedHeaders,
            });
            expect(result.payload).toEqual('data');
            yield call(done);
          };
          sagaMiddleware.run(saga);
        });
      });

      describe('when the response fails', () => {
        it('error action is dispatched with error payload', async (done) => {
          const saga = function* () {
            const errorFetcher = jest.fn(() => {
              throw 'Mock error';
            });
            setConfig({
              fetcher: errorFetcher,
            });
            yield put(action);
            yield take(mockActions.request);
            const result = yield take(mockActions.failure);
            expect(errorFetcher).toHaveBeenCalledWith(`${apiUrl}/test`, {
              headers: expectedHeaders,
            });
            expect(result.payload.toString()).toEqual('Mock error');
            yield call(done);
          };
          sagaMiddleware.run(saga);
        });
      });

      describe('when the request is cancelled', () => {
        it('fetcher cancel function is called and abort action is dispatched', async (done) => {
          const saga = function* () {
            const cancel = jest.fn();
            const slowFetcher = jest.fn(() => {
              const promise = new Promise((resolve) => {
                setTimeout(() => {
                  resolve('data');
                }, 1000);
              });
              // $FlowFixMe
              promise.cancel = cancel;
              return promise;
            });

            setConfig({
              fetcher: slowFetcher,
            });
            yield put(action);
            yield delay(500);
            yield put(actions.flush());
            const result = yield take(mockActions.abort);
            expect(slowFetcher).toHaveBeenCalledWith(`${apiUrl}/test`, {
              headers: expectedHeaders,
            });
            expect(cancel).toHaveBeenCalled();
            expect(result.type).toEqual(mockActions.abort.toString());
            yield call(done);
          };
          sagaMiddleware.run(saga);
        });
      });
    });

    describe('when using saga', () => {
      describe('executeJsonApiAction', () => {
        describe('when the response succeeds', () => {
          it('yields success action', async (done) => {
            const saga = function* () {
              const fetcher = jest.fn(() => 'data');
              setConfig({
                fetcher,
              });
              const result = yield call(executeJsonApiAction, action);
              expect(fetcher).toHaveBeenCalledWith(`${apiUrl}/test`, {
                headers: expectedHeaders,
              });
              expect(result.payload).toEqual('data');
              yield call(done);
            };
            sagaMiddleware.run(saga);
          });
        });

        describe('when the response fails', () => {
          it('yields error action', async (done) => {
            const saga = function* () {
              const errorFetcher = jest.fn(() => {
                throw 'Mock error';
              });
              setConfig({
                fetcher: errorFetcher,
              });
              const result = yield call(executeJsonApiAction, action);
              expect(errorFetcher).toHaveBeenCalledWith(`${apiUrl}/test`, {
                headers: expectedHeaders,
              });
              expect(result.type).toEqual(mockActions.failure.toString());
              expect(result.payload.toString()).toEqual('Mock error');
              yield call(done);
            };
            sagaMiddleware.run(saga);
          });
        });
      });

      describe('callJsonApiWithResponse', () => {
        describe('when the response succeeds', () => {
          it('yields success action', async (done) => {
            const saga = function* () {
              const fetcher = jest.fn(() => 'data');
              setConfig({
                fetcher,
              });
              const result = yield call(callJsonApiWithResponse, parameters);
              expect(fetcher).toHaveBeenCalledWith(`${apiUrl}/test`, {
                headers: expectedHeaders,
              });
              expect(result.success).toBeDefined();
              expect(result.failure).toBeUndefined();
              expect(result.success.payload).toEqual('data');
              yield call(done);
            };
            sagaMiddleware.run(saga);
          });
        });
      });
    });
  });
});
