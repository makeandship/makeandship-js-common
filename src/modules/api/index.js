/* @flow */

import { all, fork } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import { combineReducers } from 'redux';

export { actions as swaggerApiActions } from './swaggerApi';
export {
  actions as jsonApiActions,
  callJsonApiWithResponse,
  getIsPaused,
} from './jsonApi';
export { default as JsonApiError } from './JsonApiError';
export { setConfig } from './config';

import { reducer as jsonApi, saga as jsonApiSaga } from './jsonApi';
import { reducer as swaggerApi, saga as swaggerApiSaga } from './swaggerApi';

const apiReducer = combineReducers({
  jsonApi,
  swaggerApi,
});

export default apiReducer;

export function* rootApiSaga(): Saga {
  yield all([fork(swaggerApiSaga), fork(jsonApiSaga)]);
}
