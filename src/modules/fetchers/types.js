// @flow

export type Fetcher = (url: string, options: any) => Promise<*>;
