/* @flow */

import axios from 'axios';
import type { AxiosInstance } from 'axios';
import produce from 'immer';

import type { Fetcher } from './types';

// reshape fetch config into axios config
const makeAxiosConfig = (url: string, options: any = {}, cancelToken: any) => {
  return produce(options, (draft) => {
    draft.url = url;
    draft.cancelToken = cancelToken;
    draft.data = draft.body;
    delete draft.body;
  });
};

export const createAxiosFetcher = (axiosInstance: AxiosInstance = axios) => {
  const fetcher: Fetcher = (url: string, options: any): Promise<*> => {
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();

    const promise = new Promise((resolve, reject) => {
      const config = makeAxiosConfig(url, options, source.token);
      axiosInstance(config)
        .then((response) => {
          const data = response?.data;
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });

    // Cancel the request if the `promise.cancel` method is called
    // $FlowFixMe
    promise.cancel = () => {
      source.cancel();
    };

    return promise;
  };
  return fetcher;
};

export default createAxiosFetcher();
