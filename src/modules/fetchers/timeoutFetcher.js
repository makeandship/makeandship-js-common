/* @flow */

import type { Fetcher } from './types';

export type Options = {
  method: string,
  timeout: number,
};

const timeoutFetcher: Fetcher = (
  url: string,
  options: Options = {}
): Promise<*> => {
  const xhr = new XMLHttpRequest();

  const promise = new Promise((resolve: any, reject: any) => {
    const timeout = options.timeout || 30000;
    const method = options.method || 'GET';

    const timeoutId = setTimeout(() => {
      xhr.abort();
      reject('timeout');
    }, timeout);

    xhr.open(method, url);
    xhr.onload = () => {
      if (xhr.status >= 200 && xhr.status < 300) {
        clearTimeout(timeoutId);
        resolve(xhr.response);
      } else {
        clearTimeout(timeoutId);
        reject({
          status: xhr.status,
          statusText: xhr.statusText,
        });
      }
    };
    xhr.onerror = () => {
      clearTimeout(timeoutId);
      reject({
        status: xhr.status,
        statusText: xhr.statusText,
      });
    };
    xhr.send();
  });

  // Cancel the request if the `promise.cancel` method is called
  // $FlowFixMe
  promise.cancel = () => {
    xhr.abort();
  };

  return promise;
};

export default timeoutFetcher;
