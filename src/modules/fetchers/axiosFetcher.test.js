/* @flow */

import axios from 'axios';
import AxiosMockAdapter from 'axios-mock-adapter';

import fetcher from './axiosFetcher';

const mockAxios = new AxiosMockAdapter(axios);

describe('axiosFetcher', () => {
  afterEach(() => {
    mockAxios.reset();
  });

  describe('when request is cancelled', () => {
    it('throws', async () => {
      try {
        mockAxios.onGet('/foo/bar').reply(
          () =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve(200);
              }, 1000);
            })
        );
        const promise = fetcher('/foo/bar');
        // $FlowFixMe
        setTimeout(() => promise.cancel(), 500);
        await promise;
      } catch (error) {
        expect(axios.isCancel(error)).toBeTruthy();
      }
    });
  });

  describe('when response is successful', () => {
    it('returns data', async () => {
      const mockData = { foo: 'bar' };
      mockAxios.onGet('/foo/bar').reply(200, mockData);
      const promise = fetcher('/foo/bar');
      const data = await promise;
      expect(data).toEqual(mockData);
    });
  });

  describe('when response has an error', () => {
    it('throws', async () => {
      try {
        mockAxios.onGet('/foo/bar').reply(500);
        const promise = fetcher('/foo/bar');
        await promise;
      } catch (e) {
        expect(e).toMatchInlineSnapshot(
          `[Error: Request failed with status code 500]`
        );
      }
    });
  });
});
