/* @flow */

import { isValidJSON, isValidJSend } from './isValid';

describe('api util.js isValid', () => {
  it('should handle bad input', async () => {
    expect(isValidJSON()).toBeFalsy();
    expect(isValidJSON('')).toBeFalsy();
    expect(isValidJSON(null)).toBeFalsy();
    expect(isValidJSON(123)).toBeFalsy();

    expect(isValidJSend()).toBeFalsy();
    expect(isValidJSend('')).toBeFalsy();
    expect(isValidJSend(null)).toBeFalsy();
    expect(isValidJSend(123)).toBeFalsy();
    expect(isValidJSend('123')).toBeFalsy();
  });

  it('should validate JSON', () => {
    expect(isValidJSON({ foo: 'bar' })).toBeFalsy();
    expect(isValidJSON(`{foo:`)).toBeFalsy();
    expect(isValidJSON(JSON.stringify({ foo: 'bar' }))).toBeTruthy();
  });

  it('should validate JSend', () => {
    expect(isValidJSend({ foo: 'bar' })).toBeFalsy();
    expect(isValidJSend(`{foo:`)).toBeFalsy();
    expect(isValidJSend(JSON.stringify({ foo: 'bar' }))).toBeFalsy();
    expect(
      isValidJSend(JSON.stringify({ status: 'success', data: { foo: 'bar' } }))
    ).toBeTruthy();
  });
});
