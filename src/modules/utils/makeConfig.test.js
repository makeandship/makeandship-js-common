/* @flow */

import makeConfig from './makeConfig';

describe('makeConfig', () => {
  describe('when invalid', () => {
    describe('when no arguments are provided', () => {
      it('returns empty object', () => {
        const { getConfig } = makeConfig();
        expect(getConfig()).toEqual({});
      });
    });
    describe('when invalid arguments are provided', () => {
      it('returns empty object', () => {
        const { getConfig } = makeConfig();
        expect(getConfig()).toEqual({});
      });
    });
  });
  describe('when valid', () => {
    const defaultConfig = {
      foo: 'a',
      bar: 'z',
    };
    const { getConfig, setConfig, resetConfig } = makeConfig(defaultConfig);
    it('sets default config', () => {
      expect(getConfig()).toEqual(defaultConfig);
    });
    it('overrides default config', () => {
      setConfig({ bar: 'b' });
      expect(getConfig()).toEqual({
        foo: 'a',
        bar: 'b',
      });
      setConfig({ foo: 'z' });
      expect(getConfig()).toEqual({
        foo: 'z',
        bar: 'b',
      });
    });
    it('resets to default config', () => {
      resetConfig();
      expect(getConfig()).toEqual(defaultConfig);
    });
  });
});
