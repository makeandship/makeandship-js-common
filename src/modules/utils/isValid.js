/* @flow */

import { isString } from '../../utils/is';

export const isValidJSON = (expectedJson: string): boolean => {
  if (!isString(expectedJson)) {
    return false;
  }
  try {
    JSON.parse(expectedJson);
  } catch (e) {
    return false;
  }
  return true;
};

export const isValidJSend = (expectedJsend: string): boolean => {
  if (!isValidJSON(expectedJsend)) {
    return false;
  }
  const jsend = JSON.parse(expectedJsend);
  if (jsend.status && (jsend.data || jsend.message)) {
    return true;
  }
  return false;
};
