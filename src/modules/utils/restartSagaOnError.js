/* @flow */

import { spawn, call, delay } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';

export const restartSagaOnError = (saga: Saga) =>
  function* (): Saga {
    // Using spawn (instead of fork) spins up each saga in its own 'process'
    // meaning that an error propagating up through one of sagas, won't bring
    // down the root saga
    yield spawn(function* () {
      while (true) {
        try {
          // Try and call the saga. We expect these top level sagas to be
          // blocking and run for the lifetime of the app, so we should never
          // reach the next line after this yield
          yield call(saga);
          console.error(
            `Unexpected root saga termination in ${saga.name}`,
            saga
          );
        } catch (e) {
          // A top-level saga errored out
          console.error('Saga error, will be restarted', e);
        }
        yield delay(1000); // Avoid infinite failures blocking app
      }
    });
  };
