/* @flow */

import { isPlainObject } from '../../utils/is';

import { isValidJSend } from './isValid';

const parseJSend = (
  expectedJsend: any
): { jsend: any, error?: string, errors?: any } => {
  if (!isValidJSend(expectedJsend)) {
    throw 'Invalid JSend';
  }

  const jsend = JSON.parse(expectedJsend);

  if (jsend.status === 'success') {
    // no error
    return { jsend };
  }

  const error =
    jsend.message ||
    jsend.data ||
    `JSend status is not 'success', unable to parse error`;

  if (jsend.data?.errors) {
    // unwrap errors if they are not plain strings
    const errors = {};
    Object.keys(jsend.data.errors).forEach((key) => {
      const error = jsend.data.errors[key];
      if (isPlainObject(error) && error.message) {
        errors[key] = error.message;
      } else {
        errors[key] = error;
      }
    });
    return { jsend, error, errors };
  }

  return {
    jsend,
    error,
  };
};

export default parseJSend;
