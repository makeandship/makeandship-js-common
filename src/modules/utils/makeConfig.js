/* @flow */

const makeConfig = (defaultConfig: any) => {
  const config: any = {
    ...defaultConfig,
  };

  const getConfig = (): any => {
    return config;
  };

  const setConfig = (newConfig: any): any => {
    Object.assign(config, newConfig);
    return config;
  };

  const resetConfig = (): any => {
    setConfig(defaultConfig);
    return config;
  };

  return {
    getConfig,
    setConfig,
    resetConfig,
  };
};

export default makeConfig;
