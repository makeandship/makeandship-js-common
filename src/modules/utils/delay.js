/* @flow */

const delay = (time: number): Promise<*> =>
  new Promise((resolve) => setTimeout(resolve, time));

export default delay;
