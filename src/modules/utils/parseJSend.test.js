/* @flow */

import parseJSend from './parseJSend';

import okValidJSendResponse from './__fixtures__/jsend-success-1.json';
import errorJSendResponse1 from './__fixtures__/jsend-error-1.json';
import errorJSendResponse2 from './__fixtures__/jsend-error-2.json';
import errorJSendResponse3 from './__fixtures__/jsend-error-3.json';

describe('parseJSend', () => {
  describe('when invaid', () => {
    it('should throw', () => {
      expect(() => parseJSend()).toThrow();
      expect(() => parseJSend({})).toThrow();
      expect(() => parseJSend(null)).toThrow();
      expect(() => parseJSend(JSON.stringify({ test: 'test' }))).toThrow();
    });
  });
  describe('when valid', () => {
    it('should parse and return expected errors', () => {
      expect(
        parseJSend(JSON.stringify(okValidJSendResponse))
      ).toMatchSnapshot();
      expect(parseJSend(JSON.stringify(errorJSendResponse1))).toMatchSnapshot();
      expect(parseJSend(JSON.stringify(errorJSendResponse2))).toMatchSnapshot();
      expect(parseJSend(JSON.stringify(errorJSendResponse3))).toMatchSnapshot();
    });
  });
});
