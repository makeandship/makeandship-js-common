/* @flow */

import { take, select } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import _isEqual from 'lodash.isequal';

export function* waitForChange(selector: Function): Saga {
  const initialValue = yield select(selector);
  while (true) {
    yield take('*');
    const currentValue = yield select(selector);
    const isEqual = _isEqual(initialValue, currentValue);
    if (!isEqual) {
      return yield currentValue;
    }
  }
}
