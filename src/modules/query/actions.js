/* @flow */

import { createAction } from '@reduxjs/toolkit';

export const typePrefix = 'query/';

// Actions

// ____________________________________________________________________________________________________ query

export const maybeFetchQuery = createAction(`${typePrefix}MAYBE_FETCH_QUERY`);
export const fetchQuery = createAction(`${typePrefix}FETCH_QUERY`);
export const deleteQuery = createAction(`${typePrefix}DELETE_QUERY`);

// ____________________________________________________________________________________________________ mutation

export const useMutation = createAction(`${typePrefix}USE_MUTATION`);
export const fetchMutation = createAction(
  `${typePrefix}FETCH_MUTATION`,
  (query, parameters) => ({
    payload: parameters,
    meta: query,
  })
);

// ____________________________________________________________________________________________________ data

export const setQueryData = createAction(
  `${typePrefix}SET_QUERY_DATA`,
  (query, data) => ({
    payload: data,
    meta: query,
  })
);

// ____________________________________________________________________________________________________ fetch

export const fetchStart = createAction(`${typePrefix}FETCH_START`);
export const fetchFinish = createAction(`${typePrefix}FETCH_FINISH`);
export const fetchCancel = createAction(`${typePrefix}FETCH_CANCEL`);

export const fetchSuccess = createAction(
  `${typePrefix}FETCH_SUCCESS`,
  (query, data) => ({
    payload: data,
    meta: query,
  })
);

export const fetchError = createAction(
  `${typePrefix}FETCH_ERROR`,
  (query, error) => ({
    payload: error,
    meta: query,
  })
);

// ____________________________________________________________________________________________________ data

export const resetQueries = createAction(`${typePrefix}RESET_QUERIES`);
// export const refetchQueries = createAction(`${typePrefix}REFETCH_QUERIES`);
