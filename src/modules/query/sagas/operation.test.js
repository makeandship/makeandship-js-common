/* @flow */

import { call } from 'redux-saga/effects';

import { resetQueries } from '../actions';
import { delay as delayPromise } from '../../utils';

import spec from '../__mocks__/openapi';

import store, { sagaMiddleware } from '../__mocks__/store';
import operation from './operation';

describe('operation saga', () => {
  afterEach(() => {
    store.dispatch(resetQueries());
    jest.clearAllMocks();
  });

  describe('when valid', () => {
    it('should fetch', async (done) => {
      const fetcherFunction = jest.fn(async () => {
        await delayPromise(10);
        return 'name';
      });
      const saga = function* () {
        const { data } = yield call(
          operation,
          ['getFooBar', { id: 123 }],
          spec,
          fetcherFunction
        );
        expect(fetcherFunction).toHaveBeenCalledWith('/foo/bar/123', {
          method: 'GET',
        });
        expect(data).toEqual('name');
        yield call(done);
      };
      sagaMiddleware.run(saga);
    });
  });
});
