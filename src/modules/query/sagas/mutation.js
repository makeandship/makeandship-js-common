// @flow

import { call } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';

import { makeQuery } from '../utils';
import baseMutation from './baseMutation';

export default function* (...args: any[]): Saga {
  const query = yield call(makeQuery, ...args);
  return yield call(baseMutation, query);
}
