// @flow

import { call, select, put } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';

import { fetchQuery, fetchCancel } from '../actions';
import { makeSelector } from '../selectors';
import { maybeFetchQueryWorker, fetchQueryWorker } from '.';
import type { Query } from '../types';

function* baseQuery(query: Query): Saga {
  const selector = yield call(makeSelector, query);
  const fetchAsync = function* (parameters) {
    yield put(fetchQuery(query, parameters));
  };
  const fetch = function* () {
    const fetchAction = fetchQuery(query);
    yield call(fetchQueryWorker, fetchAction);
    const queryState = yield select(selector);
    return yield queryState;
  };
  const cancel = function* () {
    yield put(fetchCancel(query));
  };
  const fetchAction = fetchQuery(query);
  yield call(maybeFetchQueryWorker, fetchAction);
  const queryState = yield select(selector);
  return {
    ...queryState,
    selector,
    fetch,
    fetchAsync,
    cancel,
  };
}

export default baseQuery;
