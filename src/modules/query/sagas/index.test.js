/* @flow */

import store from '../__mocks__/store';

import { resetQueries, fetchQuery } from '../actions';
import { setConfig, resetConfig } from '../config';

import makeQuery from '../utils/makeQuery';

describe('query sagas', () => {
  afterEach(() => {
    store.dispatch(resetQueries());
    jest.clearAllMocks();
    resetConfig();
  });

  describe('config', () => {
    describe('when query function is set', () => {
      const configFetcher = jest.fn(() => ({
        foo: 'bar',
      }));
      setConfig({ fetcher: configFetcher });
      describe('when query has no function set', () => {
        it('should use the config query function ', async () => {
          const query = makeQuery('/foo/bar');
          store.dispatch(fetchQuery(query));
          expect(configFetcher).toHaveBeenCalled();
        });
      });
      describe('when query has function set', () => {
        it('should use the query function', async () => {
          const fetcher = jest.fn(() => ({
            foo: 'bar',
          }));
          const query = makeQuery('/foo/bar', fetcher);
          store.dispatch(fetchQuery(query));
          expect(fetcher).toHaveBeenCalled();
          expect(configFetcher).not.toHaveBeenCalled();
        });
      });
    });
  });
});
