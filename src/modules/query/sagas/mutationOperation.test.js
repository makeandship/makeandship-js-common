/* @flow */

import { call, delay } from 'redux-saga/effects';

import spec from '../__mocks__/openapi';
import fetcher from '../__mocks__/fetcher';

import { resetQueries } from '../actions';
import { delay as delayPromise } from '../../utils';
import { setConfig } from '../config';

import store, { sagaMiddleware } from '../__mocks__/store';
import mutationOperation from './mutationOperation';

setConfig({ fetcher });

describe('mutationOperation saga', () => {
  afterEach(() => {
    store.dispatch(resetQueries());
    jest.clearAllMocks();
  });

  describe('when valid', () => {
    it('should use latest mutation and cancel previous', async (done) => {
      const saga = function* () {
        const { mutate, mutateAsync } = yield call(
          mutationOperation,
          ['postFooBar', { id: 123 }],
          spec
        );
        yield call(mutateAsync, 'a');
        yield delay(50);
        yield call(mutateAsync, 'b');
        yield delay(50);
        const { data } = yield call(mutate, 'name');
        expect(data).toEqual('name');
        yield call(done);
      };
      sagaMiddleware.run(saga);
    });
    it('should allow async fetcher functions', async (done) => {
      const fetcherFunction = jest.fn(async (url, options) => {
        await delayPromise(10);
        return options.body.name;
      });
      const saga = function* () {
        const { mutate } = yield call(
          mutationOperation,
          ['postFooBar', { id: 123 }],
          spec,
          fetcherFunction
        );
        const { data } = yield call(mutate, { name: 'name' });
        expect(fetcherFunction).toHaveBeenCalledWith('/foo/bar/123', {
          method: 'POST',
          body: {
            name: 'name',
          },
        });
        expect(data).toEqual('name');
        yield call(done);
      };
      sagaMiddleware.run(saga);
    });
  });
});
