/* @flow */

import { call, delay } from 'redux-saga/effects';

import { resetQueries } from '../actions';
import { delay as delayPromise } from '../../utils';

import store, { sagaMiddleware } from '../__mocks__/store';
import query from './query';

describe('query saga', () => {
  afterEach(() => {
    store.dispatch(resetQueries());
    jest.clearAllMocks();
  });

  describe('when valid', () => {
    describe('without callbacks', () => {
      it('should fetch', async (done) => {
        const fetcherFunction = jest.fn(async () => {
          await delayPromise(10);
          return 'name';
        });
        const saga = function* () {
          const { data } = yield call(query, '/foo/bar', fetcherFunction);
          expect(fetcherFunction).toHaveBeenCalledWith('/foo/bar', undefined);
          expect(data).toEqual('name');
          yield call(done);
        };
        sagaMiddleware.run(saga);
      });
      it('should use latest query and cancel previous', async (done) => {
        const fetcherFunction = jest.fn(async () => {
          await delayPromise(10);
          return 'name';
        });
        const saga = function* () {
          const { fetch, fetchAsync } = yield call(
            query,
            '/foo/bar',
            fetcherFunction
          );
          yield call(fetchAsync);
          yield delay(50);
          yield call(fetchAsync);
          yield delay(50);
          const { data } = yield call(fetch);
          expect(fetcherFunction).toHaveBeenCalledWith('/foo/bar', undefined);
          expect(data).toEqual('name');
          yield call(done);
        };
        sagaMiddleware.run(saga);
      });
    });
    describe('with callbacks', () => {
      it('onSuccess is called with data and onError is not called', async (done) => {
        const fetcher = jest.fn(async () => {
          await delayPromise(10);
          return 'name';
        });
        const onSuccess = jest.fn();
        const onError = jest.fn();
        const saga = function* () {
          const { data } = yield call(query, {
            url: '/foo/bar',
            fetcher,
            onSuccess,
            onError,
          });
          expect(fetcher).toHaveBeenCalledWith('/foo/bar', undefined);
          expect(onSuccess).toHaveBeenCalledWith('name', expect.any(Object));
          expect(onError).not.toHaveBeenCalled();
          expect(data).toEqual('name');
          yield call(done);
        };
        sagaMiddleware.run(saga);
      });
      it('onError is called with error and onSuccess is not called', async (done) => {
        const fetcher = jest.fn(async () => {
          await delayPromise(10);
          throw 'Test error';
        });
        const onSuccess = jest.fn();
        const onError = jest.fn();
        const saga = function* () {
          const { data } = yield call(query, {
            url: '/foo/bar',
            fetcher,
            onSuccess,
            onError,
          });
          expect(fetcher).toHaveBeenCalledWith('/foo/bar', undefined);
          expect(onError).toHaveBeenCalledWith(
            'Test error',
            expect.any(Object)
          );
          expect(onSuccess).not.toHaveBeenCalled();
          expect(data).toBeUndefined();
          yield call(done);
        };
        sagaMiddleware.run(saga);
      });
    });
  });
});
