// @flow

import { call } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';

import { makeQuery } from '../utils';
import baseQuery from './baseQuery';

export default function* (...args: any[]): Saga {
  const query = yield call(makeQuery, ...args);
  return yield call(baseQuery, query);
}
