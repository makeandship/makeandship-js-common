/* @flow */

import {
  all,
  fork,
  takeEvery,
  put,
  call,
  select,
  apply,
} from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import type { PayloadAction } from '@reduxjs/toolkit';

import type { Query, QueryState } from '../types';
import { fetchToCurl } from '../../api/utils';

import {
  maybeFetchQuery,
  fetchQuery,
  fetchCancel,
  fetchMutation,
  fetchStart,
  fetchFinish,
  fetchSuccess,
  fetchError,
} from '../actions';

import { getConfig } from '../config';

import makeMutateQuery from '../utils/makeMutateQuery';

import { makeSelector } from '../selectors';

const fetchersByKey = {};

export function* watchers(): Saga {
  yield takeEvery(maybeFetchQuery, maybeFetchQueryWorker);
  yield takeEvery(fetchQuery, fetchQueryWorker);
  yield takeEvery(fetchMutation, fetchMutationWorker);
  yield takeEvery(fetchCancel, fetchCancelWorker);
}

export const shouldRefetchQuery = (query: Query, queryState: QueryState) => {
  // console.log('shouldRefetchQuery', query, queryState);
  if (queryState) {
    // error
    if (queryState.status === 'error') {
      return true;
    }
    // url changed
    if (query.url !== queryState.query?.url) {
      return true;
    }
    // already fetching
    if (queryState.isFetching) {
      return false;
    }
    // if we should use an existing query
    const msSinceQueryUpdated = Date.now() - (queryState.updatedAt || 0);
    const staleTime =
      query.staleTime || queryState.query?.staleTime || getConfig().staleTime;
    if (msSinceQueryUpdated < staleTime) {
      return false;
    }
  }
  return true;
};

export function* maybeFetchQueryWorker(action: PayloadAction): Saga {
  const query: Query = action.payload;
  const stateSelector = makeSelector(query);
  const queryState: QueryState = yield select(stateSelector);
  const shouldRefetch = yield call(shouldRefetchQuery, query, queryState);
  if (!shouldRefetch) {
    return;
  }
  yield call(fetchQueryWorker, action);
}

export function* fetchQueryWorker(action: PayloadAction): Saga {
  const query: Query = action.payload;
  const { queryKey } = query;
  if (!queryKey) {
    return;
  }
  yield call(startFetch, query);
}

export function* fetchMutationWorker(action: PayloadAction): Saga {
  const query: Query = action.meta;
  const { queryKey } = query;
  if (!queryKey) {
    return;
  }

  // modify query with provided parameters
  const parameters = action.payload;
  const mutateQuery = makeMutateQuery(query, parameters);
  yield call(startFetch, mutateQuery);
}

const isErrorCancelled = (error) => {
  if (!error) {
    return false;
  }
  if (
    error === 'cancelled' ||
    error.name === 'AbortError' || // fetch
    error.__CANCEL__ // axios
  ) {
    return true;
  }
  return false;
};

export function* startFetch(query: Query): Saga {
  const { queryKey } = query;
  if (!queryKey) {
    return;
  }

  // dedupe - cancel if there is an existing query in flight
  if (fetchersByKey[queryKey]) {
    yield put(fetchCancel(query));
  }

  yield put(fetchStart(query));

  try {
    const data = yield call(invokeQuery, query);
    if (query.onSuccess) {
      yield call(query.onSuccess, data, query);
    }
    yield put(fetchSuccess(query, data));
  } catch (error) {
    if (isErrorCancelled(error)) {
      console.log('cancelled');
    } else {
      if (query.onError) {
        yield call(query.onError, error, query);
      }
      yield put(fetchError(query, error));
      delete fetchersByKey[queryKey];
      console.log('error', error);
    }
  } finally {
    yield put(fetchFinish(query));
  }
}

const invokeQuery = async (query: Query) => {
  const { queryKey, url, options } = query;
  const config = getConfig();
  const fetcher = query.fetcher || config.fetcher;
  if (config.debug) {
    const curl = fetchToCurl(url, options);
    console.log(curl);
  }
  const promise = fetcher(url, options);
  fetchersByKey[queryKey] = promise;
  let result = await promise;
  delete fetchersByKey[queryKey];
  return result;
};

export function* fetchCancelWorker(action: PayloadAction): Saga {
  const query: Query = action.payload;
  const { queryKey } = query;
  if (fetchersByKey[queryKey]) {
    const fetcher = fetchersByKey[queryKey];
    if (fetcher?.cancel) {
      yield apply(fetcher, 'cancel');
    }
    delete fetchersByKey[queryKey];
    yield put(fetchFinish(query));
  }
}

const sagas = [watchers];

export default function* (): Saga {
  yield all(sagas.map((saga) => fork(saga)));
}
