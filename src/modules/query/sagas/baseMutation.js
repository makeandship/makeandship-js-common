// @flow

import { call, select, put } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';

import { fetchMutation, fetchCancel } from '../actions';
import { makeSelector } from '../selectors';
import { fetchMutationWorker } from '.';
import type { Query } from '../types';

function* baseMutation(query: Query): Saga {
  const selector = yield call(makeSelector, query);
  const mutateAsync = function* (parameters) {
    yield put(fetchMutation(query, parameters));
  };
  const mutate = function* (parameters) {
    const mutationAction = fetchMutation(query, parameters);
    yield call(fetchMutationWorker, mutationAction);
    const queryState = yield select(selector);
    return yield queryState;
  };
  const cancel = function* () {
    yield put(fetchCancel(query));
  };
  const queryState = yield select(selector);
  return {
    ...queryState,
    selector,
    mutate,
    mutateAsync,
    cancel,
  };
}

export default baseMutation;
