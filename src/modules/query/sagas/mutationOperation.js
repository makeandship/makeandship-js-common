// @flow

import { call } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';

import { makeOperationQuery } from '../utils';
import baseMutation from './baseMutation';

export default function* (...args: any[]): Saga {
  const query = yield call(makeOperationQuery, ...args);
  return yield call(baseMutation, query);
}
