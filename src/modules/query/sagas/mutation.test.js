/* @flow */

import { call, delay } from 'redux-saga/effects';

import { render, waitForDomChange } from '@testing-library/react';
import * as React from 'react';
import { Provider } from 'react-redux';

import { resetQueries } from '../actions';
import { setConfig } from '../config';
import useQuery from '../hooks/useQuery';
import fetcher from '../__mocks__/fetcher';

import store, { sagaMiddleware } from '../__mocks__/store';
import mutation from './mutation';

setConfig({ fetcher });

describe('mutation saga', () => {
  afterEach(() => {
    store.dispatch(resetQueries());
    jest.clearAllMocks();
  });

  describe('when valid', () => {
    it('should use latest mutation and cancel previous', async (done) => {
      const saga = function* () {
        const { mutate, mutateAsync } = yield call(mutation, '/foo/bar');
        yield call(mutateAsync, 'a');
        yield delay(50);
        yield call(mutateAsync, 'b');
        yield delay(50);
        const { data } = yield call(mutate, 'name');
        expect(data).toEqual('name');
        yield call(done);
      };
      sagaMiddleware.run(saga);
    });

    it('should allow saga to mutate data rendered by hook', async () => {
      const Page = () => {
        const { data } = useQuery('/foo/bar');
        return <div>hello, {data}</div>;
      };
      const { container } = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      const saga = function* () {
        const { mutate } = yield call(mutation, '/foo/bar');
        const { data } = yield call(mutate, 'name');
        expect(data).toEqual('name');
      };

      sagaMiddleware.run(saga);

      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"hello, "`
      );
      await waitForDomChange({ container, timeout: 5000 });
      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"hello, name"`
      );
    });
  });
});
