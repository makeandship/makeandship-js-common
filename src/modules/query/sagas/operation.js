// @flow

import { call } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';

import { makeOperationQuery } from '../utils';
import baseQuery from './baseQuery';

export default function* (...args: any[]): Saga {
  const query = yield call(makeOperationQuery, ...args);
  return yield call(baseQuery, query);
}
