// @flow

import { createSelector } from 'reselect';

import type { Query } from './types';

export const queriesSelector = (state: any) => {
  return state.query;
};

export const makeSelector = (query: Query) => {
  return createSelector(queriesSelector, (queries) => {
    const queryState = queries[query.queryKey];
    return queryState;
  });
};
