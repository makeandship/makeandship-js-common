/* @flow */

import { createReducer } from '@reduxjs/toolkit';
import produce from 'immer';

import {
  maybeFetchQuery,
  fetchQuery,
  deleteQuery,
  fetchSuccess,
  fetchStart,
  fetchCancel,
  fetchError,
  resetQueries,
  setQueryData,
} from './actions';

import type { Query, QueryState } from './types';

export type State = { [string]: QueryState };

const initialState: State = {};

const queryProp = (query: Query) =>
  produce(query, (draft) => {
    delete draft.fetcher;
    delete draft.onError;
    delete draft.onSuccess;
  });

const defaultQueryState: QueryState = {
  status: 'idle',
  isFetching: false,
  error: null,
  updatedAt: 0,
};

const updateQueryState = (
  state: State,
  query: Query,
  queryState: QueryState = {}
) => {
  if (!query.queryKey) {
    return;
  }
  if (!state[query.queryKey]) {
    state[query.queryKey] = {
      ...defaultQueryState,
    };
  }
  Object.assign(state[query.queryKey], {
    query: queryProp(query),
    ...queryState,
  });
};

// initialise query state only if none already exists for this key

const maybeUpdateQueryState = (
  state: State,
  query: Query,
  queryState?: QueryState
) => {
  if (!query.queryKey) {
    return;
  }
  if (!state[query.queryKey]) {
    updateQueryState(state, query, queryState);
  }
};

const reducer = createReducer((initialState: State), {
  [maybeFetchQuery]: (state, action) => {
    const query: Query = action.payload;
    maybeUpdateQueryState(state, query);
  },
  [fetchQuery]: (state, action) => {
    const query: Query = action.payload;
    updateQueryState(state, query);
  },
  [fetchStart]: (state, action) => {
    const query: Query = action.payload;
    updateQueryState(state, query, {
      status: 'loading',
      isFetching: true,
    });
  },
  [fetchSuccess]: (state, action) => {
    const data = action.payload;
    const query: Query = action.meta;
    if (query.options?.method === 'DELETE') {
      delete state[query.queryKey];
    } else {
      updateQueryState(state, query, {
        status: 'success',
        isFetching: false,
        error: null,
        updatedAt: Date.now(),
        data,
      });
    }
  },
  [fetchError]: (state, action) => {
    const error = action.payload;
    const query: Query = action.meta;
    updateQueryState(state, query, {
      status: 'error',
      isFetching: false,
      updatedAt: Date.now(),
      error,
    });
  },
  [fetchCancel]: (state, action) => {
    const query: Query = action.payload;
    if (!query.queryKey) {
      return;
    }
    if (state[query.queryKey]?.isFetching) {
      updateQueryState(state, query, {
        status: 'cancelled',
        isFetching: false,
      });
    }
  },
  [setQueryData]: (state, action) => {
    const data = action.payload;
    const query: Query = action.meta;
    updateQueryState(state, query, {
      status: 'success',
      isFetching: false,
      error: null,
      updatedAt: Date.now(),
      data,
    });
  },
  [deleteQuery]: (state, action) => {
    const query: Query = action.payload;
    if (!query.queryKey) {
      return;
    }
    delete state[query.queryKey];
  },
  [resetQueries]: () => {
    return initialState;
  },
});

export default reducer;
