/* @flow */

import type { Fetcher } from '../../fetchers/types';

import { delay } from '../../utils';

const fetcher: Fetcher = (url: string, options: any): Promise<*> => {
  console.log('start fetcher', { url, options });
  let cancelToken = false;
  const mockFetchGet = async () => {
    await delay(2000);
    if (cancelToken) {
      throw 'cancelled';
    }
    if (process.env.NODE_ENV === 'development' && Math.random() < 0.2) {
      throw 'Fake random error';
    }
    const data = options?.body || {
      foo: 123,
    };
    console.log('return fetcher', { url, options, data });
    return data;
  };
  const promise = mockFetchGet();
  // $FlowFixMe
  promise.cancel = () => {
    console.log('cancel fetcher', { url, options });
    cancelToken = true;
  };
  return promise;
};

export default fetcher;
