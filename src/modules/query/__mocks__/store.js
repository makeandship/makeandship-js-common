/* @flow */

import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';

import query from '../reducer';
import querySaga from '../sagas';

export const sagaMiddleware = createSagaMiddleware();
const store = configureStore({
  reducer: { query },
  middleware: [sagaMiddleware],
});
sagaMiddleware.run(querySaga);

export default store;
