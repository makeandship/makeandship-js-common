/* @flow */

import spec from '../../api/__fixtures__/openapi';

const API_URL = spec.servers[0].url;

import { emptyQuery } from './makeQuery';
import makeOperationQuery from './makeOperationQuery';

describe('makeOperationQuery', () => {
  describe('when invalid', () => {
    describe('when invalid arguments are provided', () => {
      it('throws', () => {
        expect(() => makeOperationQuery()).toThrow();
        expect(() => makeOperationQuery(123)).toThrow();
        expect(() => makeOperationQuery(null, 123)).toThrow();
        expect(() => makeOperationQuery([])).toThrow();
        expect(() => makeOperationQuery('operationId')).toThrow();
      });
    });
  });

  describe('when valid', () => {
    describe('without parameters', () => {
      it('returns query for operationId', () => {
        const query = makeOperationQuery('incidentsList', spec);
        expect(query?.queryKey).toEqual(`${API_URL}/incidents`);
      });
    });
    describe('with parameters', () => {
      describe('when parameters is an object', () => {
        it('returns query for operationId and parameters', () => {
          const query = makeOperationQuery(
            ['incidentsSearch', { status: 'foo' }],
            spec
          );
          expect(query?.queryKey).toEqual(
            `${API_URL}/incidents/search?status=foo`
          );
        });
      });
      describe('when parameters is a function', () => {
        it('returns query for operationId and parameters function', () => {
          const parametersFn = jest.fn(() => ({
            status: 'foo',
          }));
          const query = makeOperationQuery(
            ['incidentsSearch', parametersFn],
            spec
          );
          expect(parametersFn).toHaveBeenCalled();
          expect(query?.queryKey).toEqual(
            `${API_URL}/incidents/search?status=foo`
          );
        });
        it('returns empty query if parameters function throws', () => {
          /* eslint-disable no-undef */
          // $FlowFixMe
          const parametersFn = jest.fn(() => foo.id);
          /* eslint-enable no-undef */
          const query = makeOperationQuery(
            ['incidentsSearch', parametersFn],
            spec
          );
          expect(parametersFn).toHaveBeenCalled();
          expect(query).toEqual(emptyQuery);
        });
      });
    });
  });
});
