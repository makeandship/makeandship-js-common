// @flow

import _omitBy from 'lodash.omitby';
import _isNil from 'lodash.isnil';

import {
  isPlainObject,
  isString,
  isArray,
  isFunction,
} from '../../../utils/is';

import makeQueryKey from './makeQueryKey';
import type { Query } from '../types';

export const emptyQuery: Query = {};

const makeQuery = (...args: any[]): Query => {
  if (args.length > 3) {
    throw 'maximum 3 arguments expected';
  }
  let url,
    queryKey,
    parameters,
    options,
    fetcher,
    staleTime,
    onSuccess,
    onError;
  if (isPlainObject(args[0])) {
    ({
      url,
      queryKey,
      parameters,
      options,
      fetcher,
      staleTime,
      onSuccess,
      onError,
    } = args[0]);
  } else if (args.length === 3) {
    [url, options, fetcher] = args;
  } else if (args.length === 2) {
    let optionsOrFetcher;
    [url, optionsOrFetcher] = args;
    if (isFunction(optionsOrFetcher)) {
      fetcher = optionsOrFetcher;
    } else if (isPlainObject(optionsOrFetcher)) {
      options = optionsOrFetcher;
    } else {
      // ignored - may be dependency array as last argument
    }
  } else {
    [url] = args;
  }

  if (isArray(url)) {
    [url, parameters] = url;
  }

  if (isFunction(parameters)) {
    try {
      const parametersFunction: Function = parameters;
      parameters = parametersFunction();
    } catch (error) {
      // parameters function failed - do not proceed
      return emptyQuery;
    }
  }

  if (isFunction(url)) {
    try {
      url = url(parameters);
    } catch (error) {
      // allow failure
    }
    if (!isString(url)) {
      // url function failed to return string - do not proceed
      return emptyQuery;
    }
  }

  if (options !== undefined && !isPlainObject(options)) {
    throw 'options must be an object';
  }

  if (fetcher !== undefined && !isFunction(fetcher)) {
    throw 'fetcher must be a function';
  }

  // need either a queryKey or a url
  if (!queryKey) {
    if (url === undefined) {
      return emptyQuery;
    }
    queryKey = makeQueryKey(url, parameters);
  }

  const queryProps: Query = _omitBy(
    {
      url,
      queryKey,
      parameters,
      options,
      fetcher,
      staleTime,
      onSuccess,
      onError,
    },
    _isNil
  );

  return queryProps;
};

export default makeQuery;
