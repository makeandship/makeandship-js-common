// @flow

import _set from 'lodash.set';
import _get from 'lodash.get';
import produce from 'immer';

import objectToFormData from '../../../utils/objectToFormData';
import { isPlainObject, isFunction } from '../../../utils/is';

import { emptyQuery } from './makeQuery';
import type { Query } from '../types';

const makeMutateQuery = (query: Query, parameters: any): Query => {
  if (query === undefined) {
    return emptyQuery;
  }
  if (!isPlainObject(query)) {
    throw 'query must be a query object';
  }
  if (isFunction(parameters)) {
    try {
      const parametersFunction: Function = parameters;
      parameters = parametersFunction();
    } catch (error) {
      // parameters function failed - do not proceed
      return emptyQuery;
    }
  }
  const mutateQuery = produce(query, (draft) => {
    // default POST
    if (!draft.options?.method) {
      _set(draft, 'options.method', 'POST');
    }
    // default paramters in body
    if (parameters) {
      const contentType = _get(draft, 'options.headers.Content-Type');
      const isMultipartFormData = contentType?.includes('multipart/form-data');
      if (isMultipartFormData) {
        const formData = objectToFormData(parameters, {
          indices: false,
        });
        _set(draft, 'options.body', formData);
      } else {
        _set(draft, 'options.body', parameters);
      }
    }
  });
  return mutateQuery;
};

export default makeMutateQuery;
