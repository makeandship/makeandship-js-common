/* @flow */

import makeQuery, { emptyQuery } from './makeQuery';

describe('makeQuery', () => {
  describe('when invalid', () => {
    describe('when no arguments are provided', () => {
      it('returns empty query', () => {
        expect(makeQuery()).toEqual(emptyQuery);
      });
    });
    describe('when invalid arguments are provided', () => {
      it('throws', () => {
        expect(() => makeQuery(123)).toThrow();
        expect(() => makeQuery(null, 123)).toThrow();
      });
    });
  });

  describe('when valid', () => {
    describe('without parameters', () => {
      describe('when url is a string', () => {
        it('returns query for url string', () => {
          const query = makeQuery('/foo/bar');
          expect(query?.queryKey).toMatchInlineSnapshot(`"/foo/bar"`);
        });
      });

      describe('when url is a function', () => {
        it('returns query for url function', () => {
          const urlFn = jest.fn(() => '/foo/bar');
          const query = makeQuery(urlFn);
          expect(urlFn).toHaveBeenCalled();
          expect(query?.queryKey).toMatchInlineSnapshot(`"/foo/bar"`);
        });
        it('returns empty query if url function throws', () => {
          /* eslint-disable no-undef */
          // $FlowFixMe
          const query = makeQuery(() => foo.id);
          /* eslint-enable no-undef */
          expect(query).toEqual(emptyQuery);
        });
      });
    });

    describe('with parameters', () => {
      describe('when url is a string', () => {
        describe('when parameters is an object', () => {
          it('returns query for url and parameters', () => {
            const query = makeQuery(['/foo/bar', { foo: 'bar' }]);
            expect(query?.queryKey).toMatchInlineSnapshot(`"/foo/bar?foo=bar"`);
          });
        });
        describe('when parameters is a function', () => {
          it('returns query for parameters function', () => {
            const parametersFn = jest.fn(() => ({
              foo: 'bar',
            }));
            const query = makeQuery(['/foo/bar', parametersFn]);
            expect(parametersFn).toHaveBeenCalled();
            expect(query?.queryKey).toMatchInlineSnapshot(`"/foo/bar?foo=bar"`);
          });
          it('returns empty query if parameters function throws', () => {
            /* eslint-disable no-undef */
            // $FlowFixMe
            const parametersFn = jest.fn(() => foo.id);
            /* eslint-enable no-undef */
            const query = makeQuery(['/foo/bar', parametersFn]);
            expect(parametersFn).toHaveBeenCalled();
            expect(query).toEqual(emptyQuery);
          });
        });
      });
      describe('when url is a function', () => {
        describe('when parameters is an object', () => {
          it('returns query for url and parameters', () => {
            const urlFn = jest.fn(() => '/foo/bar');
            const query = makeQuery([urlFn, { foo: 'bar' }]);
            expect(urlFn).toHaveBeenCalledWith({
              foo: 'bar',
            });
            expect(query?.queryKey).toMatchInlineSnapshot(`"/foo/bar?foo=bar"`);
          });
        });
        describe('when parameters is a function', () => {
          it('returns query for url and parameters function', () => {
            const urlFn = jest.fn(() => '/foo/bar');
            const parametersFn = jest.fn(() => ({
              foo: 'bar',
            }));
            const query = makeQuery([urlFn, parametersFn]);
            expect(urlFn).toHaveBeenCalledWith({
              foo: 'bar',
            });
            expect(parametersFn).toHaveBeenCalled();
            expect(query?.queryKey).toMatchInlineSnapshot(`"/foo/bar?foo=bar"`);
          });
        });
      });
    });
  });
});
