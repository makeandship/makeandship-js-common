// @flow

import _omitBy from 'lodash.omitby';
import _isEmpty from 'lodash.isempty';

import { isPlainObject, isFunction, isArray } from '../../../utils/is';

import { getConfig } from '../config';
import type { Query } from '../types';

export const emptyQuery: Query = {};

import makeQuery from './makeQuery';
import buildRequestSwagger from '../../api/utils/swagger/buildRequestSwagger';

const makeOperationQuery = (...args: any[]): Query => {
  if (args.length > 3) {
    throw 'maximum 3 arguments expected';
  }
  let operationId, queryKey, parameters, spec, fetcher;
  if (isPlainObject(args[0])) {
    ({ operationId, queryKey, parameters, spec, fetcher } = args[0]);
  } else if (args.length === 3) {
    [operationId, spec, fetcher] = args;
  } else if (args.length === 2) {
    let specOrFetcher;
    [operationId, specOrFetcher] = args;
    if (isFunction(specOrFetcher)) {
      fetcher = specOrFetcher;
    } else {
      spec = specOrFetcher;
    }
  } else {
    [operationId] = args;
  }

  if (operationId === undefined) {
    throw 'operationId is required';
  }

  if (isArray(operationId)) {
    [operationId, parameters] = operationId;
  }

  if (isFunction(parameters)) {
    try {
      const parametersFunction: Function = parameters;
      parameters = parametersFunction();
    } catch (error) {
      // parameters function failed - do not proceed
      return emptyQuery;
    }
  }

  if (spec === undefined) {
    spec = getConfig().spec;
  }

  // convert from spec into request with url and options
  const request = buildRequestSwagger({ operationId, parameters, spec });
  const { url, method, headers, body } = request;

  const options = _omitBy(
    {
      method,
      body,
      headers,
    },
    _isEmpty
  );

  return makeQuery({ url, queryKey, parameters, options, fetcher });
};

export default makeOperationQuery;
