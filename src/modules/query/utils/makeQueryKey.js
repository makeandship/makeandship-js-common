// @flow

import qs from 'qs';

import { isPlainObject, isString } from '../../../utils/is';

const alphabeticalSort = (a, b) => {
  return a.localeCompare(b);
};

const makeQueryKey = (url: string, parameters: any) => {
  if (!isString(url)) {
    throw 'url is required and must be a string';
  }
  if (url.includes('?')) {
    const i = url.lastIndexOf('?');
    const urlQuery = url.substring(i);
    const urlParameters = qs.parse(urlQuery, { ignoreQueryPrefix: true });
    parameters = {
      ...urlParameters,
      ...parameters,
    };
    url = url.substring(0, i);
  }
  if (isPlainObject(parameters)) {
    const query = qs.stringify(parameters, { sort: alphabeticalSort });
    url = `${url}?${query}`;
  }
  return url;
};

export default makeQueryKey;
