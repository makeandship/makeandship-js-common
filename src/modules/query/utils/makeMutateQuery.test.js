/* @flow */

import makeMutateQuery from './makeMutateQuery';
import makeQuery, { emptyQuery } from './makeQuery';

describe('makeMutateQuery', () => {
  describe('when invalid', () => {
    describe('when no arguments are provided', () => {
      it('returns empty query', () => {
        // $FlowFixMe
        expect(makeMutateQuery()).toEqual(emptyQuery);
      });
    });
    describe('when invalid arguments are provided', () => {
      it('throws', () => {
        // $FlowFixMe
        expect(() => makeMutateQuery(123)).toThrow();
        // $FlowFixMe
        expect(() => makeMutateQuery(null, 123)).toThrow();
        // $FlowFixMe
        expect(() => makeMutateQuery([])).toThrow();
      });
    });
  });

  describe('when valid', () => {
    describe('without parameters', () => {
      it('sets default method to POST', () => {
        const baseQuery = makeQuery('/foo/bar');
        const query = makeMutateQuery(baseQuery);
        expect(query.options?.method).toEqual('POST');
      });
      it('does not override provided method', () => {
        const baseQuery = makeQuery('/foo/bar', { method: 'PUT' });
        const query = makeMutateQuery(baseQuery);
        expect(query.options?.method).toEqual('PUT');
      });
    });

    describe('with parameters', () => {
      describe('when parameters is an object', () => {
        it('sets body', () => {
          const baseQuery = makeQuery('/foo/bar');
          const parameters = { foo: 'bar' };
          const query = makeMutateQuery(baseQuery, parameters);
          expect(query.options?.body).toEqual(parameters);
        });
        it('sets body as form data', () => {
          const baseQuery = makeQuery('/foo/bar', {
            headers: { 'Content-Type': 'multipart/form-data' },
          });
          const parameters = { foo: 'bar' };
          const query = makeMutateQuery(baseQuery, parameters);
          expect(query.options?.body).toBeInstanceOf(FormData);
        });
      });
      describe('when parameters is a function', () => {
        it('returns query for parameters function', () => {
          const baseQuery = makeQuery('/foo/bar');
          const parameters = { foo: 'bar' };
          const parametersFn = jest.fn(() => parameters);
          const query = makeMutateQuery(baseQuery, parametersFn);
          expect(parametersFn).toHaveBeenCalled();
          expect(query.options?.body).toEqual(parameters);
        });
        it('returns empty query if parameters function throws', () => {
          // eslint-disable-next-line no-undef
          const baseQuery = makeQuery('/foo/bar');
          /* eslint-disable no-undef */
          // $FlowFixMe
          const parametersFn = jest.fn(() => foo.id);
          /* eslint-enable no-undef */
          const query = makeMutateQuery(baseQuery, parametersFn);
          expect(parametersFn).toHaveBeenCalled();
          expect(query).toEqual(emptyQuery);
        });
      });
    });
  });
});
