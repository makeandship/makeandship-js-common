/* @flow */

import makeQueryKey from './makeQueryKey';

describe('makeQueryKey', () => {
  describe('when invalid', () => {
    it('throws', () => {
      // $FlowFixMe
      expect(() => makeQueryKey()).toThrow();
    });
  });

  describe('when valid', () => {
    it('makes key in expected format', () => {
      expect(makeQueryKey('/foo/bar')).toMatchInlineSnapshot(`"/foo/bar"`);
      expect(makeQueryKey('/foo/bar', { foo: 'bar' })).toMatchInlineSnapshot(
        `"/foo/bar?foo=bar"`
      );
    });
    it('makes same key regardless of property order', () => {
      const a = 1;
      const b = 2;
      const c = { a: 1, b: 2 };
      const c2 = { b: 2, a: 1 };
      const key1 = makeQueryKey('/foo/bar', { a, b, c });
      const key2 = makeQueryKey('/foo/bar', { b, c: c2, a });
      const key3 = makeQueryKey('/foo/bar', { c, a, b });
      expect(key1).toMatchInlineSnapshot(
        `"/foo/bar?a=1&b=2&c%5Ba%5D=1&c%5Bb%5D=2"`
      );
      expect(key2).toMatchInlineSnapshot(
        `"/foo/bar?a=1&b=2&c%5Ba%5D=1&c%5Bb%5D=2"`
      );
      expect(key3).toMatchInlineSnapshot(
        `"/foo/bar?a=1&b=2&c%5Ba%5D=1&c%5Bb%5D=2"`
      );
      expect(key1).toEqual(key2);
      expect(key1).toEqual(key3);
      expect(makeQueryKey('/foo/bar?b=2&a=1')).toEqual(
        makeQueryKey('/foo/bar?a=1&b=2')
      );
      expect(makeQueryKey('/foo/bar?a=1&b=2&c%5Ba%5D=1&c%5Bb%5D=2')).toEqual(
        makeQueryKey('/foo/bar?c%5Ba%5D=1&c%5Bb%5D=2&a=1&b=2')
      );
    });
  });
});
