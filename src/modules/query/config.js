/* @flow */

import type { Config } from './types';

import makeConfig from '../utils/makeConfig';
import fetcher from '../fetchers/axiosFetcher';

const defaultConfig: Config = {
  fetcher,
  staleTime: 2000,
};

const { getConfig, setConfig, resetConfig } = makeConfig(defaultConfig);

export { getConfig, setConfig, resetConfig };
