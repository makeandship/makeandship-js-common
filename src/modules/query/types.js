// @flow

import type { Fetcher } from '../fetchers/types';

export type SuccessCallback = (query: Query, data: any) => any;
export type ErrorCallback = (query: Query, error: any) => any;
export type Parser = (data: any) => any;

export type Config = {
  fetcher: Fetcher,
  parser?: Parser,
  staleTime: number,
  spec?: any,
};

export type Query = {
  url: string,
  queryKey: string,
  parameters?: any,
  options?: any,
  fetcher?: Fetcher,
  staleTime?: number,
  parser?: Parser,
  onSuccess?: SuccessCallback,
  onError?: ErrorCallback,
};

export type QueryState = {
  status?: string,
  isFetching?: boolean,
  error?: any,
  updatedAt?: number,
  query?: Query,
};
