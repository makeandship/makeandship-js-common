// @flow

import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { fetchQuery, fetchCancel, maybeFetchQuery } from '../actions';
import { makeSelector } from '../selectors';
import type { Query } from '../types';

const useBaseQuery = (query: Query, deps: Array<*> = []) => {
  const selector = makeSelector(query);
  const dispatch = useDispatch();
  const fetch = () => dispatch(fetchQuery(query));
  const cancel = () => dispatch(fetchCancel(query));
  const { queryKey } = query;
  React.useEffect(() => {
    dispatch(maybeFetchQuery(query));
  }, [queryKey, ...deps]);
  const queryState = useSelector(selector);
  return {
    ...queryState,
    fetch,
    cancel,
  };
};

export default useBaseQuery;
