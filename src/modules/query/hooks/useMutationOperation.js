// @flow

import { makeOperationQuery } from '../utils';

import useBaseMutation from './useBaseMutation';

const useMutationOperation = (...args: any[]) => {
  const query = makeOperationQuery(...args);
  return useBaseMutation(query);
};

export default useMutationOperation;
