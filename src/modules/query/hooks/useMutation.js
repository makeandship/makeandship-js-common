// @flow

import { makeQuery } from '../utils';

import useBaseMutation from './useBaseMutation';

const useMutation = (...args: any[]) => {
  const query = makeQuery(...args);
  return useBaseMutation(query);
};

export default useMutation;
