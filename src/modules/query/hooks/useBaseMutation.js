// @flow

import { useSelector, useDispatch } from 'react-redux';

import { fetchMutation, fetchCancel } from '../actions';
import { makeSelector } from '../selectors';
import type { Query } from '../types';

const useBaseMutation = (query: Query) => {
  const selector = makeSelector(query);
  const dispatch = useDispatch();
  const mutate = (parameters) => dispatch(fetchMutation(query, parameters));
  const cancel = () => dispatch(fetchCancel(query));
  const queryState = useSelector(selector);
  return {
    ...queryState,
    mutate,
    cancel,
  };
};

export default useBaseMutation;
