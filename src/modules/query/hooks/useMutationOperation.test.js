/* @flow */

import { render, waitForDomChange } from '@testing-library/react';
import * as React from 'react';
import { Provider } from 'react-redux';

import store from '../__mocks__/store';
import spec from '../__mocks__/openapi';

import useMutationOperation from './useMutationOperation';
import { resetQueries } from '../actions';
import { delay } from '../../utils';

describe('useOperation', () => {
  afterEach(() => {
    store.dispatch(resetQueries());
  });

  describe('when valid', () => {
    it('should allow async fetcher functions', async () => {
      const fetcherFunction = jest.fn(async (url, options) => {
        await delay(10);
        return options.body.name;
      });

      const Page = () => {
        const { data, mutate } = useMutationOperation(
          ['postFooBar', { id: 123 }],
          spec,
          fetcherFunction
        );
        React.useEffect(() => {
          mutate({ name: 'name' });
        }, []);
        return <div>hello, {data}</div>;
      };
      const { container } = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"hello, "`
      );
      await waitForDomChange({ container });
      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"hello, name"`
      );

      expect(fetcherFunction).toHaveBeenCalledWith('/foo/bar/123', {
        method: 'POST',
        body: {
          name: 'name',
        },
      });
    });
  });
});
