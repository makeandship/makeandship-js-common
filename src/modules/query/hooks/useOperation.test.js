/* @flow */

import { render, waitFor } from '@testing-library/react';
import * as React from 'react';
import { Provider } from 'react-redux';

import store from '../__mocks__/store';
import spec from '../__mocks__/openapi';

import useOperation from './useOperation';
import { resetQueries } from '../actions';
import { delay } from '../../utils';

describe('useOperation', () => {
  afterEach(() => {
    store.dispatch(resetQueries());
  });

  describe('when valid', () => {
    it('returns placeholder', async () => {
      /* eslint-disable no-unused-vars */
      const fetcherFunction = jest.fn(async (url, options) => {
        await delay(10);
        return 'test';
      });
      /* eslint-enable no-unused-vars */

      const Page = () => {
        const { data = 'default' } = useOperation(
          ['getFooBar', { id: 123 }],
          spec,
          fetcherFunction
        );
        return (
          <div>
            <h1>{data}</h1>
          </div>
        );
      };

      const rendered = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      rendered.getByText('default');
      await waitFor(() => rendered.getByText('test'));

      expect(fetcherFunction).toHaveBeenCalledWith('/foo/bar/123', {
        method: 'GET',
      });
    });
  });
});
