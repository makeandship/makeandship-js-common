// @flow

import { makeQuery } from '../utils';
import { isArray } from '../../../utils/is';

import useBaseQuery from './useBaseQuery';

const useQuery = (...args: any[]) => {
  const query = makeQuery(...args);
  let deps = [];
  if (args.length === 2 && isArray(args[1])) {
    deps = args[1];
  }
  return useBaseQuery(query, deps);
};

export default useQuery;
