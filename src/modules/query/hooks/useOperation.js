// @flow

import { makeOperationQuery } from '../utils';

import useBaseQuery from './useBaseQuery';

const useOperation = (...args: any[]) => {
  const query = makeOperationQuery(...args);
  return useBaseQuery(query);
};

export default useOperation;
