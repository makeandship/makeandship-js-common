/* @flow */

// based on
// https://github.com/tannerlinsley/react-query/blob/89bec2039324282a023e4e726ea6ae2e1c45178a/src/tests/useQuery.test.js
// https://github.com/vercel/swr/blob/master/test/use-swr.test.tsx

import {
  render,
  fireEvent,
  waitFor,
  waitForDomChange,
} from '@testing-library/react';
import * as React from 'react';
import { Provider } from 'react-redux';

import store from '../__mocks__/store';

import useQuery from './useQuery';
import { resetQueries } from '../actions';
import { delay } from '../../utils';

describe('useQuery', () => {
  afterEach(() => {
    store.dispatch(resetQueries());
    jest.clearAllMocks();
  });

  xdescribe('when valid', () => {
    it('has data for a query with only a key', async () => {
      const Page = () => {
        const { data: activeData } = useQuery('/foo/bar', async () => {
          await delay(10);
          return 'test';
        });

        const { data: passiveData1 } = useQuery('/foo/bar');

        const { data: passiveData2 } = useQuery({
          queryKey: '/foo/bar',
        });

        return (
          <div>
            <h1>
              active: {activeData}, passive1: {passiveData1}, passive2:{' '}
              {passiveData2}
            </h1>
          </div>
        );
      };

      const { container } = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"active: , passive1: , passive2: "`
      );
      await waitForDomChange({ container });
      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"active: test, passive1: test, passive2: test"`
      );
    });

    it('should return `undefined` on hydration', () => {
      const Page = () => {
        const { data } = useQuery('/foo/bar', () => 'data');
        return <div>hello, {typeof data === 'undefined' ? '' : 'ERROR'}</div>;
      };
      const { container } = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"hello, "`
      );
    });

    it('should return data after hydration', async () => {
      const Page = () => {
        const { data } = useQuery('/foo/bar', () => 'name');
        return <div>hello, {data}</div>;
      };
      const { container } = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"hello, "`
      );
      await waitForDomChange({ container }); // mount
      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"hello, name"`
      );
    });

    it('should allow async fetcher functions', async () => {
      const Page = () => {
        const { data } = useQuery(
          'constant-3',
          () => new Promise((res) => setTimeout(() => res('name'), 200))
        );
        return <div>hello, {data}</div>;
      };
      const { container } = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"hello, "`
      );
      await waitForDomChange({ container });
      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"hello, name"`
      );
    });

    it('returns placeholder', async () => {
      const Page = () => {
        const { data = 'default' } = useQuery('test', async () => {
          await delay(10);
          return 'test';
        });

        return (
          <div>
            <h1>{data}</h1>
          </div>
        );
      };

      const rendered = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      rendered.getByText('default');
      await waitFor(() => rendered.getByText('test'));
    });

    it('should set status to error if fetcher throws', async () => {
      const Page = () => {
        const { status, error } = useQuery('test', () => {
          return Promise.reject('Error test');
        });

        return (
          <div>
            <h1>{status}</h1>
            <h2>{error}</h2>
          </div>
        );
      };

      const rendered = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      await waitFor(() => rendered.getByText('error'));
      await waitFor(() => rendered.getByText('Error test'));
    });

    it('should support a function that resolves a query key', async () => {
      const Page = () => {
        const { status, data, query } = useQuery(
          () => '/foo/bar',
          () => 'data'
        );

        return (
          <div>
            <div>Status: {status}</div>
            <div>Data: {data}</div>
            <div>Key: {query?.queryKey}</div>
          </div>
        );
      };

      const rendered = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      rendered.getByText('Status: loading');
      await waitFor(() => rendered.getByText('Status: success'));
      rendered.getByText('Data: data');
      rendered.getByText('Key: /foo/bar');
    });

    it('should support dependent query keys via returning falsy in a query key function', async () => {
      const Page = () => {
        const [shouldFetch, setShouldFetch] = React.useState(false);

        const query = useQuery(
          () => shouldFetch && '/foo/bar',
          () => 'data'
        );

        return (
          <div>
            <div>Status: {query.status || 'no status'}</div>
            <h2>Data: {query.data || 'no data'}</h2>
            <button onClick={() => setShouldFetch(true)}>fetch</button>
          </div>
        );
      };

      const rendered = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      rendered.getByText('Status: no status');
      rendered.getByText('Data: no data');

      fireEvent.click(rendered.getByText('fetch'));

      await waitFor(() => rendered.getByText('Status: loading'));
      await waitFor(() => [
        rendered.getByText('Status: success'),
        rendered.getByText('Data: data'),
      ]);
    });

    it('should support dependent query keys via throwing in a query key function', async () => {
      const Page = () => {
        const [shouldFetch, setShouldFetch] = React.useState();

        const query = useQuery(
          // $FlowFixMe
          () => shouldFetch.on && '/foo/bar',
          () => 'data'
        );

        return (
          <div>
            <div>Status: {query.status || 'no status'}</div>
            <h2>Data: {query.data || 'no data'}</h2>
            <button onClick={() => setShouldFetch({ on: true })}>fetch</button>
          </div>
        );
      };

      const rendered = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      rendered.getByText('Status: no status');
      rendered.getByText('Data: no data');

      fireEvent.click(rendered.getByText('fetch'));

      await waitFor(() => rendered.getByText('Status: loading'));
      await waitFor(() => [
        rendered.getByText('Status: success'),
        rendered.getByText('Data: data'),
      ]);
    });

    it('should execute query function once for multiple uses', async () => {
      const cancelFunction = jest.fn();

      const promise = async () => {
        await delay(300);
        return 'data';
      };
      promise.cancel = cancelFunction;

      const fetcher = jest.fn(promise);

      const Component = ({
        identifier,
      }: React.ElementProps<*>): React.Element<*> => {
        const query = useQuery('/foo/bar', fetcher);

        return (
          <div>
            <div>
              Identifier: {identifier}, Data: {query.data || 'no data'}
            </div>
          </div>
        );
      };

      const Page = () => {
        return (
          <div>
            <Component identifier={'1'} />
            <Component identifier={'2'} />
            <Component identifier={'3'} />
          </div>
        );
      };

      const rendered = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      await waitFor(() => rendered.getByText('Identifier: 1, Data: data'));
      await waitFor(() => rendered.getByText('Identifier: 2, Data: data'));
      await waitFor(() => rendered.getByText('Identifier: 3, Data: data'));

      expect(fetcher).toHaveBeenCalledTimes(1);
      expect(cancelFunction).toHaveBeenCalledTimes(0);
    });

    it('should have separate data when query url is dynamic', async () => {
      const fetcherFunction = jest.fn(async (url, options) => {
        await delay(200);
        console.log({ url, options });
        return url;
      });

      const Item = ({ item }: React.ElementProps<*>): React.Element<*> => {
        const { data } = useQuery(() => '/foo/bar/' + item.id, fetcherFunction);
        return <div>Item: {data}</div>;
      };

      const Page = () => {
        const { data } = useQuery(() => '/foo/bar/3');

        return (
          <div>
            <Item item={{ id: 1 }} />
            <Item item={{ id: 2 }} />
            <Item item={{ id: 3 }} />
            <div>Item: {data}</div>
          </div>
        );
      };
      const { container } = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"Item: Item: Item: Item: "`
      );
      await waitForDomChange({ container }); // mount
      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"Item: /foo/bar/1Item: /foo/bar/2Item: /foo/bar/3Item: /foo/bar/3"`
      );
    });

    it('should have same data when using the same key when query url is dynamic', async () => {
      const Item = ({ item }: React.ElementProps<*>): React.Element<*> => {
        const fetcherFunction = jest.fn(async (url, options) => {
          await delay(item.delay);
          console.log({ url, options });
          return url;
        });
        const { data } = useQuery({
          queryKey: 'CommonQueryKey',
          url: () => '/foo/bar/' + item.id,
          fetcher: fetcherFunction,
        });
        return <div>Item: {data}</div>;
      };

      const Page = () => {
        return (
          <div>
            <Item item={{ id: 1, delay: 200 }} />
            <Item item={{ id: 2, delay: 400 }} />
            <Item item={{ id: 3, delay: 600 }} />
          </div>
        );
      };
      const { container } = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"Item: Item: Item: "`
      );
      await waitForDomChange({ container }); // mount
      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"Item: /foo/bar/1Item: /foo/bar/1Item: /foo/bar/1"`
      );
      await waitForDomChange({ container }); // second url
      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"Item: /foo/bar/2Item: /foo/bar/2Item: /foo/bar/2"`
      );
      await waitForDomChange({ container }); // third url
      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"Item: /foo/bar/3Item: /foo/bar/3Item: /foo/bar/3"`
      );
    });
  });
});
