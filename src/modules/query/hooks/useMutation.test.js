/* @flow */

// based on
// https://github.com/tannerlinsley/react-query/blob/89bec2039324282a023e4e726ea6ae2e1c45178a/src/tests/useQuery.test.js
// https://github.com/vercel/swr/blob/master/test/use-swr.test.tsx

import { render, waitForDomChange } from '@testing-library/react';
import * as React from 'react';
import { Provider } from 'react-redux';

import store from '../__mocks__/store';

import useMutation from './useMutation';
import useQuery from './useQuery';
import { resetQueries } from '../actions';
import { delay } from '../../utils';

describe('useMutation', () => {
  afterEach(() => {
    store.dispatch(resetQueries());
    jest.clearAllMocks();
  });

  describe('when valid', () => {
    it('should allow async fetcher functions', async () => {
      const Page = () => {
        const { data, mutate } = useMutation('/foo/bar', async () => {
          await delay(200);
          return 'name';
        });
        React.useEffect(() => {
          mutate({ name: 'name' });
        }, []);
        return <div>hello, {data}</div>;
      };
      const { container } = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"hello, "`
      );
      await waitForDomChange({ container });
      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"hello, name"`
      );
    });

    it('should delete query', async () => {
      const fetcherFunction = jest.fn(async (url, options) => {
        await delay(200);
        console.log({ url, options });
      });

      const Page = () => {
        const { data } = useQuery('/foo/bar', () => 'name');
        const { mutate } = useMutation(
          '/foo/bar',
          { method: 'DELETE' },
          fetcherFunction
        );
        React.useEffect(() => {
          mutate();
        }, []);

        return <div>hello, {data}</div>;
      };
      const { container } = render(
        <Provider store={store}>
          <Page />
        </Provider>
      );

      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"hello, "`
      );
      await waitForDomChange({ container }); // mount
      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"hello, name"`
      );
      await waitForDomChange({ container });
      expect(fetcherFunction).toHaveBeenCalledWith('/foo/bar', {
        method: 'DELETE',
      });
      expect(container.firstChild.textContent).toMatchInlineSnapshot(
        `"hello, "`
      );
    });
  });
});
