/* @flow */

import qs from 'qs';

import { isString, isNumeric } from '../../utils/is';

// treat single string argument as key, or use id parameter from object

export const NEW_ENTITY_KEY = 'new';

export const isNewEntityKey = (key: any) =>
  key === undefined || key === NEW_ENTITY_KEY;

export const idKeyExtractor = (payload: any) => {
  if (payload && payload.id) {
    return payload.id;
  }
  if (isString(payload) || isNumeric(payload)) {
    return payload;
  }
  return NEW_ENTITY_KEY;
};

// convert query object to query string

export const queryStringKeyExtractor = (payload: any) =>
  isString(payload) ? payload : qs.stringify(payload);
