/* @flow */

export const urlJoin = (...parts: Array<any>) =>
  parts
    .map((part) => (part ? String(part) : part))
    .filter((part) => part && part.length)
    .join('/');
