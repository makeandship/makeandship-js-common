/* @flow */

import _memoize from 'lodash.memoize';
import _snakeCase from 'lodash.snakecase';
import pluralize from 'pluralize';

export const NEW = 'NEW';
export const CREATE = 'CREATE';
export const REQUEST = 'REQUEST';
export const UPDATE = 'UPDATE';
export const DELETE = 'DELETE';
export const SUCCESS = 'SUCCESS';
export const FAILURE = 'FAILURE';
export const SET = 'SET';
export const RESET = 'RESET';
export const CANCEL = 'CANCEL';
export const ABORT = 'ABORT';
export const UPLOAD_PROGRESS = 'UPLOAD_PROGRESS';
export const DOWNLOAD_PROGRESS = 'DOWNLOAD_PROGRESS';

export const types = {
  NEW,
  CREATE,
  REQUEST,
  UPDATE,
  DELETE,
  SUCCESS,
  FAILURE,
  SET,
  RESET,
  CANCEL,
  ABORT,
  UPLOAD_PROGRESS,
  DOWNLOAD_PROGRESS,
};

export const actionIdentifier = _memoize(
  (prefix: string = '', entityName: string, suffix: string = '') => {
    const parts = [];
    prefix.length && parts.push(prefix);
    parts.push(_snakeCase(entityName));
    suffix.length && parts.push(suffix);
    return parts.join('_').toUpperCase();
  },
  (prefix, entityName, suffix) => `${prefix}-${entityName}-${suffix}`
);

export const actionTypeBase = _memoize(
  (
    prefix: string = '',
    entityName: string,
    suffix: string = '',
    typePrefix: string = ''
  ) => {
    if (!typePrefix.length) {
      typePrefix = `${pluralize(entityName)}/${entityName}/`;
    }
    const identifier = actionIdentifier(prefix, entityName, suffix);
    return `${typePrefix}${identifier}`;
  },
  (typePrefix, prefix, entityName, suffix) =>
    `${typePrefix}${prefix}-${entityName}-${suffix}`
);
