/* @flow */

import { types, actionTypeBase } from './actions';

const entityName = 'incident';
const collectionName = 'incidents';

describe('generic module action types', () => {
  it('should create action types correctly', () => {
    expect(actionTypeBase(types.REQUEST, entityName)).toEqual(
      'incidents/incident/REQUEST_INCIDENT'
    );
    expect(actionTypeBase(types.REQUEST, entityName, types.SUCCESS)).toEqual(
      'incidents/incident/REQUEST_INCIDENT_SUCCESS'
    );
    expect(actionTypeBase(types.REQUEST, collectionName)).toEqual(
      'incidents/incidents/REQUEST_INCIDENTS'
    );
    expect(
      actionTypeBase(types.REQUEST, collectionName, types.SUCCESS)
    ).toEqual('incidents/incidents/REQUEST_INCIDENTS_SUCCESS');
    expect(
      actionTypeBase(
        types.REQUEST,
        'currentUser',
        types.SUCCESS,
        'users/currentUser/'
      )
    ).toEqual('users/currentUser/REQUEST_CURRENT_USER_SUCCESS');
  });
});
