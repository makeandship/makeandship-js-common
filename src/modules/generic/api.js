/* @flow */

import { call } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';

import pluralize from 'pluralize';

import { idKeyExtractor, NEW_ENTITY_KEY } from './keyExtractors';
import { urlJoin } from './utils';
import { callJsonApiWithResponse } from '../api/jsonApi';
import { callSwaggerApiWithResponse } from '../api/swaggerApi';
import { getConfig as getApiConfig } from '../api/config';

import type { CrudOptions } from './types';

// TODO: link out to CRUD options

/**
 * Generic Swagger call saga. Constructs the call with the supplied options.
 *
 * @param  {Object} args
 * @param  {String} args.types Action types [REQUEST, SUCCESS, FAILURE]
 * @param  {Object} args.action The redux action that invoked this side effect
 * @param  {CrudOptions} args.crudOptions Options specific to this request
 * @param  {Object} args.parameters The parameters to use in the request
 */

export function* genericSwaggerCall({
  types,
  crudOptions,
  action,
  parameters,
  shouldExecuteJsonApiAction,
  meta,
}: {
  types: Array<string>,
  crudOptions: CrudOptions,
  action: any,
  parameters: any,
  shouldExecuteJsonApiAction?: Saga,
  meta?: any,
}): Saga {
  if (crudOptions && crudOptions.parameters) {
    parameters = yield call(crudOptions.parameters, parameters, { action });
  }
  const { success, failure } = yield call(callSwaggerApiWithResponse, {
    operationId: crudOptions.operationId,
    parameters,
    types,
    shouldExecuteJsonApiAction,
    meta,
  });
  if (success && crudOptions.onSuccess) {
    yield call(crudOptions.onSuccess, { success, action });
  }
  if (failure && crudOptions.onFailure) {
    yield call(crudOptions.onFailure, { failure, action });
  }
}

/**
 * Generic JSON call saga. Constructs the call with the supplied options.
 *
 * @param  {Object} args
 * @param  {String} args.entityName The request entity name e.g. 'user'
 * @param  {String} args.types Action types [REQUEST, SUCCESS, FAILURE]
 * @param  {CrudOptions} [args.crudOptions] Options specific to this request
 * @param  {Object} args.action The redux action that invoked this side effect
 * @param  {Object} [args.options] fetch options specific to this request
 */

export function* genericJsonApiCall({
  entityName,
  types,
  crudOptions,
  action,
  options,
  shouldExecuteJsonApiAction,
  meta,
}: {
  entityName: string,
  types: Array<string>,
  crudOptions: CrudOptions,
  action: any,
  options?: any,
  shouldExecuteJsonApiAction?: Saga,
  meta?: any,
}): Saga {
  const apiUrl = getApiConfig().apiUrl;
  let url = urlJoin(apiUrl, pluralize(entityName));
  const id = idKeyExtractor(action.payload);
  if (id !== NEW_ENTITY_KEY) {
    url = urlJoin(url, id);
  }
  if (crudOptions && crudOptions.url) {
    if (typeof crudOptions.url === 'string') {
      url = crudOptions.url;
    } else {
      url = yield call(crudOptions.url, url, { action });
    }
  }
  const { success, failure } = yield call(callJsonApiWithResponse, {
    url,
    options,
    types,
    shouldExecuteJsonApiAction,
    meta,
  });
  if (crudOptions) {
    if (success && crudOptions.onSuccess) {
      yield call(crudOptions.onSuccess, { success, action });
    }
    if (failure && crudOptions.onFailure) {
      yield call(crudOptions.onFailure, { failure, action });
    }
  }
}
