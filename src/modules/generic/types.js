/* @flow */

import type { Saga } from 'redux-saga';

export type { GenericEntityModule } from './entity/entityModule';

export type CrudOptions = {
  operationId?: string,
  url?: CreateUrlSaga,
  parameters?: CreateParametersSaga,
  onSuccess?: SuccessCompletionSaga,
  onFailure?: FailureCompletionSaga,
};

export type CreateUrlSaga = (
  url: string,
  {
    action: any,
  }
) => Saga;

export type CreateParametersSaga = (
  parameters: any,
  {
    action: any,
  }
) => Saga;

export type SuccessCompletionSaga = ({
  action: any,
  success: any,
}) => Saga;

export type FailureCompletionSaga = ({
  action: any,
  failure: any,
}) => Saga;

export type ActionTypeFunction = (prefix: string, suffix?: string) => string;

export type Progress = {
  loaded: Number,
  total: Number,
  fraction: Number,
  percent: Number,
};

export type State = {
  isFetching?: boolean,
  isRefetching?: boolean,
  isCreating?: boolean,
  isRequesting?: boolean,
  isUpdating?: boolean,
  isDeleting?: boolean,
  data?: any,
  error?: any,
  progress?: {
    upload?: Progress,
    download?: Progress,
  },
  [string]: {
    isFetching: boolean,
    isRefetching?: boolean,
    isCreating?: boolean,
    isRequesting?: boolean,
    isUpdating?: boolean,
    isDeleting?: boolean,
    data?: any,
    error?: any,
    progress?: {
      upload?: Progress,
      download?: Progress,
    },
  },
};

export type Reducer = (state: State, action: Object) => Object;

export type ActionTypes = {
  NEW: string,
  CREATE: string,
  REQUEST: string,
  UPDATE: string,
  DELETE: string,
  SET: string,
  RESET: string,
  ABORT: string,
  CREATE_REQUEST: string,
  REQUEST_REQUEST: string,
  UPDATE_REQUEST: string,
  DELETE_REQUEST: string,
  CREATE_SUCCESS: string,
  REQUEST_SUCCESS: string,
  UPDATE_SUCCESS: string,
  DELETE_SUCCESS: string,
  CREATE_FAILURE: string,
  REQUEST_FAILURE: string,
  UPDATE_FAILURE: string,
  DELETE_FAILURE: string,
  CREATE_ABORT: string,
  REQUEST_ABORT: string,
  UPDATE_ABORT: string,
  DELETE_ABORT: string,
  CREATE_UPLOAD_PROGRESS: string,
  REQUEST_UPLOAD_PROGRESS: string,
  UPDATE_UPLOAD_PROGRESS: string,
  DELETE_UPLOAD_PROGRESS: string,
  CREATE_DOWNLOAD_PROGRESS: string,
  REQUEST_DOWNLOAD_PROGRESS: string,
  UPDATE_DOWNLOAD_PROGRESS: string,
  DELETE_DOWNLOAD_PROGRESS: string,
};
