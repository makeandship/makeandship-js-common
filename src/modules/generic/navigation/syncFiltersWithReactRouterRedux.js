/* @flow */

import { all, fork, put, takeLatest, select } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import { createSelector } from 'reselect';
import { getLocation, push, LOCATION_CHANGE } from 'connected-react-router';
import { parseQuery, stringifyQuery } from './utils';
import _isEqual from 'lodash.isequal';
import _omitBy from 'lodash.omitby';
import _isNil from 'lodash.isnil';

import type { ActionTypes } from '../filters/filtersModule';
import { createAction } from '@reduxjs/toolkit';

// Selectors

export const getQuery = createSelector(getLocation, (location) => {
  const query = parseQuery(location.search);
  return query;
});

export const getHasQuery = createSelector(getQuery, (query) => {
  const keys = Object.keys(query);
  return keys.length !== 0;
});

// Actions

export const typePrefix = 'navigation/syncFiltersWithReactRouterRedux/';

export const actions = {
  setQuery: createAction(`${typePrefix}SET_QUERY`),
  clearQuery: createAction(`${typePrefix}CLEAR_QUERY`),
};

// Sync the browser location search query and filters, preserve hash

type Sagas = {
  syncFiltersWithReactRouterReduxSaga: Function,
};

export const createSyncFiltersWithReactRouterRedux = ({
  locationPathname = '/',
  actionTypes,
  getFilters,
  setFilters,
}: {
  locationPathname: string,
  actionTypes: ActionTypes,
  getFilters: (state: any) => any,
  setFilters: (filters?: any) => any,
}): Sagas => {
  function* setQueryWatcher() {
    yield takeLatest(actions.setQuery, function* (action) {
      const location = yield select(getLocation);
      const { hash } = location;
      const query = action.payload;
      const cleanQuery = _omitBy(query, _isNil);
      const search = stringifyQuery(cleanQuery);
      yield put(push({ search, hash }));
    });
  }

  function* clearQueryWatcher() {
    yield takeLatest(actions.clearQuery, function* () {
      const location = yield select(getLocation);
      const { hash } = location;
      const search = '';
      yield put(push({ search, hash }));
    });
  }

  function* locationChangeWatcher() {
    yield takeLatest(LOCATION_CHANGE, locationChangeWorker);
  }

  function* locationChangeWorker(): Saga {
    const location = yield select(getLocation);
    const { search, pathname } = location;
    if (pathname === locationPathname) {
      const query = parseQuery(search);
      const filters = yield select(getFilters);
      if (!_isEqual(filters, query)) {
        yield put(setFilters(query));
      }
    }
  }

  function* filtersChangeWatcher() {
    yield takeLatest([actionTypes.SET, actionTypes.RESET], filtersChangeWorker);
  }

  function* filtersChangeWorker(): Saga {
    const location = yield select(getLocation);
    const { search, pathname } = location;
    if (pathname === locationPathname) {
      const query = parseQuery(search);
      const filters = yield select(getFilters);
      if (!_isEqual(filters, query)) {
        yield put(actions.setQuery(filters));
      }
    }
  }

  function* syncFiltersWithReactRouterReduxSaga(): Saga {
    yield all([
      fork(setQueryWatcher),
      fork(clearQueryWatcher),
      fork(locationChangeWatcher),
      fork(filtersChangeWatcher),
    ]);
  }

  return { syncFiltersWithReactRouterReduxSaga };
};
