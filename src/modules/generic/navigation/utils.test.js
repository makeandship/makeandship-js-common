/* @flow */

import { parseQuery, stringifyQuery } from './utils';

const search =
  'metadata.sample.dateArrived%5Bmax%5D=2018-08-29T01%3A00%3A00%2B01%3A00&metadata.sample.dateArrived%5Bmin%5D=2016-12-15T00%3A00%3A00.000Z&involved.role=Victim%20%28Violence%20and%20Agression%29&pressureUlcer.location=ward&involved.type=Employee%2FMember%20of%20staff';

const query = {
  'metadata.sample.dateArrived': {
    max: '2018-08-29T01:00:00+01:00',
    min: '2016-12-15T00:00:00.000Z',
  },
  'involved.role': 'Victim (Violence and Agression)',
  'pressureUlcer.location': 'ward',
  'involved.type': 'Employee/Member of staff',
};

describe('location util', () => {
  it('parses without changing order', () => {
    // location.search in redux-router begins with ?
    const parsed = parseQuery(`?${search}`);
    expect(parsed).toEqual(query);
    const keys = Object.keys(parsed);
    expect(keys).toEqual([
      'metadata.sample.dateArrived',
      'involved.role',
      'pressureUlcer.location',
      'involved.type',
    ]);
  });

  it('stringifies without changing order', () => {
    const stringified = stringifyQuery(query);
    expect(stringified).toEqual(search);
  });
});
