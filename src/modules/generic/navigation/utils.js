/* @flow */

import qs from 'qs';

export const parseQuery = (query: string): any => {
  return qs.parse(query, { ignoreQueryPrefix: true });
};

export const stringifyQuery = (query: any): string => {
  return qs.stringify(query);
};
