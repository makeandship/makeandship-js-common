/* @flow */

import {
  idKeyExtractor,
  NEW_ENTITY_KEY,
  isNewEntityKey,
} from './keyExtractors';

describe('generic module keyExtractors idKeyExtractor', () => {
  it('should extract id correctly', () => {
    expect(idKeyExtractor()).toEqual(NEW_ENTITY_KEY);
    expect(idKeyExtractor(null)).toEqual(NEW_ENTITY_KEY);
    expect(idKeyExtractor(undefined)).toEqual(NEW_ENTITY_KEY);
    expect(idKeyExtractor('')).toEqual('');
    expect(idKeyExtractor('123')).toEqual('123');
    expect(idKeyExtractor(123)).toEqual(123);
    expect(idKeyExtractor({})).toEqual(NEW_ENTITY_KEY);
    expect(idKeyExtractor({ foo: 'bar' })).toEqual(NEW_ENTITY_KEY);
    expect(idKeyExtractor([{ foo: 'bar' }])).toEqual(NEW_ENTITY_KEY);
    expect(idKeyExtractor({ id: 123 })).toEqual(123);
    expect(idKeyExtractor({ id: '123' })).toEqual('123');
  });
});

describe('generic module keyExtractors isNewEntityKey', () => {
  it('should determine new entity correctly', () => {
    expect(isNewEntityKey()).toBeTruthy();
    expect(isNewEntityKey(null)).toBeFalsy();
    expect(isNewEntityKey(undefined)).toBeTruthy();
    expect(isNewEntityKey('')).toBeFalsy();
    expect(isNewEntityKey('123')).toBeFalsy();
    expect(isNewEntityKey('new')).toBeTruthy();
    expect(isNewEntityKey(NEW_ENTITY_KEY)).toBeTruthy();
  });
});
