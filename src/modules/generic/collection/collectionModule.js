/* @flow */

import type { Saga } from 'redux-saga';
import _get from 'lodash.get';

import type {
  CrudOptions,
  ActionTypeFunction,
  Reducer,
  State,
  ActionTypes,
} from '../types';
import { createEntityModule } from '../entity/entityModule';
import { createFiltersModule } from '../filters/filtersModule';
import type {
  FiltersModuleOptions,
  FiltersModule,
} from '../filters/filtersModule';
import type { ModuleOptions } from '../entity/types';
import type { GenericEntityModule } from '../entity/entityModule';

export type GenericCollectionModuleOptions = CrudOptions & {
  typePrefix?: string,
  getState?: (state: any) => State,
  keyExtractor?: (payload: any) => string,
  keysMax?: number,
  shouldExecuteAction?: (key: string) => Saga,
  initialData?: any,
  choicesModuleOptions?: ModuleOptions,
  filtersModuleOptions?: FiltersModuleOptions,
};

/**
 * Create generic redux reducer, actions, selectors and sagas for requesting a collection of object entities. Thin convenience around {@link createEntityModule} which only exposes features related to request
 *
 * @param  {String} collectionName The module entity name e.g. 'user'
 * @param  {ModuleOptions} options
 * @param  {String} [options.typePrefix] Type action type prefix e.g. 'users/user'
 * @param  {function} [options.getState] Function to override the generic getState
 * @param  {function} [options.keyExtractor] Function to derive the collection storage key for state
 * @param  {number} [options.keysMax] Maximum number of keyed entities to keep in state, no limit if <= 0
 * @param  {Object={}} [options.initialData] The initial data in the state
 * @param  {string} [options.operationId] Swagger operation id
 * @param  {Generator} [options.url] Saga to modify the url
 * @param  {Generator} [options.parameters] Saga to modify the parameters
 * @param  {Generator} [options.onSuccess] Saga to call on success
 * @param  {Generator} [options.onFailure] Saga to call on failure
 *
 * @example
 * const module = createCollectionModule('users');
 *
 * const {
 *   reducer,
 *   actions: { requestCollection },
 *   selectors: { getCollection, getError, getIsFetching },
 *   sagas: { collectionSaga },
 * } = module;
 */

export type GenericCollectionModule = {
  collectionName: string,
  actionType: ActionTypeFunction,
  actionTypes: ActionTypes,
  selectors: Selectors,
  reducer: Reducer,
  actions: Actions,
  sagas: Sagas,
  keyExtractor?: (payload: any) => string,
  filtersModule: FiltersModule,
  choicesModule: GenericEntityModule,
};

type Selectors = {
  getState: (state: any) => State,
  getCollection: (state: State, key?: string) => any,
  getIsFetching: (state: State, key?: string) => boolean,
  getError: (state: State, key?: string) => any,
  makeGetCollection: (key: string) => (state: State) => any,
  makeGetIsFetching: (key: string) => (state: State) => any,
  makeGetError: (key: string) => (state: State) => any,
};

type Actions = {
  requestCollection: (entity?: any) => any,
  setCollection: (entity?: any) => any,
  resetCollection: () => any,
};

type Sagas = {
  collectionSaga: Function,
  requestCollectionWorker: Function,
};

export const createCollectionModule = (
  collectionName: string,
  options: GenericCollectionModuleOptions = {}
): GenericCollectionModule => {
  const {
    typePrefix,
    getState,
    keyExtractor,
    keysMax,
    shouldExecuteAction,
    initialData = [],
    choicesModuleOptions,
    filtersModuleOptions,
    ...request
  } = options;

  // filters

  let filtersModule: FiltersModule;
  if (filtersModuleOptions) {
    if (!filtersModuleOptions.typePrefix) {
      // default typePrefix => collection/collectionFilters/
      filtersModuleOptions.typePrefix = `${collectionName}/${collectionName}Filters/`;
    }
    if (!filtersModuleOptions.getState) {
      // default getState => state.collection.collectionFilters
      filtersModuleOptions.getState = (state: any): State =>
        _get(state, `${collectionName}.${collectionName}Filters`);
    }
    filtersModule = createFiltersModule(
      `${collectionName}Filters`,
      filtersModuleOptions
    );
    // set paramaters from filters
    if (!request.parameters) {
      request.parameters = filtersModule.sagas.getFiltersSaga;
    }
  }

  // choices

  let choicesModule: GenericEntityModule;
  if (choicesModuleOptions) {
    if (!choicesModuleOptions.typePrefix) {
      // default typePrefix => collection/collectionChoices/
      choicesModuleOptions.typePrefix = `${collectionName}/${collectionName}Choices/`;
    }
    if (!choicesModuleOptions.getState) {
      // default getState => state.collection.collectionChoices
      choicesModuleOptions.getState = (state: any): State =>
        _get(state, `${collectionName}.${collectionName}Choices`);
    }
    // set paramaters from filters
    if (
      filtersModule &&
      choicesModuleOptions.request &&
      !choicesModuleOptions.request.parameters
    ) {
      choicesModuleOptions.request.parameters =
        filtersModule.sagas.getDataFiltersSaga;
    }
    choicesModule = createEntityModule(
      `${collectionName}Choices`,
      choicesModuleOptions
    );
  }

  // module

  const module = createEntityModule(collectionName, {
    typePrefix,
    keyExtractor,
    keysMax,
    shouldExecuteAction,
    initialData,
    getState,
    request,
  });

  const {
    actionType,
    actionTypes,
    reducer,
    actions: { requestEntity, resetEntity, setEntity },
    selectors: {
      getState: getStateSelector,
      getEntity,
      getError,
      getIsFetching,
      makeGetEntity,
      makeGetError,
      makeGetIsFetching,
    },
    sagas: { entitySaga, requestEntityWorker },
  } = module;

  return {
    collectionName,
    actionType,
    actionTypes,
    reducer,
    actions: {
      requestCollection: requestEntity,
      resetCollection: resetEntity,
      setCollection: setEntity,
    },
    selectors: {
      getState: getStateSelector,
      getCollection: getEntity,
      getError,
      getIsFetching,
      makeGetCollection: makeGetEntity,
      makeGetError,
      makeGetIsFetching,
    },
    sagas: {
      collectionSaga: entitySaga,
      requestCollectionWorker: requestEntityWorker,
    },
    keyExtractor,
    filtersModule,
    choicesModule,
  };
};
