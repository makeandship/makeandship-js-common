/* @flow */

import { createCollectionModule } from './collectionModule';
import { queryStringKeyExtractor } from '../keyExtractors';

const entityName = 'incident';

const module = createCollectionModule(entityName, {
  getState: (state) => state,
  keyExtractor: queryStringKeyExtractor,
});

const {
  reducer,
  actionTypes,
  selectors: { getState, makeGetCollection, makeGetError, makeGetIsFetching },
} = module;

describe('collectionModule.js keyed reducer', () => {
  const query = { id: ['123', '456'], sort: 'desc' };
  const collection = [
    { id: '123', entity: true },
    { id: '456', entity: true },
  ];
  const error = { error: true };
  let state;

  const key = queryStringKeyExtractor(query);
  const meta = { key };

  const getCollection = makeGetCollection(key);
  const getError = makeGetError(key);
  const getIsFetching = makeGetIsFetching(key);

  it('should return the initial state', () => {
    state = reducer(state, {});
    expect(getState(state)).toEqual({});
  });

  // Request

  it('should handle request collection', () => {
    const type = actionTypes.REQUEST_REQUEST;
    state = reducer(state, { type, payload: query, meta });
    expect(getIsFetching(state)).toBeTruthy();
  });

  it('should handle request entity failure', () => {
    const type = actionTypes.REQUEST_FAILURE;
    state = reducer(state, {
      type,
      payload: error,
      meta,
    });
    expect(getError(state)).toEqual(error);
    expect(getIsFetching(state)).toBeFalsy();
  });

  it('should handle request entity success', () => {
    const type = actionTypes.REQUEST_SUCCESS;
    state = reducer(state, {
      type,
      payload: collection,
      meta,
    });
    expect(getCollection(state)).toEqual(collection);
    expect(getIsFetching(state)).toBeFalsy();
    expect(getState(state)).toMatchSnapshot();
  });

  // Reset

  it('should handle reset entity', () => {
    const type = actionTypes.RESET;
    state = reducer(state, { type });
    expect(getCollection(state)).toBeUndefined();
    expect(getIsFetching(state)).toBeFalsy();
    expect(getState(state)).toEqual({});
  });
});
