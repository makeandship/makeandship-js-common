/* @flow */

import sagaHelper from 'redux-saga-testing';

import { types } from '../actions';
import { createCollectionModule } from './collectionModule';

const collectionName = 'incidents';

const module = createCollectionModule(collectionName, {
  getState: (state) => state,
});

const {
  reducer,
  actionType,
  actions: { requestCollection },
  selectors: { getCollection, getError, getIsFetching },
  sagas: { requestCollectionWorker },
} = module;

describe('generic collection module reducer', () => {
  const collection = [
    { id: '123', entity: true },
    { id: '456', entity: true },
  ];
  const error = { error: true };
  let state;

  it('should return the initial state', () => {
    state = reducer(state, {});
    expect(state).toEqual({ data: [] });
  });

  // Request

  it('should handle request collection', () => {
    const type = actionType(types.REQUEST, types.REQUEST);
    state = reducer(state, { type });
    expect(getIsFetching(state)).toBeTruthy();
  });

  it('should handle request collection failure', () => {
    const type = actionType(types.REQUEST, types.FAILURE);
    state = reducer(state, { type, payload: error });
    expect(getError(state)).toEqual(error);
    expect(getIsFetching(state)).toBeFalsy();
  });

  it('should handle request collection success', () => {
    const type = actionType(types.REQUEST, types.SUCCESS);
    state = reducer(state, { type, payload: collection });
    expect(getCollection(state)).toEqual(collection);
    expect(getIsFetching(state)).toBeFalsy();
  });
});

describe('generic collection module request collection worker', () => {
  const test = sagaHelper(requestCollectionWorker(requestCollection()));
  test('make api call', (result) => {
    expect(result).toMatchSnapshot();
  });
});
