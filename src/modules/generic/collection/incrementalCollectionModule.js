/* @flow */

import { all, fork, takeEvery, put, select } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import _get from 'lodash.get';
import { createSelector } from 'reselect';
import deepDiff from 'deep-diff';
import produce from 'immer';

import type { GenericCollectionModule } from './collectionModule';

import { REQUEST, RESET, SET } from '../actions';

export const DEFAULT_PLACEHOLDER_ENTITY = { placeholder: true };

export type Pagination = {
  next: number,
  page: number,
  pages: number,
  per: number,
};

type State = {
  pagination?: Pagination,
  filters?: any,
  data: Array<*>,
};

type Reducer = (state?: State, action?: Object) => State;

export type IncrementalCollectionModuleOptions = {
  getState?: (state: any) => State,
  placeholderEntity?: any,
  shouldRequestOnFiltersChange?: boolean,
};

export type IncrementalCollectionModule = {
  actionTypes: ActionTypes,
  selectors: Selectors,
  reducer: Reducer,
  actions: Actions,
  sagas: Sagas,
  collectionModule: GenericCollectionModule,
};

type ActionTypes = {
  INCREMENTAL_REFRESH_REQUEST: string,
  INCREMENTAL_NEXT_REQUEST: string,
  INCREMENTAL_RESET: string,
  INCREMENTAL_SET: string,
};

type Selectors = {
  getState: (state: any) => State,
  getIncrementalCollectionPagination: (state: State) => Pagination,
  getIncrementalCollectionFilters: (state: any) => any,
  getIncrementalCollection: (state: any) => Array<*>,
  getIncrementalCollectionTotal: (state: any) => number,
};

type Actions = {
  requestIncrementalCollectionRefresh: () => any,
  requestIncrementalCollectionNext: () => any,
  setIncrementalCollection: (data: Array<*>) => any,
  resetIncrementalCollection: () => any,
};

type Sagas = {
  incrementalCollectionSaga: Function,
};

export const createIncrementalCollectionModule = (
  collectionModule: GenericCollectionModule,
  options: IncrementalCollectionModuleOptions
): IncrementalCollectionModule => {
  // Actions

  const { collectionName, actionType, filtersModule } = collectionModule;

  if (!filtersModule) {
    throw 'createIncrementalCollectionModule requires a collectionModule with a filtersModule';
  }

  const INCREMENTAL_REFRESH_REQUEST = actionType(
    'INCREMENTAL_REFRESH',
    REQUEST
  );

  const INCREMENTAL_NEXT_REQUEST = actionType('INCREMENTAL_NEXT', REQUEST);

  const INCREMENTAL_RESET = actionType('INCREMENTAL', RESET);

  const INCREMENTAL_SET = actionType('INCREMENTAL', SET);

  const actionTypes = {
    INCREMENTAL_REFRESH_REQUEST,
    INCREMENTAL_NEXT_REQUEST,
    INCREMENTAL_RESET,
    INCREMENTAL_SET,
  };

  // Reducer

  const initialState: State = {
    data: [],
  };

  const reducer = (
    state?: State = initialState,
    action?: Object = {}
  ): State => {
    switch (action.type) {
      case INCREMENTAL_SET:
        return {
          ...action.payload,
        };
      case INCREMENTAL_RESET:
        return initialState;
      default:
        return state;
    }
  };

  // Actions

  const requestIncrementalCollectionRefresh = () => ({
    type: INCREMENTAL_REFRESH_REQUEST,
  });

  const requestIncrementalCollectionNext = () => ({
    type: INCREMENTAL_NEXT_REQUEST,
  });

  const setIncrementalCollection = (payload) => ({
    type: INCREMENTAL_SET,
    payload,
  });

  const resetIncrementalCollection = () => ({
    type: INCREMENTAL_RESET,
  });

  const actions = {
    requestIncrementalCollectionRefresh,
    requestIncrementalCollectionNext,
    setIncrementalCollection,
    resetIncrementalCollection,
  };

  // Selectors

  const getState =
    options.getState ||
    ((state: any): State =>
      _get(state, `${collectionName}.${collectionName}Incremental`));

  const getIncrementalCollectionPagination = createSelector(
    getState,
    (state) => state && state.pagination
  );

  const getIncrementalCollectionFilters = createSelector(
    getState,
    (state) => state && state.filters
  );

  const getIncrementalCollection = createSelector(
    getState,
    (state) => state && state.data
  );

  const getIncrementalCollectionTotal = createSelector(
    getState,
    (state) => state && state.total
  );

  const selectors = {
    getState,
    getIncrementalCollectionPagination,
    getIncrementalCollectionFilters,
    getIncrementalCollection,
    getIncrementalCollectionTotal,
  };

  // Sagas

  const placeholderEntity =
    options.placeholderEntity || DEFAULT_PLACEHOLDER_ENTITY;

  const shouldRequestOnFiltersChange =
    options.shouldRequestOnFiltersChange === undefined
      ? true
      : options.shouldRequestOnFiltersChange;

  // reset the incremental list if the filters change, excepting page

  function* setFiltersWatcher(): Saga {
    yield takeEvery(filtersModule.actionTypes.SET, setFiltersWorker);
  }

  function* setFiltersWorker(action: any): Saga {
    const filters = action.payload;
    const currentFilters = yield select(getIncrementalCollectionFilters);
    if (!currentFilters) {
      return;
    }
    const diff = deepDiff(filters, currentFilters);
    if (diff) {
      const changedKeyPaths = diff.map((entry) =>
        entry.path ? entry.path[0] : ''
      );
      const onlyPageChanged =
        changedKeyPaths.length === 1 && changedKeyPaths[0] === 'page';
      if (!onlyPageChanged) {
        yield put(resetIncrementalCollection());
      }
    }
  }

  // fresh load of collection from page 1

  function* requestRefreshWatcher(): Saga {
    yield takeEvery(INCREMENTAL_REFRESH_REQUEST, requestRefreshWorker);
  }

  function* requestRefreshWorker(): Saga {
    const filters = yield select(filtersModule.selectors.getFilters);
    yield put(
      filtersModule.actions.setFilters({
        ...filters,
        page: 1,
      })
    );
    if (shouldRequestOnFiltersChange) {
      yield put(collectionModule.actions.requestCollection());
    }
  }

  // incremental load of collection

  function* requestWatcher(): Saga {
    yield takeEvery(INCREMENTAL_NEXT_REQUEST, requestWorker);
  }

  function* requestWorker(): Saga {
    const currentPagination = yield select(getIncrementalCollectionPagination);
    if (currentPagination) {
      if (currentPagination.page < currentPagination.pages) {
        // request the next page
        const filters = yield select(getIncrementalCollectionFilters);
        yield put(
          filtersModule.actions.setFilters({
            ...filters,
            page: currentPagination.page + 1,
          })
        );
        if (shouldRequestOnFiltersChange) {
          yield put(collectionModule.actions.requestCollection());
        }
      } else {
        // do nothing
      }
    } else {
      yield put(collectionModule.actions.requestCollection());
    }
  }

  // merge in the results if applicable

  function* requestSuccessWatcher(): Saga {
    yield takeEvery(
      collectionModule.actionTypes.REQUEST_SUCCESS,
      requestSuccessWorker
    );
  }

  function* requestSuccessWorker(action: any): Saga {
    const {
      payload: {
        total,
        pagination: { page, pages, per },
        results,
      },
    } = action;
    const filters = yield select(filtersModule.selectors.getFilters);
    let data = yield select(getIncrementalCollection);
    const parsedPage = parseInt(page);
    const parsedPages = parseInt(pages);
    const parsedPer = parseInt(per);
    if (parsedPage === 1) {
      // replace
      data = results;
    } else {
      // insert
      data = produce(data, (draft) => {
        const insertIndex = (parsedPage - 1) * parsedPer;
        while (draft.length < insertIndex) {
          draft.push(placeholderEntity);
        }
        for (let i = 0; i < results.length; i++) {
          draft[insertIndex + i] = results[i];
        }
      });
    }
    yield put(
      setIncrementalCollection({
        filters,
        total,
        pagination: {
          page: parsedPage,
          pages: parsedPages,
          per: parsedPer,
        },
        data,
      })
    );
  }

  function* incrementalCollectionSaga(): Saga {
    const sagas = [
      setFiltersWatcher,
      requestRefreshWatcher,
      requestWatcher,
      requestSuccessWatcher,
    ];
    yield all(sagas.map((saga) => fork(saga)));
  }

  const sagas = {
    incrementalCollectionSaga,
  };

  return {
    reducer,
    actions,
    actionTypes,
    selectors,
    sagas,
    collectionModule,
  };
};
