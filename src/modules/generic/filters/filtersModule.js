/* @flow */

import { select } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import { createSelector } from 'reselect';
import pluralize from 'pluralize';
import _omit from 'lodash.omit';

import { RESET, SET, actionTypeBase } from '../actions';

import type { ActionTypeFunction } from '../types';

export type State = any;

export type FiltersModuleOptions = {
  typePrefix?: string,
  getState?: (state: any) => State,
  initialData?: any,
  dataFiltersBlacklist?: Array<string>,
  choicesFiltersBlacklist?: Array<string>,
};

export type FiltersModule = {
  actionType: ActionTypeFunction,
  actionTypes: ActionTypes,
  selectors: Selectors,
  reducer: Reducer,
  actions: Actions,
  sagas: Sagas,
};

export const createFiltersModule = (
  filtersName: string = 'filters',
  options: FiltersModuleOptions = {}
): FiltersModule => {
  const actionType: ActionTypeFunction = (prefix, suffix) =>
    actionTypeBase(prefix, filtersName, suffix, options.typePrefix);
  const actionTypes = createActionTypes(actionType);
  const selectors = createSelectors(filtersName, options);
  const reducer = createReducer(filtersName, actionTypes, options);
  const actions = createActions(filtersName, actionTypes);
  const sagas = createSagas(filtersName, actionTypes, options, {
    selectors,
    actions,
  });
  return {
    actionType,
    actionTypes,
    selectors,
    reducer,
    actions,
    sagas,
  };
};

// Selectors

type Selectors = {
  getFilters: (state: any) => any,
  getDataFilters: (state: any) => any,
  getHasDataFilters: (state: any) => boolean,
  getChoicesFilters: (state: any) => any,
  getHasChoicesFilters: (state: any) => boolean,
};

export const createSelectors = (
  filtersName: string,
  options: FiltersModuleOptions
): Selectors => {
  const getFilters =
    options.getState ||
    ((state: any): State => state[pluralize(filtersName)][filtersName]);

  // return filters that affect data
  // by default, ignore keys which affect pagination / order or free-text query
  const dataFiltersBlacklist = options.dataFiltersBlacklist || [
    'page',
    'order',
    'sort',
  ];
  const getDataFilters = createSelector(getFilters, (filters) => {
    const ignoredKeys = dataFiltersBlacklist;
    const dataFilters = _omit(filters, ignoredKeys);
    return dataFilters;
  });
  const getHasDataFilters = createSelector(getDataFilters, (filters) => {
    const keys = Object.keys(filters);
    return keys.length !== 0;
  });

  // return filters that affect choices (usually data filters, minus free text query)
  // by default, same as data filters but omit free text query
  const choicesFiltersBlacklist = options.choicesFiltersBlacklist || [
    'page',
    'order',
    'sort',
    'q',
  ];
  const getChoicesFilters = createSelector(getFilters, (filters) => {
    const ignoredKeys = choicesFiltersBlacklist;
    const choicesFilters = _omit(filters, ignoredKeys);
    return choicesFilters;
  });
  const getHasChoicesFilters = createSelector(getChoicesFilters, (filters) => {
    const keys = Object.keys(filters);
    return keys.length !== 0;
  });

  return {
    getFilters,
    getDataFilters,
    getHasDataFilters,
    getChoicesFilters,
    getHasChoicesFilters,
  };
};

// Action types

export type ActionTypes = {
  SET: string,
  RESET: string,
};

export const createActionTypes = (
  actionType: ActionTypeFunction
): ActionTypes => ({
  SET: actionType(SET),
  RESET: actionType(RESET),
});

// Reducer

type Reducer = (state: State, action: Object) => Object;

export const createReducer = (
  filtersName: string,
  actionTypes: ActionTypes,
  moduleOptions: FiltersModuleOptions
): Reducer => {
  const initialState: State = moduleOptions.initialData || {};

  const reducer = (state: State = initialState, action: Object = {}) => {
    switch (action.type) {
      case actionTypes.RESET:
        return initialState;
      case actionTypes.SET:
        return {
          ...action.payload,
        };
      default:
        return state;
    }
  };
  return reducer;
};

// Actions

type Actions = {
  setFilters: (filters?: any) => any,
  resetFilters: () => any,
};

export const createActions = (
  filtersName: string,
  actionTypes: ActionTypes
): Actions => {
  const setFilters = (filters?: any) => ({
    type: actionTypes.SET,
    payload: filters,
  });

  const resetFilters = () => ({
    type: actionTypes.RESET,
  });

  return {
    setFilters,
    resetFilters,
  };
};

// Side effects

type Sagas = {
  getFiltersSaga: Function,
  getDataFiltersSaga: Function,
  getChoicesFiltersSaga: Function,
};

export const createSagas = (
  filtersName: string,
  actionTypes: ActionTypes,
  moduleOptions: FiltersModuleOptions,
  { selectors }: { selectors: Selectors, actions: Actions }
): Sagas => {
  function* getFiltersSaga(): Saga {
    const filters = yield select(selectors.getFilters);
    return yield filters;
  }

  function* getDataFiltersSaga(): Saga {
    const filters = yield select(selectors.getDataFilters);
    return yield filters;
  }

  function* getChoicesFiltersSaga(): Saga {
    const filters = yield select(selectors.getChoicesFilters);
    return yield filters;
  }

  return {
    getFiltersSaga,
    getDataFiltersSaga,
    getChoicesFiltersSaga,
  };
};
