/* @flow */

import { createFiltersModule } from './filtersModule';

const filtersName = 'filters';

const module = createFiltersModule(filtersName);

const {
  reducer,
  actions: { setFilters, resetFilters },
  selectors: { getFilters, getHasDataFilters },
} = module;

const shapeState = (state) => {
  return {
    filters: {
      filters: state,
    },
  };
};

describe('generic filters module reducer', () => {
  let state;

  it('should return the initial state', () => {
    state = reducer(state, {});
    expect(state).toEqual({});
    expect(getHasDataFilters(shapeState(state))).toBeFalsy();
  });

  it('should handle set filters', () => {
    const action = setFilters({ test: true });
    state = reducer(state, action);
    expect(getFilters(shapeState(state))).toEqual({ test: true });
    expect(getHasDataFilters(shapeState(state))).toBeTruthy();
  });

  it('should handle reset filters', () => {
    const action = resetFilters();
    state = reducer(state, action);
    expect(getFilters(shapeState(state))).toEqual({});
    expect(getHasDataFilters(shapeState(state))).toBeFalsy();
  });

  it('should ignore non-data filters', () => {
    const action = setFilters({ page: 123 });
    state = reducer(state, action);
    expect(getFilters(shapeState(state))).toEqual({ page: 123 });
    expect(getHasDataFilters(shapeState(state))).toBeFalsy();
  });
});
