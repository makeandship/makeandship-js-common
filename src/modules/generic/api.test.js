/* @flow */

import sagaHelper from 'redux-saga-testing';
import { call } from 'redux-saga/effects';

import { types } from './actions';
import { createEntityModule } from './entity/entityModule';
import { genericJsonApiCall, genericSwaggerCall } from './api';
import type { CrudOptions } from './types';

import { setConfig as setApiConfig } from '../api/config';

const apiUrl = 'http://localhost:3001';

setApiConfig({
  apiUrl,
});

const entityName = 'incident';

function* url(url) {
  return yield url;
}

function* onSuccess() {
  return yield true;
}

function* onFailure() {
  return yield true;
}

describe('generic json api call', () => {
  const crudOptions: CrudOptions = {
    url,
    onSuccess,
    onFailure,
  };

  const module = createEntityModule(entityName, { ...crudOptions });

  const {
    actionType,
    actions: { createEntity },
  } = module;

  const entity = { entity: true };
  const action = createEntity(entity);
  const options = {
    method: 'POST',
    body: action.payload,
  };
  const test = sagaHelper(
    genericJsonApiCall({
      entityName,
      type: types.REQUEST,
      actionType,
      crudOptions,
      action,
      options,
    })
  );
  test('call url', (result) => {
    expect(result).toMatchSnapshot();
    return '/url/test/incidents';
  });
  test('make api call', (result) => {
    expect(result).toMatchSnapshot();
    return { success: true };
  });
  test('call onSuccess', (result) => {
    expect(result).toEqual(call(onSuccess, { action, success: true }));
  });
});

describe('generic swagger call', () => {
  const crudOptions: CrudOptions = {
    operationId: 'testOperationId',
    onSuccess,
    onFailure,
  };

  const module = createEntityModule(entityName, { ...crudOptions });

  const {
    actionType,
    actions: { createEntity },
  } = module;

  const entity = { entity: true };
  const action = createEntity(entity);
  const parameters = {
    body: action.payload,
  };
  const test = sagaHelper(
    genericSwaggerCall({
      type: types.REQUEST,
      actionType,
      crudOptions,
      action,
      parameters,
    })
  );
  test('make api call', (result) => {
    expect(result).toMatchSnapshot();
    return { success: true };
  });
  test('call onSuccess', (result) => {
    expect(result).toEqual(call(onSuccess, { action, success: true }));
  });
});
