/* @flow */

import { urlJoin } from './utils';

describe('generic module url join', () => {
  it('should create urls correctly', () => {
    expect(urlJoin('a', 'b', '1')).toEqual('a/b/1');
    expect(urlJoin('a', 'b', 1)).toEqual('a/b/1');
    expect(urlJoin('a', 'b', '')).toEqual('a/b');
    expect(urlJoin('a', 'b', undefined)).toEqual('a/b');
    expect(urlJoin('a', 'b', false)).toEqual('a/b');
    expect(urlJoin('a', 'b', null)).toEqual('a/b');
  });
});
