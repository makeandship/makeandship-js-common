/* @flow */

import { put, call, select } from 'redux-saga/effects';
import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';

import { createEntityModule } from './entityModule';
import { idKeyExtractor } from '../keyExtractors';

import { setConfig } from '../../api/config';
import { waitForChange, delay as delayPromise } from '../../utils';

const apiUrl = 'http://localhost:3001';

setConfig({
  apiUrl,
});

const expectedHeaders = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

const entityName = 'test';

const module = createEntityModule(entityName, {
  getState: (state) => state.test,
  keyExtractor: idKeyExtractor,
});

const {
  actions,
  selectors: { makeGetEntity },
} = module;

export const sagaMiddleware = createSagaMiddleware();
/* eslint-disable no-unused-vars */
const store = configureStore({
  reducer: { test: module.reducer },
  middleware: [sagaMiddleware],
});
/* eslint-enable no-unused-vars */
sagaMiddleware.run(module.sagas.entitySaga);

describe('keyed entity module', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('when entity is requested', () => {
    describe('when request succeeds', () => {
      it('entity is availaible in the store', async (done) => {
        const saga = function* () {
          const entity = { id: '123', entity: true };
          const getEntity = makeGetEntity(entity.id);
          const fetcher = jest.fn(async () => {
            await delayPromise(500);
            return 'data';
          });
          setConfig({
            fetcher,
          });
          const initialEntity = yield select(getEntity);
          expect(initialEntity).toBeUndefined();
          yield put(actions.requestEntity(entity.id));
          const nextEntity = yield waitForChange(getEntity);
          expect(nextEntity).toEqual('data');
          expect(fetcher).toHaveBeenCalledWith(`${apiUrl}/tests/${entity.id}`, {
            headers: expectedHeaders,
          });
          yield call(done);
        };
        sagaMiddleware.run(saga);
      });
    });
  });
});
