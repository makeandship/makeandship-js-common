/* @flow */

import type { ActionTypes, ActionTypeFunction } from '../types';
import type { ModuleOptions, Actions } from './types';

import {
  NEW,
  CREATE,
  REQUEST,
  UPDATE,
  DELETE,
  SUCCESS,
  FAILURE,
  RESET,
  SET,
  ABORT,
  UPLOAD_PROGRESS,
  DOWNLOAD_PROGRESS,
} from '../actions';

export const createActionTypes = (
  actionType: ActionTypeFunction
): ActionTypes => ({
  NEW: actionType(NEW),
  CREATE: actionType(CREATE),
  REQUEST: actionType(REQUEST),
  UPDATE: actionType(UPDATE),
  DELETE: actionType(DELETE),
  SET: actionType(SET),
  RESET: actionType(RESET),
  ABORT: actionType(ABORT),
  CREATE_REQUEST: actionType(CREATE, REQUEST),
  REQUEST_REQUEST: actionType(REQUEST, REQUEST),
  UPDATE_REQUEST: actionType(UPDATE, REQUEST),
  DELETE_REQUEST: actionType(DELETE, REQUEST),
  CREATE_SUCCESS: actionType(CREATE, SUCCESS),
  REQUEST_SUCCESS: actionType(REQUEST, SUCCESS),
  UPDATE_SUCCESS: actionType(UPDATE, SUCCESS),
  DELETE_SUCCESS: actionType(DELETE, SUCCESS),
  CREATE_FAILURE: actionType(CREATE, FAILURE),
  REQUEST_FAILURE: actionType(REQUEST, FAILURE),
  UPDATE_FAILURE: actionType(UPDATE, FAILURE),
  DELETE_FAILURE: actionType(DELETE, FAILURE),
  CREATE_ABORT: actionType(CREATE, ABORT),
  REQUEST_ABORT: actionType(REQUEST, ABORT),
  UPDATE_ABORT: actionType(UPDATE, ABORT),
  DELETE_ABORT: actionType(DELETE, ABORT),
  CREATE_UPLOAD_PROGRESS: actionType(CREATE, UPLOAD_PROGRESS),
  REQUEST_UPLOAD_PROGRESS: actionType(REQUEST, UPLOAD_PROGRESS),
  UPDATE_UPLOAD_PROGRESS: actionType(UPDATE, UPLOAD_PROGRESS),
  DELETE_UPLOAD_PROGRESS: actionType(DELETE, UPLOAD_PROGRESS),
  CREATE_DOWNLOAD_PROGRESS: actionType(CREATE, DOWNLOAD_PROGRESS),
  REQUEST_DOWNLOAD_PROGRESS: actionType(REQUEST, DOWNLOAD_PROGRESS),
  UPDATE_DOWNLOAD_PROGRESS: actionType(UPDATE, DOWNLOAD_PROGRESS),
  DELETE_DOWNLOAD_PROGRESS: actionType(DELETE, DOWNLOAD_PROGRESS),
});

export const createActions = (
  entityName: string,
  actionTypes: ActionTypes,
  moduleOptions: ModuleOptions
): Actions => {
  const createMeta = (payload) => {
    if (moduleOptions.keyExtractor) {
      // always use a key
      const key = moduleOptions.keyExtractor(payload);
      return {
        key,
      };
    }
    return undefined;
  };

  const newEntity = (payload?: any) => ({
    type: actionTypes.NEW,
    payload,
    meta: createMeta(payload),
  });

  const createEntity = (payload: any) => ({
    type: actionTypes.CREATE,
    payload,
    meta: createMeta(payload),
  });

  const requestEntity = (payload?: any) => ({
    type: actionTypes.REQUEST,
    payload,
    meta: createMeta(payload),
  });

  const updateEntity = (payload: any) => ({
    type: actionTypes.UPDATE,
    payload,
    meta: createMeta(payload),
  });

  const deleteEntity = (payload?: any) => ({
    type: actionTypes.DELETE,
    payload,
    meta: createMeta(payload),
  });

  const setEntity = (payload?: any) => ({
    type: actionTypes.SET,
    payload,
    meta: createMeta(payload),
  });

  const resetEntity = () => ({
    type: actionTypes.RESET,
  });

  return {
    newEntity,
    createEntity,
    requestEntity,
    updateEntity,
    deleteEntity,
    setEntity,
    resetEntity,
  };
};
