/* @flow */

import type { Saga } from 'redux-saga';

import type { CrudOptions, ActionTypes, State } from '../types';

export type ModuleOptions = {
  typePrefix?: string,
  getState?: (state: any) => State,
  keyExtractor?: (payload: any) => string,
  keysMax?: number,
  shouldExecuteAction?: (
    action: any,
    actionTypes: ActionTypes,
    selectors: Selectors
  ) => Saga,
  initialData?: any,
  create?: CrudOptions,
  request?: CrudOptions,
  update?: CrudOptions,
  delete?: CrudOptions,
};

export type Actions = {
  newEntity: (entity?: any) => any,
  createEntity: (entity: any) => any,
  requestEntity: (entity?: any) => any,
  updateEntity: (entity: any) => any,
  deleteEntity: (entity?: any) => any,
  setEntity: (entity?: any) => any,
  resetEntity: () => any,
};

export type Selectors = {
  getState: (state: any) => State,
  getEntity: (state: State, key?: string) => any,
  getIsFetching: (state: State, key?: string) => boolean,
  getIsRefetching: (state: State, key?: string) => boolean,
  getIsCreating: (state: State, key?: string) => boolean,
  getIsRequesting: (state: State, key?: string) => boolean,
  getIsUpdating: (state: State, key?: string) => boolean,
  getIsDeleting: (state: State, key?: string) => boolean,
  getError: (state: State, key?: string) => any,
  getUploadProgress: (state: State, key?: string) => any,
  getDownloadProgress: (state: State, key?: string) => any,
  makeGetEntity?: (key: string) => (state: State) => any,
  makeGetIsFetching?: (key: string) => (state: State) => any,
  makeGetIsRefetching?: (key: string) => (state: State) => any,
  makeGetIsCreating?: (key: string) => (state: State) => any,
  makeGetIsRequesting?: (key: string) => (state: State) => any,
  makeGetIsUpdating?: (key: string) => (state: State) => any,
  makeGetIsDeleting?: (key: string) => (state: State) => any,
  makeGetError?: (key: string) => (state: State) => any,
  makeGetUploadProgress?: (key: string) => (state: State) => any,
  makeGetDownloadProgress?: (key: string) => (state: State) => any,
};

export type Sagas = {
  entitySaga: Function,
  createEntityWorker: Function,
  requestEntityWorker: Function,
  updateEntityWorker: Function,
  deleteEntityWorker: Function,
};
