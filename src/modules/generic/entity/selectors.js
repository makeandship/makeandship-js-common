/* @flow */

import { createSelector } from 'reselect';
import pluralize from 'pluralize';
import _get from 'lodash.get';

import type { State } from '../types';
import type { ModuleOptions, Selectors } from './types';

export const createSelectors = (
  entityName: string,
  options: ModuleOptions
): Selectors => {
  const getState =
    options.getState ||
    ((state: any): State => state[pluralize(entityName)][entityName]);

  if (options.keyExtractor) {
    const getEntity = (state: State, key?: string) =>
      _get(getState(state), [key, 'data']);
    const getIsFetching = (state: State, key?: string) =>
      _get(getState(state), [key, 'isFetching'], false);
    const getIsRefetching = (state: State, key?: string) =>
      _get(getState(state), [key, 'isRefetching'], false);
    const getIsCreating = (state: State, key?: string) =>
      _get(getState(state), [key, 'isCreating'], false);
    const getIsRequesting = (state: State, key?: string) =>
      _get(getState(state), [key, 'isRequesting'], false);
    const getIsUpdating = (state: State, key?: string) =>
      _get(getState(state), [key, 'isUpdating'], false);
    const getIsDeleting = (state: State, key?: string) =>
      _get(getState(state), [key, 'isDeleting'], false);
    const getError = (state: State, key?: string) =>
      _get(getState(state), [key, 'error']);
    const getUploadProgress = (state: State, key?: string) =>
      _get(getState(state), [key, 'progress', 'upload']);
    const getDownloadProgress = (state: State, key?: string) =>
      _get(getState(state), [key, 'progress', 'download']);
    const makeGetEntity = (key: string) =>
      createSelector(getState, (state) => _get(state, [key, 'data']));
    const makeGetIsFetching = (key: string) =>
      createSelector(getState, (state) =>
        _get(state, [key, 'isFetching'], false)
      );
    const makeGetIsRefetching = (key: string) =>
      createSelector(getState, (state) =>
        _get(state, [key, 'isRefetching'], false)
      );
    const makeGetIsCreating = (key: string) =>
      createSelector(getState, (state) =>
        _get(state, [key, 'isCreating'], false)
      );
    const makeGetIsRequesting = (key: string) =>
      createSelector(getState, (state) =>
        _get(state, [key, 'isRequesting'], false)
      );
    const makeGetIsUpdating = (key: string) =>
      createSelector(getState, (state) =>
        _get(state, [key, 'isUpdating'], false)
      );
    const makeGetIsDeleting = (key: string) =>
      createSelector(getState, (state) =>
        _get(state, [key, 'isDeleting'], false)
      );
    const makeGetError = (key: string) =>
      createSelector(getState, (state) => _get(state, [key, 'error']));
    const makeGetUploadProgress = (key: string) =>
      createSelector(getState, (state) =>
        _get(state, [key, 'progress', 'upload'])
      );
    const makeGetDownloadProgress = (key: string) =>
      createSelector(getState, (state) =>
        _get(state, [key, 'progress', 'download'])
      );
    return {
      getState,
      getEntity,
      getIsFetching,
      getIsRefetching,
      getIsCreating,
      getIsRequesting,
      getIsUpdating,
      getIsDeleting,
      getError,
      getUploadProgress,
      getDownloadProgress,
      makeGetEntity,
      makeGetIsFetching,
      makeGetIsRefetching,
      makeGetIsCreating,
      makeGetIsRequesting,
      makeGetIsUpdating,
      makeGetIsDeleting,
      makeGetError,
      makeGetUploadProgress,
      makeGetDownloadProgress,
    };
  }

  const getEntity = createSelector(getState, (state) => state && state.data);
  const getIsFetching = createSelector(
    getState,
    (state) => state && !!state.isFetching
  );
  const getIsRefetching = createSelector(
    getState,
    (state) => state && !!state.isRefetching
  );
  const getIsCreating = createSelector(
    getState,
    (state) => state && !!state.isCreating
  );
  const getIsRequesting = createSelector(
    getState,
    (state) => state && !!state.isRequesting
  );
  const getIsUpdating = createSelector(
    getState,
    (state) => state && !!state.isUpdating
  );
  const getIsDeleting = createSelector(
    getState,
    (state) => state && !!state.isDeleting
  );
  const getError = createSelector(getState, (state) => state && state.error);
  const getProgress = createSelector(
    getState,
    (state) => state && state.progress
  );
  const getUploadProgress = createSelector(
    getProgress,
    (progress) => progress && progress.upload
  );
  const getDownloadProgress = createSelector(
    getProgress,
    (progress) => progress && progress.download
  );
  return {
    getState,
    getEntity,
    getIsFetching,
    getIsRefetching,
    getIsCreating,
    getIsRequesting,
    getIsUpdating,
    getIsDeleting,
    getError,
    getUploadProgress,
    getDownloadProgress,
  };
};
