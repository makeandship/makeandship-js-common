/* @flow */

import sagaHelper from 'redux-saga-testing';

import { createEntityModule } from './entityModule';
import type { State } from '../types';

const entityName = 'incident';

const module = createEntityModule(entityName, { getState: (state) => state });

const {
  reducer,
  actionTypes,
  actions: { createEntity, requestEntity, updateEntity, deleteEntity },
  selectors: {
    getEntity,
    getError,
    getIsFetching,
    getIsCreating,
    getIsRequesting,
    getIsUpdating,
    getIsDeleting,
  },
  sagas: {
    createEntityWorker,
    requestEntityWorker,
    updateEntityWorker,
    deleteEntityWorker,
  },
} = module;

describe('entityModule.js reducer', () => {
  const entity = { id: '123', entity: true };
  const error = { error: true };
  let state: State;

  it('should return the initial state', () => {
    state = reducer(state, {});
    expect(state).toEqual({
      data: undefined,
    });
  });

  // Create

  it('should handle create entity', () => {
    const type = actionTypes.CREATE;
    state = reducer(state, { type });
    expect(getIsFetching(state)).toBeFalsy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeFalsy();
  });

  it('should handle create entity request', () => {
    const type = actionTypes.CREATE_REQUEST;
    state = reducer(state, { type });
    expect(getIsFetching(state)).toBeTruthy();
    expect(getIsCreating(state)).toBeTruthy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeFalsy();
  });

  it('should handle create entity failure', () => {
    const type = actionTypes.CREATE_FAILURE;
    state = reducer(state, { type, payload: error });
    expect(getError(state)).toEqual(error);
    expect(getIsFetching(state)).toBeFalsy();
  });

  it('should handle create entity success', () => {
    const type = actionTypes.CREATE_SUCCESS;
    state = reducer(state, { type, payload: entity });
    expect(getEntity(state)).toEqual(entity);
    expect(getIsFetching(state)).toBeFalsy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeFalsy();
  });

  // Request
  it('should handle request entity', () => {
    const type = actionTypes.REQUEST;
    state = reducer(state, { type });
    expect(getIsFetching(state)).toBeFalsy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeFalsy();
  });

  it('should handle request entity request', () => {
    const type = actionTypes.REQUEST_REQUEST;
    state = reducer(state, { type });
    expect(getIsFetching(state)).toBeTruthy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeTruthy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeFalsy();
  });

  it('should handle request entity failure', () => {
    const type = actionTypes.REQUEST_FAILURE;
    state = reducer(state, { type, payload: error });
    expect(getError(state)).toEqual(error);
    expect(getIsFetching(state)).toBeFalsy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeFalsy();
  });

  it('should handle request entity success', () => {
    const type = actionTypes.REQUEST_SUCCESS;
    state = reducer(state, { type, payload: entity });
    expect(getEntity(state)).toEqual(entity);
    expect(getIsFetching(state)).toBeFalsy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeFalsy();
  });

  // Update

  it('should handle update entity', () => {
    const type = actionTypes.UPDATE;
    state = reducer(state, { type });
    expect(getIsFetching(state)).toBeFalsy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeFalsy();
  });

  it('should handle update entity request', () => {
    const type = actionTypes.UPDATE_REQUEST;
    state = reducer(state, { type });
    expect(getIsFetching(state)).toBeTruthy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeTruthy();
    expect(getIsDeleting(state)).toBeFalsy();
  });

  it('should handle update entity failure', () => {
    const type = actionTypes.UPDATE_FAILURE;
    state = reducer(state, { type, payload: error });
    expect(getEntity(state)).toEqual(entity);
    expect(getError(state)).toEqual(error);
    expect(getIsFetching(state)).toBeFalsy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeFalsy();
  });

  it('should handle update entity success', () => {
    const type = actionTypes.UPDATE_SUCCESS;
    state = reducer(state, { type, payload: entity });
    expect(getEntity(state)).toEqual(entity);
    expect(getIsFetching(state)).toBeFalsy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeFalsy();
  });

  // Delete

  it('should handle delete entity', () => {
    const type = actionTypes.DELETE;
    state = reducer(state, { type });
    expect(getIsFetching(state)).toBeFalsy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeFalsy();
  });

  it('should handle delete entity request', () => {
    const type = actionTypes.DELETE_REQUEST;
    state = reducer(state, { type });
    expect(getIsFetching(state)).toBeTruthy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeTruthy();
  });

  it('should handle delete entity failure', () => {
    const type = actionTypes.DELETE_FAILURE;
    state = reducer(state, { type, payload: error });
    expect(getEntity(state)).toEqual(entity);
    expect(getError(state)).toEqual(error);
    expect(getIsFetching(state)).toBeFalsy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeFalsy();
  });

  it('should handle delete entity success', () => {
    const type = actionTypes.DELETE_SUCCESS;
    state = reducer(state, { type, payload: entity });
    expect(getEntity(state)).toBeUndefined();
    expect(getIsFetching(state)).toBeFalsy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeFalsy();
  });

  // Reset

  it('should handle reset entity', () => {
    const type = actionTypes.RESET;
    state = reducer(state, { type });
    expect(getEntity(state)).toBeUndefined();
    expect(getIsFetching(state)).toBeFalsy();
    expect(getIsCreating(state)).toBeFalsy();
    expect(getIsRequesting(state)).toBeFalsy();
    expect(getIsUpdating(state)).toBeFalsy();
    expect(getIsDeleting(state)).toBeFalsy();
  });
});

describe('entityModule.js create entity worker', () => {
  const entity = { entity: true };
  const test = sagaHelper(createEntityWorker(createEntity(entity)));
  test('make api call', (result) => {
    expect(result).toMatchSnapshot();
  });
});

describe('entityModule.js request entity worker', () => {
  const test = sagaHelper(requestEntityWorker(requestEntity('123')));
  test('make api call', (result) => {
    expect(result).toMatchSnapshot();
  });
});

describe('entityModule.jse update entity worker', () => {
  const entity = { id: 123, entity: true };
  const test = sagaHelper(updateEntityWorker(updateEntity(entity)));
  test('make api call', (result) => {
    expect(result).toMatchSnapshot();
  });
});

describe('entityModule.js delete entity worker', () => {
  const test = sagaHelper(deleteEntityWorker(deleteEntity('123')));
  test('make api call', (result) => {
    expect(result).toMatchSnapshot();
  });
});
