/* @flow */

export const DEFAULT_KEYS_MAX = 100;

import produce from 'immer';

import { isString } from '../../../utils/is';

import type { Reducer, ActionTypes, State } from '../types';
import type { ModuleOptions } from './types';

export const createReducer = (
  entityName: string,
  actionTypes: ActionTypes,
  moduleOptions: ModuleOptions
): Reducer => {
  return moduleOptions.keyExtractor
    ? createKeyedReducer(entityName, actionTypes, moduleOptions)
    : createVanillaReducer(entityName, actionTypes, moduleOptions);
};

// Vanilla reducer with single entity
export const createVanillaReducer = (
  entityName: string,
  actionTypes: ActionTypes,
  moduleOptions: ModuleOptions
): Reducer => {
  const initialState: State = {
    data: moduleOptions.initialData || undefined,
  };
  const reducer = (state?: State = initialState, action?: Object = {}): State =>
    produce(state, (draft) => {
      switch (action.type) {
        case actionTypes.NEW:
        case actionTypes.RESET:
          return initialState;
        case actionTypes.REQUEST: {
          const stateEntityId = state.data?.id;
          const actionEntityId = isString(action.payload)
            ? action.payload
            : action.payload?.id;
          const isRefetching =
            stateEntityId !== undefined && stateEntityId === actionEntityId;
          return {
            ...draft,
            isRefetching,
          };
        }
        case actionTypes.CREATE_REQUEST:
        case actionTypes.REQUEST_REQUEST:
        case actionTypes.UPDATE_REQUEST:
        case actionTypes.DELETE_REQUEST:
          return {
            ...draft,
            isFetching: true,
            isCreating: action.type === actionTypes.CREATE_REQUEST,
            isRequesting: action.type === actionTypes.REQUEST_REQUEST,
            isUpdating: action.type === actionTypes.UPDATE_REQUEST,
            isDeleting: action.type === actionTypes.DELETE_REQUEST,
            error: undefined,
          };

        case actionTypes.CREATE_SUCCESS:
        case actionTypes.REQUEST_SUCCESS:
        case actionTypes.UPDATE_SUCCESS:
        case actionTypes.SET:
          return {
            ...draft,
            data: action.payload,
            isFetching: false,
            isRefetching: false,
            isCreating: false,
            isRequesting: false,
            isUpdating: false,
            isDeleting: false,
            error: undefined,
          };
        case actionTypes.DELETE_SUCCESS:
          return {
            ...draft,
            data: moduleOptions.initialData || undefined,
            isFetching: false,
            isRefetching: false,
            isCreating: false,
            isRequesting: false,
            isUpdating: false,
            isDeleting: false,
            error: undefined,
          };
        case actionTypes.CREATE_FAILURE:
        case actionTypes.REQUEST_FAILURE:
        case actionTypes.UPDATE_FAILURE:
        case actionTypes.DELETE_FAILURE:
          return {
            ...draft,
            isFetching: false,
            isRefetching: false,
            isCreating: false,
            isRequesting: false,
            isUpdating: false,
            isDeleting: false,
            error: action.payload,
          };
        case actionTypes.CREATE_UPLOAD_PROGRESS:
        case actionTypes.REQUEST_UPLOAD_PROGRESS:
        case actionTypes.UPDATE_UPLOAD_PROGRESS:
        case actionTypes.DELETE_UPLOAD_PROGRESS:
          draft.progress = {
            ...draft.progress,
            upload: action.payload,
          };
          return;
        case actionTypes.CREATE_DOWNLOAD_PROGRESS:
        case actionTypes.REQUEST_DOWNLOAD_PROGRESS:
        case actionTypes.UPDATE_DOWNLOAD_PROGRESS:
        case actionTypes.DELETE_DOWNLOAD_PROGRESS:
          draft.progress = {
            ...draft.progress,
            download: action.payload,
          };
          return;
        default:
          return;
      }
    });
  return reducer;
};
// Keyed reducer with multiple keyed entity
export const createKeyedReducer = (
  entityName: string,
  actionTypes: ActionTypes,
  { keysMax = DEFAULT_KEYS_MAX }: ModuleOptions
): Reducer => {
  const initialState: State = {};
  const reducer = (state?: State = initialState, action?: Object = {}): State =>
    produce(state, (draft) => {
      switch (action.type) {
        case actionTypes.NEW:
        case actionTypes.RESET:
          return initialState;
        case actionTypes.REQUEST: {
          const { key } = action.meta;
          const stateEntityId = state[key]?.data?.id;
          const actionEntityId = isString(action.payload)
            ? action.payload
            : action.payload?.id;
          const isRefetching =
            stateEntityId !== undefined && stateEntityId === actionEntityId;
          draft[key] = {
            ...draft[key],
            isRefetching,
          };
          return;
        }
        case actionTypes.CREATE_REQUEST:
        case actionTypes.REQUEST_REQUEST:
        case actionTypes.UPDATE_REQUEST:
        case actionTypes.DELETE_REQUEST: {
          const { key } = action.meta;
          draft[key] = {
            ...draft[key],
            isFetching: true,
            isCreating: action.type === actionTypes.CREATE_REQUEST,
            isRequesting: action.type === actionTypes.REQUEST_REQUEST,
            isUpdating: action.type === actionTypes.UPDATE_REQUEST,
            isDeleting: action.type === actionTypes.DELETE_REQUEST,
            error: undefined,
          };
          return;
        }
        case actionTypes.CREATE_SUCCESS:
        case actionTypes.REQUEST_SUCCESS:
        case actionTypes.UPDATE_SUCCESS:
        case actionTypes.SET: {
          const { key } = action.meta;
          draft[key] = {
            ...draft[key],
            data: action.payload,
            isFetching: false,
            isRefetching: false,
            isCreating: false,
            isRequesting: false,
            isUpdating: false,
            isDeleting: false,
            error: undefined,
          };
          if (keysMax > 0) {
            while (Object.keys(draft).length > keysMax) {
              delete draft[Object.keys(draft)[0]];
            }
          }
          return;
        }
        case actionTypes.DELETE_SUCCESS: {
          const { key } = action.meta;
          delete draft[key];
          return;
        }
        case actionTypes.CREATE_FAILURE:
        case actionTypes.REQUEST_FAILURE:
        case actionTypes.UPDATE_FAILURE:
        case actionTypes.DELETE_FAILURE: {
          const { key } = action.meta;
          draft[key] = {
            ...draft[key],
            isFetching: false,
            isRefetching: false,
            isCreating: false,
            isRequesting: false,
            isUpdating: false,
            isDeleting: false,
            error: action.payload,
          };
          return;
        }
        case actionTypes.CREATE_UPLOAD_PROGRESS:
        case actionTypes.REQUEST_UPLOAD_PROGRESS:
        case actionTypes.UPDATE_UPLOAD_PROGRESS:
        case actionTypes.DELETE_UPLOAD_PROGRESS: {
          const { key } = action.meta;
          draft[key].progress = {
            ...draft[key].progress,
            upload: action.payload,
          };
          return;
        }
        case actionTypes.CREATE_DOWNLOAD_PROGRESS:
        case actionTypes.REQUEST_DOWNLOAD_PROGRESS:
        case actionTypes.UPDATE_DOWNLOAD_PROGRESS:
        case actionTypes.DELETE_DOWNLOAD_PROGRESS: {
          const { key } = action.meta;
          draft[key].progress = {
            ...draft[key].progress,
            download: action.payload,
          };
          return;
        }
        default:
          return;
      }
    });
  return reducer;
};
