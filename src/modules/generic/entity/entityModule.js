/* @flow */

import { select } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';

import { actionTypeBase } from '../actions';

import type { ActionTypeFunction, Reducer, ActionTypes } from '../types';
import type { ModuleOptions, Actions, Sagas, Selectors } from './types';

// TODO: this is okay for rarely changing entities or collections
// in the long-term, replace with more robust caching strategy

export function* shouldExecuteRequestActionOncePerKey(
  action: any,
  actionTypes: ActionTypes,
  selectors: Selectors
): Saga {
  const { type, meta } = action;
  // only for fetch entity requests
  if (type !== actionTypes.REQUEST) {
    return true;
  }
  const { key } = meta;
  const state = yield select();
  const isFetching = selectors.getIsFetching(state, key);
  const value = selectors.getEntity(state, key);
  if (value || isFetching) {
    // don't request if there is already a cached value, or we are fetching
    return yield false;
  }
  return yield true;
}

/**
 * Create generic redux reducer, actions, selectors and sagas for an individual object entity
 *
 * @param  {String} entityName The module entity name e.g. 'user'
 * @param  {ModuleOptions={}} [options]
 * @param  {String} [options.typePrefix] Type action type prefix e.g. 'users/user'
 * @param  {function} [options.getState] Function to override the generic getState
 * @param  {function} [options.keyExtractor] Function to derive the entity storage key for state
 * @param  {number} [options.keysMax] Maximum number of keyed entities to keep in state, no limit if <= 0
 * @param  {Object={}} [options.initialData] The initial data in the state
 * @param  {CrudOptions} [options.create] Create options
 * @param  {CrudOptions} [options.request] Request options
 * @param  {CrudOptions} [options.update] Update options
 * @param  {CrudOptions} [options.delete] Delete options
 *
 * @example
 * // simple - will infer urls, action types, state shape etc.
 * const module = createEntityModule('user');
 *
 * // customised - providing sagas for success, will use swagger where id's are supplied
 * const module = createEntityModule('user', {
 *   typePrefix: 'users/user/',
 *   getState: state => state.users.user,
 *   create: {
 *     operationId: 'usersCreate',
 *     onSuccess: function*() {
 *       yield put(showNotification('User created'));
 *       yield put(push('/users'));
 *     },
 *   },
 *   request: {
 *     operationId: 'usersGetById',
 *   },
 *   update: {
 *     operationId: 'usersUpdateById',
 *     onSuccess: function*() {
 *       yield put(showNotification('User updated'));
 *       yield put(push('/users'));
 *     },
 *   },
 *   delete: {
 *     operationId: 'usersDeleteById',
 *     onSuccess: function*() {
 *       yield put(showNotification('User deleted'));
 *       yield put(push('/users'));
 *     },
 *   },
 * });
 *
 * // destructure the desired reducer, actions, selectors, sagas
 * const {
 *   reducer,
 *   actions: {
 *     newEntity,
 *     createEntity,
 *     requestEntity,
 *     updateEntity,
 *     deleteEntity,
 *   },
 *   selectors: { getEntity, getError, getIsFetching },
 *   sagas: { entitySaga },
 * } = module;
 */

import { createActions, createActionTypes } from './actions';
import { createSelectors } from './selectors';
import { createReducer } from './reducer';
import { createSagas } from './sagas';

export type GenericEntityModule = {
  entityName: string,
  actionType: ActionTypeFunction,
  actionTypes: ActionTypes,
  selectors: Selectors,
  reducer: Reducer,
  actions: Actions,
  sagas: Sagas,
  keyExtractor?: (payload: any) => string,
};

export const createEntityModule = (
  entityName: string,
  options: ModuleOptions = {}
): GenericEntityModule => {
  const actionType: ActionTypeFunction = (prefix, suffix) =>
    actionTypeBase(prefix, entityName, suffix, options.typePrefix);
  const actionTypes = createActionTypes(actionType);
  const { keyExtractor } = options;
  const selectors = createSelectors(entityName, options);
  return {
    entityName,
    actionType,
    actionTypes,
    selectors,
    reducer: createReducer(entityName, actionTypes, options),
    actions: createActions(entityName, actionTypes, options),
    sagas: createSagas(entityName, actionTypes, options, selectors),
    keyExtractor,
  };
};
