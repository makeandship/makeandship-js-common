/* @flow */

import { all, fork, takeEvery, call } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';

import { genericJsonApiCall, genericSwaggerCall } from '../api';
import { isString } from '../../../utils/is';

import type { ActionTypes } from '../types';
import type { ModuleOptions, Sagas, Selectors } from './types';

export const createSagas = (
  entityName: string,
  actionTypes: ActionTypes,
  moduleOptions: ModuleOptions,
  selectors: Selectors
): Sagas => {
  //
  // Create
  //

  function* createEntityWatcher() {
    yield takeEvery(actionTypes.CREATE, createEntityWorker);
  }

  function* createEntityWorker(action: any): Saga {
    const crudOptions = moduleOptions.create;
    const types = [
      actionTypes.CREATE_REQUEST,
      actionTypes.CREATE_SUCCESS,
      actionTypes.CREATE_FAILURE,
      actionTypes.CREATE_ABORT,
      actionTypes.CREATE_UPLOAD_PROGRESS,
      actionTypes.CREATE_DOWNLOAD_PROGRESS,
    ];
    const { meta } = action;
    if (crudOptions && crudOptions.operationId) {
      const parameters = action.payload;
      yield call(genericSwaggerCall, {
        types,
        crudOptions,
        action,
        parameters,
        meta,
      });
    } else {
      const options = {
        method: 'POST',
        body: action.payload,
      };
      yield call(genericJsonApiCall, {
        entityName,
        types,
        crudOptions,
        action,
        options,
        meta,
      });
    }
  }

  //
  // Request
  //

  function* requestEntityWatcher() {
    yield takeEvery(actionTypes.REQUEST, requestEntityWorker);
  }

  function* requestEntityWorker(action: any): Saga {
    const crudOptions = moduleOptions.request;
    const types = [
      actionTypes.REQUEST_REQUEST,
      actionTypes.REQUEST_SUCCESS,
      actionTypes.REQUEST_FAILURE,
      actionTypes.REQUEST_ABORT,
      actionTypes.REQUEST_UPLOAD_PROGRESS,
      actionTypes.REQUEST_DOWNLOAD_PROGRESS,
    ];
    const { meta } = action;
    let shouldExecuteJsonApiAction;
    if (moduleOptions.shouldExecuteAction) {
      shouldExecuteJsonApiAction = function* () {
        const shouldExecute = yield call(
          moduleOptions.shouldExecuteAction,
          action,
          actionTypes,
          selectors
        );
        return yield shouldExecute;
      };
    }
    if (crudOptions && crudOptions.operationId) {
      let parameters = action.payload;
      // as a convenience, allow request with string and convert it to id
      if (isString(action.payload)) {
        parameters = {
          id: action.payload,
        };
      }
      yield call(genericSwaggerCall, {
        types,
        crudOptions,
        action,
        shouldExecuteJsonApiAction,
        parameters,
        meta,
      });
    } else {
      yield call(genericJsonApiCall, {
        entityName,
        types,
        crudOptions,
        action,
        shouldExecuteJsonApiAction,
        meta,
      });
    }
  }

  //
  // Update
  //

  function* updateEntityWatcher() {
    yield takeEvery(actionTypes.UPDATE, updateEntityWorker);
  }

  function* updateEntityWorker(action: any): Saga {
    const crudOptions = moduleOptions.update;
    const types = [
      actionTypes.UPDATE_REQUEST,
      actionTypes.UPDATE_SUCCESS,
      actionTypes.UPDATE_FAILURE,
      actionTypes.UPDATE_ABORT,
      actionTypes.UPDATE_UPLOAD_PROGRESS,
      actionTypes.UPDATE_DOWNLOAD_PROGRESS,
    ];
    const { meta } = action;
    if (crudOptions && crudOptions.operationId) {
      const parameters = action.payload;
      yield call(genericSwaggerCall, {
        types,
        crudOptions,
        action,
        parameters,
        meta,
      });
    } else {
      const options = {
        method: 'PUT',
        body: action.payload,
      };
      yield call(genericJsonApiCall, {
        entityName,
        types,
        crudOptions,
        action,
        options,
        meta,
      });
    }
  }

  //
  // Delete
  //

  function* deleteEntityWatcher() {
    yield takeEvery(actionTypes.DELETE, deleteEntityWorker);
  }

  function* deleteEntityWorker(action: any): Saga {
    const crudOptions = moduleOptions.delete;
    const types = [
      actionTypes.DELETE_REQUEST,
      actionTypes.DELETE_SUCCESS,
      actionTypes.DELETE_FAILURE,
      actionTypes.DELETE_ABORT,
      actionTypes.DELETE_UPLOAD_PROGRESS,
      actionTypes.DELETE_DOWNLOAD_PROGRESS,
    ];
    const { meta } = action;
    if (crudOptions && crudOptions.operationId) {
      let parameters = action.payload;
      // as a convenience, allow request with string and convert it to id
      if (isString(action.payload)) {
        parameters = {
          id: action.payload,
        };
      }
      yield call(genericSwaggerCall, {
        types,
        crudOptions,
        action,
        parameters,
        meta,
      });
    } else {
      const options = {
        method: 'DELETE',
      };
      yield call(genericJsonApiCall, {
        entityName,
        types,
        crudOptions,
        action,
        options,
        meta,
      });
    }
  }

  function* entitySaga(): Saga {
    yield all([
      fork(createEntityWatcher),
      fork(requestEntityWatcher),
      fork(updateEntityWatcher),
      fork(deleteEntityWatcher),
    ]);
  }

  return {
    entitySaga,
    createEntityWorker,
    requestEntityWorker,
    updateEntityWorker,
    deleteEntityWorker,
  };
};
