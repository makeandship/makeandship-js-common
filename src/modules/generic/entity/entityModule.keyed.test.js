/* @flow */

import {
  createEntityModule,
  shouldExecuteRequestActionOncePerKey,
} from './entityModule';
import { idKeyExtractor } from '../keyExtractors';

const entityName = 'incident';

const module = createEntityModule(entityName, {
  getState: (state) => state,
  keyExtractor: idKeyExtractor,
  keysMax: 5,
  shouldExecuteAction: shouldExecuteRequestActionOncePerKey,
});

const {
  reducer,
  actionTypes,
  selectors: {
    getState,
    getEntity,
    getError,
    getIsFetching,
    getIsCreating,
    getIsRequesting,
    getIsUpdating,
    getIsDeleting,
    makeGetEntity,
    makeGetError,
    makeGetIsFetching,
    makeGetIsCreating,
    makeGetIsRequesting,
    makeGetIsUpdating,
    makeGetIsDeleting,
  },
} = module;

describe('entityModule.js keyed reducer', () => {
  const newEntity = { entity: true };
  const entity = { id: '123', entity: true };
  const error = { error: true };
  let state;

  const newKey = idKeyExtractor(newEntity);
  const key = idKeyExtractor(entity);
  const newMeta = { key: newKey };
  const meta = { key };

  // memoized selectors tied to key
  // const newGetEntitySelector = makeGetEntity(newKey);
  const newGetErrorSelector = makeGetError(newKey);
  const newGetIsFetchingSelector = makeGetIsFetching(newKey);
  const newGetIsCreatingSelector = makeGetIsCreating(newKey);
  const newGetIsRequestingSelector = makeGetIsRequesting(newKey);
  const newGetIsUpdatingSelector = makeGetIsUpdating(newKey);
  const newGetIsDeletingSelector = makeGetIsDeleting(newKey);

  const getEntitySelector = makeGetEntity(key);
  const getErrorSelector = makeGetError(key);
  const getIsFetchingSelector = makeGetIsFetching(key);
  const getIsCreatingSelector = makeGetIsCreating(key);
  const getIsRequestingSelector = makeGetIsRequesting(key);
  const getIsUpdatingSelector = makeGetIsUpdating(key);
  const getIsDeletingSelector = makeGetIsDeleting(key);

  it('should return the initial state', () => {
    state = reducer(state, {});
    expect(getState(state)).toEqual({});
  });

  // Create

  it('should handle create entity', () => {
    const type = actionTypes.CREATE_REQUEST;
    state = reducer(state, { type, payload: newEntity, meta: newMeta });
    expect(getIsFetching(state, newKey)).toBeTruthy();
    expect(getIsCreating(state, newKey)).toBeTruthy();
    expect(getIsRequesting(state, newKey)).toBeFalsy();
    expect(getIsUpdating(state, newKey)).toBeFalsy();
    expect(getIsDeleting(state, newKey)).toBeFalsy();
    expect(newGetIsFetchingSelector(state)).toBeTruthy();
    expect(newGetIsCreatingSelector(state)).toBeTruthy();
    expect(newGetIsRequestingSelector(state)).toBeFalsy();
    expect(newGetIsUpdatingSelector(state)).toBeFalsy();
    expect(newGetIsDeletingSelector(state)).toBeFalsy();
  });

  it('should handle create entity failure', () => {
    const type = actionTypes.CREATE_FAILURE;
    state = reducer(state, { type, payload: error, meta: newMeta });
    expect(getError(state, newKey)).toEqual(error);
    expect(newGetErrorSelector(state)).toEqual(error);

    expect(getIsFetching(state, newKey)).toBeFalsy();
    expect(getIsCreating(state, newKey)).toBeFalsy();
    expect(getIsRequesting(state, newKey)).toBeFalsy();
    expect(getIsUpdating(state, newKey)).toBeFalsy();
    expect(getIsDeleting(state, newKey)).toBeFalsy();
    expect(newGetIsFetchingSelector(state)).toBeFalsy();
    expect(newGetIsCreatingSelector(state)).toBeFalsy();
    expect(newGetIsRequestingSelector(state)).toBeFalsy();
    expect(newGetIsUpdatingSelector(state)).toBeFalsy();
    expect(newGetIsDeletingSelector(state)).toBeFalsy();
  });

  it('should handle create entity success', () => {
    const type = actionTypes.CREATE_SUCCESS;
    state = reducer(state, { type, payload: entity, meta });
    expect(getEntity(state, key)).toEqual(entity);
    expect(getEntitySelector(state)).toEqual(entity);
    expect(getState(state)).toMatchSnapshot();

    expect(getIsFetching(state, key)).toBeFalsy();
    expect(getIsCreating(state, key)).toBeFalsy();
    expect(getIsRequesting(state, key)).toBeFalsy();
    expect(getIsUpdating(state, key)).toBeFalsy();
    expect(getIsDeleting(state, key)).toBeFalsy();
    expect(getIsFetchingSelector(state)).toBeFalsy();
    expect(getIsCreatingSelector(state)).toBeFalsy();
    expect(getIsRequestingSelector(state)).toBeFalsy();
    expect(getIsUpdatingSelector(state)).toBeFalsy();
    expect(getIsDeletingSelector(state)).toBeFalsy();
  });

  // Request

  it('should handle request entity', () => {
    const type = actionTypes.REQUEST_REQUEST;
    state = reducer(state, { type, payload: entity.id, meta });
    expect(getIsFetching(state, key)).toBeTruthy();
    expect(getIsCreating(state, key)).toBeFalsy();
    expect(getIsRequesting(state, key)).toBeTruthy();
    expect(getIsUpdating(state, key)).toBeFalsy();
    expect(getIsDeleting(state, key)).toBeFalsy();
    expect(getIsFetchingSelector(state)).toBeTruthy();
    expect(getIsCreatingSelector(state)).toBeFalsy();
    expect(getIsRequestingSelector(state)).toBeTruthy();
    expect(getIsUpdatingSelector(state)).toBeFalsy();
    expect(getIsDeletingSelector(state)).toBeFalsy();
  });

  it('should handle request entity failure', () => {
    const type = actionTypes.REQUEST_FAILURE;
    state = reducer(state, {
      type,
      payload: error,
      meta,
    });
    expect(getError(state, key)).toEqual(error);
    expect(getErrorSelector(state)).toEqual(error);

    expect(getIsFetching(state, key)).toBeFalsy();
    expect(getIsCreating(state, key)).toBeFalsy();
    expect(getIsRequesting(state, key)).toBeFalsy();
    expect(getIsUpdating(state, key)).toBeFalsy();
    expect(getIsDeleting(state, key)).toBeFalsy();
    expect(getIsFetchingSelector(state)).toBeFalsy();
    expect(getIsCreatingSelector(state)).toBeFalsy();
    expect(getIsRequestingSelector(state)).toBeFalsy();
    expect(getIsUpdatingSelector(state)).toBeFalsy();
    expect(getIsDeletingSelector(state)).toBeFalsy();
  });

  it('should handle request entity success', () => {
    const type = actionTypes.REQUEST_SUCCESS;
    state = reducer(state, {
      type,
      payload: entity,
      meta,
    });
    expect(getEntity(state, key)).toEqual(entity);
    expect(getEntitySelector(state)).toEqual(entity);
    expect(getState(state)).toMatchSnapshot();

    expect(getIsFetching(state, key)).toBeFalsy();
    expect(getIsCreating(state, key)).toBeFalsy();
    expect(getIsRequesting(state, key)).toBeFalsy();
    expect(getIsUpdating(state, key)).toBeFalsy();
    expect(getIsDeleting(state, key)).toBeFalsy();
    expect(getIsFetchingSelector(state)).toBeFalsy();
    expect(getIsCreatingSelector(state)).toBeFalsy();
    expect(getIsRequestingSelector(state)).toBeFalsy();
    expect(getIsUpdatingSelector(state)).toBeFalsy();
    expect(getIsDeletingSelector(state)).toBeFalsy();
  });

  // Update

  it('should handle update entity', () => {
    const type = actionTypes.UPDATE_REQUEST;
    state = reducer(state, { type, payload: entity, meta });
    expect(getIsFetching(state, key)).toBeTruthy();
    expect(getIsCreating(state, key)).toBeFalsy();
    expect(getIsRequesting(state, key)).toBeFalsy();
    expect(getIsUpdating(state, key)).toBeTruthy();
    expect(getIsDeleting(state, key)).toBeFalsy();
    expect(getIsFetchingSelector(state)).toBeTruthy();
    expect(getIsCreatingSelector(state)).toBeFalsy();
    expect(getIsRequestingSelector(state)).toBeFalsy();
    expect(getIsUpdatingSelector(state)).toBeTruthy();
    expect(getIsDeletingSelector(state)).toBeFalsy();
  });

  it('should handle update entity failure', () => {
    const type = actionTypes.UPDATE_FAILURE;
    state = reducer(state, { type, payload: error, meta });
    expect(getEntity(state, key)).toEqual(entity);
    expect(getError(state, key)).toEqual(error);
    expect(getEntitySelector(state)).toEqual(entity);
    expect(getErrorSelector(state)).toEqual(error);

    expect(getIsFetching(state, key)).toBeFalsy();
    expect(getIsCreating(state, key)).toBeFalsy();
    expect(getIsRequesting(state, key)).toBeFalsy();
    expect(getIsUpdating(state, key)).toBeFalsy();
    expect(getIsDeleting(state, key)).toBeFalsy();
    expect(getIsFetchingSelector(state)).toBeFalsy();
    expect(getIsCreatingSelector(state)).toBeFalsy();
    expect(getIsRequestingSelector(state)).toBeFalsy();
    expect(getIsUpdatingSelector(state)).toBeFalsy();
    expect(getIsDeletingSelector(state)).toBeFalsy();
  });

  it('should handle update entity success', () => {
    const type = actionTypes.UPDATE_SUCCESS;
    state = reducer(state, { type, payload: entity, meta });
    expect(getEntity(state, key)).toEqual(entity);
    expect(getEntitySelector(state)).toEqual(entity);
    expect(getState(state)).toMatchSnapshot();

    expect(getIsFetching(state, key)).toBeFalsy();
    expect(getIsCreating(state, key)).toBeFalsy();
    expect(getIsRequesting(state, key)).toBeFalsy();
    expect(getIsUpdating(state, key)).toBeFalsy();
    expect(getIsDeleting(state, key)).toBeFalsy();
    expect(getIsFetchingSelector(state)).toBeFalsy();
    expect(getIsCreatingSelector(state)).toBeFalsy();
    expect(getIsRequestingSelector(state)).toBeFalsy();
    expect(getIsUpdatingSelector(state)).toBeFalsy();
    expect(getIsDeletingSelector(state)).toBeFalsy();
  });

  // Delete

  it('should handle delete entity', () => {
    const type = actionTypes.DELETE_REQUEST;
    state = reducer(state, { type, payload: entity, meta });
    expect(getIsFetching(state, key)).toBeTruthy();
    expect(getIsCreating(state, key)).toBeFalsy();
    expect(getIsRequesting(state, key)).toBeFalsy();
    expect(getIsUpdating(state, key)).toBeFalsy();
    expect(getIsDeleting(state, key)).toBeTruthy();
    expect(getIsFetchingSelector(state)).toBeTruthy();
    expect(getIsCreatingSelector(state)).toBeFalsy();
    expect(getIsRequestingSelector(state)).toBeFalsy();
    expect(getIsUpdatingSelector(state)).toBeFalsy();
    expect(getIsDeletingSelector(state)).toBeTruthy();
  });

  it('should handle delete entity failure', () => {
    const type = actionTypes.DELETE_FAILURE;
    state = reducer(state, { type, payload: error, meta });
    expect(getEntity(state, key)).toEqual(entity);
    expect(getError(state, key)).toEqual(error);
    expect(getEntitySelector(state)).toEqual(entity);
    expect(getErrorSelector(state)).toEqual(error);

    expect(getIsFetching(state, key)).toBeFalsy();
    expect(getIsCreating(state, key)).toBeFalsy();
    expect(getIsRequesting(state, key)).toBeFalsy();
    expect(getIsUpdating(state, key)).toBeFalsy();
    expect(getIsDeleting(state, key)).toBeFalsy();
    expect(getIsFetchingSelector(state)).toBeFalsy();
    expect(getIsCreatingSelector(state)).toBeFalsy();
    expect(getIsRequestingSelector(state)).toBeFalsy();
    expect(getIsUpdatingSelector(state)).toBeFalsy();
    expect(getIsDeletingSelector(state)).toBeFalsy();
  });

  it('should handle delete entity success', () => {
    const type = actionTypes.DELETE_SUCCESS;
    state = reducer(state, { type, meta });
    expect(getEntity(state, key)).toBeUndefined();
    expect(getEntitySelector(state)).toBeUndefined();
    expect(getState(state)).toMatchSnapshot();

    expect(getIsFetching(state, key)).toBeFalsy();
    expect(getIsCreating(state, key)).toBeFalsy();
    expect(getIsRequesting(state, key)).toBeFalsy();
    expect(getIsUpdating(state, key)).toBeFalsy();
    expect(getIsDeleting(state, key)).toBeFalsy();
    expect(getIsFetchingSelector(state)).toBeFalsy();
    expect(getIsCreatingSelector(state)).toBeFalsy();
    expect(getIsRequestingSelector(state)).toBeFalsy();
    expect(getIsUpdatingSelector(state)).toBeFalsy();
    expect(getIsDeletingSelector(state)).toBeFalsy();
  });

  // Reset

  it('should handle reset entity', () => {
    const type = actionTypes.RESET;
    state = reducer(state, { type });
    expect(getEntity(state, key)).toBeUndefined();
    expect(getEntitySelector(state)).toBeUndefined();
    expect(getState(state)).toEqual({});
    expect(getIsFetching(state, key)).toBeFalsy();
    expect(getIsCreating(state, key)).toBeFalsy();
    expect(getIsRequesting(state, key)).toBeFalsy();
    expect(getIsUpdating(state, key)).toBeFalsy();
    expect(getIsDeleting(state, key)).toBeFalsy();
    expect(getIsFetchingSelector(state)).toBeFalsy();
    expect(getIsCreatingSelector(state)).toBeFalsy();
    expect(getIsRequestingSelector(state)).toBeFalsy();
    expect(getIsUpdatingSelector(state)).toBeFalsy();
    expect(getIsDeletingSelector(state)).toBeFalsy();
  });

  // Max

  it('should have a maximum number of entities', () => {
    const type = actionTypes.CREATE_SUCCESS;
    for (let id = 10; id <= 99; id++) {
      const payload = { ...entity, id: `${id}` };
      const key = idKeyExtractor(payload);
      const dynamicMeta = { key };
      state = reducer(state, { type, payload, meta: dynamicMeta });
    }
    expect(Object.keys(getState(state)).length).toEqual(5);
    expect(getState(state)).toMatchSnapshot();
  });
});
