/* @flow */

import {
  all,
  fork,
  put,
  take,
  call,
  takeLeading,
  apply,
  select,
} from 'redux-saga/effects';
import { channel } from 'redux-saga';
import type { Saga } from 'redux-saga';
import type { PayloadAction } from '@reduxjs/toolkit';

import { getConfig } from './config';
import * as actions from './actions';
import * as selectors from './selectors';

// Instances

export const authActionChannel = channel();

// Reducer

// Side effects

export function* checkInitialised(): Saga {
  const isInitialised = yield select(selectors.getIsInitialised);
  if (!isInitialised) {
    yield take(actions.initialiseSuccess);
  }
}

export function* authActionChannelWatcher(): Saga {
  while (true) {
    const action = yield take(authActionChannel);
    yield put(action);
  }
}

export function* initWatcher(): Saga {
  yield takeLeading(actions.initialise, initWorker);
}

export function* initWorker(action: PayloadAction): Saga {
  yield put(actions.setIsFetching(true));
  const config = yield call(getConfig);
  const provider = config.provider;

  provider.onAuthSuccess = () => {
    authActionChannel.put(actions.authSuccess());
  };
  provider.onAuthError = (error) => {
    authActionChannel.put(actions.authError(error));
  };
  provider.onAuthLogout = () => {
    authActionChannel.put(actions.authLogout());
  };
  provider.onUpdateTokenSuccess = () => {
    authActionChannel.put(actions.updateTokenSuccess());
  };
  provider.onUpdateTokenError = (error) => {
    authActionChannel.put(actions.updateTokenError(error));
  };

  const isAuthenticated = yield apply(provider, 'init', [action.payload]);
  yield put(actions.setIsAuthenticated(isAuthenticated));
  yield put(actions.setIsInitialised(true));
  yield put(actions.setIsFetching(false));
  yield put(actions.initialiseSuccess());
}

export function* updateIsAuthenticated(): Saga {
  const config = yield call(getConfig);
  const provider = config.provider;
  const isAuthenticated = yield apply(provider, 'getIsAuthenticated');
  yield put(actions.setIsAuthenticated(isAuthenticated));
}

export function* actionWatcher(): Saga {
  const config = yield call(getConfig);
  const provider = config.provider;

  yield takeLeading(actions.authSuccess, updateIsAuthenticated);
  yield takeLeading(actions.updateTokenError, updateIsAuthenticated);
  yield takeLeading(actions.authLogout, updateIsAuthenticated);

  yield takeLeading(actions.login, function* (action: PayloadAction) {
    yield call(checkInitialised);
    try {
      yield apply(provider, 'login', [action.payload]);
    } catch (error) {
      // ignore login cancellation or error here
      // should be handled by provider callbacks
    }
  });

  yield takeLeading(actions.logout, function* (action: PayloadAction) {
    yield call(checkInitialised);
    yield apply(provider, 'logout', [action.payload]);
  });

  yield takeLeading(actions.register, function* (action: PayloadAction) {
    yield call(checkInitialised);
    yield apply(provider, 'register', [action.payload]);
  });

  yield takeLeading(actions.profile, function* () {
    yield call(checkInitialised);
    yield apply(provider, 'profile');
  });

  yield takeLeading(actions.updateToken, function* () {
    yield call(checkInitialised);
    try {
      yield apply(provider, 'updateToken');
    } catch (error) {
      // ignore token refesh error here
      // should be handled by provider callbacks
    }
  });

  yield takeLeading(actions.loadProfile, function* () {
    yield call(checkInitialised);
    try {
      const profile = yield apply(provider, 'loadUserProfile');
      yield put(actions.loadProfileSuccess(profile));
    } catch (error) {
      yield put(actions.loadProfileFailure(error));
    }
  });
}

const sagas = [authActionChannelWatcher, initWatcher, actionWatcher];

export default function* (): Saga {
  yield all(sagas.map((saga) => fork(saga)));
}
