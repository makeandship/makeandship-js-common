/* @flow */

import type { AxiosRequestConfig } from 'axios';

export const createAxiosAuthInterceptor = (provider: any) => {
  return async (config: AxiosRequestConfig): Promise<AxiosRequestConfig> => {
    const isAuthenticated = await provider.getIsAuthenticated();
    if (isAuthenticated) {
      try {
        await provider.updateToken();
        const token = await provider.getToken();
        if (token) {
          config.headers.Authorization = `Bearer ${token}`;
        }
      } catch (error) {
        // ignore token refesh error here
        // should be handled by provider callbacks
      }
    }
    return config;
  };
};
