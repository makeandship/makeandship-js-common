/* @flow */

import makeConfig from '../utils/makeConfig';
import provider from './providers/emptyProvider';

const defaultConfig = {
  provider,
};

const { getConfig, setConfig, resetConfig } = makeConfig(defaultConfig);

export { getConfig, setConfig, resetConfig };
