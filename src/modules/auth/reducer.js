/* @flow */

import { createReducer } from '@reduxjs/toolkit';

import * as actions from './actions';
import type { State } from './types';

// Reducer

export const initialState: State = {
  isFetching: true,
  isInitialised: false,
  isAuthenticated: false,
};

const reducer = createReducer(initialState, {
  [actions.setIsFetching]: (state, action) => {
    state.isFetching = action.payload;
  },
  [actions.setIsInitialised]: (state, action) => {
    state.isInitialised = action.payload;
  },
  [actions.setIsAuthenticated]: (state, action) => {
    state.isAuthenticated = action.payload;
  },
  [actions.loadProfileSuccess]: (state, action) => {
    state.profile = action.payload;
  },
  [actions.authError]: (state, action) => {
    state.error = action.payload;
  },
  [actions.updateTokenError]: (state, action) => {
    state.error = action.payload;
  },
  [actions.login]: (state) => {
    delete state.error;
  },
  [actions.register]: (state) => {
    delete state.error;
  },
  [actions.updateToken]: (state) => {
    delete state.error;
  },
});

export default reducer;
