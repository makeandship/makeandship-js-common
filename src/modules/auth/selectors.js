/* @flow */

import { createSelector } from 'reselect';
import type { State } from './types';

// Selectors

export const getState = (state: any): State => state.auth;

export const getIsFetching = createSelector(
  getState,
  (state) => state.isFetching
);

export const getIsInitialised = createSelector(
  getState,
  (state) => state.isInitialised
);

export const getIsAuthenticated = createSelector(
  getState,
  (state) => state.isAuthenticated
);

export const getError = createSelector(getState, (state) => state.error);

export const getProfile = createSelector(getState, (state) => state.profile);
