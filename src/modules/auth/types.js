/* @flow */

export type State = {
  isFetching: boolean,
  isInitialised: boolean,
  isAuthenticated: boolean,
  error?: any,
  profile?: any,
};
