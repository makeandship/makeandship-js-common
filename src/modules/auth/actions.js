/* @flow */

import { createAction } from '@reduxjs/toolkit';

export const typePrefix = 'auth/';

export const initialise = createAction(`${typePrefix}INITIALISE`);
export const initialiseSuccess = createAction(
  `${typePrefix}INITIALISE_SUCCESS`
);
export const loadProfile = createAction(`${typePrefix}LOAD_PROFILE`);
export const loadProfileSuccess = createAction(
  `${typePrefix}LOAD_PROFILE_SUCCESS`
);
export const loadProfileFailure = createAction(
  `${typePrefix}LOAD_PROFILE_FAILURE`
);
export const setIsFetching = createAction(`${typePrefix}SET_IS_FETCHING`);
export const setIsInitialised = createAction(`${typePrefix}SET_IS_INITIALISED`);
export const setIsAuthenticated = createAction(
  `${typePrefix}SET_IS_AUTHENTICATED`
);
export const login = createAction(`${typePrefix}LOGIN`);
export const logout = createAction(`${typePrefix}LOGOUT`);
export const register = createAction(`${typePrefix}REGISTER`);
export const profile = createAction(`${typePrefix}PROFILE`);

export const authSuccess = createAction(`${typePrefix}AUTH_SUCCESS`);
export const authError = createAction(`${typePrefix}AUTH_ERROR`);
export const authLogout = createAction(`${typePrefix}AUTH_LOGOUT`);

export const updateToken = createAction(`${typePrefix}UPDATE_TOKEN`);
export const updateTokenSuccess = createAction(
  `${typePrefix}UPDATE_TOKEN_SUCCESS`
);
export const updateTokenError = createAction(`${typePrefix}UPDATE_TOKEN_ERROR`);
