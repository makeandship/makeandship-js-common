/* @flow */

import { put, take, call, select } from 'redux-saga/effects';

import { sagaMiddleware } from './__mocks__/store';

import { actions, selectors } from './';

import { setConfig, resetConfig } from './config';

describe('auth module', () => {
  afterEach(() => {
    jest.clearAllMocks();
    resetConfig();
  });

  describe('when initialised', () => {
    it('should use the provider', async (done) => {
      const saga = function* () {
        const provider = {
          init: jest.fn(() => true),
        };
        setConfig({
          provider,
        });
        yield put(actions.initialise('test'));
        yield take(actions.initialiseSuccess);
        expect(provider.init).toHaveBeenCalledWith('test');
        const isInitialised = yield select(selectors.getIsInitialised);
        const isAuthenticated = yield select(selectors.getIsAuthenticated);
        expect(isInitialised).toBeTruthy();
        expect(isAuthenticated).toBeTruthy();
        yield call(done);
      };
      sagaMiddleware.run(saga);
    });
  });
});
