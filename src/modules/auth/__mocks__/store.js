/* @flow */

import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';

import auth from '../reducer';
import authSaga from '../sagas';

export const sagaMiddleware = createSagaMiddleware();
const store = configureStore({
  reducer: { auth },
  middleware: [sagaMiddleware],
});
sagaMiddleware.run(authSaga);

export default store;
