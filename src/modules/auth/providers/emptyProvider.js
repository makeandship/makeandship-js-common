/* @flow */

const provider = () => {
  const init = async () => {};
  const ready = async () => {};
  const getIsAuthenticated = async () => {};
  const login = async () => {};
  const logout = async () => {};
  const register = async () => {};
  const profile = async () => {};
  const loadUserProfile = async () => {};
  const getToken = async () => {};
  const updateToken = async () => {};
  return {
    init,
    ready,
    getIsAuthenticated,
    login,
    logout,
    register,
    profile,
    loadUserProfile,
    getToken,
    updateToken,
  };
};

export default provider;
