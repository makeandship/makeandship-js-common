/* @flow */

import { Auth0Client } from '@auth0/auth0-spa-js';
import qs from 'qs';

import { delay } from '../../utils';

export const createAuth0Provider = (auth0: Auth0Client) => {
  const provider = {};

  let _ready = false;
  const init = async () => {
    try {
      // check window.location.href for e.g. ?code=foo&state=bar
      const url = new URL(window.location.href);
      const query = qs.parse(url.search, { ignoreQueryPrefix: true });
      if (query.code && query.state) {
        await auth0.handleRedirectCallback();
        const isAuthenticated = await auth0.isAuthenticated();
        if (isAuthenticated) {
          // clean redirect to origin
          window.location = url.origin;
        }
      } else {
        await auth0.checkSession();
      }
    } catch (e) {
      // ignore error here
    }
    const isAuthenticated = await auth0.isAuthenticated();
    _ready = true;
    return isAuthenticated;
  };
  const ready = async () => {
    if (_ready) {
      return;
    }
    while (true) {
      if (_ready) {
        return;
      }
      await delay(0);
    }
  };
  const getIsAuthenticated = async () => await auth0.isAuthenticated();
  const login = async () => {
    await auth0.loginWithRedirect();
  };
  const logout = async () => {
    const returnTo =
      window.env?.REACT_APP_AUTH0_LOGOUT_REDIRECT_URI ||
      auth0.options.redirect_uri;
    await auth0.logout({
      returnTo,
    });
  };
  const register = async () => {
    await auth0.loginWithRedirect({ screen_hint: 'signup' });
  };
  const profile = async () => {
    console.warn('TODO: implement - navigation to Auth0 profile screen?');
  };
  const loadUserProfile = async () => {
    await auth0.getUser();
  };
  const getToken = async () => {
    const token = await auth0.getTokenSilently();
    return token;
  };
  const updateToken = async () => {
    await ready();
    return await auth0.getTokenSilently();
  };
  Object.assign(provider, {
    init,
    getIsAuthenticated,
    ready,
    login,
    logout,
    register,
    profile,
    loadUserProfile,
    getToken,
    updateToken,
  });
  return provider;
};
