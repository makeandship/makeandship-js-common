/* @flow */

import Keycloak from 'keycloak-js';

import { delay } from '../../utils';

export const createKeycloakProvider = (
  keycloakInstance: Keycloak.KeycloakInstance
) => {
  const provider = {};

  let _ready = false;
  const init = async (...args: Keycloak.KeycloakInitOptions) => {
    try {
      await keycloakInstance.init(...args);
    } catch (e) {
      // ignore error here
    }
    _ready = true;
    return keycloakInstance.authenticated;
  };
  const ready = async () => {
    if (_ready) {
      return;
    }
    while (true) {
      if (_ready) {
        return;
      }
      await delay(0);
    }
  };
  const getIsAuthenticated = async () => keycloakInstance.authenticated;
  const login = keycloakInstance.login;
  const logout = keycloakInstance.logout;
  const register = keycloakInstance.register;
  const profile = keycloakInstance.accountManagement;
  const loadUserProfile = keycloakInstance.loadUserProfile;
  const getToken = async () => keycloakInstance.token;
  const updateToken = async (minValidity: number) => {
    await ready();
    return await keycloakInstance.updateToken(minValidity);
  };

  keycloakInstance.onAuthRefreshError = () => {
    provider.onUpdateTokenError && provider.onUpdateTokenError();
  };
  keycloakInstance.onAuthRefreshSuccess = () => {
    provider.onUpdateTokenSuccess && provider.onUpdateTokenSuccess();
  };
  keycloakInstance.onAuthSuccess = () => {
    provider.onAuthSuccess && provider.onAuthSuccess();
  };
  keycloakInstance.onAuthError = () => {
    provider.onAuthError && provider.onAuthError();
  };
  keycloakInstance.onAuthLogout = () => {
    provider.onAuthLogout && provider.onAuthLogout();
  };

  Object.assign(provider, {
    init,
    getIsAuthenticated,
    ready,
    login,
    logout,
    register,
    profile,
    loadUserProfile,
    getToken,
    updateToken,
  });
  return provider;
};
