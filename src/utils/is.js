/* @flow */

import * as React from 'react';

export const isPlainObject = (objectToCheck: any): boolean =>
  typeof objectToCheck === 'object' &&
  !Array.isArray(objectToCheck) &&
  objectToCheck !== null &&
  objectToCheck.constructor.name === 'Object';

export const isFunction = (functionToCheck: any): boolean =>
  functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';

export const isClassInstance = (instanceToCheck: any): boolean => {
  return (
    typeof instanceToCheck === 'object' &&
    !Array.isArray(instanceToCheck) &&
    instanceToCheck !== null &&
    instanceToCheck.constructor.name !== 'Object'
  );
};

export const isArray = (arrayToCheck: any): boolean =>
  Array.isArray(arrayToCheck);

export const isString = (stringToCheck: any): boolean =>
  typeof stringToCheck === 'string' || stringToCheck instanceof String;

export const isNumeric = (numToCheck: any): boolean =>
  !isNaN(parseFloat(numToCheck)) && isFinite(numToCheck);

export const isTrue = (trueToCheck: any = false): boolean => {
  const asNumber = +trueToCheck;
  return !isNaN(asNumber)
    ? !!asNumber
    : !!String(trueToCheck).toLowerCase().replace(!!0, '');
};

export const isReactComponent = (componentToCheck: any) =>
  React.isValidElement(componentToCheck);

export const isReactComponentClass = (classToCheck: any) =>
  Object.prototype.isPrototypeOf.call(React.PureComponent, classToCheck) ||
  Object.prototype.isPrototypeOf.call(React.Component, classToCheck);

export const isDataURI = (stringToCheck: any): boolean =>
  isString(stringToCheck) && stringToCheck.startsWith('data:');

export const isBoolean = (booleanToCheck: any): boolean =>
  typeof booleanToCheck === 'boolean';

export const isFile = (obj: any, navigatorObj: any) => {
  if (!navigatorObj && typeof navigator !== 'undefined') {
    // eslint-disable-next-line no-undef
    navigatorObj = navigator;
  }
  if (navigatorObj && navigatorObj.product === 'ReactNative') {
    if (obj && typeof obj === 'object' && typeof obj.uri === 'string') {
      return true;
    }
    return false;
  }
  if (typeof File !== 'undefined') {
    // eslint-disable-next-line no-undef
    return obj instanceof File;
  }
  return (
    obj !== null && typeof obj === 'object' && typeof obj.pipe === 'function'
  );
};

export const isBlob = (value: any) => {
  return (
    value &&
    typeof value.size === 'number' &&
    typeof value.type === 'string' &&
    typeof value.slice === 'function'
  );
};

export const isDate = (value: any) => {
  return value instanceof Date;
};

export const isFormData = (value: any) => {
  return value instanceof FormData;
};

export const isUndefined = (value: any) => {
  return value === undefined;
};

export const isNull = (value: any) => {
  return value === null;
};
