/* @flow */

import { isString } from './is';

export const getFileExtension = (str: string): string | void => {
  if (!isString(str)) {
    return;
  }
  const index = str.lastIndexOf('.');
  if (index === -1) {
    return;
  }
  const extension = str.substr(index + 1);
  return extension.toLowerCase();
};

export const setFileExtension = (str: string, ext: string): string | void => {
  if (!isString(str) || !isString(ext)) {
    return;
  }
  const index = str.lastIndexOf('.');
  if (index === -1) {
    return `${str}.${ext}`;
  }
  const prefix = str.substr(0, index);
  return `${prefix}.${ext}`;
};
