/* @flow */

// https://date-fns.org/docs/I18n

import * as dateFns from 'date-fns';
import _get from 'lodash.get';

import { isString } from './is';

import enGB from 'date-fns/locale/en-GB';
import enUS from 'date-fns/locale/en-US';

const locales = { enGB, enUS };

//https://date-fns.org/v2.0.0-alpha.9/docs/format

export const formats = {
  date: 'P',
  dateTime: 'P HH:mm',
  time: 'HH:mm',
};

export const getDefaultLocale = () => {
  try {
    const locale = window.navigator.language.replace('-', '');
    return locale;
  } catch (e) {
    console.error(e);
  }
  return 'enGB';
};

export const setCurrentLocale = (localeId: string) => {
  window.__localeId__ = localeId;
};

export const getCurrentLocale = () => {
  return _get(locales, window.__localeId__);
};

export const formatDate = (date: Date, formatStr: string = 'PP') => {
  try {
    if (isString(date)) {
      date = new Date(date);
    }
    return dateFns.format(date, formatStr, {
      locale: getCurrentLocale(),
    });
  } catch (error) {
    console.error(error);
  }
};

setCurrentLocale(getDefaultLocale());
