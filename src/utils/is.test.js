/* @flow */

import * as React from 'react';

import {
  isFunction,
  isPlainObject,
  isClassInstance,
  isString,
  isNumeric,
  isTrue,
  isReactComponent,
  isReactComponentClass,
  isDataURI,
  isBoolean,
} from './is';

const f = () => {};

class klass extends Error {}

const k = new klass();

const o = {};

const s = 's';

const n = 1;

const x = null;

class FooComponent extends React.Component<*> {}

describe('is', function spec() {
  it('should detect function', () => {
    expect(isFunction(f)).toBeTruthy();
    expect(isFunction(k)).toBeFalsy();
    expect(isFunction(o)).toBeFalsy();
    expect(isFunction(s)).toBeFalsy();
    expect(isFunction(n)).toBeFalsy();
    expect(isFunction(x)).toBeFalsy();
    expect(isFunction()).toBeFalsy();
  });
  it('should detect plain object', () => {
    expect(isPlainObject(f)).toBeFalsy();
    expect(isPlainObject(k)).toBeFalsy();
    expect(isPlainObject(o)).toBeTruthy();
    expect(isPlainObject(s)).toBeFalsy();
    expect(isPlainObject(n)).toBeFalsy();
    expect(isPlainObject(x)).toBeFalsy();
    expect(isPlainObject()).toBeFalsy();
  });
  it('should detect class instance', () => {
    expect(isClassInstance(f)).toBeFalsy();
    expect(isClassInstance(k)).toBeTruthy();
    expect(isClassInstance(o)).toBeFalsy();
    expect(isClassInstance(s)).toBeFalsy();
    expect(isClassInstance(n)).toBeFalsy();
    expect(isClassInstance(x)).toBeFalsy();
    expect(isClassInstance()).toBeFalsy();
  });
  it('should detect string', () => {
    expect(isString(f)).toBeFalsy();
    expect(isString(k)).toBeFalsy();
    expect(isString(o)).toBeFalsy();
    expect(isString(s)).toBeTruthy();
    expect(isString(n)).toBeFalsy();
    expect(isString(x)).toBeFalsy();
    expect(isString()).toBeFalsy();
  });
  it('should detect numeric', () => {
    expect(isNumeric(f)).toBeFalsy();
    expect(isNumeric(k)).toBeFalsy();
    expect(isNumeric(o)).toBeFalsy();
    expect(isNumeric(s)).toBeFalsy();
    expect(isNumeric(n)).toBeTruthy();
    expect(isNumeric(x)).toBeFalsy();
    expect(isNumeric()).toBeFalsy();
    expect(isNumeric('1')).toBeTruthy();
    expect(isNumeric('1b')).toBeFalsy();
  });
  it('should detect true / false', () => {
    expect(isTrue(true)).toBeTruthy();
    expect(isTrue('true')).toBeTruthy();
    expect(isTrue('TRUE')).toBeTruthy();
    expect(isTrue(1)).toBeTruthy();
    expect(isTrue('1')).toBeTruthy();
    expect(isTrue(false)).toBeFalsy();
    expect(isTrue('false')).toBeFalsy();
    expect(isTrue('FALSE')).toBeFalsy();
    expect(isTrue(0)).toBeFalsy();
    expect(isTrue('0')).toBeFalsy();
    expect(isTrue()).toBeFalsy();
    expect(isTrue(null)).toBeFalsy();
    expect(isTrue(undefined)).toBeFalsy();
  });
  it('should detect react component', () => {
    expect(isReactComponent()).toBeFalsy();
    expect(isReactComponent(null)).toBeFalsy();
    expect(
      isReactComponent((props) => <FooComponent {...props} />)
    ).toBeFalsy();
    expect(isReactComponent(FooComponent)).toBeFalsy();
    expect(isReactComponent(<FooComponent />)).toBeTruthy();
  });
  it('should detect react component class', () => {
    expect(isReactComponentClass()).toBeFalsy();
    expect(isReactComponentClass(null)).toBeFalsy();
    expect(
      isReactComponentClass((props) => <FooComponent {...props} />)
    ).toBeFalsy();
    expect(isReactComponentClass(FooComponent)).toBeTruthy();
    expect(isReactComponentClass(<FooComponent />)).toBeFalsy();
  });
  it('should detect data uri', () => {
    expect(isDataURI()).toBeFalsy();
    expect(isDataURI(null)).toBeFalsy();
    expect(isDataURI('http://www.example.com')).toBeFalsy();
    expect(isDataURI('/path/to/document.pdf')).toBeFalsy();
    expect(isDataURI('data:something')).toBeTruthy();
  });
  it('should detect boolean', () => {
    expect(isBoolean(f)).toBeFalsy();
    expect(isBoolean(k)).toBeFalsy();
    expect(isBoolean(o)).toBeFalsy();
    expect(isBoolean(s)).toBeFalsy();
    expect(isBoolean(n)).toBeFalsy();
    expect(isBoolean(x)).toBeFalsy();
    expect(isBoolean()).toBeFalsy();
    expect(isBoolean(true)).toBeTruthy();
    expect(isBoolean(false)).toBeTruthy();
  });
});
