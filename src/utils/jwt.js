/* @flow */

import jwtDecode from 'jwt-decode';
import memoizeOne from 'memoize-one';
import { isString } from './is';

export const parseToken = memoizeOne((token: string) => {
  const tokenParsed = jwtDecode(token);
  return tokenParsed;
});

export const getTokenExp = (token: string) => {
  const tokenParsed = parseToken(token);
  return tokenParsed.exp;
};

export const isTokenExpired = (token: string) => {
  const exp = getTokenExp(token);
  const currentTime = Date.now() / 1000;
  if (exp < currentTime) {
    return true;
  }
  return false;
};

// https://www.iana.org/assignments/jwt/jwt.xhtml

export const userFromToken = (token: string) => {
  if (!isString(token)) {
    return;
  }
  const parsed = parseToken(token);
  const {
    email,
    phone_number: phone,
    given_name: firstname,
    family_name: lastname,
  } = parsed;
  const roles = parsed.realm_access?.roles ?? [];
  const user = {
    email,
    phone,
    firstname,
    lastname,
    roles,
  };
  return user;
};
