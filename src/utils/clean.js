/* @flow */

// strip empty values, functions and instances

import {
  isFunction,
  isArray,
  isPlainObject,
  isClassInstance,
  isString,
  isNumeric,
} from './is';

export const cleanObject = (o: any): any => {
  if (isString(o) || isNumeric(o)) {
    return o;
  }
  let filtered = {};
  if (isPlainObject(o)) {
    Object.entries(o).map(([key, value]) => {
      if (value !== null && value !== undefined) {
        if (isPlainObject(value) || isArray(value)) {
          const cleaned = cleanObject(value);
          if (cleaned) {
            filtered[key] = cleaned;
          }
        } else if (!isFunction(value) && !isClassInstance(value)) {
          filtered[key] = value;
        } else {
          // ignore
        }
      }
    });
  } else if (isArray(o)) {
    const values = o
      .map((entry) => cleanObject(entry))
      .filter((entry) => entry !== undefined);
    if (values.length > 0) {
      return values;
    }
  }
  if (Object.keys(filtered).length > 0) {
    return filtered;
  }
};

export const isUninitialised = (valueToCheck: any): boolean => {
  if (valueToCheck === undefined) {
    return true;
  }
  if (isPlainObject(valueToCheck)) {
    if (Object.keys(valueToCheck).length === 0) {
      return true;
    }
  }
  if (isArray(valueToCheck)) {
    if (valueToCheck.length === 0) {
      return true;
    }
  }
  if (isString(valueToCheck)) {
    if (valueToCheck.length === 0) {
      return true;
    }
  }
  return false;
};

export const filterNewUninitialised = (
  referenceObject: any,
  newObject: any
): any => {
  if (isPlainObject(newObject)) {
    let filtered = {};
    Object.entries(newObject).map(([key, value]) => {
      if (isUninitialised(value)) {
        // does the reference object contain this key?
        if (
          isPlainObject(referenceObject) &&
          Object.prototype.hasOwnProperty.call(referenceObject, key)
        ) {
          filtered[key] = value;
        } else {
          // new undefined, ignore
        }
      } else if (isPlainObject(value)) {
        // does the reference object contain this key?
        if (
          isPlainObject(referenceObject) &&
          Object.prototype.hasOwnProperty.call(referenceObject, key)
        ) {
          // iterate, keep if not undefined
          const filteredValue = filterNewUninitialised(
            referenceObject[key],
            value
          );
          if (!isUninitialised(filteredValue)) {
            filtered[key] = filteredValue;
          }
        } else {
          // iterate, keep if not undefined
          const filteredValue = filterNewUninitialised(undefined, value);
          if (!isUninitialised(filteredValue)) {
            // keep it as it's new but not undefined
            filtered[key] = filteredValue;
          }
        }
      } else {
        // not an object with keys, not undefined - keep it
        filtered[key] = value;
      }
    });
    // don't return empty object
    if (Object.keys(filtered).length) {
      return filtered;
    }
    return undefined;
  }
  return newObject;
};
