/* @flow */

import { getFileExtension, setFileExtension } from './extension';

describe('extension', function spec() {
  it('should get extension', () => {
    // $FlowFixMe
    expect(getFileExtension()).toBeUndefined();
    // $FlowFixMe
    expect(getFileExtension(null)).toBeUndefined();
    expect(getFileExtension('')).toBeUndefined();
    expect(getFileExtension('foo')).toBeUndefined();
    expect(getFileExtension('foo.bar')).toEqual('bar');
  });
  it('should replace extension', () => {
    // $FlowFixMe
    expect(setFileExtension()).toBeUndefined();
    // $FlowFixMe
    expect(setFileExtension(null)).toBeUndefined();
    // $FlowFixMe
    expect(setFileExtension('')).toBeUndefined();
    // $FlowFixMe
    expect(setFileExtension('foo')).toBeUndefined();
    expect(setFileExtension('foo', 'baz')).toEqual('foo.baz');
    expect(setFileExtension('foo.bar', 'baz')).toEqual('foo.baz');
    expect(setFileExtension('foo.foo.foo.bar', 'baz')).toEqual(
      'foo.foo.foo.baz'
    );
  });
});
