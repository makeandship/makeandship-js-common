/* @flow */

import { cleanObject, isUninitialised, filterNewUninitialised } from './clean';

const f = () => {};

class klass extends Error {}

const k = new klass();

const o = {};

const s = 's';

const n = 1;

describe('cleanObject', function spec() {
  it('should strip functions', () => {
    expect(cleanObject(f)).toEqual(undefined);
  });
  it('should strip instances', () => {
    expect(cleanObject(k)).toEqual(undefined);
  });
  it('should keep strings etc.', () => {
    expect(
      cleanObject({
        f,
        a: 1,
        b: 'c',
        d: { e: 'f', f, k },
        g: { h: { f, k } },
        i: [
          {
            j: 1,
          },
          {
            j: 2,
            k,
          },
          {
            k,
          },
        ],
        l: o,
        m: s,
        n: n,
      })
    ).toEqual({
      a: 1,
      b: 'c',
      d: { e: 'f' },
      i: [
        {
          j: 1,
        },
        {
          j: 2,
        },
      ],
      m: 's',
      n: 1,
    });
  });
  it('should handle bad input', () => {
    expect(cleanObject()).toEqual(undefined);
    expect(cleanObject('')).toEqual('');
    expect(cleanObject(' ')).toEqual(' ');
    expect(cleanObject(null)).toEqual(undefined);
    expect(cleanObject(undefined)).toEqual(undefined);
    expect(cleanObject(123)).toEqual(123);
    expect(cleanObject({})).toEqual(undefined);
    expect(cleanObject([])).toEqual(undefined);
  });
});

describe('isUninitialised', function spec() {
  it('should handle bad input', () => {
    expect(isUninitialised()).toEqual(true);
    expect(isUninitialised(null)).toEqual(false);
  });

  it('should detect values that are uninitialised', () => {
    expect(isUninitialised(undefined)).toEqual(true);
    expect(isUninitialised([])).toEqual(true);
    expect(isUninitialised({})).toEqual(true);
    expect(isUninitialised('')).toEqual(true);

    expect(isUninitialised(0)).toEqual(false);
    expect(isUninitialised(null)).toEqual(false);
    expect(isUninitialised(' ')).toEqual(false);
    expect(isUninitialised([0])).toEqual(false);
    expect(isUninitialised([''])).toEqual(false);
    expect(isUninitialised({ foo: 'foo' })).toEqual(false);
  });
});

describe('filterNewUninitialised', function spec() {
  it('should handle bad input', () => {
    expect(filterNewUninitialised()).toEqual(undefined);
    expect(filterNewUninitialised(undefined, {})).toEqual(undefined);
    expect(filterNewUninitialised({}, undefined)).toEqual(undefined);
    expect(
      filterNewUninitialised(undefined, {
        multipleSelect: undefined,
        dynamic: undefined,
        dynamicArray: undefined,
      })
    ).toEqual(undefined);
  });

  it('should filter new entries that are uninitialised', () => {
    expect(filterNewUninitialised(undefined, { foo: 'foo' })).toEqual({
      foo: 'foo',
    });
    expect(filterNewUninitialised({}, {})).toEqual(undefined);
    expect(filterNewUninitialised({ foo: 'foo' }, { foo: 'foo' })).toEqual({
      foo: 'foo',
    });
    expect(filterNewUninitialised({ foo: 'foo' }, { bar: 'bar' })).toEqual({
      bar: 'bar',
    });
    expect(filterNewUninitialised({ foo: 'foo' }, { bar: undefined })).toEqual(
      undefined
    );
    expect(
      filterNewUninitialised({ foo: 'foo' }, { foo: 'foo', bar: undefined })
    ).toEqual({ foo: 'foo' });
    expect(
      filterNewUninitialised(
        { foo: 'foo', bar: 'bar' },
        { foo: 'foo', bar: undefined }
      )
    ).toEqual({ foo: 'foo', bar: undefined });

    expect(
      filterNewUninitialised(
        { foo: 'foo', bar: ['bar'] },
        { foo: 'foo', bar: undefined }
      )
    ).toEqual({ foo: 'foo', bar: undefined });

    expect(
      filterNewUninitialised(
        { foo: 'foo', bar: ['bar'] },
        { foo: 'foo', bar: [] }
      )
    ).toEqual({ foo: 'foo', bar: [] });

    expect(
      filterNewUninitialised({ foo: { bar: 'bar' } }, { foo: 'foo' })
    ).toEqual({ foo: 'foo' });

    expect(
      filterNewUninitialised(
        { foo: { bar: 'bar' } },
        { foo: { bar: undefined } }
      )
    ).toEqual({ foo: { bar: undefined } });

    expect(
      filterNewUninitialised(
        { foo: { bar: 'bar' } },
        { foo: { bar: 'bar', baz: undefined } }
      )
    ).toEqual({ foo: { bar: 'bar' } });

    expect(
      filterNewUninitialised(
        { foo: 'foo', bar: ['bar'] },
        { foo: 'foo', bar: [] }
      )
    ).toEqual({ foo: 'foo', bar: [] });

    expect(
      filterNewUninitialised(
        { foo: 'foo', bar: { baz: 'baz' } },
        { foo: 'foo', bar: {} }
      )
    ).toEqual({ foo: 'foo', bar: {} });

    expect(
      filterNewUninitialised(
        { foo: { bar: 'bar' } },
        { foo: { bar: 'bar', baz: {} } }
      )
    ).toEqual({ foo: { bar: 'bar' } });

    expect(
      filterNewUninitialised(
        { foo: { bar: 'bar' } },
        { foo: { bar: 'bar', baz: [] } }
      )
    ).toEqual({ foo: { bar: 'bar' } });
  });

  it('should filter nested new entries that are uninitialised', () => {
    expect(
      filterNewUninitialised({ foo: { bar: undefined } }, undefined)
    ).toEqual(undefined);
    expect(
      filterNewUninitialised(undefined, { foo: { bar: undefined } })
    ).toEqual(undefined);
  });
});
