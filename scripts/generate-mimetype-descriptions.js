/* @flow */

const request = require('request');
const fs = require('fs-extra');
const path = require('path');
const debug = require('debug');
const d = debug('ms:generate-mimetype-descriptions');

const MIMETYPES_URL =
  'https://raw.githubusercontent.com/myhau/mimetype-descriptions/master/mimetype-descriptions-map.js';
const EXTENSION_TO_MIMETYPE_URL =
  'https://raw.githubusercontent.com/jshttp/mime-db/master/db.json';

export const getUrl = async (url: string) => {
  return new Promise((resolve, reject) => {
    d(`Downloading ${url}...`);
    request(
      {
        url,
      },
      (error, response, body) => {
        if (error) {
          reject(error);
        } else if (response.statusCode === 200) {
          resolve(body);
        }
      }
    );
  });
};

export const getMimetypes = async () => {
  const body = await getUrl(MIMETYPES_URL);
  const cleaned = body.replace('module.exports = ', '');
  const json = JSON.parse(cleaned);
  const mimetypes = {};
  Object.entries(json).forEach(([key, value]) => {
    const enDescription = value['description']['en_GB'];
    if (enDescription) {
      const cleanedEnDescription =
        enDescription.substr(0, 1).toUpperCase() + enDescription.substr(1);
      mimetypes[key] = cleanedEnDescription;
    }
  });
  return mimetypes;
};

export const getExtensionToMimetype = async () => {
  const body = await getUrl(EXTENSION_TO_MIMETYPE_URL);
  const json = JSON.parse(body);
  const extensionToMimetype = {};
  Object.entries(json).forEach(([key, value]) => {
    const extensions = value['extensions'];
    if (extensions) {
      extensions.forEach((extension) => {
        extensionToMimetype[extension] = key;
      });
    }
  });
  return extensionToMimetype;
};

(async () => {
  const mimetypes = await getMimetypes();
  const extensionToMimetype = await getExtensionToMimetype();
  fs.writeFileSync(
    path.join(__dirname, '../src/utils/mimetypes.json'),
    JSON.stringify(mimetypes),
    'utf8'
  );
  d('Wrote src/utils/extensionToMimetype.json');
  fs.writeFileSync(
    path.join(__dirname, '../src/utils/extensionToMimetype.json'),
    JSON.stringify(extensionToMimetype),
    'utf8'
  );
  d('Wrote src/utils/extensionToMimetype.json');
})();
