const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');

const getReactAppEnv = require('./scripts/react-app-env');
const dirNode = 'node_modules';
const dirApp = path.join(__dirname, 'src');

const appHtmlTitle = 'Common JavaScript module';

const getHtmlWebpackPluginConfig = () => {
  let config = {
    template: path.join(__dirname, 'example-web-client/index.template.ejs'),
    title: appHtmlTitle,
    minify: {
      collapseWhitespace: true,
      preserveLineBreaks: false,
    },
  };

  if (process.env.NODE_ENV === 'development') {
    const env = getReactAppEnv();
    config.env = JSON.stringify(env);
  } else {
    // in production, inject an ejs template tag `<%- REACT_APP_ENV %>` that can be replaced by the express server
    config.env = `<%- REACT_APP_ENV %>`;
    config.filename = `index.ejs`;
  }

  return config;
};

module.exports = {
  entry: {
    bundle: ['whatwg-fetch', path.join(__dirname, 'example-web-client/index')],
  },
  resolve: {
    modules: [dirApp, dirNode],
  },
  plugins: [
    new HtmlWebpackPlugin(getHtmlWebpackPluginConfig()),
    new CircularDependencyPlugin({
      // exclude detection of files based on a RegExp
      exclude: /a\.js|node_modules/,
      // add errors to webpack instead of warnings
      failOnError: true,
      // allow import cycles that include an asyncronous import,
      // e.g. via import(/* webpackMode: "weak" */ './file.js')
      allowAsyncCycles: false,
      // set the current working directory for displaying module paths
      cwd: process.cwd(),
    }),
  ],
  module: {
    rules: [
      // BABEL
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules\/(?!(makeandship-js-common|swagger-client))/,
      },
    ],
  },
  stats: 'errors-only',
};
