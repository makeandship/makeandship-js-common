# Common JavaScript module

Copyright (c) Make and Ship Limited all rights reserved

## Install

First, clone the repo then install dependencies.

```
$ yarn
```

## Test

```
$ yarn test
```

## Setup in your own project

Add as a dependency

```
yarn add https://bitbucket.org/makeandship/makeandship-js-common#version-tag
```

If you are using `babel-loader` with `webpack` and exluding the entire `node_modules` folder, you will need to add an exception in your config, for example:

```
module: {
  rules: [
    {
      test: /\.js$/,
      loader: 'babel-loader',
      exclude: /node_modules\/(?!(makeandship-js-common|swagger-client|<some-other-module>))/,
    },
  ],
},
```

...and also in the `jest` conifg:

```
"jest": {
  "transformIgnorePatterns": [
    "/node_modules/(?!(makeandship-js-common|swagger-client|<some-other-module>)).+\\.js$"
  ]
}
```

Copy the parameters in the example `.env.example` env file to a local `.env` file in your project

## Code Documentation

See [/docs](/docs/index.html)

### Regenerate documentation

```
$ yarn docs
```

This will generate code documentation inside the `docs` folder

## Dependency notes

### swagger-js

This is added as source code - the distribution version is not currently compatible with a react native produciton build and will fail silently.
