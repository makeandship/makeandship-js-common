import getReactAppEnv from './scripts/react-app-env';

// set up env vars that would be in browser window.env
window.env = getReactAppEnv();
