/* @flow */

import * as React from 'react';

import Notifications from '../../../src/components/notifications/Notifications';

class Root extends React.Component<*> {
  render() {
    return (
      <React.Fragment>
        {this.props.children}
        <Notifications />
      </React.Fragment>
    );
  }
}

export default Root;
