/* @flow */

import * as React from 'react';
import { Link } from 'react-router-dom';

import { IconButton } from '../../../src/components/ui/buttons';

import { useQuery, useMutation } from '../../../src/modules/query';
import { delay } from '../../../src/modules/utils';

const IncidentView = (): React.Element<*> => {
  const { fetch, cancel, ...rest } = useQuery(
    `/incidents/5cd171754c58e20010d56df3`
  );
  return (
    <div>
      <h1>IncidentView</h1>
      <pre>{JSON.stringify({ rest }, null, 2)}</pre>
      <div onClick={fetch}>refetch</div>
      <div onClick={cancel}>cancel</div>
    </div>
  );
};

const IncidentEdit = (): React.Element<*> => {
  const { mutate, cancel, ...rest } = useMutation(
    `/incidents/5cd171754c58e20010d56df3`,
    async (url, options) => {
      await delay(300);
      return options.body;
    }
  );

  const onMutate = React.useCallback(() => {
    mutate({ foo: 'bar' });
  }, [mutate]);
  return (
    <div>
      <h1>IncidentEdit</h1>
      <pre>{JSON.stringify({ rest }, null, 2)}</pre>
      <div onClick={onMutate}>mutate</div>
      <div onClick={cancel}>cancel</div>
    </div>
  );
};

const Home = (): React.Element<*> => {
  // const f = async (url) => {
  //   console.log(url);
  //   return {
  //     data: {
  //       xyz: 123,
  //     },
  //   };
  // };

  // const { refetch, cancel, ...rest } = useQuery(
  //   `/incidents/5cd171754c58e20010d56df3`,
  //   (url) => ({
  //     xyz: 1233,
  //   })
  // );

  // const { refetch, cancel, ...rest } = useQuery(
  //   `/incidents/5cd171754c58e20010d56df3`,
  //   f
  // );

  const [user, setUser] = React.useState();

  const { fetch, cancel, ...rest } = useQuery(
    user && [`/incidents/5cd171754c58e20010d56df3`, user]
  );

  // const { refetch, cancel, ...rest } = useQuery(
  //   `/incidents/5cd171754c58e20010d56df3`
  // );

  // const { refetch, cancel, ...rest } = useQuery(
  //   () => 'key',
  //   () => 'data'
  // );

  // const { refetch, cancel, ...rest } = useQuery(
  //   () => user && '/foo/bar',
  //   () => 'data'
  // );

  const { mutate, ...mutateRest } = useMutation(
    `/incidents/5cd171754c58e20010d56df3`
  );
  // React.useEffect(() => {
  //   mutate({ name: 'name' });
  // });
  return (
    <React.Fragment>
      <h1>Hello World</h1>
      <IconButton>IconButton</IconButton>
      <Link to="/incidents/5cd171754c58e20010d56df3">Edit incident</Link>
      <Link to="/things/5cd171754c58e20010d56df3">Edit thing</Link>
      <div onClick={() => setUser({ id: Math.round(Math.random() * 123) })}>
        set user
      </div>
      <pre>{JSON.stringify({ user, rest, mutateRest }, null, 2)}</pre>
      <div onClick={() => mutate({ name: 'test' })}>mutate</div>
      <div onClick={fetch}>refetch</div>
      <div onClick={cancel}>cancel</div>
      <div style={{ display: 'flex' }}>
        <IncidentEdit />
        <IncidentView />
        <IncidentView />
      </div>
    </React.Fragment>
  );
};

export default Home;
