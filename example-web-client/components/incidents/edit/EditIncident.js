/* @flow */

import * as React from 'react';

import {
  Select,
  DecoratedForm,
  decoratedFormWithModule,
  FileInputBinary,
} from '../../../../src/components/ui/form';

import incidentSchema from '../../../schemas/incidentSchema';
import { incidentModule } from '../../../modules/incidents/incident';
import { useDispatch, useSelector } from 'react-redux';

export const incidentUiSchema = {
  status: {
    'ui:widget': Select,
    'ui:layout': {
      md: 3,
    },
  },
  attachment: {
    'ui:widget': FileInputBinary,
    'ui:layout': {
      md: 9,
    },
  },
  attachments: {
    items: {
      'ui:widget': FileInputBinary,
      'ui:layout': {
        md: 9,
      },
    },
  },
};

const Form = decoratedFormWithModule(
  DecoratedForm,
  incidentModule,
  incidentSchema,
  incidentUiSchema
);

const Controls = () => {
  const dispatch = useDispatch();
  const isFetching = useSelector(incidentModule.selectors.getIsFetching);
  const isRefetching = useSelector(incidentModule.selectors.getIsRefetching);
  const onRefetch = React.useCallback(() => {
    dispatch(incidentModule.actions.requestEntity('5cd171754c58e20010d56df3'));
  }, [dispatch]);
  return (
    <div>
      <a onClick={onRefetch}>Refetch</a> isFetching:{' '}
      {isFetching ? 'true' : 'false'} isRefetching:{' '}
      {isRefetching ? 'true' : 'false'}
    </div>
  );
};

class EditIncident extends React.Component<*> {
  render() {
    return (
      <React.Fragment>
        <h1>Edit incident</h1>
        <Controls />
        <Form {...this.props} validationEnabled={false} requestEntityOnMount />
      </React.Fragment>
    );
  }
}

export default EditIncident;
