/* @flow */

import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import type { Match } from 'react-router-dom';

import Incidents from './Incidents';
import EditIncident from './edit/EditIncident';

const IncidentsRoutes = ({ match }: { match: Match }) => (
  <Switch>
    <Route path={`${match.url}`} exact component={Incidents} />
    <Route path={`${match.url}/:incidentId`} component={EditIncident} />
  </Switch>
);

export default IncidentsRoutes;
