/* @flow */

import * as React from 'react';

import {
  Select,
  DecoratedForm,
  FileInputBinary,
} from '../../../../src/components/ui/form';

import incidentSchema from '../../../schemas/incidentSchema';
import { useQuery, useMutation } from '../../../../src/modules/query';

export const incidentUiSchema = {
  status: {
    'ui:widget': Select,
    'ui:layout': {
      md: 3,
    },
  },
  attachment: {
    'ui:widget': FileInputBinary,
    'ui:layout': {
      md: 9,
    },
  },
  attachments: {
    items: {
      'ui:widget': FileInputBinary,
      'ui:layout': {
        md: 9,
      },
    },
  },
};

const EditIncident = (props: React.ElementProps<*>): React.Element<*> => {
  const thingId = props.match?.params?.thingId;
  const { data, isFetching } = useQuery(() => `/incidents/${thingId}`);
  const { mutate } = useMutation(() => `/incidents/${thingId}`, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    method: 'PUT',
  });
  const onSubmit = React.useCallback(
    (data) => {
      mutate(data);
    },
    [mutate]
  );
  return (
    <React.Fragment>
      <pre>{JSON.stringify({ thingId, isFetching, data }, null, 2)}</pre>
      <h1>Edit thing</h1>
      <DecoratedForm
        formKey={`/things/edit`}
        formData={data}
        schema={incidentSchema}
        uiSchema={incidentUiSchema}
        onSubmit={onSubmit}
        validationEnabled={false}
      />
    </React.Fragment>
  );
};

export default EditIncident;
