/* @flow */

import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import type { Match } from 'react-router-dom';

import Things from './Things';
import EditThing from './edit/EditThing';

const ThingsRoutes = ({ match }: { match: Match }) => (
  <Switch>
    <Route path={`${match.url}`} exact component={Things} />
    <Route path={`${match.url}/:thingId`} component={EditThing} />
  </Switch>
);

export default ThingsRoutes;
