/* @flow */

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import {
  all,
  call,
  fork,
  put,
  takeLeading,
  take,
  takeEvery,
} from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import axios from 'axios';
import Keycloak from 'keycloak-js';
import { Auth0Client } from '@auth0/auth0-spa-js';

import notifications, {
  rootNotificationsSaga,
  showNotification,
  NotificationCategories,
} from '../../src/modules/notifications';
import { restartSagaOnError } from '../../src/modules/utils';

import form from '../../src/modules/form';

import {
  reducer as auth,
  saga as authSaga,
  setConfig as setAuthConfig,
  actions as authActions,
} from '../../src/modules/auth';
import api, {
  rootApiSaga,
  setConfig as setApiConfig,
  jsonApiActions,
} from '../../src/modules/api';
import { saga as downloadSaga } from '../../src/modules/api/download';
import {
  reducer as query,
  saga as querySaga,
  setConfig as setQueryConfig,
} from '../../src/modules/query';
// import networkStatus, { networkStatusSaga } from './networkStatus';

import incidents, { rootIncidentsSaga } from './incidents';

import { createAxiosFetcher } from '../../src/modules/fetchers/axiosFetcher';
import jsendResponseTransformer from '../../src/modules/transformers/jsendResponseTransformer';
import { isTrue } from '../../src/utils/is';
import { createAxiosAuthInterceptor } from '../../src/modules/auth/interceptors/axiosAuthInterceptor';
import { createKeycloakProvider } from '../../src/modules/auth/providers/keycloakProvider';
import { createAuth0Provider } from '../../src/modules/auth/providers/auth0Provider';

const axiosInstance = axios.create({
  baseURL: window.env?.REACT_APP_API_URL,
  transformResponse: [jsendResponseTransformer],
});

const keycloakInstance = new Keycloak({
  url: window.env?.REACT_APP_KEYCLOAK_URL,
  realm: window.env?.REACT_APP_KEYCLOAK_REALM,
  clientId: window.env?.REACT_APP_KEYCLOAK_CLIENT_ID,
  pkceMethod: 'S256',
});

const auth0 = new Auth0Client({
  domain: window.env?.REACT_APP_AUTH0_DOMAIN,
  client_id: window.env?.REACT_APP_AUTH0_CLIENT_ID,
  redirect_uri: window.env?.REACT_APP_AUTH0_REDIRECT_URI,
  audience: window.env?.REACT_APP_AUTH0_AUDIENCE,
  cacheLocation: 'localstorage',
  useRefreshTokens: true,
});

const provider = true
  ? createAuth0Provider(auth0)
  : createKeycloakProvider(keycloakInstance);
const authInterceptor = createAxiosAuthInterceptor(provider);
axiosInstance.interceptors.request.use(authInterceptor);

const fetcher = createAxiosFetcher(axiosInstance);

setAuthConfig({
  provider,
});

setQueryConfig({
  fetcher,
});

setApiConfig({
  fetcher,
  apiUrl: window.env?.REACT_APP_API_URL,
  apiSpecUrl: window.env?.REACT_APP_API_SPEC_URL,
  debug: isTrue(window.env?.REACT_APP_API_DEBUG),
});

export const rootReducer = (history: any) =>
  combineReducers({
    router: connectRouter(history),
    query,
    api,
    auth,
    form,
    incidents,
    notifications,
    // networkStatus,
  });

const sagasPreAuth = [
  authSaga,
  querySaga,
  rootApiSaga,
  downloadSaga,
  rootNotificationsSaga,
  // networkStatusSaga,
];

const sagasPostAuth = [rootIncidentsSaga];

export function* startSagas(sagas: Array<Saga>): Saga {
  if (process.env.NODE_ENV !== 'development') {
    yield all(sagas.map(restartSagaOnError).map((saga) => call(saga)));
  } else {
    yield all(sagas.map((saga) => fork(saga)));
  }
}

export function* rootSaga(): Saga {
  // if auth token refresh fails, invoke login
  yield takeLeading(authActions.updateTokenError, function* () {
    yield call(provider.login);
  });
  // start the pre-auth sagas
  yield fork(startSagas, sagasPreAuth);
  // initialise auth with options
  yield put(
    authActions.initialise({
      onLoad: 'check-sso',
    })
  );
  // wait for auth intialisation
  yield take(authActions.initialiseSuccess);
  // start the post-auth sagas
  yield fork(startSagas, sagasPostAuth);
  // display api errors as notifications
  yield takeEvery(jsonApiActions.error, function* (action) {
    const content = action.payload?.message;
    if (content) {
      yield put(
        showNotification({
          category: NotificationCategories.ERROR,
          content,
        })
      );
    }
  });
}
