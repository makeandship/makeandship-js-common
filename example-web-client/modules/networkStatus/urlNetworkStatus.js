/* @flow */

import { createUrlNetworkStatusModule } from '../../../src/modules/networkStatus/urlNetworkStatusModule';

const urlNetworkStatusModule = createUrlNetworkStatusModule({
  typePrefix: 'networkStatus/urlNetworkStatus/',
  getState: (state: any) => state?.networkStatus?.urlNetworkStatus,
  // default timeout after 50 seconds for testing only
  url: 'http://httpstat.us/200?sleep=50000',
  timeout: 5000,
});

const {
  reducer,
  actions,
  selectors,
  sagas: { urlNetworkStatusSaga: saga },
} = urlNetworkStatusModule;

export { reducer, actions, selectors, saga };
