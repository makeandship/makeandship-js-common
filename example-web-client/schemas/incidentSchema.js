const incidentSchema = {
  type: 'object',
  $schema: 'http://json-schema.org/draft-07/schema#',
  properties: {
    status: {
      type: 'string',
      enum: ['Draft', 'Submitted', 'Under Review', 'Reviewed'],
    },
    attachment: {
      type: 'string',
    },
    attachments: {
      type: 'array',
      title: 'Attachments',
      items: {
        type: 'string',
      },
    },
  },
};

export default incidentSchema;
