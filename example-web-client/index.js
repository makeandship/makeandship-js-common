/* @flow */

import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

import store, { history } from './store'; // eslint-disable-line import/default

import '../src/styles/default.scss';

let element = document.getElementById('root');

if (!element) {
  throw new Error(`Fatal - div with id 'root' not found`);
}

console.log('window.env', JSON.stringify(window.env));

const renderRoot = () => {
  const routes = require('./routes').default;
  render(
    <Provider store={store}>
      <ConnectedRouter history={history}>{routes}</ConnectedRouter>
    </Provider>,
    element
  );
};

renderRoot();

if (module.hot) {
  module.hot.accept('./routes', () => {
    renderRoot();
  });
}
