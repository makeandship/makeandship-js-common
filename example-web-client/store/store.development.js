/* @flow */

import { createStore, applyMiddleware, compose as vanillaCompose } from 'redux';

import thunk from 'redux-thunk';
import { routerMiddleware } from 'connected-react-router';
import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory } from 'history';
import { createLogger } from 'redux-logger';

import { actions as deviceNetworkStatusActions } from '../../src/modules/networkStatus/deviceNetworkStatus';

import { actions as urlNetworkStatusActions } from '../modules/networkStatus/urlNetworkStatus';

import { setFormData, clearFormData } from '../../src/modules/form';
import { rootReducer, rootSaga } from '../modules';

import { UPDATE as UPDATE_NOTIFICATION } from '../../src/modules/notifications/notifications';

import {
  login,
  register,
  logout,
  updateToken,
} from '../../src/modules/auth/actions';

import { requestIncident } from '../modules/incidents/incident';

export const history = createBrowserHistory();

const devToolsPresent =
  window && typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function';

let compose;

const actionsBlacklist = [
  UPDATE_NOTIFICATION,
  urlNetworkStatusActions.check.toString(),
  urlNetworkStatusActions.checkSuccess.toString(),
  urlNetworkStatusActions.setCountDownSeconds.toString(),
];

if (devToolsPresent) {
  const actionCreators = {
    login,
    register,
    logout,
    updateToken,
    setFormData,
    clearFormData,
    requestIncident,
    deviceNetworkOffline: deviceNetworkStatusActions.deviceNetworkOffline,
    deviceNetworkOnline: deviceNetworkStatusActions.deviceNetworkOnline,
    urlNetworkOffline: urlNetworkStatusActions.networkOffline,
    urlNetworkOnline: urlNetworkStatusActions.networkOnline,
  };
  compose = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    actionCreators,
    actionsBlacklist,
  });
} else {
  compose = vanillaCompose;
}

const router = routerMiddleware(history);

export const sagaMiddleware = createSagaMiddleware();

const middleware = [thunk, sagaMiddleware, router];

const logger = createLogger({
  level: 'info',
  collapsed: true,
  predicate: (getState, action) => {
    return actionsBlacklist.indexOf(action.type) === -1;
  },
});
middleware.push(logger);

const enhancer = compose(applyMiddleware(...middleware));

const store = createStore(rootReducer(history), enhancer);

sagaMiddleware.run(rootSaga);

if (module.hot) {
  module.hot.accept(
    '../modules',
    () => store.replaceReducer(require('../modules')) // eslint-disable-line global-require
  );
}

export default store;
