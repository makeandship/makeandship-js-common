/* @flow */

import * as React from 'react';
import { Route, Switch } from 'react-router-dom';

import Root from './components/root/Root';
import Home from './components/home/Home';
import IncidentsRoutes from './components/incidents/IncidentsRoutes';
import ThingsRoutes from './components/things/ThingsRoutes';

export default (
  <Root>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/incidents" component={IncidentsRoutes} />
      <Route path="/things" component={ThingsRoutes} />
    </Switch>
  </Root>
);
