module.exports = async ({ config, mode }) => {
  const customConfig =
    mode === 'DEVELOPMENT'
      ? require('../webpack.config.development')
      : require('../webpack.config.production');
  config.resolve = { ...config.resolve, ...customConfig.resolve };
  config.module.rules = [...config.module.rules, ...customConfig.module.rules];
  return config;
};
